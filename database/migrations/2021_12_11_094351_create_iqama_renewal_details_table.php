<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIqamaRenewalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iqama_renewal_details', function (Blueprint $table) {
            $table->id('IqamaRenewId');
            $table->integer('EmplId');
            $table->float('Cost1',11,2)->default(0);
            $table->float('Cost2',11,2)->default(0);
            $table->float('Cost3',11,2)->default(0);
            $table->float('Cost4',11,2)->default(0);
            $table->float('Cost5',11,2)->default(0);
            $table->float('Cost6',11,2)->default(0);
            $table->float('Cost7',11,2)->default(0);
            $table->float('Cost8',11,2)->default(0);
            $table->integer('Year')->nullable();
            $table->float('jawazat_penalty',11,2)->default(0);
            $table->integer('duration',4)->default(3);
            $table->date('renewal_date');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iqama_renewal_details');
    }
}
