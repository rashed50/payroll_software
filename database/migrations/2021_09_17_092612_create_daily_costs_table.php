<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('daily_costs', function (Blueprint $table) {
            $table->id('cost_id');
            $table->integer('cost_type_id');
            $table->integer('project_id');
            $table->integer('employee_id');
            $table->date('expire_date');
            $table->float('amount',12,2);
            $table->string('vouchar',100)->nullable();
            $table->string('vouchar_no',50);
            $table->string('status',30)->default('pending');
            $table->integer('entered_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_costs');
    }
}
