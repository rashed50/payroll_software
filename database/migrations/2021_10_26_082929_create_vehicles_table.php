<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id('veh_id');
            $table->string('veh_name');
            $table->string('veh_plate_number');
            $table->string('veh_model_number');
            $table->string('veh_brand_name');
            $table->date('veh_insurrance_date');
            $table->double('veh_price',20,2);
            $table->string('veh_color');
            $table->date('veh_purchase_date');
            $table->integer('veh_present_metar')->default(0);
            $table->integer('driver_id')->nullable();
            $table->string('remarks');
            $table->integer('create_by_id');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
