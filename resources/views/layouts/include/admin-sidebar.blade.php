<div id="sidebar-menu">
    <ul>
        <!-- <li><a href="{{ url('/') }}" class="waves-effect"><i class="fas fa-globe-europe"></i><span>Website </span></a></li> -->
        <li><a href="{{ route('admin.dashboard') }}" class="waves-effect"><i class="md md-home"></i><span>Dashboard </span></a></li>

        <!-- Administor Settings -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-user-cog"></i><span>Admin Settings</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                {{-- User Role Permision --}}

                <li><a href="{{ route('users.index') }}"><span><i class="fas fa-arrow-right"></i></span> User</a></li>
                <!-- <button onclick="playButton()">play</button> -->
                @can ('role-list')
                <li><a href="{{ route('roles.index') }}"><span><i class="fas fa-arrow-right"></i></span> Role</a></li>
                @endcan
                {{-- User Role Permision --}}
                @can('company-profile')
                <li><a href="{{ route('company-profiles') }}"><span><i class="fas fa-arrow-right"></i></span> Company Profile</a></li>
                @endcan
                @can('subcompany-list')
                <li><a href="{{ route('sub-comp-info') }}"><span><i class="fas fa-arrow-right"></i></span> Sub Company Profile</a></li>
                @endcan

                @can('evoucher-list')
                <li><a href="{{ route('e.voucher.add') }}"><span><i class="fas fa-arrow-right"></i></span> E Voucher</a></li>
                <li><a href="{{ route('bill-voucher.create') }}"><span><i class="fas fa-arrow-right"></i></span> Bill Voucher</a></li>

                @endcan

                @can('banner-list')
                <li><a href="{{ route('banner-info') }}"><span><i class="fas fa-arrow-right"></i></span> Upload Banner</a></li>
                @endcan
                <!-- <li><a href="{{ route('employee-deduction') }}"><span><i class="fas fa-arrow-right"></i></span> Deduction</a></li> -->
                @can('country-list')
                <li><a href="{{ route('add-country') }}"><span><i class="fas fa-arrow-right"></i></span> Country</a></li>
                @endcan

                @can('division-list')
                <li><a href="{{ route('add-division') }}"><span><i class="fas fa-arrow-right"></i></span> Division</a></li>
                @endcan

                @can('district-list')
                <li><a href="{{ route('add-district') }}"><span><i class="fas fa-arrow-right"></i></span> District</a></li>
                @endcan

                @can('designation-list')
                <li><a href="{{ route('emp-category') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Designation</a></li>
                @endcan

                @can('sponser-list')
                <li><a href="{{ route('add-sponser') }}"><span><i class="fas fa-arrow-right"></i></span> Add Sponser</a></li>
                @endcan


            </ul>
        </li>

        <!-- Project -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-project-diagram"></i><span>Project</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('project-list')
                <li><a href="{{ route('project-info') }}"><span><i class="fas fa-arrow-right"></i></span> Add New Project</a></li>
                @endcan

                @can('projectincharge-list')
                <li><a href="{{ route('add-project.incharge') }}"><span><i class="fas fa-arrow-right"></i></span> Add Project In-charge</a></li>
                @endcan

                @can('projectimage-list')
                <li><a href="{{ route('project-image-upload') }}"><span><i class="fas fa-arrow-right"></i></span> Upload Project Photo </a></li>
                @endcan
            </ul>
        </li>

        <!-- Approval -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-users"></i><span>Approval</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('income-approve')
                <li><a href="{{ route('income-list') }}"><span><i class="fas fa-arrow-right"></i></span> Income Approval </a></li>
                @endcan

                @can('expenditure-approve')
                <li><a href="{{ route('cost-list') }}"><span><i class="fas fa-arrow-right"></i></span> Expenditure Approval </a></li>
                @endcan

                @can('job-approve')
                <li><a href="{{ route('employee-job-approver') }}"><span><i class="fas fa-arrow-right"></i></span> New Employee Approval </a></li>
                @endcan

                @can('leave-approve')
                <li><a href="{{ route('employee-leave-approver') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Leave Approval </a></li>
                @endcan
            </ul>
        </li>



        <!-- Employee -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-users"></i><span>Employee</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('employee-add')
                <li><a href="{{ route('add-employee') }}"><span><i class="fas fa-arrow-right"></i></span> Add Employee</a></li>
                @endcan

                @can('salarydetails-list')
                <li><a href="{{ route('salary-details') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Details</a></li>
                @endcan

                @can('employee-list')
                <li><a href="{{ route('employee-list')}}"><span><i class="fas fa-arrow-right"></i></span> Employee List</a></li>
                @endcan

                @can('employee-search')
                <li><a href="{{ route('search-employee')}}"><span><i class="fas fa-arrow-right"></i></span> Employee Search</a></li>
                <li><a href="{{ route('search-employee.update.info')}}"><span><i class="fas fa-arrow-right"></i></span> Employee Update Info</a></li>
                @endcan

                @can('employee-status')
                <li><a href="{{ route('search-employee.status')}}"><span><i class="fas fa-arrow-right"></i></span> Employee Status</a></li>
                @endcan

                @can('employee-leave')
                <li><a href="{{ route('employee-leave-work') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Leave Form</a></li>
                @endcan


                @can('employee-contact-persion')
                <li><a href="{{ route('emp-contact-person') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Contact Person</a></li>
                @endcan

                @can('employee-job-experience')
                <li><a href="{{ route('emp-job-experience') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Job Experience</a></li>
                @endcan

                @can('employee-iqama-expire')
                <li><a href="{{ route('iqama-expired') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Iqama Expire </a></li>
                @endcan

                @can('employee-passport-expire')
                <li><a href="{{ route('passport-expired') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Passport Expire </a></li>
                @endcan

                <!-- <li><a href="#"><span><i class="fas fa-arrow-right"></i></span> Relese Employee</a></li> -->
                @can('employee-promotion')
                <li><a href="{{ route('employee-promosion') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Promosion</a></li>
                @endcan
            </ul>
        </li>

        <!-- Work Menu -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-users"></i><span>Work History</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('month-work-history')
                <li><a href="{{ route('add-daily-work') }}"><span><i class="fas fa-arrow-right"></i></span> Add Monthly Work</a></li>
                <li><a href="{{ route('employee.month.work.record.searchui') }}"><span><i class="fas fa-arrow-right"></i></span>Work Record Search</a></li>
                @endcan


                @can('month-work-report')
                <li><a href="{{ route('monthwork-reportprocess')}}"><span><i class="fas fa-arrow-right"></i></span> Monthly Work Report</a></li>
                @endcan

                @can('project-work-report')
                <li><a href="{{ route('project-wise.in-out.time')}}"><span><i class="fas fa-arrow-right"></i></span> Project Wise Report</a></li>
                @endcan

                @can('attendence-in')
                <li><a href="{{ route('employee-in.time')}}"><span><i class="fas fa-arrow-right"></i></span> Emp Daily Attendance</a></li>
                <li><a href="{{ route('employee.multiple.in-out.time')}}"><span><i class="fas fa-arrow-right"></i></span> Multi Project Work</a></li>
                @endcan

                @can('attendence-out')
                <li><a href="{{ route('employee-out.time')}}"><span><i class="fas fa-arrow-right"></i></span> Employee Out Time</a></li>
                @endcan
            </ul>
        </li>

        <!-- Advance Paid -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-money-check"></i><span>Iqama & Advance </span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('employee-contribution')
                <li><a href="{{ route('cpf-Contribution.set') }}"><span><i class="fas fa-arrow-right"></i></span> Emp. Contribution</a></li>
                @endcan

                @can('employee-anualsfee')
                <li><a href="{{ route('anual-fee.details') }}"><span><i class="fas fa-arrow-right"></i></span>Iqama Renewal Details</a></li>
                @endcan

                @can('set-advance')
                <li><a href="{{ route('addvance.payment') }}"><span><i class="fas fa-arrow-right"></i></span>Emp. Advance</a></li>
                @endcan

                @can('summary-adjuget')
                <li><a href="{{ route('employee.search.with.cash-payment') }}"><span><i class="fas fa-arrow-right"></i></span>Emp. Cash Payment</a></li>
                @endcan
                @can('advance-adjuget')
                <li><a href="{{ route('advance-pay.adjust') }}"><span><i class="fas fa-arrow-right"></i></span>Iqama & Advance Setting </a></li>
                @endcan
            </ul>
        </li>


        <!-- Salary System -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-money-check"></i><span>Salary System</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('salary-processing')
                <li><a href="{{ route('salary-processing-for-month') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Processing</a></li>
                @endcan
                @can('salary-pending')
                <li><a href="{{ route('salary-pending') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Pending</a></li>
                <li><a href="{{ route('salary-paid') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Paid</a></li>
                <li><a href="{{ route('salary-sheet') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Sheet Upload</a></li>
                @endcan
                @can('salary-correction')
                <li><a href="{{ route('salary-currection') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Correction</a></li>
                @endcan

                @can('salary-payment')
                <li><a href="{{ route('salary-pay') }}"><span><i class="fas fa-arrow-right"></i></span> Salary Payment</a></li>
                @endcan
            </ul>
        </li>

        <!-- Report Generate -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fas fa-record-vinyl"></i><span>Report</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('report-attendence')
                <li><a href="{{ route('employee-entry-out-report') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Attendance </a></li>
                @endcan

                @can('report-employee-iqama-single')
                <li><a href="{{ route('employee-iqama-renewal') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Iqama (single)</a></li>
                @endcan

                @can('report-employee-iqama-project')
                <li><a href="{{ route('project.wise-employee-iqama-renewal') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Iqama (Project based) </a></li>
                @endcan

                @can('report-employee-salary-history')
                <li><a href="{{ route('single-employee-salary.report') }}"><span><i class="fas fa-arrow-right"></i></span>Emp Salary History</a></li>
                @endcan

                @can('report-employee-salary-summary')
                <li><a href="{{ route('employee-salary.summary') }}"><span><i class="fas fa-arrow-right"></i></span> Employee Salary Summary </a></li>
                @endcan

                @can ('report-monthly-salary-report')
                <li><a href="{{ route('salary-report') }}"><span><i class="fas fa-arrow-right"></i></span> Monthly Salary Report</a></li>
                @endcan

                @can('report-total-hour-project')
                <li><a href="{{ route('project-wise-total-hours') }}"><span><i class="fas fa-arrow-right"></i></span>Total Hours (Project Base)</a></li>
                @endcan

                @can('report-total-salary-project')
                <li><a href="{{ route('project-wise-total-salary') }}"><span><i class="fas fa-arrow-right"></i></span>Total Salary (Project Base)</a></li>
                @endcan

                @can('report-employee-list-project')
                <li><a href="{{ route('project-wise.employee') }}"><span><i class="fas fa-arrow-right"></i></span> Emp-List (Project Base)</a></li>
                @endcan

                @can('report-employee-list-trade')
                <li><a href="{{ route('trade-wise.employee') }}"><span><i class="fas fa-arrow-right"></i></span> Emp-List (Salary or Trade)</a></li>
                @endcan

                @can('report-employee-list-sponser')
                <li><a href="{{ route('sponser-wise.employee') }}"><span><i class="fas fa-arrow-right"></i></span> Emp-List (Sponsor Base)</a></li>
                @endcan

                @can('report-cpf-contribution')
                <li><a href="{{ route('CPF-contribution-report') }}"><span><i class="fas fa-arrow-right"></i></span> CPF Contribution</a></li>
                @endcan

                @can('report-sponser-employee')
                <li><a href="{{ route('sponser-wise.employee') }}"><span><i class="fas fa-arrow-right"></i></span> Sponser Employee </a></li>
                @endcan

                @can('report-expenditure')
                <li><a href="{{ route('report-expenditure') }}"><span><i class="fas fa-arrow-right"></i></span> Expenditure</a></li>
                @endcan

                @can('report-income')
                <li><a href="{{ route('report-income') }}"><span><i class="fas fa-arrow-right"></i></span> Founding Source</a></li>
                @endcan

                @can('report-tools-metarials')
                <li><a href="{{ route('tools-metarial-report') }}"><span><i class="fas fa-arrow-right"></i></span> Tools & Metarials</a></li>
                @endcan

                @can('report-item-stock')
                <li><a href="{{ route('items-report') }}"><span><i class="fas fa-arrow-right"></i></span> Item Stock </a></li>
                @endcan

                @can('report-log-book')
                <li><a href="{{ route('log-book.report') }}"><span><i class="fas fa-arrow-right"></i></span> Log Book</a></li>
                @endcan

                <li><a href="{{ route('income-expense-report-ui') }}"><span><i class="fas fa-arrow-right"></i></span>Income Expense</a></li>

            </ul>
        </li>



        <!-- Income Cost -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fab fa-acquisitions-incorporated"></i><span>Income & Cost</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('income-add')
                <li><a href="{{ route('income-source') }}"><span><i class="fas fa-arrow-right"></i></span> Income</a></li>
                @endcan

                @can('expenditure-list')
                <li><a href="{{ route('daily-cost') }}"><span><i class="fas fa-arrow-right"></i></span> Expenditure</a></li>
                @endcan

                @can('expenditure-type')
                <li><a href="{{ route('cost-type') }}"><span><i class="fas fa-arrow-right"></i></span> Expenditure Type</a></li>
                @endcan

                <li><a href="{{ route('submited-bill-voucher-ui') }}"><span><i class="fas fa-arrow-right"></i></span> Submited Bill Voucher</a></li>

            </ul>
        </li>

        <!-- Metarial & Tools -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fab fa-asymmetrik"></i><span>Metarials & Tools</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('tools-category-add')
                <li><a href="{{ route('metarial-tools-category') }}"><span><i class="fas fa-arrow-right"></i></span> Category</a></li>
                @endcan

                @can('tools-subcategory-add')
                <li><a href="{{ route('metarial-tools-sub-category') }}"><span><i class="fas fa-arrow-right"></i></span> Sub Category</a></li>
                @endcan

                @can('tools-purchase')
                <li><a href="{{ route('order-metarial-tools') }}"><span><i class="fas fa-arrow-right"></i></span> Purchase</a></li>
                @endcan

                @can('tools-condem')
                <li><a href="#"><span><i class="fas fa-arrow-right"></i></span> Tools Condem</a></li>
                @endcan
            </ul>
        </li>

        <!-- Vehicle and Bulding -->
        <li class="has_sub">
            <a href="#" class="waves-effect"><i class="fab fa-asymmetrik"></i><span>Vehicle & Building</span><span class="pull-right"><i class="md md-add"></i></span></a>
            <ul class="list-unstyled">
                @can('vichle-add')
                <li><a href="{{ route('add-new.vehicle') }}"><span><i class="fas fa-arrow-right"></i></span> Add New Vehicle</a></li>
                @endcan

                @can('vichle-add')
                <li><a href="{{ route('add.vehicle.fine') }}"><span><i class="fas fa-arrow-right"></i></span>Vehicle Fine</a></li>
                @endcan

                @can('vichle-maintanece')
                <li><a href="#"><span><i class="fas fa-arrow-right"></i></span> Vehicle Maintenance</a></li>
                @endcan

                @can('log-book')
                <li><a href="{{ route('add-new.LogBook') }}"><span><i class="fas fa-arrow-right"></i></span> Log Book</a></li>
                @endcan


                <li><a href="#" style="padding: 10px 25px 10px 30px;"> .............................. </a></li>
                @can('rent-new-building')
                <li><a href="{{ route('rent.new-building') }}"><span><i class="fas fa-arrow-right"></i></span> Rent New Building </a></li>
                @endcan
            </ul>
        </li>

    </ul>
    <div class="clearfix"></div>
</div>