<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Pay-Roll @yield('title') </title>
  <link rel="shortcut icon" href="">
  <link href="{{asset('contents/admin')}}/assets/css/salary-bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
  <link href="{{asset('contents/admin')}}/assets/css/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/common')}}/css/magnific-popup.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/all.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/plugins/summernote/summernote-bs4.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/moltran.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/chosen.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/assets/css/style.css" rel="stylesheet" type="text/css" />
  <script src="{{asset('contents/admin')}}/assets/js/modernizr.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/tags/bootstrap-tagsinput.css"></script>
  <script src="{{asset('contents/admin')}}/assets/tags/bootstrap-tagsinput.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/font_end_validation/jquery.validate.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/font_end_validation/salary-details.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/font_end_validation/employee-info.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/font_end_validation/division.js"></script>
  {{-- Table Link --}}

  {{-- <link href="{{asset('contents/admin')}}/table/vendor/animate/animate.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/table/vendor/select2/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/table/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/table/css/util.css" rel="stylesheet" type="text/css" />
  <link href="{{asset('contents/admin')}}/table/css/main.css" rel="stylesheet" type="text/css" /> --}}

  {{-- Table Link --}}
  <!-- internal css -->
  @yield('internal-css')

</head>



<style>
  .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid blue;
    border-right: 16px solid green;
    border-bottom: 16px solid red;
    border-left: 16px solid pink;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% {
      -webkit-transform: rotate(0deg);
    }

    100% {
      -webkit-transform: rotate(360deg);
    }
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }

    100% {
      transform: rotate(360deg);
    }
  }


  body {
    overflow: hidden;
  }

  #preloader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #fff;
    /* change if the mask should have another color then white */
    z-index: 99;
    /* makes sure it stays on top */
  }

  #status {
    width: 200px;
    height: 200px;
    position: absolute;
    left: 50%;
    /* centers the loading animation horizontally one the screen */
    top: 50%;
    /* centers the loading animation vertically one the screen */
    background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
    /* path to your loading animation */
    background-repeat: no-repeat;
    background-position: center;
    margin: -100px 0 0 -100px;
    /* is width and height divided by two */

    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid blue;
    border-right: 16px solid green;
    border-bottom: 16px solid red;
    border-left: 16px solid pink;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }
</style>

<!-- <div class="pre-loader">
  <div class="sk-fading-circle">
    <div class="sk-circle1 sk-circle"></div>
    <div class="sk-circle2 sk-circle"></div>
    <div class="sk-circle3 sk-circle"></div>
    <div class="sk-circle4 sk-circle"></div>
    <div class="sk-circle5 sk-circle"></div>
    <div class="sk-circle6 sk-circle"></div>
    <div class="sk-circle7 sk-circle"></div>
    <div class="sk-circle8 sk-circle"></div>
    <div class="sk-circle9 sk-circle"></div>
    <div class="sk-circle10 sk-circle"></div>
    <div class="sk-circle11 sk-circle"></div>
    <div class="sk-circle12 sk-circle"></div>
  </div>
</div> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- Preloader -->
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>

<script>
  $(window).on('load', function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({
      'overflow': 'visible'
    });
  })
</script>
<!-- 
<script>
  (function($) {
    'use strict';
    $(window).on('load', function() {
      if ($(".pre-loader").length > 0) {
        $(".pre-loader").fadeOut("slow");
      }
    });
  })(jQuery)
</script> -->

<body class="fixed-left">


  <div id="wrapper">
    @include('layouts.include.topbar')
    <div class="left side-menu">
      <div class="sidebar-inner slimscrollleft">
        <div class="user-details">
          <div class="pull-left">
            @if(Auth::user()->emp_id == 0)
            <img class="thumb-md rounded-circle" src="{{asset('uploads/employee')}}/avatar-1.jpg" alt="user-photo" />
            @else
            <!-- second step -->
            @if(Auth::user()->employee->profile_photo == "")
            <img class="thumb-md rounded-circle" src="{{asset('uploads/employee')}}/avatar-1.jpg" alt="user-photo" />
            @else
            <img class="thumb-md rounded-circle" src="{{ asset('uploads/employee/'.Auth::user()->employee->profile_photo) }}" alt="No Image" />
            @endif

            @endif
          </div>
          <div class="user-info">
            <div class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }}
              </a>
              <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item"><i class="md md-face-unlock mr-2"></i> Profile<div class="ripple-wrapper"></div></a></li>
                <li><a href="javascript:void(0)" class="dropdown-item"><i class="md md-settings mr-2"></i> Settings</a></li>
                <li><a href="javascript:void(0)" class="dropdown-item"><i class="md md-lock mr-2"></i> Lock screen</a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item"><i class="md md-settings-power mr-2"></i> Logout</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </ul>
            </div>
            <p class="text-muted m-0">Active</p>
          </div>
        </div>
        @include('layouts.include.admin-sidebar');
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="content-page">
      <div class="content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
      <footer class="footer">
        3I Engineers | Developed by <a target="_blank" href="#">Rashedul Hoque</a>
      </footer>
    </div>
  </div>

  <script>
    var resizefunc = [];
  </script>
  <script src="{{asset('contents/admin')}}/assets/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/datatables.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/detect.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/fastclick.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.slimscroll.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.blockUI.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery-validator.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/form-validation.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/waves.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/wow.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.nicescroll.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.scrollTo.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/bootstrap-datepicker.js"></script>
  <!-- Sweet Alert  -->
  <script src="{{asset('contents/common')}}/js/sweetalert2.min.js"></script>

  <!-- Sweet Alert 2 -->
  <script src="{{asset('contents/admin')}}/assets/js/sweetalert/sweetalert.min.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/sweetalert/code.js"></script>
  <!-- end Sweet Alert -->
  <script src="{{asset('contents/admin')}}/plugins/moment/moment.min.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/waypoints/lib/jquery.waypoints.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/counterup/jquery.counterup.min.js"></script>
  <script src="{{asset('contents/common')}}/js/magnific.min.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.min.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.time.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.resize.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.pie.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.selection.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.stack.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/flot-chart/jquery.flot.crosshair.js"></script>
  <script src="{{asset('contents/admin')}}/assets/pages/jquery.todo.js"></script>
  <script src="{{asset('contents/admin')}}/assets/pages/jquery.dashboard.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.app.js"></script>
  <script src="{{asset('contents/admin')}}/plugins/summernote/summernote-bs4.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/chosen.jquery.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/jquery.printPage.js"></script>
  <script src="{{asset('contents/admin')}}/assets/js/axios.min.js"></script>



  <script src="{{asset('contents/admin')}}/assets/js/designation.js"></script>
  @yield('script')
  <script src="{{asset('contents/admin')}}/assets/js/custom.js"></script>
  <script type="text/javascript" src="{{ asset('contents/admin') }}/assets/js/ajax/typehead.min.js"></script>

  <!-- Datepicker -->
  <script>
    $('#datepicker').datepicker({
      autoclose: true,
      toggleActive: true,
    });

    $('#workDatepicker').datepicker({
      autoclose: true,
      toggleActive: true,
    });
  </script>


  {{-- show password --}}

  <script type="text/javascript">
    $(document).ready(function() {
      $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if ($('#show_hide_password input').attr("type") == "text") {
          $('#show_hide_password input').attr('type', 'password');
          $('#show_hide_password i').addClass("fa-eye-slash");
          $('#show_hide_password i').removeClass("fa-eye");
        } else if ($('#show_hide_password input').attr("type") == "password") {
          $('#show_hide_password input').attr('type', 'text');
          $('#show_hide_password i').removeClass("fa-eye-slash");
          $('#show_hide_password i').addClass("fa-eye");
        }
      });
      $("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if ($('#show_hide_password2 input').attr("type") == "text") {
          $('#show_hide_password2 input').attr('type', 'password');
          $('#show_hide_password2 i').addClass("fa-eye-slash");
          $('#show_hide_password2 i').removeClass("fa-eye");
        } else if ($('#show_hide_password2 input').attr("type") == "password") {
          $('#show_hide_password2 input').attr('type', 'text');
          $('#show_hide_password2 i').removeClass("fa-eye-slash");
          $('#show_hide_password2 i').addClass("fa-eye");
        }
      });
      $("#show_hide_password3 a").on('click', function(event) {
        event.preventDefault();
        if ($('#show_hide_password3 input').attr("type") == "text") {
          $('#show_hide_password3 input').attr('type', 'password');
          $('#show_hide_password3 i').addClass("fa-eye-slash");
          $('#show_hide_password3 i').removeClass("fa-eye");
        } else if ($('#show_hide_password3 input').attr("type") == "password") {
          $('#show_hide_password3 input').attr('type', 'text');
          $('#show_hide_password3 i').removeClass("fa-eye-slash");
          $('#show_hide_password3 i').addClass("fa-eye");
        }
      });
    });
  </script>
  {{-- show password --}}
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    })

    function empSearch() {
      let empId = $("#emp_id_search").val();
      if (empId.length > 0) {
        $.ajax({
          type: 'POST',
          url: "{{ url('/admin/find/employee-id') }}",
          data: {
            empId: empId
          },
          success: function(result) {
            $("#showEmpId").html(result);
          }
        });
      }

      if (empId.length < 1) $("#showEmpId").html("");
    }

    function dirEmpSearch() {
      let empId = $("#dir_emp_id_search").val();
      if (empId.length > 0) {
        $.ajax({
          type: 'POST',
          url: "{{ url('/admin/find/direct/employee-id') }}",
          data: {
            empId: empId
          },
          success: function(result) {
            $("#showEmpId2").html(result);
          }
        });
      }

      if (empId.length < 1) $("#showEmpId2").html("");
    }



    function indirectempSearch() {
      let empId = $("#emp_id_search").val();
      if (empId.length > 0) {
        $.ajax({
          type: 'POST',
          url: "{{ url('/admin/find/indirectemployee-id') }}",
          data: {
            empId: empId
          },
          success: function(result) {
            $("#showEmpId").html(result);
          }
        });
      }

      if (empId.length < 1) $("#showEmpId").html("");
    }

    function showResult() {
      $("#showEmpId").slideDown();
    }

    function hideResult() {
      $("#showEmpId").slideUp();
    }

    // function showResultAll(){
    //     $("#showEmpId2").slideDown();
    // }
    //
    // function hideResultAll(){
    //     $("#showEmpId2").slideUp();
    // }

    /* ================= Metarial & Tools Add to Cart ================= */

    function addToCart() {
      var itype_id = $('#itype_id option:selected').val();
      var itype_name = $('#itype_id option:selected').text();
      var icatg_id = $('#icatg_id option:selected').val();
      var icatg_name = $('#icatg_id option:selected').text();
      var iscatg_id = $('#iscatg_id option:selected').val();
      var iscatg_name = $('#iscatg_id option:selected').text();
      var quantity = $('#quantity').val();
      var stock_amount = $('#stock_amount').val();
      /* ajax request execute */
      $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {
          itype_id: itype_id,
          icatg_id: icatg_id,
          iscatg_id: iscatg_id,
          quantity: quantity,
          stock_amount: stock_amount,
          itype_name: itype_name,
          icatg_name: icatg_name,
          iscatg_name: iscatg_name,
        },
        url: "/admin/metarial-tools/cart/store",
        success: function(data) {
          getMetarialList();
          //  start message
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          })
          if ($.isEmptyObject(data.error)) {
            Toast.fire({
              type: 'success',
              title: data.success
            })
          } else {
            Toast.fire({
              type: 'error',
              title: data.error
            })
          }
          //  end message

        }



      });
    }
    addToCart();
    /* ================= Metarial & Tools View to List ================= */
    function getMetarialList() {
      $.ajax({
        type: 'GET',
        url: '/admin/metarial-tools/list/view',
        dataType: 'json',
        success: function(response) {
          $('span[id="cartSubTotal"]').text(response.cartTotal);
          $('input[id="net_amount"]').val(response.cartTotal);
          $('input[id="net_amount_hidden"]').val(response.cartTotal);
          var rows = "";
          $.each(response.carts, function(key, value) {
            rows += `
                <tr>
                  <td>${value.options.itype_id}, ${value.options.itype_name}</td>
                  <td>${value.options.icatg_id}, ${value.options.icatg_name}</td>
                  <td>${value.name}, ${value.options.iscatg_name}</td>
                  <td>( ${value.qty} X ${value.price} )</td>
                  <td><a style="cursor:pointer"  type="submit" title="delete" id="${value.rowId}" onclick="removeToCart(this.id)"><i class="fa fa-trash fa-lg delete_icon"></i></a></td>
                  <td>${value.subtotal}</td>
                </tr>

                `
          });
          $('#metarial_tools_list_view').html(rows);
        }
      });
    }
    getMetarialList();

    /* ================= remove type ================= */
    function removeToCart(rowId) {
      $.ajax({
        type: 'GET',
        url: '/admin/metarial-tools/single-list/remove/' + rowId,
        dataType: 'json',
        success: function(data) {
          getMetarialList();
          //  start message
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          })
          if ($.isEmptyObject(data.error)) {
            Toast.fire({
              type: 'success',
              title: data.success
            })
          } else {
            Toast.fire({
              type: 'error',
              title: data.error
            })
          }
          //  end message
        }
      });
    }
    /* ================= find employee type ================= */
    function findEmployeeType() {

      var emp_type_id = $('select[name="find_emp_type_id"]').val();
      // console.log(emp_type_id);
      $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {
          emp_type_id: emp_type_id,
        },
        url: "/admin/find/employee/type-id",
        success: function(response) {

          if (response.emp_type_id == 1) {
            $("#direct_man_power").removeClass('d-none').addClass('d-block');

            $("#indirect_man_power").removeClass('d-block').addClass(' d-none');
          }
          if (response.emp_type_id == 2) {
            $("#indirect_man_power").removeClass('d-none').addClass('d-block');

            $("#direct_man_power").removeClass('d-block').addClass(' d-none');
          }


        }
      });


    }
    /* ================= Show approver Form ================= */
    function showApproverForm($id) {
      var leave_id = $id;

      $.ajax({
        type: 'GET',
        dataType: 'json',
        url: "/admin/employee/leave/work/apply-form/" + leave_id,
        success: function(response) {
          $("#approver_form").removeClass('d-none').addClass('d-block');
          $("#leave_id").val(response.emleave_id);
          $("#start_date").val(response.start_date);
          $("#end_date").val(response.end_date);
          $("#show_days").val(response.required_day);
          $("#description").val(response.description);


        }

      });
      /* ################################ */
    }
  </script>
  <!-- employee Details function -->
  <script type="text/javascript">
    /* ================= search Employee Details ================= */
    function searchEmployeeDetails() {
      var emp_id = $("#emp_id_search").val();
      var iqamaNo = $("input[id='iqamaNoSearch']").val();
      $.ajax({
        type: 'POST',
        url: "{{ route('search.employee-details') }}",
        data: {
          emp_id: emp_id,
          iqamaNo: iqamaNo
        },
        dataType: 'json',
        success: function(response) {

          if (response.status == "error") {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").text('This Id Dosn,t Match!');
            $("span[id='error_show']").addClass('d-block').removeClass('d-none');
            $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
          } else {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").removeClass('d-block').addClass('d-none');
            $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
          }

          // $("#employee_photo_show").()
          /* show employee information in employee table */
          $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
          $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
          $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);
          $("span[id='show_employee_akama_expire_date']").text(response.findEmployee.akama_expire_date);


          $("span[id='show_employee_passport_no']").text(response.findEmployee.passfort_no);
          $("span[id='show_employee_passport_expire_date']").text(response.findEmployee.passfort_expire_date);

          $("span[id='show_employee_job_status']").text(response.findEmployee.status.title);
          /* conditionaly show project name */
          if (response.findEmployee.project_id == null) {
            $("span[id='show_employee_project_name']").text("No Assigned Project!");
          } else {
            $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
          }

          /* conditionaly show sponsor name */
          if (response.findEmployee.sponsor_id == null) {
            $("span[id='show_employee_sponsor_name']").text("No Assigned Sponsor!");
          } else {
            $("span[id='show_employee_sponsor_name']").text(response.findEmployee.sponsor.spons_name);
          }


          /* Direct And Indirect Status */
          if (response.findEmployee.emp_type_id == 2) {
            $("#work_hours_field").addClass('d-none').removeClass('d-block');
            $("#work_hours_field_custom").val(0);
          } else {
            $("#work_hours_field").addClass('d-block').removeClass('d-none');
          }
          /* Direct And Indirect Status */

          /* conditionaly show project name */
          /* conditionaly show Department name */
          if (response.findEmployee.department_id == null) {
            $("span[id='show_employee_department']").text("No Assigned Department");
          } else {
            $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
          }
          /* conditionaly show project name */
          /* show Relationaly data */
          if (response.findEmployee.emp_type_id == 1) {
            $("span[id='show_employee_type']").text("Direct Man Power");
          } else {
            $("span[id='show_employee_type']").text("Indirect Man Power");
          }
          $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
          $("span[id='show_employee_address_C']").text(response.findEmployee.country.country_name);
          $("span[id='show_employee_address_D']").text(response.findEmployee.division.division_name);
          $("span[id='show_employee_address_Ds']").text(response.findEmployee.district.district_name);

          $("span[id='show_employee_confirmation_date']").text(response.findEmployee.confirmation_date);
          $("span[id='show_employee_appointment_date']").text(response.findEmployee.appointment_date);
          $("span[id='show_employee_date_of_birth']").text(response.findEmployee.date_of_birth);
          $("span[id='show_employee_mobile_no']").text(response.findEmployee.mobile_no);
          $("span[id='show_employee_email']").text(response.findEmployee.email);
          $("span[id='show_employee_joining_date']").text(response.findEmployee.joining_date);
          if (response.findEmployee.maritus_status == 1) {
            $("span[id='show_employee_metarials']").text('Unmarid');
          } else {
            $("span[id='show_employee_metarials']").text('Marid');
          }
          /* show Relationaly data */
          /* show employee Salary */
          $("span[id='show_employee_basic']").text(response.salary.basic_amount);
          $("span[id='show_employee_house_rent']").text(response.salary.house_rent);
          $("span[id='show_employee_hourly_rent']").text(response.salary.hourly_rent);
          $("span[id='show_employee_mobile_allow']").text(response.salary.mobile_allowance);
          $("span[id='show_employee_food_allow']").text(response.salary.food_allowance);
          $("span[id='show_employee_medical_allow']").text(response.salary.medical_allowance);
          $("span[id='show_employee_local_travel_allow']").text(response.salary.local_travel_allowance);
          $("span[id='show_employee_conveyance_allow']").text(response.salary.conveyance_allowance);
          $("span[id='show_employee_increment_no']").text(response.salary.increment_no);
          $("span[id='show_employee_increment_amount']").text(response.salary.increment_amount);
          $("span[id='show_employee_food_allowance']").text(response.salary.food_allowance);
          $("span[id='show_employee_saudi_tax']").text(response.salary.saudi_tax);

          $("span[id='show_employee_address_details']").text(response.findEmployee.details);
          $("span[id='show_employee_present_address']").text(response.findEmployee.present_address);
          /* show salary details in input form */
          $('input[id="emp_status"]').val(response.findEmployee.job_status);
          $('input[id="emp_auto_id"]').val(response.findEmployee.emp_auto_id);
          $('input[id="input_basic_amount"]').val(response.salary.basic_amount);
          $('input[id="input_hourly_rate"]').val(response.salary.hourly_rent);
          $('input[id="input_house_rate"]').val(response.salary.house_rent);
          $('input[id="input_mobile_allowance"]').val(response.salary.mobile_allowance);
          $('input[id="input_medical_allowance"]').val(response.salary.medical_allowance);
          $('input[id="input_local_travel_allowance"]').val(response.salary.local_travel_allowance);
          $('input[id="input_conveyance_allowance"]').val(response.salary.conveyance_allowance);
          $('input[id="input_others1"]').val(response.salary.others1);
          /*hidden field*/
          $('input[id="input_emp_id_in_desig"]').val(response.findEmployee.emp_auto_id);
          $('input[id="hidden_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="hidden_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          $('input[id="hidden_input_emp_email_in_user"]').val(response.findEmployee.email);
          // user module
          $('input[id="show_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="show_input_emp_category_in_user"]').val(response.findEmployee.category.catg_name);
          $('input[id="show_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          // user module

          $('input[id="input_emp_desig_id_in_desig"]').val(response.findEmployee.designation_id);

          /* employee job experience list */
          var job_experience = "";
          $.each(response.find_job_experience, function(key, value) {
            var start_date = value.starting_date;
            var end_date = value.end_date;
            var st_date = new Date(start_date);
            var en_date = new Date(end_date);
            var total = (en_date - st_date);
            var days = total / 1000 / 60 / 60 / 24;

            job_experience += `
                        <tr>
                          <td>${value.company_name}</td>
                          <td>${value.ejex_title}</td>
                          <td>${days}</td>
                          <td>${value.designation}</td>
                          <td>${value.responsibity}</td>
                        </tr>

                        `
          });

          $("#show_employee_job_experience_list").html(job_experience);

          // employee photo part
          var passport = ` 
                          <td class="emp" style="text-align: center" colspan="3">Pasport:
                              <img height="50" width="50" src="{{ asset('uploads/zip.jpg') }}" alt="">
                          
                          <span class="emp2" style="text-align: center; text-transform: capitalize;" colspan="3"><a target="_blank" href="{{url('${response.findEmployee.pasfort_photo}')}}" class="btn btn-danger">Download</a></span></td>
                        `
          $("#show_employee_Pasport_photo").append(passport);
          var Medical = ` 
                          <td class="emp" style="text-align: center" colspan="3"> Medical :
                              <img height="50" width="50" src="{{ asset('uploads/zip.jpg') }}" alt="">
                          
                          <span class="emp2" style="text-align: center; text-transform: capitalize;" colspan="3"><a target="_blank" href="{{url('${response.findEmployee.medical_report}')}}" class="btn btn-danger">Download</a></span></td>
                        `
          $("#show_employee_Medical_photo").append(Medical);

          var Iqama = ` 
                          <td class="emp" style="text-align: center" colspan="3"> Akama :
                              <img height="50" width="50" src="{{ asset('uploads/zip.jpg') }}" alt="">
                          
                          <span class="emp2" style="text-align: center; text-transform: capitalize;" colspan="3"><a target="_blank" href="{{url('${response.findEmployee.akama_photo}')}}" class="btn btn-danger">Download</a></span></td>
                        `
          $("#show_employee_Iqama_photo").append(Iqama);

          var Appointment = ` 
                          <td class="emp" style="text-align: center" colspan="3"> Appoint :
                              <img height="50" width="50" src="{{ asset('uploads/zip.jpg') }}" alt="">
                          
                          <span class="emp2" style="text-align: center; text-transform: capitalize;" colspan="3"><a target="_blank" href="{{url('${response.findEmployee.employee_appoint_latter}')}}" class="btn btn-danger">Download</a></span></td>
                        `
          $("#show_employee_Appointment_photo").append(Appointment);

          var Profile = ` 
                          <td class="emp" style="text-align: center" colspan="3"> profile :
                              <img height="50" width="50" src="{{ asset('uploads/zip.jpg') }}" alt="">
                          
                          <span class="emp2" style="text-align: center; text-transform: capitalize;" colspan="3"><a target="_blank" href="{{url('${response.findEmployee.profile_photo}')}}" class="btn btn-danger">Download</a></span></td>
                        `
          $("#show_employee_Profile_photo").append(Profile);


          /* employee contact person list */
          var contact_person = "";
          $.each(response.find_emp_contact_person, function(key, value) {

            contact_person += `
                        <tr>
                          <td>${value.ecp_name}</td>
                          <td>${value.ecp_mobile1}</td>
                          <td>${value.ecp_relationship}</td>
                        </tr>

                        `
          });
          $("#show_employee_contact_person_list").html(contact_person);
          /* employee designation list */
          var designation = "";
          $.each(response.designation, function(key, value) {

            designation += `
                        <option value="${value.catg_id}">${value.catg_name}</option>

                        `
          });
          $("select[id='designation_id']").html(designation);
          /* ====================================================================*/
        }

      });
    }
  </script>

  <script type="text/javascript">
    // ================= search Employee Details for all update ui ================= 
    function searchEmployeeForUpdate() {
      var emp_id = $("#emp_id_search").val();
      var iqamaNo = $("input[id='iqamaNoSearch']").val();
      $.ajax({
        type: 'POST',
        url: "{{ route('search.employee-for-update') }}",
        data: {
          emp_id: emp_id,
          iqamaNo: iqamaNo
        },
        dataType: 'json',
        success: function(response) {

          if (response.status == "error") {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").text('This Id Dosn,t Match!');
            $("span[id='error_show']").addClass('d-block').removeClass('d-none');
            $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
          } else {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").removeClass('d-block').addClass('d-none');
            $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
          }

          /* show employee information in employee table */
          if (response.findEmployee.profile_photo != null) {
            var img = response.findEmployee.profile_photo;

            var html = ` <img height="80" src="{{asset('${img}')}}" alt=""> `;

            $('#profile_img').html(html);

            var imgPath = '{{ asset("' + img + '") }}';
            $('#asloob_img').addClass('d-none').removeClass('d-block');
            $('#profile_img').addClass('d-block').removeClass('d-none');

          } else {
            $('#profile_img').addClass('d-none').removeClass('d-block');
            $('#asloob_img').addClass('d-block').removeClass('d-none');
          }

          $("input[id='show_employee_id']").val(response.findEmployee.employee_id);
          $("#employee_id").text(response.findEmployee.employee_id);
          $("input[id='show_employee_name']").val(response.findEmployee.employee_name);
          $("input[id='show_employee_akama_no']").val(response.findEmployee.akama_no);
          $("input[id='show_employee_akama_expire_date']").val(response.findEmployee.akama_expire_date);

          $("input[id='show_employee_passport_no']").val(response.findEmployee.passfort_no);
          $("input[id='show_employee_passport_expire_date']").val(response.findEmployee.passfort_expire_date);

          $("input[id='show_employee_confirmation_date']").val(response.findEmployee.confirmation_date);
          $("input[id='show_employee_appointment_date']").val(response.findEmployee.appointment_date);
          $("input[id='show_employee_date_of_birth']").val(response.findEmployee.date_of_birth);
          $("input[id='show_employee_mobile_no']").val(response.findEmployee.mobile_no);
          $("input[id='show_employee_email']").val(response.findEmployee.email);
          $("input[id='show_employee_joining_date']").val(response.findEmployee.joining_date);

          /* show Relationaly data */
          /* show employee Salary */
          $("input[id='show_employee_basic']").val(response.salary.basic_amount);
          $("input[id='show_employee_house_rent']").val(response.salary.house_rent);
          $("input[id='show_employee_hourly_rent']").val(response.salary.hourly_rent);
          $("input[id='show_employee_mobile_allow']").val(response.salary.mobile_allowance);
          $("input[id='show_employee_food_allow']").val(response.salary.food_allowance);
          $("input[id='show_employee_contribution_amoun']").val(response.salary.cpf_contribution);
          $("input[id='show_employee_saudi_tax']").val(response.salary.saudi_tax);
          $("input[id='show_employee_medical_allow']").val(response.salary.medical_allowance);
          $("input[id='show_employee_local_travel_allow']").val(response.salary.local_travel_allowance);
          $("input[id='show_employee_conveyance_allow']").val(response.salary.conveyance_allowance);
          $("input[id='show_employee_increment_no']").val(response.salary.increment_no);
          $("input[id='show_employee_increment_amount']").val(response.salary.increment_amount);
          $("input[id='show_employee_food_allowance']").val(response.salary.food_allowance);
          $("input[id='show_employee_saudi_tax']").val(response.salary.saudi_tax);
          $("textarea[id='show_employee_others']").val(response.salary.others1);

          $("input[id='show_employee_address_details']").val(response.findEmployee.details);
          $("input[id='show_employee_present_address']").val(response.findEmployee.present_address);
          /* show salary details in input form */
          $('input[id="emp_auto_id"]').val(response.findEmployee.emp_auto_id);
          $('input[id="show_employee_basic_ours"]').val(response.salary.basic_hours);
          $('input[id="input_basic_amount"]').val(response.salary.basic_amount);
          $('input[id="input_hourly_rate"]').val(response.salary.hourly_rent);
          $('input[id="input_house_rate"]').val(response.salary.house_rent);
          $('input[id="input_mobile_allowance"]').val(response.salary.mobile_allowance);
          $('input[id="input_medical_allowance"]').val(response.salary.medical_allowance);
          $('input[id="input_local_travel_allowance"]').val(response.salary.local_travel_allowance);
          $('input[id="input_conveyance_allowance"]').val(response.salary.conveyance_allowance);
          $('input[id="input_others1"]').val(response.salary.others1);
          /*hidden field*/
          $('input[id="employee_auto_id"]').val(response.findEmployee.emp_auto_id);
          $('input[id="employee_id"]').val(response.findEmployee.employee_id);
          // user module
          $('input[id="show_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="show_input_emp_category_in_user"]').val(response.findEmployee.category.catg_name);
          $('input[id="show_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          // user module

          $('input[id="input_emp_desig_id_in_desig"]').val(response.findEmployee.designation_id);

          if (response.findEmployee.maritus_status == 1) {
            $("input[id='show_employee_metarials']").val('Unmarid');
          } else {
            $("input[id='show_employee_metarials']").val('Marid');
          }

          // department
          if (response.allDepartment != '') {
            $('select[name="department_id"]').empty();
            if (response.findEmployee.department_id == null) {
              $('select[name="department_id"]').append('<option value="">No Assigned Department</option>');

            } else {

              $('select[name="department_id"]').append('<option value="' + response.findEmployee.department.dep_id + '">' + response.findEmployee.department.dep_name + '</option>');
            }
            $.each(response.allDepartment, function(key, value) {
              $('select[name="department_id"]').append('<option value="' + value.dep_id + '">' + value.dep_name + '</option>');
            });
          } else {
            $('select[[name="department_id"]').append('<option>Data Not Found</option>');
          }

          // man power
          if (response.allEmpType != '') {
            $('select[name="emp_type_id"]').empty();
            if (response.findEmployee.emp_type_id == 1) {
              $('select[name="emp_type_id"]').append('<option value="1">Direct Man Power</option>');
              $("#emp_type_wise_show").addClass('d-block').removeClass('d-none');
              if (response.findEmployee.hourly_employee == 1) {
                $('#hourly_type_emp').attr('checked', true);
              } else {
                $('#hourly_type_emp').attr('checked', false);
              }

            } else {
              $('select[name="emp_type_id"]').append('<option value="2">Indirect Man Power</option>');
              $("#emp_type_wise_show").addClass('d-none').removeClass('d-block');
            }

            $.each(response.allEmpType, function(key, value) {
              $('select[name="emp_type_id"]').append('<option value="' + value.id + '">' + value.name + '</option>');
            });
          } else {
            $('select[[name="emp_type_id"]').append('<option>Data Not Found</option>');
          }


          // country
          if (response.allCountry != '') {
            $('select[name="country_id"]').empty();
            $('select[name="division_id"]').empty();
            $('select[name="district_id"]').empty();
            $("input[id='post_code']").val('');
            $("input[id='details']").val('');
            $("input[id='post_code']").val(response.findEmployee.post_code);
            $("input[id='details']").html(response.findEmployee.details);
            $('select[name="country_id"]').append('<option value="' + response.findEmployee.country_id + '">' + response.findEmployee.country.country_name + '</option>');
            $('select[name="division_id"]').append('<option value="' + response.findEmployee.division_id + '">' + response.findEmployee.division.division_name + '</option>');
            $('select[name="district_id"]').append('<option value="' + response.findEmployee.district_id + '">' + response.findEmployee.district.district_name + '</option>');
            $.each(response.allCountry, function(key, value) {
              $('select[name="country_id"]').append('<option value="' + value.id + '">' + value.country_name + '</option>');
            });
          } else {
            $('select[[name="country_id"]').append('<option>Data Not Found</option>');
          }
          // sponsor
          if (response.allSponsor != '') {
            $('select[name="sponsor_id"]').empty();
            $('select[name="sponsor_id"]').append('<option value="' + response.findEmployee.sponsor.spons_id + '">' + response.findEmployee.sponsor.spons_name + '</option>');
            $.each(response.allSponsor, function(key, value) {
              $('select[name="sponsor_id"]').append('<option value="' + value.spons_id + '">' + value.spons_name + '</option>');
            });
          } else {
            $('select[[name="sponsor_id"]').append('<option>Data Not Found</option>');
          }

          // project
          if (response.getAllProject != '') {
            $('select[name="projectStatus"]').empty();
            $('select[name="projectStatus"]').append('<option value="' + response.findEmployee.project.proj_id + '">' + response.findEmployee.project.proj_name + '</option>');
            $.each(response.getAllProject, function(key, value) {
              $('select[name="projectStatus"]').append('<option value="' + value.proj_id + '">' + value.proj_name + '</option>');
            });
          } else {
            $('select[[name="projectStatus"]').append('<option>Data Not Found</option>');
          }
          // designation
          if (response.designation != '') {
            $('select[name="designation_id"]').empty();
            $('select[name="designation_id"]').append('<option value="' + response.findEmployee.designation_id + '">' + response.findEmployee.category.catg_name + '</option>');
            $.each(response.designation, function(key, value) {
              $('select[name="designation_id"]').append('<option value="' + value.catg_id + '">' + value.catg_name + '</option>');
            });
          } else {
            $('select[[name="designation_id"]').append('<option>Data Not Found</option>');
          }

          //if (response.findEmployee.maritus_status == 1)   
          // employee status
          if (response.allEmployeeStatus != '') {
            $('select[name="EmpStatus_id"]').empty();
            $('select[name="EmpStatus_id"]').append('<option value="' + response.findEmployee.status.id + '">' + response.findEmployee.status.title + '</option>');
            $.each(response.allEmployeeStatus, function(key, value) {
              $('select[name="EmpStatus_id"]').append('<option value="' + value.id + '">' + value.title + '</option>');

            });

          } else {
            $('select[name="EmpStatus_id"]').append('<option value="">Data Not Found!</option>');
          }

          // var designation = "";
          // $.each(response.designation, function(key, value) {

          //   designation += `
          //               <option value="${value.catg_id}">${value.catg_name}</option>

          //               `
          // });
          // $("select[id='designation_id']").html(designation);
          /* ====================================================================*/
        }

      });
    }
  </script>

  <!-- ========= adjustment =========== -->
  <script type="text/javascript">
    function searchEmployeeAdvAdjust11() {
      var emp_id = $("#emp_id_search").val();
      var iqamaNo = $("input[id='iqamaNoSearch']").val();
      $.ajax({
        type: 'POST',
        url: "{{ route('search.employee-adjustment') }}",
        data: {
          emp_id: emp_id,
          iqamaNo: iqamaNo
        },
        dataType: 'json',
        success: function(response) {

          if (response.status == "error") {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").text('This Id Dosn,t Match!');
            $("span[id='error_show']").addClass('d-block').removeClass('d-none');
            $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
          } else {
            $("input[id='emp_id_search']").val('');

            $("span[id='error_show']").removeClass('d-block').addClass('d-none');
            $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
          }

          // $("#employee_photo_show").()
          /* show employee information in employee table */
          $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
          // aler(response.findEmployee.employee_id);
          $("input[id='emp_id']").val(response.findEmployee.employee_id);

          $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
          $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);


          $("span[id='show_employee_passport_no']").text(response.findEmployee.passfort_no);

          /* conditionaly show project name */
          if (response.findEmployee.project_id == null) {
            $("span[id='show_employee_project_name']").text("No Assigned Project!");
          } else {
            $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
          }

          /* Direct And Indirect Status */
          if (response.findEmployee.emp_type_id == 2) {
            $("#work_hours_field").addClass('d-none').removeClass('d-block');
          } else {
            $("#work_hours_field").addClass('d-block').removeClass('d-none');
          }
          /* Direct And Indirect Status */


          if (response.findEmployee.department_id == null) {
            $("span[id='show_employee_department']").text("No Assigned Department");
          } else {
            $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
          }
          /* conditionaly show project name */
          /* show Relationaly data */
          if (response.findEmployee.emp_type_id == 1) {
            $("span[id='show_employee_type']").text("Direct Man Power");
          } else {
            $("span[id='show_employee_type']").text("Indirect Man Power");
          }
          $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
          $("span[id='show_employee_address_C']").text(response.findEmployee.country.country_name);
          $("span[id='show_employee_address_D']").text(response.findEmployee.division.division_name);
          $("span[id='show_employee_address_Ds']").text(response.findEmployee.district.district_name);

          $("span[id='show_employee_mobile_no']").text(response.findEmployee.mobile_no);
          $("span[id='show_employee_joining_date']").text(response.findEmployee.joining_date);


          // show employee Salary 
          $("span[id='show_employee_basic']").text(response.salary.basic_amount);
          $("span[id='show_employee_house_rent']").text(response.salary.house_rent);
          $("span[id='show_employee_hourly_rent']").text(response.salary.hourly_rent);
          $("span[id='show_employee_mobile_allow']").text(response.salary.mobile_allowance);
          $("span[id='show_employee_medical_allow']").text(response.salary.medical_allowance);
          $("span[id='show_employee_local_travel_allow']").text(response.salary.local_travel_allowance);
          $("span[id='show_employee_conveyance_allow']").text(response.salary.conveyance_allowance);
          $("span[id='show_employee_increment_no']").text(response.salary.increment_no);
          $("span[id='show_employee_increment_amount']").text(response.salary.increment_amount);
          $("span[id='show_employee_others']").text(response.salary.others1 + response.salary.food_allowance);

          $("span[id='show_employee_address_details']").text(response.findEmployee.details);
          $("span[id='show_employee_present_address']").text(response.findEmployee.present_address);
          /* show salary details in input form */
          $('input[id="emp_auto_id"]').val(response.findEmployee.emp_auto_id);
          $('input[id="input_basic_amount"]').val(response.salary.basic_amount);
          $('input[id="input_hourly_rate"]').val(response.salary.hourly_rent);
          $('input[id="input_house_rate"]').val(response.salary.house_rent);
          $('input[id="input_mobile_allowance"]').val(response.salary.mobile_allowance);
          $('input[id="input_medical_allowance"]').val(response.salary.medical_allowance);
          $('input[id="input_local_travel_allowance"]').val(response.salary.local_travel_allowance);
          $('input[id="input_conveyance_allowance"]').val(response.salary.conveyance_allowance);
          $('input[id="input_others1"]').val(response.salary.others1);
          /*hidden field*/
          $('input[id="input_emp_id_in_desig"]').val(response.findEmployee.emp_auto_id);
          $('input[id="hidden_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="hidden_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          $('input[id="hidden_input_emp_email_in_user"]').val(response.findEmployee.email);
          // user module
          $('input[id="show_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="show_input_emp_category_in_user"]').val(response.findEmployee.category.catg_name);
          $('input[id="show_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          // user module

          $('input[id="input_emp_desig_id_in_desig"]').val(response.findEmployee.designation_id);



          //Sallary info

          // $('input[id="salarySummary"]').val(response.salarySummary);
          // $('input[id="totalSalary"]').val(response.totalSalary);
          // $('input[id="totalHours"]').val(response.totalHours);
          // $('input[id="totalContribution"]').val(response.totalContribution);
          // $('input[id="totalIqama"]').val(response.totalIqama);
          // $('input[id="totalAdvance"]').val(response.totalAdvance + response.totalSaudiTax);
          // $('input[id="totalAmount"]').val(response.totalAmount);
          // $('input[id="totalSaudiTax"]').val(response.totalSaudiTax);
          // $('input[id="remainingAmount"]').val(response.totalAmount - response.totalAdvance);
          // $('input[id="anualFee"]').val(response.anualFee);




          // employee job experience list 

          // var job_experience = "";
          // $.each(response.find_job_experience, function(key, value) {
          //   var start_date = value.starting_date;
          //   var end_date = value.end_date;
          //   var st_date = new Date(start_date);
          //   var en_date = new Date(end_date);
          //   var total = (en_date - st_date);
          //   var days = total / 1000 / 60 / 60 / 24;


          //   job_experience += `
          //               <tr>
          //                 <td>${value.company_name}</td>
          //                 <td>${value.ejex_title}</td>
          //                 <td>${days}</td>
          //                 <td>${value.designation}</td>
          //                 <td>${value.responsibity}</td>
          //               </tr>

          //               `
          // });

          // $("#show_employee_job_experience_list").html(job_experience);
          // // employee contact person list 
          // var contact_person = "";
          // $.each(response.find_emp_contact_person, function(key, value) {

          //   contact_person += `
          //               <tr>
          //                 <td>${value.ecp_name}</td>
          //                 <td>${value.ecp_mobile1}</td>
          //                 <td>${value.ecp_relationship}</td>
          //               </tr>

          //               `
          // });
          // $("#show_employee_contact_person_list").html(contact_person);
          // // employee designation list 
          // var designation = "";
          // $.each(response.designation, function(key, value) {

          //   designation += `
          //               <option value="${value.catg_id}">${value.catg_name}</option>

          //               `
          // });
          // $("select[id='designation_id']").html(designation);



        }

      });
    }
  </script>
  <!-- ========= adjustment =========== -->


  <!-- ==================== employee multi project work record insert =================== -->
  <script>
    function employeeMultipleEntryTime() {
      // alert('ookk');
      var emp_id = $('input[name="emp_id"]').val();
      var proj_name = $('select[name="proj_name"]').val();
      var startDate = $('input[name="startDate"]').val();
      var endDate = $('input[name="endDate"]').val();
      var totalHourTime = $('input[name="totalHourTime"]').val();
      var totalOverTime = $('input[name="totalOverTime"]').val();

      /* ajax request */
      if (emp_id != "" && proj_name != "" && startDate != "" && endDate != "" && totalHourTime != "") {
        // alert(proj_name)
        $.ajax({
          type: 'POST',
          dataType: 'json',
          data: {
            emp_id: emp_id,
            proj_name: proj_name,
            startDate: startDate,
            endDate: endDate,
            totalHourTime: totalHourTime,
            totalOverTime: totalOverTime
          },
          url: "{{ route('employee-multiple-time-insert') }}",
          success: function(data) {

            // error_massage
            if (data.error) {
              $("span[id='error_massage']").text("Employee Not Found!");
              $("span[id='error_massage']").removeClass('d-none').addClass('d-block');
            } else {
              // alert('ok');
              var emp_id = $('input[name="emp_id"]').val("");
              $("span[id='error_massage']").addClass('d-none').removeClass('d-block');
            }

            //  start message
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000
            })
            if ($.isEmptyObject(data.error)) {
              Toast.fire({
                type: 'success',
                title: data.success
              })
            } else {
              Toast.fire({
                type: 'error',
                title: data.error
              })
            }
            //  end message
          }
        });
      }

    }
  </script>

  <!-- ==================== employee multi project work record update =================== -->
  <script>
    function employeeMultipleWorkRecrdTimeUpdate() {
      // alert('update');
      var emp_id = $('input[name="emp_id"]').val();
      var proj_name = $('select[name="proj_name"]').val();
      var startDate = $('input[name="startDate"]').val();
      var endDate = $('input[name="endDate"]').val();
      var totalHourTime = $('input[name="totalHourTime"]').val();
      var totalOverTime = $('input[name="totalOverTime"]').val();
      var empwh_auto_id = $('input[name="empwh_auto_id"]').val();
      var total_day = $('input[name="total_day"]').val();
      // var total_day = $('input[name="total_day"]').val();

      /* ajax request */
      if (emp_id != "" && proj_name != "" && startDate != "" && endDate != "" && totalHourTime != "") {
        // alert(proj_name)
        $.ajax({
          type: 'POST',
          dataType: 'json',
          data: {
            emp_id: emp_id,
            proj_name: proj_name,
            startDate: startDate,
            endDate: endDate,
            totalHourTime: totalHourTime,
            totalOverTime: totalOverTime,
            empwh_auto_id: empwh_auto_id,
            total_day: total_day
          },
          url: "{{ route('employee-multiple-time-update') }}",
          success: function(data) {

            // error_massage
            if (data.error) {
              $("span[id='error_massage']").text("Employee Not Found!");
              $("span[id='error_massage']").removeClass('d-none').addClass('d-block');
            } else {
              // alert('ok');
              var emp_id = $('input[name="emp_id"]').val("");
              $("span[id='error_massage']").addClass('d-none').removeClass('d-block');
            }

            //  start message
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000
            })
            if ($.isEmptyObject(data.error)) {
              Toast.fire({
                type: 'success',
                title: data.success
              })
            } else {
              Toast.fire({
                type: 'error',
                title: data.error
              })
            }
            //  end message
          }
        });
      }

    }
  </script>

  <!-- ========= status =========== -->
  <script type="text/javascript">
    /* ================= search Employee Details ================= */
    function searchEmployeeStatus() {
      var emp_id = $("#emp_id_search").val();
      var iqamaNo = $("input[id='iqamaNoSearch']").val();
      // alert(emp_id);
      $.ajax({
        type: 'POST',
        url: "{{ route('search.employee-status') }}",
        data: {
          emp_id: emp_id,
          iqamaNo: iqamaNo
        },
        dataType: 'json',
        success: function(response) {

          if (response.status == "error") {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").text('This Id Dosn,t Match!');
            $("span[id='error_show']").addClass('d-block').removeClass('d-none');
            $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
          } else {
            $("input[id='emp_id_search']").val('');
            $("span[id='error_show']").removeClass('d-block').addClass('d-none');
            $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
          }

          // $("#employee_photo_show").()
          /* show employee information in employee table */
          $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
          $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
          $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);


          $("span[id='show_employee_passport_no']").text(response.findEmployee.passfort_no);

          /* conditionaly show project name */
          if (response.findEmployee.project_id == null) {
            $("span[id='show_employee_project_name']").text("No Assigned Project!");
          } else {
            $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
          }


          /* Direct And Indirect Status */
          if (response.findEmployee.emp_type_id == 2) {
            $("#work_hours_field").addClass('d-none').removeClass('d-block');
          } else {
            $("#work_hours_field").addClass('d-block').removeClass('d-none');
          }
          /* Direct And Indirect Status */

          /* conditionaly show project name */
          /* conditionaly show Department name */
          if (response.findEmployee.department_id == null) {
            $("span[id='show_employee_department']").text("No Assigned Department");
          } else {
            $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
          }
          /* conditionaly show project name */
          /* show Relationaly data */
          if (response.findEmployee.emp_type_id == 1) {
            $("span[id='show_employee_type']").text("Direct Man Power");
          } else {
            $("span[id='show_employee_type']").text("Indirect Man Power");
          }
          $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
          $("span[id='show_employee_address_C']").text(response.findEmployee.country.country_name);
          $("span[id='show_employee_address_D']").text(response.findEmployee.division.division_name);
          $("span[id='show_employee_address_Ds']").text(response.findEmployee.district.district_name);

          $("span[id='show_employee_mobile_no']").text(response.findEmployee.mobile_no);
          $("span[id='show_employee_joining_date']").text(response.findEmployee.joining_date);

          /* show Relationaly data */
          /* show employee Salary */
          $("span[id='show_employee_basic']").text(response.salary.basic_amount);
          $("span[id='show_employee_house_rent']").text(response.salary.house_rent);
          $("span[id='show_employee_hourly_rent']").text(response.salary.hourly_rent);
          $("span[id='show_employee_mobile_allow']").text(response.salary.mobile_allowance);
          $("span[id='show_employee_medical_allow']").text(response.salary.medical_allowance);
          $("span[id='show_employee_local_travel_allow']").text(response.salary.local_travel_allowance);
          $("span[id='show_employee_conveyance_allow']").text(response.salary.conveyance_allowance);
          $("span[id='show_employee_increment_no']").text(response.salary.increment_no);
          $("span[id='show_employee_increment_amount']").text(response.salary.increment_amount);
          $("span[id='show_employee_others']").text(response.salary.others1 + response.salary.food_allowance);

          $("span[id='show_employee_address_details']").text(response.findEmployee.details);
          $("span[id='show_employee_present_address']").text(response.findEmployee.present_address);
          /* show salary details in input form */
          $('input[id="emp_auto_id"]').val(response.findEmployee.emp_auto_id);
          $('input[id="input_basic_amount"]').val(response.salary.basic_amount);
          $('input[id="input_hourly_rate"]').val(response.salary.hourly_rent);
          $('input[id="input_house_rate"]').val(response.salary.house_rent);
          $('input[id="input_mobile_allowance"]').val(response.salary.mobile_allowance);
          $('input[id="input_medical_allowance"]').val(response.salary.medical_allowance);
          $('input[id="input_local_travel_allowance"]').val(response.salary.local_travel_allowance);
          $('input[id="input_conveyance_allowance"]').val(response.salary.conveyance_allowance);
          $('input[id="input_others1"]').val(response.salary.others1);
          /*hidden field*/
          $('input[id="input_emp_id_in_desig"]').val(response.findEmployee.emp_auto_id);
          $('input[id="hidden_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="hidden_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          $('input[id="hidden_input_emp_email_in_user"]').val(response.findEmployee.email);
          // user module
          $('input[id="show_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
          $('input[id="show_input_emp_category_in_user"]').val(response.findEmployee.category.catg_name);
          $('input[id="show_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
          // user module

          $('input[id="input_emp_desig_id_in_desig"]').val(response.findEmployee.designation_id);

          $("span[id='show_employee_job_status']").text(response.findEmployee.status.title);

          if (response.getAllProject != '') {
            $('select[name="projectStatus"]').empty();
            $('select[name="projectStatus"]').append('<option>Please Select Project</option>');
            $.each(response.getAllProject, function(key, value) {
              $('select[name="projectStatus"]').append('<option value="' + value.proj_id + '">' + value.proj_name + '</option>');
            });
          } else {
            $('select[[name="projectStatus"]').append('<option>Data Not Found</option>');
          }

          if (response.allEmployeeStatus != '') {
            $('select[name="empStatus"]').empty();
            $('select[name="empStatus"]').append('<option value="">Please Select</option>');
            $.each(response.allEmployeeStatus, function(key, value) {
              $('select[name="empStatus"]').append('<option value="' + value.id + '">' + value.title + '</option>');

            });

          } else {
            $('select[name="empStatus"]').append('<option value="">Data Not Found!</option>');
          }

          /* employee job experience list */
          var job_experience = "";
          $.each(response.find_job_experience, function(key, value) {
            var start_date = value.starting_date;
            var end_date = value.end_date;
            var st_date = new Date(start_date);
            var en_date = new Date(end_date);
            var total = (en_date - st_date);
            var days = total / 1000 / 60 / 60 / 24;


            job_experience += `
                        <tr>
                          <td>${value.company_name}</td>
                          <td>${value.ejex_title}</td>
                          <td>${days}</td>
                          <td>${value.designation}</td>
                          <td>${value.responsibity}</td>
                        </tr>

                        `
          });

          $("#show_employee_job_experience_list").html(job_experience);
          /* employee contact person list */
          var contact_person = "";
          $.each(response.find_emp_contact_person, function(key, value) {

            contact_person += `
                        <tr>
                          <td>${value.ecp_name}</td>
                          <td>${value.ecp_mobile1}</td>
                          <td>${value.ecp_relationship}</td>
                        </tr>

                        `
          });
          $("#show_employee_contact_person_list").html(contact_person);
          /* employee designation list */
          var designation = "";
          $.each(response.designation, function(key, value) {

            designation += `
                        <option value="${value.catg_id}">${value.catg_name}</option>

                        `
          });
          $("select[id='designation_id']").html(designation);
          /* ====================================================================*/
        }

      });
    }
  </script>



  <!-- ========= status =========== -->

  {{-- Address script --}}
  <script type="text/javascript">
    $(document).ready(function() {
      $('select[name="country_id"]').on('change', function() {
        var country_id = $(this).val();
        if (country_id) {
          $.ajax({
            url: "{{  url('/admin/division/ajax') }}/" + country_id,
            type: "GET",
            dataType: "json",
            success: function(data) {
              if (data == "") {
                // Division
                $('select[name="division_id"]').empty();
                $('select[name="division_id"]').append('<option value="">Data Not Found! </option>');
                // District
                $('select[name="district_id"]').empty();
                $('select[name="district_id"]').append('<option value=""> Data Not Found! </option>');

              } else {
                // Division
                $('select[name="division_id"]').empty();
                $('select[name="division_id"]').append('<option value="">Select Division</option>');
                // District
                $('select[name="district_id"]').empty();
                $('select[name="district_id"]').append('<option value=""> Select District </option>');
                // Division List
                $.each(data, function(key, value) {
                  $('select[name="division_id"]').append('<option value="' + value.division_id + '">' + value.division_name + '</option>');
                });
              }

            },

          });
        } else {

        }
      });
      /* call district */
      $('select[name="division_id"]').on('change', function() {
        var division_id = $(this).val();
        if (division_id) {
          $.ajax({
            url: "{{  url('/admin/district/ajax') }}/" + division_id,
            type: "GET",
            dataType: "json",
            success: function(data) {
              $('select[name="district_id"]').empty();
              if (data == "") {
                // District
                $('select[name="district_id"]').empty();
                $('select[name="district_id"]').append('<option value=""> Data Not Found! </option>');
              } else {
                // District
                $('select[name="district_id"]').empty();
                $('select[name="district_id"]').append('<option value=""> Select District </option>');
                // District List
                $.each(data, function(key, value) {
                  $('select[name="district_id"]').append('<option value="' + value.district_id + '">' + value.district_name + '</option>');
                });
              }





            },

          });
        } else {

        }
      });
      /* call employee category */
      $('select[name="emp_type_id"]').on('change', function() {
        var emp_type_id = $(this).val();

        if (emp_type_id == 1) {
          $("#hourlyEmployee").removeClass(' d-none').addClass(' d-block');
        } else {
          $("#hourlyEmployee").addClass('d-none').removeClass('d-block');
        }

        if (emp_type_id) {
          $.ajax({
            url: "{{  url('/admin/employee/category/ajax') }}/" + emp_type_id,
            type: "GET",
            dataType: "json",
            success: function(data) {
              $('select[name="designation_id"]').empty();
              $.each(data, function(key, value) {
                $('select[name="designation_id"]').append('<option value="' + value.catg_id + '">' + value.catg_name + '</option>');
              });

            },

          });
        } else {
          alert('danger');
        }
      });

    });






    function searchVehicleDetails() {
      var value = $("input[id='veh_plate_number']").val();
      

        //alert(value);

      $.ajax({
        type: 'POST',
        url: "{{ url('/admin/vehicle/search') }}",
      // url: "{{  url('/admin/division/ajax') }}/" +1,
        data: {
          value: value,
        },
        dataType: 'json',
        success: function(data) {

         // alert(' jjj text');

          if (data.status_code != 200) {

            alert(data.status_code);
            // $("input[id='emp_id_search']").val('');
            // $("span[id='error_show']").text('This Id Dosn,t Match!');
            // $("span[id='error_show']").addClass('d-block').removeClass('d-none');
            // $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
          } else {
          //  $("#showVehicleFineDetails").removeClass("d-none").addClass("d-block");
              var vehicle = data.data;
            //alert();
            // $("input[id='emp_id_search']").val('');
            // $("span[id='error_show']").removeClass('d-block').addClass('d-none');
          
             $("span[id='show_vechicle_employee_id']").text(vehicle.employee_id);
            // $("span[id='show_vechicle_employee_auto_id']").text(vehicle.emp_auto_id);
             
             $("span[id='show_vechicle_employee_name']").text(vehicle.employee_name);
             $("span[id='show_vechicle_akama_no']").text(vehicle.akama_no);
             $("span[id='show_vechicle_mobile_no']").text(vehicle.mobile_no);
             $("span[id='show_vechicle_model_number']").text(vehicle.veh_model_number);
             $("span[id='show_vechicle_plate_number']").text(vehicle.veh_plate_number);
             $("input[id='veh_id']").val(vehicle.veh_id);
             $("input[id='emp_auto_id']").val(vehicle.emp_auto_id);
             
            
            //  $request['description'],
            // $request['postCode'],
            // $request['roadNo'],
            // $request['houseNo'],
            // $request['floorNo'],
            // $request['plateNo'],
             

          }
        }
           

      });
    }

  </script>

  <!-- <script>
		function playButton(){
			document.getElementById('audio');
		}
		playButton();
</script> -->



  {{-- Check Employee Id For Ignore Duplicate --}}


  {{-- Manage User --}}
  @yield('manageUser')
  {{-- Manage User --}}
</body>

</html>