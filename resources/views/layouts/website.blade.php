<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/flaticon.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/plugins/slick.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/plugins/cssanimation.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/plugins/justifiedGallery.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/plugins/light-gallery.min.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/style.css">
    <link rel="stylesheet" href="{{ asset('contents/website') }}/assets/css/custom.css">
    <style>
        .brand-logo a img{
          height: 60px;
        }
    </style>
</head>
<body>
    <div class="header-area header-sticky bg-img space__inner--y40 background-repeat--x background-color--dark d-none d-lg-block" data-bg="{{ asset('contents/website') }}/assets/img/icons/ruler.png">
        <!-- header top -->
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                      @if (Route::has('login'))
                        <div class="header-top-info text-right">
                          @auth
                            <a class="header-top-info__link" href="{{ route('admin.dashboard') }}"><span>Dashboard</span></a>
                          @else
                            <a class="header-top-info__link" href="{{ route('login') }}"><span>Login</span></a>
                          @endauth
                        </div>
                      @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- menu bar -->
            <!-- sonrokkhon.blade.php file e ache -->
        <!-- end menu bar -->
    </div>
    <!--====================  End of header area  ====================-->

    <!--====================  mobile header ====================-->
    <div class="mobile-header header-sticky bg-img space__inner--y30 background-repeat--x background-color--dark d-block d-lg-none" data-bg="{{ asset('contents/website') }}/assets/img/icons/ruler.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <div class="brand-logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('contents/website') }}/assets/img/logo.png" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6">
                    <!-- <div class="mobile-menu-trigger-wrapper text-right" id="mobile-menu-trigger">
                        <span class="mobile-menu-trigger"></span>
                    </div> -->
                    <div class="text-right">
                      @if (Route::has('login'))
                          @auth
                            <a class="header-top-info__link" href="{{ route('admin.dashboard') }}"> <span>Dashboard</span> </a>
                          @else
                            <a class="header-top-info__link" href="{{ route('login') }}"> <span>Login</span> </a>
                          @endauth
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of mobile header  ====================-->

    <!--====================  offcanvas mobile menu ====================-->
    <div class="offcanvas-mobile-menu" id="mobile-menu-overlay">
        <a href="javascript:void(0)" class="offcanvas-menu-close" id="mobile-menu-close-trigger">
            <span class="menu-close"></span>
        </a>
        <div class="offcanvas-wrapper">
            <div class="offcanvas-inner-content">
                <div class="offcanvas-mobile-search-area">
                    <form action="#">
                        <input type="search" placeholder="Search ...">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <nav class="offcanvas-navigation">
                    <ul>
                        <li class="menu-item-has-children">
                            <a href="#">Home</a>
                            <ul class="sub-menu-mobile">
                                <li><a href="index.html">Homepage One</a></li>
                                <li><a href="index-2.html">Homepage Two</a></li>
                            </ul>
                        </li>
                        <li><a href="about.html">About</a></li>
                        <li class="menu-item-has-children">
                            <a href="service.html">Service</a>
                            <ul class="sub-menu-mobile">
                                <li><a href="service.html">Service Page</a></li>
                                <li><a href="service-details.html">Service Details Left Sidebar</a></li>
                                <li><a href="service-details-right-sidebar.html">Service Details Right Sidebar</a></li>
                                <li class="menu-item-has-children">
                                    <a href="#">Submenu Level One</a>
                                    <ul class="sub-menu-mobile">
                                        <li><a href="#">Submenu Level Two</a></li>
                                        <li><a href="#">Submenu Level Two</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Project</a>
                            <ul class="sub-menu-mobile">
                                <li><a href="project.html">Project Page</a></li>
                                <li><a href="project-details.html">Project Details</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Blog</a>
                            <ul class="sub-menu-mobile">
                                <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                <li><a href="blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                <li><a href="blog-details-right-sidebar.html">Blog Details Right Sidebar</a></li>
                                <li class="menu-item-has-children">
                                    <a href="#">Submenu Level One</a>
                                    <ul class="sub-menu-mobile">
                                        <li class="menu-item-has-children">
                                            <a href="#">Submenu Level Two</a>
                                            <ul class="sub-menu-mobile">
                                                <li><a href="#">Submenu Level Three</a></li>
                                                <li><a href="#">Submenu Level Three</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Submenu Level Two</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <div class="header-contact-info">
                            <ul class="header-contact-info__list">
                                <li><i class="fa fa-phone"></i> 01225 265 847</li>
                                <li><i class="fa fa-clock-o"></i> 9.00 am - 11.00 pm</li>
                                <li><i class="fa fa-user"></i> <a href="login-register.html">Login / Register</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--Off Canvas Widget Social Start-->
                    <div class="off-canvas-widget-social">
                        <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                        <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                        <a href="#" title="Vimeo"><i class="fa fa-vimeo-square"></i></a>
                    </div>
                    <!--Off Canvas Widget Social End-->
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of offcanvas mobile menu  ====================-->

    @yield('content');


    <!--====================  footer area ====================-->

    <!-- <div class="footer-area bg-img space__inner--ry120" data-bg="{{ asset('contents/website') }}/assets/img/backgrounds/footer-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <div class="footer-widget__logo space__bottom--40">
                            <a href="index.html">
                                <img src="{{ asset('contents/website') }}/assets/img/logo-white.png" class="img-fluid" alt="">
                            </a>
                        </div>
                        <p class="footer-widget__text space__bottom--20">Publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
                        <ul class="social-icons">
                            <li><a href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>


                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">
                        <h5 class="footer-widget__title space__bottom--20">Information</h5>
                        <ul class="footer-widget__menu">
                            <li><a href="#">About Brenda</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Our Projects</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Blog Post</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">
                        <h5 class="footer-widget__title space__bottom--20">Support</h5>
                        <ul class="footer-widget__menu">
                            <li><a href="#">About Brenda</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Our Projects</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Blog Post</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <h5 class="footer-widget__title space__top--15 space__bottom--20 space__top__md--40 space__top__lm--40">Contact us</h5>
                    <div class="footer-contact-wrapper">
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-map-marker"></i></div>
                            <div class="single-footer-contact__text">245 Distrealy Road, Central Park, New York</div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-phone"></i></div>
                            <div class="single-footer-contact__text"> <a href="tel:98548658125">+98548 658 125</a> <br> <a href="tel:65487235457">+65487 235 457</a> </div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-globe"></i></div>
                            <div class="single-footer-contact__text"><a href="mailto:info@example.com">info@example.com</a> <br> <a href="#">www.example.com</a> </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div> -->

    <!-- copyright text -->
    <div class="copyright-area background-color--deep-dark space__inner--y30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright-text">Copyright &copy; <a href="#">ASLOOB</a>, All Rights Reserved - 2019</p>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of footer area  ====================-->
    <!--====================  scroll top ====================-->
    <button class="scroll-top" id="scroll-top">
        <i class="fa fa-angle-up"></i>
    </button>
    <!--====================  End of scroll top  ====================-->
    <!-- JS
    ============================================ -->
    <!-- Modernizer JS -->
    <script src="{{ asset('contents/website') }}/assets/js/modernizr-2.8.3.min.js"></script>
    <!-- jQuery JS -->
    <script src="{{ asset('contents/website') }}/assets/js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('contents/website') }}/assets/js/bootstrap.min.js"></script>
    <!-- Popper JS -->
    <script src="{{ asset('contents/website') }}/assets/js/popper.min.js"></script>
    <!-- Slick slider JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/slick.min.js"></script>
    <!-- Counterup JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/counterup.min.js"></script>
    <!-- Waypoint JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/waypoint.min.js"></script>
    <!-- Justified Gallery JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/justifiedGallery.min.js"></script>
    <!-- Image Loaded JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/imageloaded.min.js"></script>
    <!-- Maosnry JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/masonry.min.js"></script>
    <!-- Light Gallery JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/light-gallery.min.js"></script>
    <!-- Mailchimp JS -->
    <script src="{{ asset('contents/website') }}/assets/js/plugins/mailchimp-ajax-submit.min.js"></script>
    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->
    <!--
    <script src="{{ asset('contents/website') }}/assets/js/plugins/plugins.min.js"></script>
    -->
    <!-- Main JS -->
    <script src="{{ asset('contents/website') }}/assets/js/main.js"></script>
</body>
<!-- Mirrored from demo.hasthemes.com/brenda-preview/brenda/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 15 Mar 2020 20:52:41 GMT -->
</html>
