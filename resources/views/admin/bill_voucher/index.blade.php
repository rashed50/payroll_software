@extends('layouts.admin-master')
@section('title') Electric Voucher @endsection
@section('content')

<div class="row bread_part">
  <div class="col-sm-12 bread_col">

    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active">Create QR Code Bill Voucher </li>
    </ol>
  </div>
</div>
<div class="row">
  <div class="card pt-2">
    <form id="eVoucherAddToCartForm">
      <div class="row">
        <div class="col-md-2">
          <label class="control-label">Items Details:<span class="req_star">*</span></label>
          <textarea name="" class="form-control" id="cartDescription" name="cartDescription" cols="30" rows="2" required>{{old('cartDescription')}}</textarea>
          @if ($errors->has('cartDescription'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cartDescription') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-md-2">
          <label class="control-label custom-control-label">Rate:<span class="req_star">*</span></label>
          <input type="number" class="form-control" name="cartRate" id="cartRate" value="0{{old('cartRate')}}" placeholder="Rate" required min="1">
          @if ($errors->has('cartRate'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cartRate') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-md-2">
          <label class="control-label custom-control-label">Quantity:<span class="req_star">*</span></label>
          <input type="number" class="form-control" name="cartQuantity" id="cartQuantity" value="0{{old('cartQuantity')}}" placeholder="Quantity" required min="1">
          @if ($errors->has('cartQuantity'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cartQuantity') }}</strong>
          </span>
          @endif
        </div>
        <!-- <div class="col-md-1">
          <label class="control-label custom-control-label">Vat:<span class="req_star">*</span></label>
          <input type="hidden" class="form-control" name="cartVat" id="cartVat" value="0" placeholder="cartVat" required>
          @if ($errors->has('cartVat'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cartVat') }}</strong>
          </span>
          @endif
        </div> -->

        <div class="col-md-1">
          <label class="control-label custom-control-label">Unit:</label>
          <select class="form-control" id="qty_unit" name="qty_unit">
            <option value="LM">LM</option>
            <option value="m2">m2</option>
            <option value="m3">m3</option>
            <option value="cm2">cm2</option>
            <option value="cm3">cm3</option>
            <option value="TM">TM</option>
          </select>
        </div>

        <div class="col-md-2">
          <label class="control-label custom-control-label">Total:<span class="req_star">*</span></label>
          <input type="text" class="form-control" name="cartTotal" id="cartTotal" value="0{{old('cartTotal')}}" placeholder="cartTotal" required min="1">
          @if ($errors->has('cartTotal'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cartTotal') }}</strong>
          </span>
          @endif
        </div>

        <!-- <div class="col-md-2">
          <label class="control-label custom-control-label">Sub Total:<span class="req_star">*</span></label>
          <input type="number" class="form-control" name="total" id="" value="{{old('total')}}" placeholder="total">
        </div> -->
        <div class="col-md-2">
          <div class="form-group row custom_form_group" style="text-align:right;padding-top: 21px;">
            <button class="btn btn-primary waves-effect" type="button" style="font-size: 12px; text-transform: capitalize; padding: 1px 5px;" id="eVoucherAddToCart">ADD TO CART</button>
          </div>
        </div>

      </div>

    </form>
    <!-- cart table section start -->
    <div class="card text-center mt-1">
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-bordered custom_table mb-0">
              <thead>
                <tr>
                  <th>Item Details</th>
                  <th>Rate</th>
                  <th>Qty</th>
                  <th>Sub Total</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody id="eVoucherAddToCartList"></tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td> <span id="eVouchercartTotQty" style=" font-weight:bold; "> </span> </td>
                  <td> <span id="eVouchercartSubTotal" style="font-weight:bold; "> </span> </td>
                </tr>
              </tfoot>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-lg-12">
    <form class="form-horizontal company-form" id="registration" method="post" target="_blank" action="{{ route('e.voucher.process') }}">
      @csrf
      <div class="card">


        <div class="card-body card_form">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-7">
              @if(Session::has('success'))
              <div class="alert alert-success alertsuccess" role="alert" style="margin-left: -20px">
                <strong>Successfully!</strong>Created QR Code Bill Voucher.
              </div>
              @endif
              @if(Session::has('error'))
              <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
                <strong>Opps!</strong> please try again.
              </div>
              @endif
            </div>
            <div class="col-md-2"></div>
          </div>
          <div class="row">
            <!-- first coll -->
            <div class="col-md-6">

              <div class="form-group row custom_form_group{{ $errors->has('main_contractor_en') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Main Contractor(Eng) :<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="main_contractor_en" value="" placeholder="Main Contractor Name (Eng)" required>
                  @if ($errors->has('main_contractor_en'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('main_contractor_en') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group row custom_form_group{{ $errors->has('main_contractor_rb') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Main Contractor (Arabic) :<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="main_contractor_rb" value="" placeholder="Enter Main Contractor Name (Arabic)" required>
                  @if ($errors->has('main_contractor_rb'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('main_contractor_rb') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group row custom_form_group{{ $errors->has('main_con_vat_no') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label"> VAT No. :<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="main_con_vat_no" value="" placeholder="Main Contractor Vat No." required>
                  @if ($errors->has('main_con_vat_no'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('main_con_vat_no') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <br />
              <div class="form-group row custom_form_group{{ $errors->has('sub_contractor_en') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Sub-Contractor(Eng) :<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="sub_contractor_en" value="" placeholder="Enter Sub-Contractor Name(Eng)" required>
                  @if ($errors->has('sub_contractor_en'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('sub_contractor_en') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group row custom_form_group{{ $errors->has('sub_contractor_rb') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Sub-Contractor(Arabic):<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="sub_contractor_rb" value="" placeholder="Enter Sub-Contractor Name (Arabic)" required>
                  @if ($errors->has('sub_contractor_rb'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('sub_contractor_rb') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('sub_con_vat_no') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label"> VAT No. :<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="sub_con_vat_no" value="" placeholder="Sub Contractor Vat No." required>
                  @if ($errors->has('sub_con_vat_no'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('sub_con_vat_no') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('total') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Total (Excl. VAT):<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="custom-form-control form-control" id="total" name="total" value="" placeholder="Total(Excluding VAT)">
                  @if ($errors->has('total'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('total') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
            </div>

            <!-- secound column  -->
            <div class="col-md-6">

              <div class="form-group row custom_form_group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Project:<span class="req_star">*</span></label>

                <div class="col-sm-7">
                  <select class="form-control" name="project_id" required>
                    <option value=""> Project</option>
                    @foreach($project as $proj)
                    <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('project_id'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('project_id') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('employee_id') ? ' has-error' : '' }}">

                <label class="col-sm-5 control-label">Submitted By:<span class="req_star">*</span></label>

                <div class="col-sm-7">
                  <select class="form-control" name="employee_id" required>
                    <option value="">Select Here</option>
                    @foreach($employee as $emp)
                    <option value="{{ $emp->emp_auto_id }}">{{ $emp->employee_name }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('employee_id'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('employee_id') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Invoice No:<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{ old('invoice_no') }}" placeholder="Invoice No" required>

                  @if ($errors->has('invoice_no'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('invoice_no') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('invoice_status') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Invoice Status:<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <select class="form-control" id="invoice_status" name="invoice_status" required>
                    <option value="">Select Here</option>

                    <option value="0">Pending</option>
                    <option value="1">Released</option>
                  </select>
                  @if ($errors->has('invoice_status'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('invoice_status') }}</strong>
                  </span>
                  @endif
                </div>
              </div>

              <!-- <div class="form-group row custom_form_group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Description:<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <textarea style="resize:none" name="description" rows="3" class="form-control" placeholder="Description" required>{{ old('description') }}</textarea>
                  @if ($errors->has('description'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                  </span>
                  @endif
                </div>
              </div> -->

              <div class="form-group row custom_form_group">
                <label class="col-sm-5 control-label">Vourcher Date::<span class="req_star">*</span></label>

                <div class="col-sm-7">
                  <input type="date" class="form-control" name="start_date" value="{{ Date('Y-m-d') }}" required>
                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('submitted_date') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Submitted Date:<span class="req_star">*</span></label>
                <div class="col-sm-7">
                  <input type="date" class="form-control" id="submitted_date" name="submitted_date" value="{{Date('Y-m-d')}}" required>

                </div>
              </div>

              <div class="form-group row custom_form_group{{ $errors->has('invoice_status') ? ' has-error' : '' }}">
                <label class="col-sm-5 control-label">Remarks:</label>
                <div class="col-sm-7">
                  <textarea style="height:100px; resize:none" name="remarks" class="form-control" placeholder="Remarks">{{ old('remarks') }}</textarea>

                </div>
              </div>



            </div>
          </div>


          <br />



          <!-- Double collum calculation -->
          <div class="row">
            <!-- 1st collum -->


            <div class="col-md-4">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row custom_form_group{{ $errors->has('vat') ? ' has-error' : '' }}">
                    <label class="col-sm-8 control-label">Total Vat(%):<span class="req_star">*</span></label>
                    <div class="col-sm-4">
                      <input type="number" class="form-control" name="vat" id="vat" value="0">
                      @if ($errors->has('vat'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('vat') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group row custom_form_group{{ $errors->has('retention') ? ' has-error' : '' }}">
                    <label class="col-sm-8 control-label">Retention(%):<span class="req_star">*</span></label>
                    <div class="col-sm-4">
                      <input type="number" class="form-control" name="retention" id="retention" value="0">
                      @if ($errors->has('retention'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('retention') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="col-sm-12">
                <div class="form-group row custom_form_group{{ $errors->has('vat_total') ? ' has-error' : '' }}">
                  <label class="col-sm-8 control-label">Total VAT :<span class="req_star">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="vat_total" id="vat_total" value="0">
                    @if ($errors->has('vat_total'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('vat_total') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group row custom_form_group{{ $errors->has('vat_total') ? ' has-error' : '' }}">
                  <label class="col-sm-8 control-label">Total Retention:<span class="req_star">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="retention_total" id="retention_total" value="0">
                    @if ($errors->has('retention_total'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('retention_total') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="col-sm-12">
                <div class="form-group row custom_form_group{{ $errors->has('total_with_vat') ? ' has-error' : '' }}">
                  <div class="col-sm-12">
                    <label class="control-label">Total Amount Included VAT:<span class="req_star">*</span></label>
                  </div>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="total_with_vat" id="total_with_vat" placeholder="Total Amount Included VAT" value="" required>
                    @if ($errors->has('total_with_vat'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('total_with_vat') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group row custom_form_group{{ $errors->has('grandTotal') ? ' has-error' : '' }}">
                  <div class="col-sm-12">
                    <label class="control-label">Total Amount(Incl. VAT & Excl. Retension):<span class="req_star">*</span></label>
                  </div>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="grandTotal" id="grandTotal" placeholder="Total Amount Included VAT and Exclusive Retention" value="" required>
                    @if ($errors->has('grandTotal'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('grandTotal') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12 text-center mt-2">
              <button type="submit" id="onSubmit" onclick="formValidation();" class="btn btn-primary waves-effect">PROCESS</button>
            </div>

          </div>
          <!-- row end -->
        </div>

      </div>
  </div>
  </form>


</div>
</div>

<script>
  $(document).ready(function() {
    getEVoucherAddToCartData();
    $('#cartQuantity').on('keyup', function() {
      // alert('ookk');
      var cartQuantity = $('#cartQuantity').val();
      var cartRate = $('#cartRate').val();
      var QntyAndRate = cartQuantity * cartRate;
      $('#cartTotal').val(QntyAndRate);
    });

    $('#cartRate').on('keyup', function() {
      // alert('ookk');
      var cartQuantity = $('#cartQuantity').val();
      var cartRate = $('#cartRate').val();
      var QntyAndRate = cartQuantity * cartRate;
      $('#cartTotal').val(QntyAndRate);
    });


    $("#eVoucherAddToCartForm").validate({
      rules: {
        cartDescription: {
          required: true,
        },
        cartVat: {
          number: true,
        },
        cartQuantity: {
          required: true,
          number: true,
        },
        cartRate: {
          required: true,
          number: true,
        },
      },

      messages: {
        cartDescription: {
          number: "Invalid Number!",
        },
        cartQuantity: {
          number: "Invalid Number!",
          required: "This Field Must be Required!",
        },
        cartVat: {
          required: "This Field Must be Required!",
        },
        cartRate: {
          required: "This Field Must be Required!",
        },
      },


    });



    $("#unitRate").keyup(function() {

      if ($('#unitRate').val() == "" || $('#unitRate').val() == null) {
        $('#unitRate').val('0');
      }
      var unitRate = parseFloat($('#unitRate').val());
      var quantity = parseFloat($('#quantity').val());
      var total = $('#total').val(unitRate * quantity);
      var vat = parseFloat($('#vat').val());
      var total = parseFloat($('#total').val());
      var totalWithVat = (vat * total) / 100;
      var vat_total = $('#vat_total').val(totalWithVat);
      var total = parseFloat($('#total').val());
      var totalWithVat = parseFloat($('#vat_total').val());
      var retention = parseFloat($('#retention').val());
      var totalWithRetention = parseFloat((retention * total) / 100);
      $('#retention_total').val(totalWithRetention);
      var grandTotal = (total + totalWithVat) - totalWithRetention;
      $('#grandTotal').val(grandTotal);
    });


    $("#quantity").keyup(function() {

      if ($('#quantity').val() == "" || $('#quantity').val() == null) {
        $('#quantity').val('0');
      }
      var unitRate = parseFloat($('#unitRate').val());
      var quantity = parseFloat($('#quantity').val());
      var total = $('#total').val(unitRate * quantity);
      var vat = parseFloat($('#vat').val());
      var total = parseFloat($('#total').val());
      var totalWithVat = (vat * total) / 100;
      var vat_total = $('#vat_total').val(totalWithVat);
      var total = parseFloat($('#total').val());
      var totalWithVat = parseFloat($('#vat_total').val());
      var retention = parseFloat($('#retention').val());
      var totalWithRetention = parseFloat((retention * total) / 100);
      $('#retention_total').val(totalWithRetention);
      var grandTotal = (total + totalWithVat) - totalWithRetention;
      $('#grandTotal').val(grandTotal);
    });


    $('#vat').keyup(function() {

      if ($('#vat').val() == "" || $('#vat').val() == null) {
        $('#vat').val('0');

      }
      var vat = parseFloat($('#vat').val());
      var total = parseFloat($('#total').val());
      var totalWithVat = (vat * total) / 100;
      var vat_total = $('#vat_total').val(totalWithVat);
      $('#grandTotal').val(totalWithVat + total);

      $('#total_with_vat').val(totalWithVat + total);
    });

    $('#retention').keyup(function() {

      if ($('#retention').val() == "" || $('#retention').val() == null) {
        $('#retention').val('0');

      }
      var total = parseFloat($('#total').val());
      if (total != 0) {
        var totalWithVat = parseFloat($('#vat_total').val());
        var retention = parseFloat($('#retention').val());
        var totalWithRetention = parseFloat((retention * total) / 100);
        $('#retention_total').val(totalWithRetention);
        var grandTotal = (total + totalWithVat) - totalWithRetention;
        $('#grandTotal').val(grandTotal);
      } else {
        $('#retention_total').val('0');
      }
    });


    /* ================= add cart type ================= */
    $('#eVoucherAddToCart').on('click', function() {
      $(this).submit('false');
      // alert('calling me');
      var cartDescription = 'tes'; // $('#cartDescription').val();
      var cartVat = 0; // $('#cartVat').val();
      var cartQuantity = $('#cartQuantity').val();
      var cartRate = $('#cartRate').val();
      var cartTotal = $('#cartTotal').val();
      var qty_unit = $('#qty_unit').val();
      //alert( cartTotal, qty_unit);
      if (cartDescription != '' && cartQuantity != '' && cartRate != '' && cartTotal != '') {

        //if (cartDescription != ''  && cartQuantity != '' && cartRate != '' && cartTotal != '') {
        // alert(qty_unit);
        $.ajax({
          type: "POST",
          url: "{{ route('evoucher-item-add.tocart')}}",
          dataType: "json",
          data: {
            cartDescription: cartDescription,
            cartVat: cartVat,
            cartQuantity: cartQuantity,
            qty_unit: qty_unit,
            cartRate: cartRate,
            cartTotal: cartTotal,

          },
          success: function(data) {

            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: true,
              timer: 4000
            })

            if ($.isEmptyObject(data.error)) {
              Toast.fire({
                type: 'success',
                title: data.success
              });
              getEVoucherAddToCartData();
              $('#description').val('');
              $('#cartVat').val('0');
              $('#cartQuantity').val('0');
              $('#cartRate').val('0');
              $('#cartTotal').val('0');

            } else {
              Toast.fire({
                type: 'error',
                title: data.error
              })
            }
          }
        });
      } else {
        alert(qty_unit);
      } // end if condition
    });


    /* ================= get cart type ================= */
    function getEVoucherAddToCartData() {
      $.ajax({
        type: "GET",
        url: "{{ route('get.evoucher-add.to.cart.data')}}",
        dataType: "json",
        success: function(data) {

          $('#eVouchercartTotQty').text(data.cartQty);
          $('#eVouchercartSubTotal').text(data.cartTotal);
          // grand total
          $('input[id="total"]').val(data.cartTotal);

          var html = '';

          $.each(data.cartContect, function(key, value) {
            html +=
              `
                <tr>
                  <td>${value.name}</td>
                  <td>${value.price} ${value.options.qty_unit}</td>
                  <td>${value.qty}</td>
                  <td>${value.options.cartTotal}</td>
                  <td><a style="cursor:pointer"  type="submit" title="delete" id="${value.rowId}" onclick="removeToCart(this.id)"><i class="fa fa-trash fa-lg delete_icon"></i></a></td>                </tr>
              `
          });
          $('#eVoucherAddToCartList').html(html);
        }
      });
    }

    /* ================= remove type ================= */
    function removeToCart(rowId) {
      $.ajax({
        type: 'POST',
        url: "{{ route('evoucher-remove.to.cart')}}",
        data: {
          rowId: rowId
        },
        dataType: 'json',
        success: function(data) {
          getEVoucherAddToCartData();
          //  start message
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          })
          if ($.isEmptyObject(data.error)) {
            Toast.fire({
              type: 'success',
              title: data.success
            })
          } else {
            Toast.fire({
              type: 'error',
              title: data.error
            })
          }
          //  end message
        }
      });
    }



  });
</script>
@endsection