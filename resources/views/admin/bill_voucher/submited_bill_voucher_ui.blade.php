@extends('layouts.admin-master')
@section('title') submited bill voucher @endsection
@section('content')
<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">submited bill voucher</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">submited bill voucher</li>
        </ol>
    </div>
</div>
<!-- add division -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> submited bill voucher </h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                                <thead>
                                    <tr>

                                        <th>Project</th>
                                        <th>Submited By</th>
                                        <th>Date</th>
                                        <th>Invoice</th>
                                        <th>Main Contractor</th>
                                        <th>Sub Contractor</th>
                                        <th>Status</th>
                                        <th>VAT</th>
                                        <th>Retention</th>
                                        <th>Total Amount</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($billVoucher as $aRecord)
                                    <tr>
                                        <td>{{ $aRecord->proj_name }}</td>
                                        <td>{{ $aRecord->employee_name }}</td>
                                        <td>{{ $aRecord->submitted_date }}</td>
                                        <td>{{ $aRecord->invoice_no }}</td>
                                        <td>{{ $aRecord->main_contractor_en }}</td>
                                        <td>{{ $aRecord->sub_contractor_en }}</td>
                                        <td>{{ $aRecord->invoice_status_id }}</td>
                                        <td>{{ $aRecord->total_vat }}</td>
                                        <td>{{ $aRecord->total_retention }}</td>
                                        <td>{{ $aRecord->total_amount }}</td>
                                        <td>

                                            <a href="{{ route('submited-bill-voucher-regenerate',$aRecord->invoice_record_auto_id) }}" target="_blank"><i class="fas fa-eye fa-lg view_icon"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection