<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Invoice</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
            margin: 0;
        }

        table {
            border-spacing: 0;
        }

        td {
            padding: 0;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }

        .font {
            font-size: 15px;
        }

        .authority {
            /*text-align: center;*/
            float: right
        }

        .authority h5 {
            margin-top: -10px;
            color: green;
            /*text-align: center;*/
            margin-left: 35px;
        }

        .thanks p {
            color: green;
            ;
            font-size: 16px;
            font-weight: normal;
            font-family: serif;
            margin-top: 20px;
        }

        .main-border {
            border: 1px solid;
        }

        .table-border {
            border: 1px solid;
            font-size: 11px;
            text-align: center;
            padding: 5px 0px;
        }

        .single-padding {
            padding: 10px;
        }

        .single-paddingTwo {
            padding: 15px 0px;
        }

        .single-padding-three {
            padding: 5px;
        }

        table.main-body-border {
            border: 2px solid #000000;
            margin: 80px 0px 20px 60px;
            padding: 0px 0px 20px 0px;
        }
    </style>
</head>

<body>

    <table width="100%" style="padding:0 60px 0 60px;">
        <tr>
            <td width="550" valign="top">
                <h2 style="color: #488987; font-size: 18px;line-height: 63px;letter-spacing: -1px;"><strong style="color: #5365a4;">ASLOOB INTERNATIONAL</strong> CONTRACTING COMPANY</h2>
            </td>
            <td width="100">
                <img style="width: 45px;height: 60px;" src="https://i.postimg.cc/Pr24NYWH/1.png" alt="">
            </td>
            <td width="500" align="right">
                <h2 style="color: #488987; font-size: 30px; line-height: 20px; letter-spacing: 4px;">شركة <strong style="color: #5365a4;">اسلوب الدولية</strong> للمقاولات</h2>
            </td>
        </tr>
    </table>

    <table width="100%" style="padding:0 60px 0 60px;">
        <tr>
            <td style="background-color: #5365a4;height: 3px;width: 100%;"></td>
        </tr>
    </table>



    <table class="main-body-border">
        <tr>
            <td>


                <table width="100%" style="text-align: center;">
                    <tr>
                        <td>
                            <strong style="border-bottom: 2px solid #000000;display: inline-block;font-weight: 800;font-size: 16px;letter-spacing: -1px;">TAX INVOICE #263</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong style="font-size: 14px;font-weight: 600;">الفاتورة الضريبية</strong>
                        </td>
                    </tr>
                </table>

                <table width="100%" style="padding: 10px 5px 10px 5px;">
                    <tr>
                        <td width="500" valign="top">
                            <p class="font" style="font-size: 13px;">
                                <strong>Project</strong><br> <span style="padding-left: 27px;font-size: 12px;">: {{$invoiceRecord->project_id}}</span> <br>
                                <strong>Date</strong><br><span style="padding-left: 27px;">: {{$invoiceRecord->submitted_date}} </span><br>
                                <strong>Invoice No</strong><br><span style="padding-left: 27px;">: {{$invoiceRecord->invoice_no}}</span> <br>
                                <strong>Main Contractor</strong><br><span style="padding-left: 27px;">:{{$invoiceRecord->main_contractor_en}}</span> <br>
                                <strong>Sub. Contractor</strong><br><span style="padding-left: 27px;">: {{$invoiceRecord->sub_contractor_en}}</span> <br>
                                <strong>Attn.</strong><br><span style="padding-left: 27px;">: Accounts Department {{$invoiceRecord->main_contractor_en}}</span> <br>
                            </p>
                        </td>
                        <td width="100">
                            <img src="{{$qrCoded}}" alt="Red dot" />
                        </td>
                        <td width="500" align="right">
                            <p class="font" style="font-size: 13px;">
                                <strong>مشروع</strong><br> <span style="padding-right: 90px;font-size: 12px;">مبنى سابك الجبيل الرئيسي {{$invoiceRecord->project_id}} </span> <br>
                                <strong>تاريخ</strong><br><span style="padding-right: 90px;">{{$invoiceRecord->submitted_date}} </span><br>
                                <strong>رقم الفاتورة</strong><br><span style="padding-right: 90px;">{{$invoiceRecord->invoice_no}}</span> <br>
                                <strong>المقاول الرئيسي</strong><br><span style="padding-right: 90px;">شابورجي بالونجي الشرق الأوسط {{$invoiceRecord->main_contractor_arb}}</span> <br>
                                <strong>الفرعية. مقاول</strong><br><span style="padding-right: 90px;">شركة أسلوب الدولية للمقاولين {{$invoiceRecord->sub_contractor_arb}}</span>
                            </p>
                        </td>
                    </tr>
                </table>

                <table width="100%" style="padding: 0px 0px 0px 0px;">
                    <tr>
                        <th class="table-border">Sl No.</th>
                        <th class="table-border">DESCRIPTION <br> (وصف)</th>
                        <th class="table-border">REFERENCE NO. <br> (رقم المرجع)</th>
                        <th class="table-border">UNIT <br> (وحدة)</th>
                        <th class="table-border">QUANTITY <br> (كمية)</th>
                        <th class="table-border">UNIT RATE <br> (معدل الوحدة)</th>
                        <th class="table-border">TAXABLE AMOUNT <br> (المبلغ الخاضع للضريبة)</th>
                        <th class="table-border">TAX RATE <br>(معدل الضريبة)</th>
                        <th class="table-border">TAX (SAR) <br> (ضريبة)</th>
                        <th class="table-border">TOTAL <br> (المجموع)</th>
                    </tr>
                    @php
                    $totalRetention = ($invoiceRecord->total_amount * $invoiceRecord->percent_of_retention) / 100;
                    $totalVAT = ($invoiceRecord->total_amount * $invoiceRecord->percent_of_vat) / 100;
                    $counter = 1;
                    @endphp

                    @foreach($invoiceRecordDetails as $invRecord)
                    <tr>
                        <td class="table-border">{{$counter++}}</td>
                        <td class="table-border">{{ $invRecord->items_details }}</td>
                        <td class="table-border">Ref No.</td>
                        <td class="table-border">Unit</td>
                        <td class="table-border">{{$invRecord->quantity }}</td>
                        <td class="table-border">{{$invRecord->rate }}</td>
                        <td class="table-border">{{ ($invRecord->total * $invRecord->percent_of_vat) / 100 }}</td>
                        <td class="table-border">{{ $invRecord->percent_of_vat }}</td>
                        <td class="table-border"></td>
                        <td class="table-border" style="text-align:right; padding-right:5px;">
                            {{$invRecord->total}}
                        </td>
                    </tr>
                    @endforeach



                    <tr>
                        <td colspan="6" class="table-border">
                            <strong style="float: left;">Total (Excluding VAT)</strong>
                            <strong style="float: right;padding-right: 10px;">الإجمالي (باستثناء ضريبة القيمة المضافة)</strong>
                        </td>
                        <td class="table-border"><strong></td>
                        <td class="table-border"></td>
                        <td class="table-border" colspan="2" style="text-align:right; padding-right:5px;">
                            <strong>{{$invoiceRecord->total_amount}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="table-border">
                            <strong style="float: left;">TOTAL VAT</strong>
                            <strong style="float: right;padding-right: 10px;">إجمالي ضريبة القيمة المضافة</strong>
                        </td>
                        <td class="table-border"><strong></strong></td>
                        <td class="table-border"></td>
                        <td class="table-border" colspan="2" style="text-align:right; padding-right:5px;">
                            <strong>{{$totalVAT}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="table-border">
                            <strong style="float: left;">Total Amount Including VAT</strong>
                            <strong style="float: right;padding-right: 10px;">المبلغ الإجمالي شاملاً ضريبة القيمة المضافة</strong>
                        </td>
                        <td class="table-border"><strong></strong></td>
                        <td class="table-border"></td>
                        <td class="table-border" colspan="2" style="text-align:right; padding-right:5px;">
                            <strong>{{ $invoiceRecord->total_amount + $totalVAT }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="table-border">
                            <strong style="float: left;">Retention Amount ({{$invoiceRecord->percent_of_retention}}%)</strong>
                            <strong style="float: right;padding-right: 10px;">({{$invoiceRecord->percent_of_retention}}%)مبلغ الاحتفاظ</strong>
                        </td>
                        <td class="table-border"><strong></strong></td>
                        <td class="table-border"></td>
                        <td class="table-border" colspan="2" style="text-align:right; padding-right:5px;">
                            <strong>{{ $totalRetention}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="table-border">
                            <strong style="float: left;">Total Amount Including VAT and Exclusive ({{$invoiceRecord->percent_of_retention}}% Retention</strong>
                            <strong style="float: right;  padding-right:5px;">المبلغ الإجمالي شاملاً ضريبة القيمة المضافة و 5٪ حصريًا للاحتفاظ</strong>
                        </td>
                        <td class="table-border"></strong></td>
                        <td class="table-border"></td>
                        <td class="table-border" colspan="2" style="text-align:right; padding-right:5px;"><strong>{{ $invoiceRecord->total_amount + $totalVAT - $totalRetention }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="10" class="table-border single-paddingTwo">
                            <p style="float: left;"><strong style="border-bottom: 2px solid;">In Words:</strong>
                                @php
                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                $inword = $f->format($invoiceRecord->total_amount + $totalVAT - $totalRetention );
                                @endphp
                                {{$inword}}
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" class="table-border">
                            <p style="float: left;">
                                <span>Value Added Tax Number</span><br>
                                <span>{{$invoiceRecord->main_con_vat_no }}</span>
                            </p>
                            <p style="float: right;">
                                <strong>رقم ضريبة القيمة المضافة</strong><br>
                                <strong>رقم ضريبة القيمة المضافة {{$invoiceRecord->main_con_vat_no }}</strong>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" class="table-border">
                            <p style="float: left;">
                                <span>Value Added Tax Number</span><br>
                                <span>{{$invoiceRecord->sub_con_vat_no }}</span>
                            </p>
                            <p style="float: right;">
                                <strong>رقم ضريبة القيمة المضافة</strong><br>
                                <strong>رقم ضريبة القيمة المضافة {{$invoiceRecord->sub_con_vat_no }}</strong>
                            </p>
                        </td>
                    </tr>
                </table>

                <table width="100%" style="text-align: center;">
                    <td width="50%" valign="top" align="left">
                        <p style="padding-top:30px;font-size: 14px;">With Regards</p>
                    </td>
                    <td width="50%" align="right">
                        <table>
                            <tr>
                                <td colspan="5" class="table-border single-padding-three" align="left"><strong style="float: left;">Please Pay To</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="table-border single-padding-three" align="left"><strong style="float: left;">Bank Name</strong></td>
                                <td colspan="5" class="table-border single-padding-three" align="left"><strong style="float: left;">Alawal bank</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="table-border single-padding-three" align="left"><strong style="float: left;">IBAN NUMBER</strong></td>
                                <td colspan="5" class="table-border single-padding-three" align="left"><strong style="float: left;">SA75500000000010809027002</strong></td>
                            </tr>
                        </table>
                    </td>
                </table>

                <table width="100%" style="padding: 20px 0px 15px 0px;font-size: 14px;">
                    <tr>
                        <td style="width: 200px;display: block;">
                            <p style="border-top: 2px solid #000000;">Mohd.Kitab Ali</p>
                        </td>
                    </tr>
                </table>

                <table width="100%" style="">
                    <tr>
                        <td width="500" valign="top">
                            <p class="font">
                                <strong style="padding-bottom:5px;display:inline-block">Engineer</strong><br>
                            <p style="font-size: 14px;padding-bottom: 4px;">Asloob International Contracting Company</p>
                            <p style="font-size: 14px;">Mob #050-344-2611</p>
                            <p style="border-bottom: 1px solid #000000;display: inline-block;padding-top: 5px;font-size: 13px;">zumonhossain10@gmail.com</p>
                            </p>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>


    <table width="100%" style="padding:0 60px 0 60px;">
        <tr>
            <td style="background-color: #5365a4;height: 3px;width: 100%;"></td>
        </tr>
    </table>


    <table width="100%" style="padding:10px 60px 10px 60px;">
        <tr>
            <td valign="top" align="center">
                <strong style="color: #5365a4;font-size: 18px;">المملكة العربية السعودية - ريناد - سلافمانيفاه لمملكة العربية السعودية - ريناد - سلافمانيفاه لمملكة العربية السعودية - ريناد - سلافمانيفاه لمملكة العربية السعودية - ريناد - سل</strong>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <strong style="color: #5365a4;font-size: 18px;">Kingdom of Saudi Arabia - Rivadh - Sulavmanivah - P0 Box 8460 - Postal Code 14233 - C.R. 1010503330</strong>
            </td>
        </tr>
    </table>

</body>

</html>