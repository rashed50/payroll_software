@extends('layouts.admin-master')
@section('title') Employee Information Search @endsection
@section('content')

@section('internal-css')
<style media="screen">
  a.checkButton {
    background: teal;
    color: #fff !important;
    font-size: 13px;
    padding: 5px 10px;
    cursor: pointer;
  }
</style>
@endsection

<div class="row bread_part">
  <div class="col-sm-12 bread_col">
    <h4 class="pull-left page-title bread_title">Employee Summary Search</h4>
    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active">Employee Summary Search</li>
    </ol>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header"></div>
      <div class="card-body card_form">
        <div class="row">
          <div class="col-md-10">
            {{-- checkbox for iqama No wise search employee --}}
            <div class="row">
              <div class="col-md-5"></div>
              <div class="col-md-3">
                <div class="form-check" style="margin-bottom:15px;">
                  <a id="iqamaWiseSearch" class=" d-block checkButton" onclick="iqamaWiseSearch()">Iqama Wise Search?</a>
                  <a id="idWiseSearch" class=" d-none checkButton" onclick="idWiseSearch()">ID Wise Search?</a>
                </div>
              </div>
            </div>

            {{-- Search Employee Id --}}
            <div id="searchEmployeeId" class=" d-block">
              <form action="{{ route('employee-salary.summary-process') }}" target="_blank" method="post">
                @csrf
                <div class="form-group row custom_form_group ">
                  <label class="col-sm-2 control-label">Employee ID:<span class="req_star">*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control typeahead" placeholder="Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">
                    <div id="showEmpId"></div>
                    <span id="error_show" class="d-none" style="color: red"></span>
                  </div>

                  <label class="col-sm-2 control-label">Salary Year:<span class="req_star">*</span></label>
                  <div class="col-sm-2">
                    <select class="form-control" name="year">
                      @foreach(range(date('Y'), date('Y')-5) as $y)
                      <option value="{{$y}}" {{$y}}>{{$y}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-sm-3">
                    <input name="chbox_sal_details" id="chbox_sal_details" type="checkbox" value="0">
                    <label for="saldetlabel">Salary Details</label>
                    <!-- <br>
                    <input name="chbox_upaid_salary" id="chbox_upaid_salary" type="checkbox" value="0">
                    <label for="saldetlabel">Unpaid Salary</label> -->
                  </div>


                  <div class="col-sm-1">
                    <button type="submit" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>

                  </div>
                </div>
              </form>
            </div>


            {{-- Search Employee IQama No --}}
            <div id="searchIqamaNo" class=" d-none">
              <form action="{{ route('employee-salary.summary-process') }}" target="_blank" method="post">
                @csrf
                <div class="form-group row custom_form_group">
                  <label class="col-sm-2 control-label">IQama No:</label>
                  <div class="col-sm-3">
                    <input type="text" id="iqamaNoSearch" class="form-control typeahead" placeholder="Input IQama No" name="iqamaNo">
                    <span id="error_show" class="d-none" style="color: red"></span>
                  </div>


                  <label class="col-sm-2 control-label">Salary Year:<span class="req_star">*</span></label>
                  <div class="col-sm-3">
                    <select class="form-control" name="year">
                      @foreach(range(date('Y'), date('Y')-5) as $y)
                      <option value="{{$y}}" {{$y}}>{{$y}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-sm-2">
                    <button type="submit" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- iqama no wise Search --}}
<script type="text/javascript">
  // iqama Wise search
  function iqamaWiseSearch() {
    $('#iqamaWiseSearch').removeClass('d-block').addClass('d-none');
    $('#idWiseSearch').removeClass('d-none').addClass('d-block');
    // input field
    $('#searchEmployeeId').removeClass('d-block').addClass('d-none');
    $('#searchIqamaNo').removeClass('d-none').addClass('d-block');

  }
  // id Wise search
  function idWiseSearch() {
    $('#idWiseSearch').removeClass('d-block').addClass('d-none');
    $('#iqamaWiseSearch').addClass('d-block').removeClass('d-none');
    // input field
    $('#searchIqamaNo').removeClass('d-block').addClass('d-none');
    $('#searchEmployeeId').removeClass('d-none').addClass('d-block');
  }
</script>
@endsection