@extends('layouts.admin-master')
@section('title') Employee Information Search @endsection
@section('content')

@section('internal-css')
<style media="screen">
    a.checkButton {
        background: teal;
        color: #fff !important;
        font-size: 13px;
        padding: 5px 10px;
        cursor: pointer;
    }
</style>
<style>
    tr {
        height: 20px;
        padding: 0;
        margin: 0;
    }

    td {
        padding-bottom: 0;
        margin: 0;
    }
</style>
@endsection

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">Employee Information Update</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Employee Information Update</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body card_form">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                        @if(Session::has('success_update'))
                        <div class="alert alert-success alertsuccess" role="alert" style="margin-left: -20px">
                            <strong>Successfully!</strong> Update Employee information.
                        </div>
                        @endif
                        @if(Session::has('error'))
                        <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
                            <strong>Opps!</strong> please try again.
                        </div>
                        @endif
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        {{-- checkbox for iqama no wise search employee --}}
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-3">
                                <div class="form-check" style="margin-bottom:15px;">
                                    <a id="iqamaWiseSearch" class=" d-block checkButton" onclick="iqamaWiseSearch()">Iqama Wise Search?</a>
                                    <a id="idWiseSearch" class=" d-none checkButton" onclick="idWiseSearch()">ID Wise Search?</a>
                                </div>
                            </div>
                        </div>
                        {{-- Search Employee Id --}}
                        <div id="searchEmployeeId" class=" d-block">
                            <div class="form-group row custom_form_group ">
                                <label class="col-sm-5 control-label">Employee ID:</label>
                                <div class="col-sm-4">

                                    <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">

                                    <div id="showEmpId"></div>
                                    <span id="error_show" class="d-none" style="color: red"></span>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" onclick="searchEmployeeForUpdate()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                        {{-- Search Employee IQama No --}}
                        <div id="searchIqamaNo" class=" d-none">
                            <div class="form-group row custom_form_group ">
                                <label class="col-sm-5 control-label">Iqama No:</label>
                                <div class="col-sm-4">
                                    <input type="text" id="iqamaNoSearch" class="form-control typeahead" placeholder="Input IQama No" name="iqamaNo">
                                    <span id="error_show" class="d-none" style="color: red"></span>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" onclick="searchEmployeeForUpdate()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>


                    </div>
                    {{-- show image --}}
                    <div class="col-md-3">
                        <div class="asloob_img" id="asloob_img">
                            <img src="{{ asset('contents/admin') }}/assets/images/avatar-1.jpg" alt="" class="image-resize">
                        </div>

                        <div class="employee_photo_show d-none" id="profile_img">
                            <!-- <img id="employee_photo_show" src="" alt="" class="image-resize"> -->
                        </div>
                    </div>
                </div>



                <!-- show employee information -->
                <div class="col-md-12">
                    <div id="showEmployeeDetails" class="d-none">
                        <form action="{{ route('employee-all-information-update') }}" method="POST">
                            @csrf
                            <div class="row">
                                <!-- employee Deatils -->
                                <div class="col-md-6">
                                    <div class="header_row">
                                        <span class="emp_info">Employee Information</span>
                                    </div>
                                    <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-8 control-label">Employee ID : <strong id="employee_id"></strong> </label>
                                                    <input id="employee_auto_id" name="id" type="hidden">
                                                    <!-- <div class="col-sm-7">
                                                    </div> -->
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Emp. Name :</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_name" name="emp_name" type="text">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Iqama No:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_akama_no" name="akama_no" type="text">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Iqama Expire:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_akama_expire_date" name="akama_expire" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Passport No:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_passport_no" name="passfort_no" type="text">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Iqama Expiry:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_passport_expire_date" name="passfort_expire_date" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">DoB :</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_date_of_birth" name="date_of_birth" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Mobile No. :</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_mobile_no" name="mobile_no" type="text">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Email:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_email" name="email" type="email">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Joining:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_joining_date" name="joining_date" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Confirmation:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_confirmation_date" name="confirmation_date" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="form-group row custom_form_group">
                                                    <label class="col-sm-3 control-label">Appointed:</label>
                                                    <div class="col-sm-7">
                                                        <input class="form-control" id="show_employee_confirmation_date" name="appointment_date" type="date">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="row custom_form_group">
                                                    <label class="col-sm-3 control-label">Parmanent Addre.:<span class="req_star">*</span></label>
                                                    <div class="col-sm-7">
                                                        <div class="parmanent_address">
                                                            <!-- country -->
                                                            <div class="form-group">
                                                                <select class="form-control" name="country_id">
                                                                    <option value="">Select Country</option>
                                                                    <option value="id "> </option>
                                                                </select>
                                                            </div>
                                                            <!-- division -->
                                                            <div class="form-group">
                                                                <select class="form-control" name="division_id">
                                                                    <option value="">Select Division</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <select class="form-control" name="district_id">
                                                                    <option value="">Select District</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" value="{{ old('post_code') }}" id="post_code" name="post_code" placeholder="Input Post Code">
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea class="form-control detailsAdd" id="details" name="details" placeholder="Input Address Details">{{ old('details') }}</textarea>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Salary Deatils -->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="header_row">
                                                <span class="emp_info">Employee Salary Details</span>
                                            </div>
                                            <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group{{ $errors->has('sponsor_id') ? ' has-error' : '' }}">
                                                            <label class="col-sm-3 control-label"> Sponsor:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" name="sponsor_id">
                                                                    <option value="">Select Sponsor Name</option>
                                                                    <option value="">Please Select</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="row custom_form_group">
                                                            <label class="col-sm-3 control-label">Emp. Type:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" name="emp_type_id">
                                                                    <option value="">Select Employee Type</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row custom_form_group d-none" id="emp_type_wise_show">
                                                            <label class="col-sm-3 control-label"></label>
                                                            <div class="col-sm-5">
                                                            Hourly Employee: <input class="" type="checkbox" id="hourly_type_emp" name="" value="">
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="row custom_form_group">
                                                            <label class="col-sm-3 control-label">Emp. Status:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7">
                                                                <div class="">
                                                                    <select class="form-control" name="EmpStatus_id">
                                                                        <option value="">Select Status</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="row custom_form_group">
                                                            <label class="col-sm-3 control-label">Designation:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7">
                                                                <div class="">
                                                                    <select class="form-control" name="designation_id">
                                                                        <option value="">Select Designation</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Project:</label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" name="projectStatus">
                                                                    <option value="">Select Here</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Asign Date:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="" name="asign_date" type="date" value="{{date('Y-m-d')}}">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Department:</label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" name="department_id">
                                                                    <option value="">Select Department</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Marital Status:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" name="maritus_status">
                                                                    <option value="1">Unmarried</option>
                                                                    <option value="2">Married</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Gender:<span class="req_star">*</span></label>
                                                            <div class="col-sm-7 gender">
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="gender" checked id="gender" value="1">
                                                                    <label class="form-check-label">Male</label>
                                                                </div>

                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="2">
                                                                    <label class="form-check-label">Female</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Basic Hours:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_basic_ours" name="basic_hours" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Basic Amount:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_basic" name="basic_amount" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Hourly Rate:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_hourly_rent" name="hourly_rent" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">House Rent:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_house_rent" name="house_rent" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Mobile:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_mobile_allow" name="mobile_allowance" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Food:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_food_allow" name="food_allowance" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Contribution Amoun:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_contribution_amoun" name="contribution_amoun" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Saudi Tax:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_saudi_tax" name="saudi_tax" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Medical:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_medical_allow" name="medical_allowance" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label"> Travels:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_local_travel_allow" name="local_travel_allowance" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Conveyance:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_conveyance_allow" name="conveyance_allowance" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Increment Amount:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_increment_amount" name="increment_amount" type="text">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group row custom_form_group">
                                                            <label class="col-sm-3 control-label">Others:</label>
                                                            <div class="col-sm-7">
                                                                <input class="form-control" id="show_employee_others" name="employee_others" type="text" value="0">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="submit" style="margin-top: 2px" class="btn btn-primary waves-effect">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-footer card_footer_button text-center">
                .
            </div>
        </div>
    </div>
</div>




{{-- iqama no wise Search --}}
<script type="text/javascript">
    // iqama Wise
    function iqamaWiseSearch() {
        $('#iqamaWiseSearch').removeClass('d-block').addClass('d-none');
        $('#idWiseSearch').removeClass('d-none').addClass('d-block');
        // input field
        $('#searchEmployeeId').removeClass('d-block').addClass('d-none');
        $('#searchIqamaNo').removeClass('d-none').addClass('d-block');

    }
    // id Wise
    function idWiseSearch() {
        $('#idWiseSearch').removeClass('d-block').addClass('d-none');
        $('#iqamaWiseSearch').addClass('d-block').removeClass('d-none');
        // input field
        $('#searchIqamaNo').removeClass('d-block').addClass('d-none');
        $('#searchEmployeeId').removeClass('d-none').addClass('d-block');
    }
</script>
@endsection