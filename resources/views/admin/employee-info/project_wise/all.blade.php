@extends('layouts.admin-master')
@section('title') Project Wise Employees @endsection
@section('content')
<div class="row bread_part">
  <div class="col-sm-12 bread_col">
    <h4 class="pull-left page-title bread_title"> Employees List Report</h4>
    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active"> Employees Report </li>
    </ol>
  </div>
</div>
<!-- add division -->
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    @if(Session::has('error'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> This Project Employee Not Assigned!.
    </div>
    @endif
  </div>
</div>

<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" target="_blank" action="{{ route('project-wise.employee.process') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header"></div>
        <div class="card-body card_form" style="padding-top: 0;">


          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3">Project Name:</label>
            <div class="col-md-6">
              <select class="form-control" name="proj_id">
                <option value="">Select Project Name:</option>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3">Sponser Name:</label>
            <div class="col-md-6">
              <select class="form-control" name="spons_id">
                <option value="">Select Sponser</option>
                @foreach($sponser as $spons)
                <option value="{{ $spons->spons_id }}">{{ $spons->spons_name }}</option>
                @endforeach
              </select>
            </div>
          </div>


          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3">Employee Trade:</label>
            <div class="col-md-6">
              <select class="form-control" name="catg_id">
                <option value="">Select Employee Trade</option>
                @foreach($category as $cate)
                <option value="{{ $cate->catg_id }}">{{ $cate->catg_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3">Employee Status:</label>
            <div class="col-md-6">
              <select class="form-control" name="job_status">
                <option value="">Select Employee Status</option>
                @foreach($jobStatus as $status)
                <option value="{{ $status->id }}">{{ $status->title }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Employee type List 1 = direct, 2 = indirect --}}
          <hr>
          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3"> Employee Type:</label>
            <div class="col-md-6">
              <select class="form-control" name="emp_type_id" required>
                <option value="0">Select Employee Type</option>
                <option value="-1">Direct Employee (Basic Salary)</option>
                @foreach($emp_types as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>


        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Show Report</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>

<!-- search job status -->

@endsection