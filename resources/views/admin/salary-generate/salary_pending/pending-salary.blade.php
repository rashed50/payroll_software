@extends('layouts.admin-master')
@section('title') Employee Pending Salary list @endsection
@section('content')

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">Employee Pending Salary list</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Pending Salary</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-7">
        @if(Session::has('payment'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> Payment Employee Salary.
        </div>
        @endif
        @if(Session::has('success_soft'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> delete  information.
        </div>
        @endif
        @if(Session::has('success_publish'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> publish  information.
        </div>
        @endif
        @if(Session::has('success_update_image'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> Update New Image.
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-warning alerterror" role="alert">
            <strong>Opps!</strong> please try again.
        </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Pending Salary List </h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="card-body">

                <form method="post" action="#">
                    @csrf


                    <div class="row">
                        <div class= "col-md-2">   </div>
                        <div class ="col-md-4">
                            {{-- Sponser List --}}
                                <label class="control-label d-block" style="text-align: left;">Sponser Name:</label>
                                <div>
                                    <select class="form-control" name="SponsId" id="SponsId" required>
                                        <option value="0">ALL</option>
                                        @foreach($sponserList as $spons)
                                        <option value="{{ $spons->spons_id }}" >{{ $spons->spons_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                        </div>
                        <div class ="col-md-4">
                        {{-- Project List --}}
                             
                                <label class="control-label d-block" style="text-align: left;">Project Name:</label>
                                <div>
                                    <select class="form-control" name="proj_id" id="proj_id" required>
                                        <option value="0">ALL</option>
                                        @foreach($projectlist as $proj)
                                        <option value="{{ $proj->proj_id }}" >{{ $proj->proj_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                           
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br/>
                    {{-- Date To Date --}}
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row custom_form_group">
                                        <label class="col-sm-3 control-label">From:<span class="req_star">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control fromDate" id="datepickerFrom" autocomplete="off" name="fromDate" value="{{date('m/Y')}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row custom_form_group">
                                        <label class="col-sm-3 control-label">To:<span class="req_star">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control toDate" id="datepickerTo" autocomplete="off" name="toDate" value="{{date('m/Y')}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" onclick="searchEmpPendingSalaryList()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>


                </form>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">

                            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Iqama No</th>
                                        <th>Sponser</th>
                                        <th>Trade</th>
                                        <th>Emp Type</th>
                                        <th>Month</th>
                                        <th>Salary</th>
                                        <th colspan="2" class="text-center">Manage</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="employeePendingList"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        searchEmpPendingSalaryList();
    });
</script>

<script>
    function searchEmpPendingSalaryList(){
        var fromDate = $('.fromDate').val();
        var toDate = $('.toDate').val();
        var proj_id = $('#proj_id').val();
        var SponsId = $('#SponsId').val();

        if (fromDate != '' && toDate != '') {

            $('#employeePendingList').html('');

            $.ajax({
                type: "POST",
                url: "{{ route('salary-pending.list') }}",
                data: {
                    fromDate: fromDate,
                    toDate: toDate,
                    SponsId: SponsId,
                    proj_id: proj_id
                },
                dataType: "json",

                success: function(response) {
                    // alert(response);
                    var rows = "";
                    var counter = 0;
                    $.each(response.pendingSalary, function(key, value) {
                        counter++;
                        rows += `
                                <tr>
                                    <td>${counter}</td>
                                    <td>${value.employee.employee_id}</td>
                                    <td>${value.employee.employee_name}</td>
                                    <td>${value.employee.akama_no}</td>
                                    <td>${value.employee.sponsor.spons_name}</td>
                                    <td>${value.employee.category.catg_name}</td>
                                    <td>${value.employee.type.name}</td>
                                    <td>${value.month.month_name}</td>
                                    <td>${value.slh_total_salary}</td>
                                    <td>
                                    <a title="Add" onclick="pendingSalaryPay(${value.slh_auto_id})"><i class="fas fa-thumbs-up fa-lg edit_icon"></i></a>
                                    </td>
                                </tr>
                               `
                    });

                    $('#employeePendingList').html(rows);

                }

            });
        } else {
            alert('Please input date');
        }
    }

    // pay salay function

    function pendingSalaryPay(id){
        if(id){
            $.ajax({
                type: "POST",
                url: "{{route('payment.salary')}}",
                dataType: "json",
                data:{id:id},
                success: function(response){
                    
                    searchEmpPendingSalaryList();
                    //  start message
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    })
                    if($.isEmptyObject(data.error)){
                        Toast.fire({
                            type: 'success',
                            title: data.success
                        })
                    }else{
                        Toast.fire({
                        type: 'error',
                        title: data.error
                        })
                    }
                    //  end message
                }
            })
        }else{
            alert('Employee not found');
        }

    }

    // Datepicker 
    $('document').ready(function() {
        $('#datepickerFrom').datepicker({
            autoclose: true,
            toggleActive: true,
            // startView: "months",  // Ata 1st month select korle date asbe
            minViewMode: "months",
            // format: "mm/yyyy",
        });

        $('#datepickerTo').datepicker({
            autoclose: true,
            toggleActive: true,
            // viewMode: "months",
            minViewMode: "months",
            // format: "mm/yyyy",
        });
    });
</script>
@endsection