@extends('layouts.admin-master')
@section('title') Employee Information Search @endsection
@section('content')

@section('internal-css')
  <style media="screen">
  a.checkButton {
    background: teal;
    color: #fff!important;
    font-size: 13px;
    padding: 5px 10px;
    cursor: pointer;
  }
  </style>
@endsection

{{-- Response Massege --}}
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if(Session::has('update_success'))
          <div class="alert alert-success alertsuccess" role="alert">
             <strong>Successfully!</strong> Correction Salary Information.
          </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>
{{-- Response Massege --}}

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">Employee Information Search</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Employee Information Search</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <span id="error_show2" class="d-none" style="color: red"></span>
              </div>
              <div class="card-body card_form">
                  <div class="row">
                    <div class="col-md-9">
                      {{-- checkbox for iqama no wise search employee --}}
                      <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-3">
                          <div class="form-check" style="margin-bottom:15px;">
                            <a id="iqamaWiseSearch" class=" d-block checkButton" onclick="iqamaWiseSearch()">Iqama Wise Search?</a>
                            <a id="idWiseSearch" class=" d-none checkButton" onclick="idWiseSearch()">ID Wise Search?</a>
                          </div>
                        </div>
                      </div>
                      {{-- Search Employee Id --}}
                      <div id="searchEmployeeId"  class=" d-block">
                        <div class="form-group row custom_form_group ">
                            <label class="col-sm-5 control-label">Employee ID:</label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">
                              <div id="showEmpId"></div>
                              <span id="error_show" class="d-none" style="color: red"></span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" onclick="searchEmployeeDetails()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                      </div>
                      {{-- Search Employee IQama No --}}
                      <div id="searchIqamaNo" class=" d-none">
                        <div class="form-group row custom_form_group ">
                            <label class="col-sm-5 control-label">IQama No:</label>
                            <div class="col-sm-4">
                              <input type="text" id="iqamaNoSearch" class="form-control typeahead" placeholder="Input IQama No" name="iqamaNo">
                              <span id="error_show" class="d-none" style="color: red"></span>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" onclick="searchEmployeeDetails()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                      </div>


                    </div>
                    {{-- show image --}}
                    <div class="col-md-3">
                      <div class="employee_photo_show" id="employee_photo_show">
                            <img src="{{ asset('contents/admin') }}/assets/images/avatar-1.jpg" alt="" class="image-resize">
                      </div>
                    </div>
                  </div>

                <!-- show employee information -->
                <div class="col-md-12">
                  <div id="showEmployeeDetails" class="d-block">
                      <div class="row">
                          <div class="col-md-3"></div>
                          <div class="col-md-6">
                              <div class="header_row">
                                  <span class="emp_info">Employee Information Details</span>
                              </div>
                              <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">
                                <tr>
                                    <td> <span class="emp">Project Name:</span>  <span id="show_employee_project_name" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Employee Id:</span>  <span id="show_employee_id" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Employee Name:</span>  <span id="show_employee_name" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Iqama No:</span>  <span id="show_employee_akama_no" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Type:</span>  <span id="show_employee_type" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Trade:</span>  <span id="show_employee_category" class="emp2"></span> </td>
                                </tr>
                                <tr>
                                    <td> <span class="emp">Department:</span>  <span id="show_employee_department" class="emp2"></span> </td>
                                </tr>
                              </table>
                          </div>
                          <div class="col-md-3"></div>
                      </div>
                  </div>
                </div>
              </div>
              <div class="card-footer card_footer_button text-center">
                .
              </div>
          </div>
    </div>
</div>
{{-- iqama no wise Search --}}
<script type="text/javascript">
  // iqama Wise
  function iqamaWiseSearch(){
    $('#iqamaWiseSearch').removeClass('d-block').addClass('d-none');
    $('#idWiseSearch').removeClass('d-none').addClass('d-block');
    // input field
    $('#searchEmployeeId').removeClass('d-block').addClass('d-none');
    $('#searchIqamaNo').removeClass('d-none').addClass('d-block');

  }
  // id Wise
  function idWiseSearch(){
    $('#idWiseSearch').removeClass('d-block').addClass('d-none');
    $('#iqamaWiseSearch').addClass('d-block').removeClass('d-none');
    // input field
    $('#searchIqamaNo').removeClass('d-block').addClass('d-none');
    $('#searchEmployeeId').removeClass('d-none').addClass('d-block');
  }

</script>
{{-- Employee Salary Currection --}}
<script type="text/javascript">
  /* ================= search Employee Details ================= */
  function searchEmployeeSallaryPaySalary(){
    var emp_id = $("#emp_id_search").val();
    var iqamaNo = $("input[id='iqamaNoSearch']").val();
    var month = $("select[name='month']").val();
    var year = $("select[name='year']").val();
    $.ajax({
      type:'POST',
      url: "#",
      data:{ emp_id:emp_id, iqamaNo:iqamaNo, month:month, year:year },
      dataType:'json',
      success:function(response){
        /* ==================== Error1 ==================== */
        if(response.status == "error"){
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show']").text('This Id Dosn,t Match!');
          $("span[id='error_show']").addClass('d-block').removeClass('d-none');
          $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
        }else{
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show']").removeClass('d-block').addClass('d-none');
          $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
        }
        /* ==================== Error2 ==================== */
        if(response.status == "error2"){
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show2']").text('This Record Not Found!');
          $("span[id='error_show2']").addClass('d-block').removeClass('d-none');
          $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
        }else{
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show2']").removeClass('d-block').addClass('d-none');
          $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
        }
        /* ==================== Error2 ==================== */
        /* show employee information in employee table */
         $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
         $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
         $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);
         $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
        /* conditionaly show project name */
        if(response.findEmployee.project_id == null){
          $("span[id='show_employee_project_name']").text("No Assigned Project!");
        }else{
          $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
        }
        /* conditionaly show Department name */
        if(response.findEmployee.department_id == null){
          $("span[id='show_employee_department']").text("No Assigned Department");
        }else{
          $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
        }
        /* conditionaly show project name */
        /* show Relationaly data */
        if(response.findEmployee.emp_type_id == 1){
          $("span[id='show_employee_type']").text("Direct Man Power");
        }else{
          $("span[id='show_employee_type']").text("Indirect Man Power");
        }
        /* ================ Salary Information ================ */
        $("input[id='salaryHistoryId']").val(response.salaryInfo.slh_auto_id);
        $("input[id='basic_amount']").val(response.salaryInfo.basic_amount);
        $("input[id='basic_hours']").val(response.salaryInfo.basic_hours);
        $("input[id='house_rent']").val(response.salaryInfo.house_rent);
        $("input[id='hourly_rent']").val(response.salaryInfo.hourly_rent);
        $("input[id='mobile_allowance']").val(response.salaryInfo.mobile_allowance);
        $("input[id='medical_allowance']").val(response.salaryInfo.medical_allowance);
        $("input[id='local_travel_allowance']").val(response.salaryInfo.local_travel_allowance);
        $("input[id='conveyance_allowance']").val(response.salaryInfo.conveyance_allowance);
        $("input[id='food_allowance']").val(response.salaryInfo.food_allowance);
        $("input[id='others']").val(response.salaryInfo.others);
        $("input[id='slh_total_overtime']").val(response.salaryInfo.slh_total_overtime);
        $("input[id='slh_overtime_amount']").val(response.salaryInfo.slh_overtime_amount);
        $("input[id='slh_total_salary']").val(response.salaryInfo.slh_total_salary);
        $("input[id='slh_total_hours']").val(response.salaryInfo.slh_total_hours);
        $("input[id='slh_total_working_days']").val(response.salaryInfo.slh_total_working_days);
        $("input[id='slh_saudi_tax']").val(response.salaryInfo.slh_saudi_tax);
        $("input[id='slh_cpf_contribution']").val(response.salaryInfo.slh_cpf_contribution);
        $("input[id='slh_iqama_advance']").val(response.salaryInfo.slh_iqama_advance);
        $("input[id='slh_other_advance']").val(response.salaryInfo.slh_other_advance);
        /* ================ Salary Information ================ */

        /* ====================================================================*/
      }

    });
  }
  /* ================= Find employee Type Id ================= */
</script>

{{-- Employee Salary Currection --}}
@endsection
