@extends('layouts.admin-master')
@section('title') Salary Processing @endsection
@section('content')
<div class="row bread_part">
  <div class="col-sm-12 bread_col">
    <h4 class="pull-left page-title bread_title"> Salary Processing </h4>
    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active"> Salary Processing</li>
    </ol>
  </div>
</div>
<!-- response Message -->
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    @if(Session::has('success'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> Salary Processing Done.
    </div>
    @endif
    @if(Session::has('not_record'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> Monthly Salary Record Not Found!.
    </div>
    @endif
    @if(Session::has('salary_not_found'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> This Employee Salary Not Found!.
    </div>
    @endif
    @if(Session::has('employeNotFound'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> Invalid Employee Id!.
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> please try again!.
    </div>
    @endif
    @if(Session::has('error_date'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> Access Permission Date over
    </div>
    @endif
  </div>
  <div class="col-md-2"></div>
</div>
{{-- Salary Record Processing --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-lg-8">
    <form class="form-horizontal" id="registration" method="post" action="{{ route('salary-record.store') }}">
      @csrf
      <div class="card">
        <div class="card-header"></div>
        <div class="card-body card_form">

          {{-- Month List --}}
          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Salary Month:<span class="req_star">*</span></label>
            <div class="col-sm-4">
              <select class="form-control" name="month">
                @foreach($month as $data)
                <option value="{{ $data->month_id }}" {{ $data->month_id == $currentMonth ? 'selected' : "" }}>{{ $data->month_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Year List --}}
          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Salary Year:<span class="req_star">*</span></label>
            <div class="col-sm-4">
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Processing</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>
{{-- Employee Id base Salary Report --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('single-employee-salary-generat') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading"> Processing Salary for an Employee</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          <div class="form-group custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Employee ID:</label>
            <div>
              <input type="text" class="form-control typeahead" placeholder="Type Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()" value="{{ old('emp_id') }}">
              @if ($errors->has('emp_id'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('emp_id') }}</strong>
              </span>
              @endif
            </div>
            <div id="showEmpId"></div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group{{ $errors->has('month') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month">
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>

              @if ($errors->has('month'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('month') }}</strong>
              </span>
              @endif
            </div>
          </div>

          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Salary Process & Show</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>

{{-- Project & Employee Status Wise Salary Report --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('project-month-empStatus.wise-salary') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading">Projet & Employee Status base Salary Report</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Project List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Project:</label>
            <div>
              <select class="form-control" name="proj_id" required>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Employee Status --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Employee Status:</label>
            <div>
              <select class="form-control" name="emp_status_id" required>
                <option value="0">All</option>
                @foreach($allEmployeeStatus as $status)
                <option value="{{ $status->id }}">{{ $status->title }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month" required>
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Status</label>
            <div>
              <select class="form-control" name="salary_status" required>
                <option value="0">Unpaid</option>
                <option value="1">Paid</option>
              </select>
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Report Show </button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>



{{-- Project & Employee Type (Basic Salary, Hourly,Direct) salary Report --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('project-month-empType.wise-salary') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading">Projet & Employee type base Salary Report</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Project List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Project:</label>
            <div>
              <select class="form-control" name="proj_id" required>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>


          {{-- Employee type List 1 = direct, 2 = indirect --}}

          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;"> Employee Type:</label>
            <div>
              <select class="form-control" name="emp_type_id" required>
                <option value="0">Select Employee Type</option>
                <option value="-1">Direct Employee (Basic Salary)</option>
                @foreach($emp_types as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>



            </div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month" required>
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Status</label>
            <div>
              <select class="form-control" name="salary_status" required>
                <option value="0">Unpaid</option>
                <option value="1">Paid</option>
              </select>
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Report Show </button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>


{{-- Sponser Wise Salary Generate --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('sponser-month.wise-salary-report') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading"> Sponser base Salary Report</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Sponser List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Sponser:</label>
            <div>
              <select class="form-control" name="SponsId" required>
                @foreach($sponser as $spons)
                <option value="{{ $spons->spons_id }}">{{ $spons->spons_name }}</option>
                @endforeach
              </select>
            </div>
          </div>


          {{-- Project List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Project:</label>
            <div>
              <select class="form-control" name="proj_id" required>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month" required>
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Status</label>
            <div>
              <select class="form-control" name="salary_status" required>
                <option value="0">Unpaid</option>
                <option value="1">Paid</option>
              </select>
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Report Show </button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>

{{--Month wise All Employee Salary with PAID/Unpaid--}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('all-emp.salary-without-project-emp-type') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading">All Employees Salary Report</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Month List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month" required>
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>
            </div>
          </div>


          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary Status</label>
            <div>
              <select class="form-control" name="salary_status" required>
                <option value="0">Unpaid</option>
                <option value="1">Paid</option>
              </select>
            </div>
          </div>

        </div>

        {{-- Month List --}}
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Report Show </button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>


{{--Monthly Paid/Unpaid Salary Summary --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('year-month.wise-salary-summary') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading"> Monthly Paid & Unpaid Salary Summary</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Project List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Project:</label>
            <div>
              <select class="form-control" name="proj_id" required>
                <option value="0"> Select Project</option>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group{{ $errors->has('month') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month">
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>

              @if ($errors->has('month'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('month') }}</strong>
              </span>
              @endif
            </div>
          </div>

          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-5) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Process</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>


{{-- Project wise Total Working Hours and Total Salary --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('project-wise-Total.Working.Hours-And-Total.Salary') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading"> Project wise Total Working Hours and Total Salary</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          {{-- Project List --}}
          <div class="form-group custom_form_group">
            <label class="control-label d-block" style="text-align: left;">Salary for Project:</label>
            <div>
              <select class="form-control" name="proj_id" required>
                <option value="0"> Select Project</option>
                @foreach($projects as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          {{-- From Date --}}
          <div class="form-group row custom_form_group">
            <label class="control-label d-block" style="text-align: left;">From Date:</label>
            <div class="col-sm-6">
              <input type="date" name="from_date" value="<?= date("Y-m-d") ?>" class="form-control">
            </div>
          </div>

          {{-- To Date --}}
          <div class="form-group row custom_form_group">
            <label class="control-label d-block" style="text-align: left;">From Date:</label>
            <div class="col-sm-6">
              <input type="date" name="to_date" value="<?= date("Y-m-d") ?>" class="form-control">
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Process</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/path/to/inputTags.jquery.js"></script>

{{--Multiple Employee ID Base Salary Summary --}}

<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    <form class="form-horizontal" id="registration" target="_blank" action="{{ route('multiple-empidbase-salary-process') }}" method="post">
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-12">
              <h3 class="card-title card_top_title salary-generat-heading"> Multiple Emplyees ID Salary</h3>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="card-body card_form" style="padding-top: 0;">

          <div class="form-group custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Employee ID:</label>
            <div>

              <!-- <input type="hidden" class="tagsinput"  value="" data-role="tagsinput" /> -->
              <input type="text" class="form-control typeahead" placeholder="Type Employee ID" name="multiple_emp_Id" id="multiple_empId" value="{{ old('multiple_emp_Id') }}">
              @if ($errors->has('emp_id'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('emp_id') }}</strong>
              </span>
              @endif
            </div>
            <div id="showEmpId"></div>
          </div>

          {{-- Month List --}}
          <div class="form-group custom_form_group{{ $errors->has('month') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
            <div>
              <select class="form-control" name="month">
                @foreach($month as $item)
                <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                @endforeach
              </select>

              @if ($errors->has('month'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('month') }}</strong>
              </span>
              @endif
            </div>
          </div>

          {{-- Year List --}}
          <div class="form-group custom_form_group{{ $errors->has('year') ? ' has-error' : '' }}">
            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
            <div>
              <select class="form-control" name="year">
                @foreach(range(date('Y'), date('Y')-2) as $y)
                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                @endforeach
              </select>

              @if ($errors->has('year'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('year') }}</strong>
              </span>
              @endif
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="submit" class="btn btn-primary waves-effect">Salary Process</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-2"></div>
</div>
{{-- end section --}}


<script>
  $('#tags').inputTags();
</script>
@endsection