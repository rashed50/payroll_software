@extends('layouts.admin-master')
@section('title') Pending Salary list @endsection
@section('content')



<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">Pending Salary List</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Pending Salary List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-7">
        @if(Session::has('success'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> {{Session::get('success')}}
        </div>
        @endif
        @if(Session::has('delete'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> {{Session::get('delete')}}
        </div>
        @endif


        @if(Session::has('error'))
        <div class="alert alert-warning alerterror" role="alert">
            <strong>Opps!</strong> please try again.
        </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Pending Salary List </h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="card-body">

                <form method="post" action="{{ route('salary-sheet.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-md-6">

                            <label class="control-label d-block">No Of Emp:<span class="req_star">*</span></label>
                            <div class="">
                                <input type="text" class="form-control" name="no_of_emp" value="" required>
                            </div>

                            <label class="control-label d-block" style="text-align: left;">Salary Month:</label>
                            <div>
                                <select class="form-control" name="month" required>
                                    @foreach($month as $item)
                                    <option value="{{ $item->month_id }}" {{ $item->month_id == Carbon\Carbon::now()->format('m') ? 'selected' :'' }}>{{ $item->month_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="control-label d-block" style="text-align: left;">Salary Year:</label>
                            <div>
                                <select class="form-control" name="year">
                                    @foreach(range(date('Y'), date('Y')-5) as $y)
                                    <option value="{{$y}}" {{$y}}>{{$y}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="control-label d-block">Salary Date:<span class="req_star">*</span></label>
                            <div class="">
                                <input type="text" class="form-control fromDate" id="datepickerFrom" autocomplete="off" name="salary_date" value="{{date('Y-m-d')}}" required>
                            </div>

                        </div>
                        <div class="col-md-6">
                            {{-- Project List --}}

                            <label class="control-label d-block" style="text-align: left;">Project Name:</label>
                            <div>
                                <select class="form-control" name="proj_od" id="proj_id" required>
                                    <option value="0">ALL</option>
                                    @foreach($projectlist as $proj)
                                    <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="control-label d-block" style="text-align: left;">Sponser Name:</label>
                            <div>
                                <select class="form-control" name="spons_id" id="SponsId" required>
                                    <option value="0">ALL</option>
                                    @foreach($sponserList as $spons)
                                    <option value="{{ $spons->spons_id }}">{{ $spons->spons_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <label class="control-label d-block">Total Salary:<span class="req_star">*</span></label>
                            <div class="">
                                <input type="number" class="form-control" name="total_salary" value="" required>
                            </div>

                            <label class="control-label d-block">Remarks:<span class="req_star">*</span></label>
                            <div class="">
                                <input type="text" class="form-control" name="remarks" value="">
                            </div>


                            <label class="control-label d-block">Image:<span class="req_star">*</span></label>
                            <div class="">
                                <input type="file" class="form-control" name="file_name">
                            </div>



                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">

                                <button type="submit" class="btn btn-dark">SAVE</button>
                            </div>
                        </div>
                    </div>


                </form>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">

                            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                                <thead>
                                    <tr>
                                        <th> Emp No </th>
                                        <th> Project </th>
                                        <th> Sponsor </th>
                                        <th> Month </th>
                                        <th> Year </th>
                                        <th> Date </th>
                                        <th> Total Salary </th>
                                        <th> Remarks </th>
                                        <th>Image</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($allSheet as $item)
                                    <tr>
                                        <td>{{ $item->no_of_emp}}</td>
                                        <td>{{ $item->proj_od }}</td>
                                        <td>{{ $item->spons_id}}</td>
                                        <td>{{ $item->montd}}</td>
                                        <td>{{ $item->year}}</td>
                                        <td>{{ $item->salary_date}}</td>
                                        <td>{{ $item->total_salary}}</td>
                                        <td>{{ $item->remarks}}</td>
                                        <td><img height="50" src="{{asset($item->file_path.$item->file_name) }}" alt=""></td>
                                        <td>
                                            <a href="#" title="view"><i class="fas fa-eye fa-lg view_icon"></i></a>
                                            <a href="#" title="edit"><i class="fa fa-pencil-square fa-lg edit_icon"></i></a>
                                            <a href="{{ route('salary-sheet.delete',$item->ss_auto_id) }}" title="delete" id="delete"><i class="fa fa-trash fa-lg delete_icon"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Datepicker 
    $('document').ready(function() {
        $('#datepickerFrom').datepicker({
            autoclose: true,
            toggleActive: true,
            // startView: "months",  // Ata 1st month select korle date asbe
            // minViewMode: "months",
            // format: "mm/yyyy",
        });
    });
</script>
@endsection