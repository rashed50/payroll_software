@extends('layouts.admin-master')
@section('title') Employee Salary Information @endsection
@section('content')

@section('internal-css')
<style media="screen">
  a.checkButton {
    background: teal;
    color: #fff !important;
    font-size: 13px;
    padding: 5px 10px;
    cursor: pointer;
  }
</style>
@endsection

{{-- Response Massege --}}
<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    @if(Session::has('update_success'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> Correction Salary Information.
    </div>
    @endif
  </div>
  <div class="col-md-2"></div>
</div>
{{-- Response Massege --}}

<div class="row bread_part">
  <div class="col-sm-12 bread_col">
    <h4 class="pull-left page-title bread_title">Employee Salary Search</h4>
    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active">Employee Salary Search</li>
    </ol>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <span id="error_show2" class="d-none" style="color: red"></span>
      </div>
      <div class="card-body card_form">
        <div class="row">
          <div class="col-md-9">
            {{-- checkbox for iqama no wise search employee --}}
            <div class="row">
              <div class="col-md-5"></div>
              <div class="col-md-3">
                <div class="form-check" style="margin-bottom:15px;">
                  <a id="iqamaWiseSearch" class=" d-block checkButton" onclick="iqamaWiseSearch()">Iqama Wise Search?</a>
                  <a id="idWiseSearch" class=" d-none checkButton" onclick="idWiseSearch()">ID Wise Search?</a>
                </div>
              </div>
            </div>
            {{-- Search Employee Id --}}
            <div id="searchEmployeeId" class=" d-block">
              <div class="form-group row custom_form_group ">
                <div class="col-sm-4">
                  <label class="control-label">Employee ID:</label>
                  <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">
                  <div id="showEmpId"></div>
                  <span id="error_show" class="d-none" style="color: red"></span>
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Month:</label>
                  <select class="form-control" name="month">
                    <option value="{{ $currentMonth }}">{{ $currentMonthName }}</option>
                    <option value="{{ $previousMonth }}">{{ $previousMonthName }}</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Year:</label>
                  <select class="form-control" name="year">
                    <option value="{{ $previousYear }}">{{ $previousYear }}</option>
                    <option value="{{ $currentYear }}" selected>{{ $currentYear }}</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="text-align:center">
                  <button type="submit" onclick="searchEmployeeSallaryCurrection()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                </div>
              </div>
            </div>
            {{-- Search Employee IQama No --}}
            <div id="searchIqamaNo" class=" d-none">
              <div class="form-group row custom_form_group ">
                <div class="col-sm-4">
                  <label class="control-label">IQama No:</label>
                  <input type="text" id="iqamaNoSearch" class="form-control typeahead" placeholder="Input IQama No" name="iqamaNo">
                  <span id="error_show" class="d-none" style="color: red"></span>
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Month:</label>
                  <select class="form-control" name="month">
                    <option value="{{ $currentMonth }}">{{ $currentMonthName }}</option>
                    <option value="{{ $previousMonth }}">{{ $previousMonthName }}</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label class="control-label">Year:</label>
                  <select class="form-control" name="year">
                    <option value="{{ $previousYear }}">{{ $previousYear }}</option>
                    <option value="{{ $currentYear }}" selected>{{ $currentYear }}</option>
                  </select>
                </div>

              </div>
              <div class="row">
                <div class="col-md-12" style="text-align:center">
                  <button type="submit" onclick="searchEmployeeSallaryCurrection()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                </div>
              </div>
            </div>


          </div>
          {{-- show image --}}
          <div class="col-md-3">
            <div class="employee_photo_show" id="employee_photo_show">
              <img src="{{ asset('contents/admin') }}/assets/images/avatar-1.jpg" alt="" class="image-resize">
            </div>
          </div>
        </div>



        <!-- show employee information -->
        <div class="col-md-12">
          <div id="showEmployeeDetails" class="d-none">
            <div class="row">
              <!-- employee Deatils -->
              <div class="col-md-5">
                <div class="header_row">
                  <span class="emp_info">Employee Information Details</span>
                </div>
                <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">
                  <tr>
                    <td> <span class="emp">Project Name:</span> <span id="show_employee_project_name" class="emp2"></span>

                    </td>
                  </tr>
                  <tr>
                    <td> <span class="emp">Employee Id:</span> <span id="show_employee_id" class="emp2"></span> </td>
                  </tr>
                  <tr>
                    <td> <span class="emp">Employee Name:</span> <span id="show_employee_name" class="emp2"></span> </td>
                  </tr>

                  <tr>
                    <td> <span class="emp">Iqama No:</span> <span id="show_employee_akama_no" class="emp2"></span> </td>
                  </tr>
                  <tr>
                    <td> <span class="emp">Type:</span> <span id="show_employee_type" class="emp2"></span> </td>
                  </tr>
                  <tr>
                    <td> <span class="emp">Trade:</span> <span id="show_employee_category" class="emp2"></span> </td>
                  </tr>
                  <tr>
                    <td> <span class="emp">Department:</span> <span id="show_employee_department" class="emp2"></span> </td>
                  </tr>
                </table>
              </div>
              {{-- From --}}
              <div class="col-md-7">
                <form action="{{ route('salary.currection-process') }}" method="post">
                  @csrf
                  <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body card_form">
                      <input type="hidden" id="salaryHistoryId" name="salaryHistoryId" value="">
                      {{-- from element --}}
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label">Basic Amount:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="basic_amount" name="basic_amount" value="" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label">Basic Hours:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="basic_hours" name="basic_hours" value="{{old('basic_hours')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> House Rent:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="house_rent" name="house_rent" value="{{old('house_rent')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Hourly Rate:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="hourly_rent" name="hourly_rent" value="{{old('hourly_rent')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Mobile Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="mobile_allowance" name="mobile_allowance" value="{{old('mobile_allowance')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Medical Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="medical_allowance" name="medical_allowance" value="{{old('medical_allowance')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Local Travel Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="local_travel_allowance" name="local_travel_allowance" value="{{old('local_travel_allowance')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Conveyance Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="conveyance_allowance" name="conveyance_allowance" value="{{old('conveyance_allowance')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Food Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="food_allowance" name="food_allowance" value="{{old('food_allowance')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Others Allowance:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="others1" name="others1" value="{{old('others1')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Overtime:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="overtime" name="overtime" value="{{old('overtime')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Overtime Amount:<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="slh_overtime_amount" name="slh_overtime_amount" value="{{old('slh_overtime_amount')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Total Salary :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="gross_salary" name="gross_salary" value="{{old('gross_salary')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Total Hours :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="total_hours" name="total_hours" value="{{old('total_hours')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Work Days :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="total_work_day" name="total_work_day" value="{{old('total_work_day')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Saudi Tax :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="saudi_tax" name="saudi_tax" value="{{old('saudi_tax')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Contribution :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="cpf_contribution" name="cpf_contribution" value="{{old('cpf_contribution')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Iqama Advance :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="iqama_adv_inst_amount" name="iqama_adv_inst_amount" value="{{old('iqama_adv_inst_amount')}}" min="0">
                        </div>
                      </div>
                      <div class="form-group row custom_form_group">
                        <label class="col-sm-4 control-label"> Others Advance :<span class="req_star">*</span></label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="other_adv_inst_amount" name="other_adv_inst_amount" value="{{old('other_adv_inst_amount')}}" min="0">
                        </div>
                      </div>

                      {{-- form hidden elements --}}

                      <div class="form-group row custom_form_group">

                        <div class="col-sm-11">
                          <input type="hidden" class="form-control" id="project_id" name="project_id">
                          <input type="hidden" class="form-control" id="emp_salary_month" name="slh_month">
                          <input type="hidden" class="form-control" id="emp_salary_year" name="slh_year">
                          <input type="hidden" class="form-control" id="work_multi_project" name="work_multi_project">
                          <input type="hidden" class="form-control" id="emp_auto_id" name="emp_auto_id">
                        </div>
                      </div>


                      {{-- from element --}}
                    </div>
                    <div class="card-footer card_footer_button text-center">
                      <button type="submit" class="btn btn-primary waves-effect">CORRECTION</button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer card_footer_button text-center">
        .
      </div>
    </div>
  </div>
</div>
{{-- iqama no wise Search --}}
<script type="text/javascript">
  // iqama Wise
  function iqamaWiseSearch() {
    $('#iqamaWiseSearch').removeClass('d-block').addClass('d-none');
    $('#idWiseSearch').removeClass('d-none').addClass('d-block');
    // input field
    $('#searchEmployeeId').removeClass('d-block').addClass('d-none');
    $('#searchIqamaNo').removeClass('d-none').addClass('d-block');

  }
  // id Wise
  function idWiseSearch() {
    $('#idWiseSearch').removeClass('d-block').addClass('d-none');
    $('#iqamaWiseSearch').addClass('d-block').removeClass('d-none');
    // input field
    $('#searchIqamaNo').removeClass('d-block').addClass('d-none');
    $('#searchEmployeeId').removeClass('d-none').addClass('d-block');
  }
</script>
{{-- Employee Salary Currection --}}
<script type="text/javascript">
  /* ================= search Employee Details ================= */
  function searchEmployeeSallaryCurrection() {
    var emp_id = $("#emp_id_search").val();
    var iqamaNo = $("input[id='iqamaNoSearch']").val();
    var month = $("select[name='month']").val();
    var year = $("select[name='year']").val();
    $.ajax({
      type: 'POST',
      url: "{{ route('search.employee-salary-for.currection') }}",
      data: {
        emp_id: emp_id,
        iqamaNo: iqamaNo,
        month: month,
        year: year
      },
      dataType: 'json',
      success: function(response) {
        /* ==================== Error1 ==================== */
        if (response.status == "error") {
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show']").text('This Id Dosn,t Match!');
          $("span[id='error_show']").addClass('d-block').removeClass('d-none');
          $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
        } else {
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show']").removeClass('d-block').addClass('d-none');
          $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
        }
        /* ==================== Error2 ==================== */
        if (response.status == "error2") {
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show2']").text('This Record Not Found!');
          $("span[id='error_show2']").addClass('d-block').removeClass('d-none');
          $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
        } else {
          $("input[id='emp_id_search']").val('');
          $("span[id='error_show2']").removeClass('d-block').addClass('d-none');
          $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
        }
        /* ==================== Error2 ==================== */
        /* show employee information in employee table */
        $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
        $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
        $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);
        $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
        /* conditionaly show project name */
        if (response.findEmployee.project_id == null) {
          $("span[id='show_employee_project_name']").text("No Assigned Project!");
        } else {

          $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
        }

        /* conditionaly show Department name */
        if (response.findEmployee.department_id == null) {
          $("span[id='show_employee_department']").text("No Assigned Department");
        } else {
          $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
        }
        /* conditionaly show project name */
        /* show Relationaly data */
        if (response.findEmployee.emp_type_id == 1) {
          $("span[id='show_employee_type']").text("Direct Man Power");
        } else {
          $("span[id='show_employee_type']").text("Indirect Man Power");
        }
        /* ================ Salary Information ================ */
        $("input[id='salaryHistoryId']").val(response.salaryInfo.slh_auto_id);
        $("input[id='basic_amount']").val(response.salaryInfo.basic_amount);
        $("input[id='basic_hours']").val(response.salaryInfo.basic_hours);
        $("input[id='house_rent']").val(response.salaryInfo.house_rent);
        $("input[id='hourly_rent']").val(response.salaryInfo.hourly_rent);
        $("input[id='mobile_allowance']").val(response.salaryInfo.mobile_allowance);
        $("input[id='medical_allowance']").val(response.salaryInfo.medical_allowance);
        $("input[id='local_travel_allowance']").val(response.salaryInfo.local_travel_allowance);
        $("input[id='conveyance_allowance']").val(response.salaryInfo.conveyance_allowance);
        $("input[id='food_allowance']").val(response.salaryInfo.food_allowance);
        $("input[id='others1']").val(response.salaryInfo.others);
        $("input[id='overtime']").val(response.salaryInfo.slh_total_overtime);
        $("input[id='slh_overtime_amount']").val(response.salaryInfo.slh_overtime_amount);
        $("input[id='gross_salary']").val(response.salaryInfo.slh_total_salary);
        $("input[id='total_hours']").val(response.salaryInfo.slh_total_hours);
        $("input[id='total_work_day']").val(response.salaryInfo.slh_total_working_days);
        $("input[id='saudi_tax']").val(response.salaryInfo.slh_saudi_tax);
        $("input[id='cpf_contribution']").val(response.salaryInfo.slh_cpf_contribution);
        $("input[id='iqama_adv_inst_amount']").val(response.salaryInfo.slh_iqama_advance);
        $("input[id='other_adv_inst_amount']").val(response.salaryInfo.slh_other_advance);
        // Hidden Field
        $("input[id='project_id']").val(response.salaryInfo.project_id);
        $("input[id='emp_salary_month']").val(response.salaryInfo.slh_month);
        $("input[id='emp_salary_year']").val(response.salaryInfo.slh_year);
        $("input[id='work_multi_project']").val(response.salaryInfo.multProject);
        $("input[id='emp_auto_id']").val(response.salaryInfo.emp_auto_id);



        //  alert(response.salaryInfo.project_id);

        /* ================ Salary Information ================ */

        /* ====================================================================*/
      }

    });
  }
  /* ================= Find employee Type Id ================= */
</script>

{{-- Employee Salary Currection --}}
@endsection