<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Salary Report
    </title>
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        body {
            overflow: hidden;
            border: 1px solid black;
            margin: 50px;
            margin-top: 5px;
            margin-bottom: 5px;

        }


        /* Preloader */

        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #fff;
            /* change if the mask should have another color then white */
            z-index: 99;
            /* makes sure it stays on top */
        }

        #status {
            width: 200px;
            height: 200px;
            position: absolute;
            left: 50%;
            /* centers the loading animation horizontally one the screen */
            top: 50%;
            /* centers the loading animation vertically one the screen */
            background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
            /* path to your loading animation */
            background-repeat: no-repeat;
            background-position: center;
            margin: -100px 0 0 -100px;
            /* is width and height divided by two */

            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        * {
            margin: 0;
            padding: 0;
            outline: 0;
        }


        @media print {
            .container {
                max-width: 98%;
                margin: 0 auto 10px;

            }

            @page {
                size: A4 landscape;
                margin: 15mm 0mm 10mm 0mm;
                /* top, right,bottom last value was= 10, left */

            }

            a[href]:after {
                content: none !important;
            }
        }

        .main__wrap {
            width: 90%;
            margin: 20px auto;
            margin-top: 5px;

        }

        .header__part {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .title__part {
            text-align: center;
            font-size: 25px;
            color: red;
        }

        .sub_title__part {
            text-align: center;
            font-size: 18px;
            color: green;
            border: 1px;
        }


        /* Employee Information Table */

        #employeeinfo {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;

        }

        #employeeinfo td,
        #employeeinfo th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #employeeinfo tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #employeeinfo tr:hover {
            background-color: #ddd;
        }

        #employeeinfo th {
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            background-color: #EAEDED;
            color: black;
        }
    </style>
</head>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Preloader -->
    


    <div class="main__wrap">
        <!-- Report Header part-->
        <section class="header__part">

            <div class="title__part"> </div>
            <!-- title -->
            <div class="title__part">

                <p>{{$company->comp_name_en}} <small>{{$company->comp_name_arb}} </small> </p>
                <address class="address">
                    {{$company->comp_address}}
                </address>
            </div>
            <!-- print button -->
            <div class="print__part">
                <p> <strong>Print:</strong> {{ Carbon\Carbon::now()->format('d/m/Y') }} </p>
                <button type="" onclick="window.print()" class="print__button">Print</button>
            </div>

        </section>
        <section>
            <h1 class="sub_title__part"><u> Employee Salary Summary</u></h1><br>
        </section>
        <!-- Employee Info part -->
        <section>
            <br>
            <table id="employeeinfo">
                <tbody>
                    <tr>
                        <td> Employee ID:</td>
                        <td> {{ $employee->employee_id}}</td>

                        <td> Project:</td>
                        <td> {{$employee->project->proj_name}} </td>
                    </tr>
                    <tr>
                        <td> Employee Name:</td>
                        <td>{{$employee->employee_name}}</td>

                        <td> Sponser:</td>
                        <td> {{$employee->sponser->spons_name}} </td>
                    </tr>
                    <tr>
                        <td> Iqama No:</td>
                        <td> {{$employee->akama_no}}, {{$employee->akama_expire_date}}</td>

                        <td> Trade:</td>
                        <td> {{$employee->employeeType->name}}, {{$employee->category->catg_name}} </td>
                    </tr>
                    <tr>
                        <td> Passport No:</td>
                        <td> {{$employee->passfort_no}}, {{$employee->passfort_expire_date}}</td>

                        <td> Joining:</td>
                        <td> {{$employee->joining_date }}</td>
                    </tr>
                    <tr>
                        <td> Passport:</td>
                        <td> {{$employee->passfort_no}}</td>

                        <td> Emp. Status:</td>
                        <td> {{$employee->job_status == 1 ? "Active": "InActive" }}</td>
                    </tr>

                    <tr>
                        <td>Address:</td>
                        <td colspan="3"> {{$employee->mobile_no}}, {{$employee->details}},{{$employee->address}},{{$employee->country->country_name}}</td>
                    </tr>

                    <tr>
                        <td colspan="3"><br /></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="2" style="font-weight:bold">UNPAID SALARY TOTAL AMOUNT:</td>
                        <td>{{ $totalUnPaidSalary  }} </td>

                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="2" style="font-weight:bold">TOTAL AMOUNT EXP OF IQAMA:</td>
                        <td> {{$iqamaRenewalTotalExpence }}</td>

                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="2" style="font-weight:bold"> TOTAL RECEIVED FROM SALARY:</td>
                        <td> {{$totalIqamaAmountPaid }}</td>

                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="2" style="font-weight:bold"> TOTAL CASH RECEIVED FROM EMPLOYEE:</td>
                        <td> {{$cashReceiveTotalPaidAmount }}</td>

                    </tr>

                    <tr>
                        <td></td>
                        <td colspan="2" style="font-weight:bold"> TOTAL REMAINING AMOUNT:</td>
                        <td> {{ $iqamaRenewalTotalExpence  - ($totalIqamaAmountPaid + $cashReceiveTotalPaidAmount) }} </td>


                </tbody>
            </table>
            <br><br>

        </section>
        <!-- Iqama Renewal Details part -->


        <section>
            {{-- Officer Signature --}}
            <div class="row" style="padding-top: 30px;">
                <div style="display: flex; justify-content:space-between">
                    <p> ---------------------------- </p>
                    <p> ---------------------------- </p>
                    <p> ---------------------------- </p>
                    <p> ---------------------------- </p>
                </div>
                <div style="display: flex; justify-content:space-between">
                    <p>Employee Signature</p>
                    <p>Accountant</p>
                    <p>Verified</p>
                    <p>General Manager</p>
                </div>

            </div>
        </section>
    </div>
</body>

<script>
    $(window).on('load', function() { // makes sure the whole site is loaded 
        $('#status').fadeOut(); // will first fade out the loading animation 
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
        $('body').delay(350).css({
            'overflow': 'visible'
        });
    })
</script>

</html>