<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Salary Report
    </title>
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }


        body {
            overflow: hidden;
            border: 1px solid black;
            margin: 5px;

        }

        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #fff;
            /* change if the mask should have another color then white */
            z-index: 99;
            /* makes sure it stays on top */
        }

        #status {
            width: 200px;
            height: 200px;
            position: absolute;
            left: 50%;
            /* centers the loading animation horizontally one the screen */
            top: 50%;
            /* centers the loading animation vertically one the screen */
            background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
            /* path to your loading animation */
            background-repeat: no-repeat;
            background-position: center;
            margin: -100px 0 0 -100px;
            /* is width and height divided by two */

            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid blue;
            border-right: 16px solid green;
            border-bottom: 16px solid red;
            border-left: 16px solid pink;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        * {
            margin: 0;
            padding: 0;
            outline: 0;
        }


        @media print {
            .container {
                max-width: 98%;
                margin: 0 auto 5px;

            }


            @page {
                size: A4 landscape;
                margin: 5mm 0mm 5mm 0mm;
                /* top, right,bottom last value was= 10, left */

            }

            a[href]:after {
                content: none !important;
            }
        }


        .main__wrap {
            width: 90%;
            margin: 10px auto;
            margin-top: 5px;
        }

        .header__part {
            display: flex;
            justify-content: space-between;
            align-items: right;
            margin-bottom: 5px;
        }

        .title__part {
            align-items: center;
        }

        .print__part {}

        /* table part */
        .table__part {
            display: flex;
        }

        table {
            width: 100%;
            padding: 5px;
        }

        table,
        tr {
            border: 1px solid gray;
            border-collapse: collapse;
        }

        table th {
            font-size: 13px;
        }

        table td {
            text-align: left;
            font-size: 13px;

        }

        th,
        td {
            padding: 5px 2px;
            /* Top,Right,Bottom,left */
        }

        .td__center {
            text-align: center
        }

        .td__bold {
            font-weight: 700;
        }

        .td__emplyoeeId {
            font-size: 14px;
            padding-bottom: 25px;
            color: blue;
            font-weight: 900;
            text-align: center
        }

        .td__sponser {
            color: green;
            font-weight: 300;
            text-align: center;

        }

        .sponser__name {
            font-size: 10px;
            font-weight: 100;

        }

        .country {
            color: red;
            text-align: center;
            font-size: 10px;
        }

        .employe__trade {
            color: red;
            text-align: center;
            font-size: 10px;
        }

        .td__project {
            font-size: 8px;
            padding-bottom: 5px;
            color: red;
            font-weight: 100;
            text-align: center;

        }

        .td__gross_salary {
            font-size: 14px;
            color: navy;
            font-weight: 900;
            text-align: center
        }

        .box__signature {
            width: 150px;
            height: 30px;
            border: 1px solid gray;
            margin: 0px;
            color: lightgray;
        }

        .final_table {
            width: 70%;
            margin-left: auto;
            margin-right: auto;

        }

        .final_table tr {
            border: 0px;
        }

        /* Employee Information Table */

        #employeeinfo {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;

        }

        #employeeinfo td,
        #employeeinfo th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #employeeinfo tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #employeeinfo tr:hover {
            background-color: #ddd;
        }

        #employeeinfo th {
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            background-color: #EAEDED;
            color: black;
        }
    </style>
</head>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>



    <div class="main__wrap">
        <!-- Report Header part-->
        <section class="header__part">

            <div class="date__part">

            </div>
            <!-- title -->
            <div class="title__part">

                <h4>{{$company->comp_name_en}} <small>{{$company->comp_name_arb}} </small> </h4>
                <address class="address" style="text-align:center;">
                    {{$company->comp_address}}
                </address>
            </div>
            <!-- print button -->
            <div class="print__part">
                <p> <strong>Print:</strong> {{ Carbon\Carbon::now()->format('d/m/Y') }} </p>
                <button type="" onclick="window.print()" class="print__button">Print</button>
            </div>

        </section>

        <section>
            <br>
            <h3 style=" text-align:center; font-size:20px; "><b> Employee Salary Summary</b></h3><br>
        </section>
        <!-- Employee Info part -->
        <section class="table__part">
            <table id="employeeinfo">
                <tbody>
                    <tr>
                        <td> Employee ID:</td>
                        <td class="td__bold"> {{ $employee->employee_id}}</td>

                        <td> Emp. Status:</td>
                        <td class="td__bold"> {{$employee->job_status == 1 ? "Active": "InActive" }}</td>
                    </tr>
                    <tr>
                        <td> Employee Name:</td>
                        <td class="td__bold">{{$employee->employee_name}}</td>
                        <td> Sponser:</td>
                        <td> {{$employee->sponser->spons_name}} </td>
                    </tr>
                    <tr>
                        <td> Iqama No:</td>
                        <td> {{$employee->akama_no}}, {{$employee->akama_expire_date}}</td>
                        <td> Trade:</td>
                        <td> {{$employee->employeeType->name}}, {{$employee->category->catg_name}} </td>
                    </tr>
                    <tr>
                        <td> Passport No:</td>
                        <td> {{$employee->passfort_no}}, {{$employee->passfort_expire_date}}</td>
                        <td> Joining:</td>
                        <td> {{$employee->joining_date }}</td>
                    </tr>
                    <tr>
                        <td> Address:</td>
                        <td> {{$employee->mobile_no}}, {{$employee->details}},{{$employee->address}},{{$employee->country->country_name}}
                        </td>
                        <td> Project:</td>
                        <td> {{$employee->project->proj_name}} </td>
                    </tr>


                </tbody>
            </table>
        </section>
        <section>
            <br>
            <p style=" text-align:center; font-size:16px; "> Salary Details</p>
        </section>
        <!-- Salary Details part -->
        <section class="table__part">
            <table id="employeeinfo">
                <thead>
                    <tr>
                        <th>SL No </th>
                        <th> Month </th>
                        <th> BS/Rate </th>
                        <th>Hr/WrD </th>
                        <th> OT/Amt</th>
                        <th> (Food+Other) </th>
                        <th> Total Salary </th>
                        <th> (Adv1+Adv2) </th>
                        <th> Contri<br>ID Renew</th>
                        <th> Gross Salary </th>
                        <th> Project </th>
                        <th> Status </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $totalHours = 0;
                    $totalOverTimeHours = 0;
                    $totalOverTimeAmount = 0;
                    $totalFoodAllowance = 0;
                    $totalSaudiTax = 0;
                    $totalContribution = 0;
                    $totalOtherAdvance = 0;
                    $totalIqamaRenewal = 0;
                    $totalSalaryAmount = 0;

                    @endphp

                    @foreach($salary as $salary)

                    @php
                    $totalHours += $salary->slh_total_hours;
                    $totalOverTimeHours += $salary->slh_total_overtime;
                    $totalOverTimeAmount += $salary->slh_overtime_amount;
                    $totalFoodAllowance += $salary->food_allowance;
                    $totalIqamaRenewal += $salary->slh_iqama_advance;
                    $totalSaudiTax += $salary->slh_saudi_tax;
                    $totalContribution += $salary->slh_cpf_contribution;
                    $totalOtherAdvance += $salary->slh_other_advance;
                    $totalSalaryAmount += $salary->slh_total_salary;

                    @endphp

                    <tr>
                        <td class="td__center">{{ $loop->iteration }}</td>

                        <td class="td__gross_salary"> {{ date('F', mktime(0, 0, 0, $salary->slh_month, 10)); }}, {{ $salary->slh_year }} </td>
                        <td class="td__center"> {{ $salary->basic_amount }}/{{ $salary->hourly_rent }}</td>
                        <td class="td__sponser"> {{ $salary->slh_total_hours }}/{{ $salary->slh_total_working_days }} </td>
                        <td class="td__center"> {{ $salary->slh_total_overtime }}/{{ $salary->slh_overtime_amount }}</td>

                        <td class="td__center"> {{round($salary->food_allowance) }} + {{ $salary->otherAmount }} </td>
                        <td class="td__gross_salary"> {{round( $salary->slh_total_salary + $salary->slh_cpf_contribution + $salary->slh_iqama_advance + $salary->slh_saudi_tax + $salary->slh_other_advance) }} </td>
                        <td class="td__center"> {{ round($salary->slh_saudi_tax) }} + {{round($salary->slh_other_advance) }}</td>

                        <td class="td__center">{{ round($salary->slh_cpf_contribution)}}/{{ round($salary->slh_iqama_advance) }} </td>
                        <td class="td__gross_salary"><u> {{ round($salary->slh_total_salary) }}</u> </td>
                        <td class="td__project"> {{ Str::limit($salary->project->proj_name,40) }}<br></td>
                        <td class="td__gross_salary">
                            <div class="box__signature"> {{ $salary->Status == 0 ? 'Unpaid':'Paid'}} </div>
                        </td>

                    </tr>
                    <p style="page-break-after: always;"></p>
                    @endforeach

                    <tr>
                        <td class="td__center"></td>
                        <td class="td__gross_salary">Total </td>
                        <td class="td__center"> </td>
                        <td class="td__gross_salary">{{ $totalHours }} </td>
                        <td class="td__center"></td>
                        <td class="td__center"> </td>
                        <td class="td__center"> </td>
                        <td class="td__gross_salary">{{$totalSaudiTax}}</td>
                        <td class="td__gross_salary">{{ $totalContribution }}/{{ $totalIqamaRenewal }}</td>
                        <td class="td__gross_salary"> {{ $totalSalaryAmount }} </td>
                        <td class="td__center"> </td>
                        <td class="td__center"> </td>
                    </tr>

                </tbody>
            </table>
        </section>

        <!-- Iqama Renewal Details part -->
        <section>
            <br>
            <p style=" text-align:center; font-size:16px; "> ID Renewal Cost Details</p>
        </section>
        <section>
            <table id="employeeinfo">
                <thead>
                    <tr>
                        <th class="td__center">S.N</th>
                        <th class="td__center">Date</th>
                        <th class="td__center">Iqama Duration</th>
                        <th class="td__center">Jawat Fee</th>
                        <th class="td__center">Amal Fee</th>
                        <th class="td__center">BD Amount</th>
                        <th class="td__center">Medical Insurance</th>
                        <th class="td__center">Jawat Fee Penalty</th>
                        <th class="td__center">Others1</th>
                        <th class="td__center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $counter = 1;
                    $totalIqamaExpence = 0;
                    @endphp

                    @foreach($iqamaDetails as $item)
                    @php
                    $totalIqamaExpence += $item->Cost1 + $item->Cost2 +$item->Cost3 +$item->Cost4 + $item->Cost5 + $item->Cost6 + $item->jawazat_penalty;
                    @endphp
                    <tr>
                        <td class="td__center">{{$counter++}}</td>
                        <td class="td__center"> {{ $item->renewal_date }} </td>
                        <td class="td__center"> {{ $item->duration }} Month </td>
                        <td class="td__center"> {{ $item->Cost1 }} </td>
                        <td class="td__center"> {{ $item->Cost2 }} </td>
                        <td class="td__center"> {{ $item->Cost3 }} </td>
                        <td class="td__center"> {{ $item->Cost4 }} </td>
                        <td class="td__center"> {{ $item->jawazat_penalty }} </td>
                        <td class="td__center"> {{ $item->Cost5 }} </td>
                        <td class="td__center"> {{ $item->Cost1 + $item->Cost2 +$item->Cost3 +$item->Cost4 +$item->Cost5 + $item->jawazat_penalty }} </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </section>

        <section>
            <br>

            <table id="employeeinfo">
                <tbody>
                    <tr>
                        <td class="td__gross_salary">TOTAL AMOUNT EXPENSE OF IQAMA:</td>
                        <td class="td__gross_salary">{{ $iqamaRenewalTotalExpence  }} </td>
                        <td class="td__gross_salary">UNPAID SALARY TOTAL AMOUNT:</td>
                        <td class="td__gross_salary">{{ $totalUnPaidSalary  }} </td>
                    </tr>
                    <tr>

                        <td class="td__gross_salary">TOTAL RECEIVED FROM SALARY: </td>
                        <td class="td__gross_salary">{{ $totalIqamaRenewal }} </td>
                        <td class="td__gross_salary">TOTAL REMAINING AMOUNT:</td>
                        <td class="td__gross_salary"> {{ $totalIqamaExpence  - ($totalIqamaRenewal+$cashReceiveTotalPaidAmount) }} </td>

                    </tr>

                    <tr>
                        <td class="td__gross_salary"> TOTAL CASH RECEIVED FROM EMPLOYEE:</td>
                        <td class="td__gross_salary"> {{$cashReceiveTotalPaidAmount }}</td>
                        <td colspan="2"></td>
                    </tr>


                </tbody>
            </table>

        </section>




        <section>
            {{-- Officer Signature --}}
            <div class="row" style="padding-top: 40px;">
                <div class="officer-signature" style="display: flex; justify-content:space-between">
                    <p>Accountant</p>
                    <p>Verified</p>
                    <p>General Manager</p>
                </div>
            </div>
            {{-- Officer Signature --}}
        </section>
    </div>
</body>

<script>
    $(window).on('load', function() { // makes sure the whole site is loaded 
        $('#status').fadeOut(); // will first fade out the loading animation 
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
        $('body').delay(350).css({
            'overflow': 'visible'
        });
    })
</script>

</html>