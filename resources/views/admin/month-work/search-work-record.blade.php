@extends('layouts.admin-master')
@section('title') Work Record @endsection
@section('content')

<style>
    body {
        font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
</style>
<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title"> Employee Work Records Search</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Employee Work Record</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if(Session::has('success'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> Added information.
        </div>
        @endif
        @if(Session::has('delete_multi_proj'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> {{ Session::get('delete_multi_proj')}}
        </div>
        @endif
        @if(Session::has('success_soft'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> delete information.
        </div>
        @endif

        @if(Session::has('success_update'))
        <div class="alert alert-success alertsuccess" role="alert">
            <strong>Successfully!</strong> update information.
        </div>
        @endif

        @if(Session::has('data_not_found'))
        <div class="alert alert-warning alerterror" role="alert">
            <strong>Opps!</strong> invalid Employee Id.
        </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <form class="form-horizontal project-details-form" method="POST" action="">

            <div class="card">
                <div class="card-body card_form">

                    <!-- Employee ID -->
                    <div class="form-group row custom_form_group">
                        <label class="control-label col-md-3">Employee ID:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control typeahead" placeholder="Input Employee ID" id="emp_id" value="{{ old('emp_id') }}">
                            <span class="error d-none" id="error_massage"></span>
                        </div>
                    </div>
                    <!-- Month Dropdown Menu -->
                    <div class="form-group row custom_form_group">
                        <label class="control-label col-3">Month</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="month" id="month">
                                @foreach($months as $month)
                                <option value="{{$month->month_id}}" {{$month->month_id == $currentMonth ? 'selected' : "" }}>{{$month->month_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!-- Year Dropdown Menu -->
                    <div class="form-group row custom_form_group">
                        <label class="control-label col-3">Year</label>
                        <div class="col-md-7">
                            <select class="form-control" name="year" id="year">
                                @foreach(range(date('Y'), date('Y')-1) as $y)
                                <option value="{{$y}}" {{$y}}>{{$y}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>


                </div>
                <div class="card-footer card_footer_button text-center">
                    <button type="button" class="btn btn-primary waves-effect" onclick="employeeMultipleWordRecordSearchBtn()">Search</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-1"></div>
</div>

<!-- Table list -->


<div class="row">
    <div class="col-12">
        <div class="table-responsive">

            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Emp. Id</th>
                        <th>Name</th>
                        <th>Akama No</th>
                        <th>Project</th>
                        <th>Sponer</th>
                        <th>Hours</th>
                        <th>Overtime</th>
                        <th>Days</th>
                        <th>Month</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody id="empMultiProjectWorkRecords"></tbody>
            </table>


            <!-- <div class="modal fade" id="practice_modal">
                <div class="modal-dialog">
                    <form id="companydata">
                        <div class="modal-content">
                            <input type="hidden" id="color_id" name="color_id" value="">
                            <div class="modal-body">
                                <input type="text" name="name" id="name" value="" class="form-control">

                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-3 control-label">Total Hour:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " name="totalHourTime" value="" required>
                                    </div>
                                </div>
                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-3 control-label">Overtime:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " name="totalOverTime" value="" required>
                                    </div>
                                </div>
                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-3 control-label">No of Day:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " name="total_day" value="" readonly>
                                    </div>
                                </div>
                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-3 control-label">Work From:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control datepicker" id="datepicker" name="startDate" value="" required>
                                    </div>
                                </div>
                                <div class="form-group row custom_form_group">
                                    <label class="col-sm-3 control-label">Work End:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control datepicker" id="workDatepicker" name="endDate" value="" required>
                                    </div>
                                </div>

                            </div>
                            <input type="submit" value="Submit" id="submit" class="btn btn-sm btn-outline-danger py-0" style="font-size: 0.8em;">
                        </div>
                    </form>
                </div>
            </div> -->



        </div>
    </div>
</div>









<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Emp Multi Project Work Record <span class="text-danger" id="errorData"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST">
                <div class="modal-body">
                    
                    <div class="form-group row custom_form_group">
                        <label class="control-label col-sm-3">Project Name:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="proj_name" id="project_id">
                            <option value="">Select Project</option>
                        </select>
                        <span class="error d-none" id="error_massage"></span>
                        </div>
                    </div>

                    <div class="form-group row custom_form_group">
                        <label class="col-sm-3 control-label">Total Hour:</label>
                        <div class="col-sm-7">
                            <input type="text" id="total_hour" class="form-control " name="totalHourTime" value="" required>
                            <input type="hidden" id="empwh_auto_id"  name="empwh_auto_id" value="">
                            <input type="hidden" id="emp_auto_id" name="emp_id" value="">
                        </div>
                    </div>
    
                    <div class="form-group row custom_form_group">
                        <label class="col-sm-3 control-label">Overtime:</label>
                        <div class="col-sm-7">
                            <input type="text" id="total_overtime" class="form-control " name="totalOverTime" value="" required>
                        </div>
                    </div>
                    <div class="form-group row custom_form_group">
                        <label class="col-sm-3 control-label">No of Day:</label>
                        <div class="col-sm-7">
                            <input type="text" id="total_day" class="form-control " name="total_day" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group row custom_form_group">
                        <label class="col-sm-3 control-label">Work From:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control datepicker start_date" id="datepicker" name="startDate" value="" required>
                        </div>
                    </div>
                    <div class="form-group row custom_form_group">
                        <label class="col-sm-3 control-label">Work End:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control datepicker endDate" id="workDatepicker" name="endDate" value="" required>
                        </div>
                    </div>
    
    
                </div>
                <div class="modal-footer">
                    <button type="button" id="submit" class="btn btn-sm btn-outline-danger" onclick="employeeMultipleWorkRecrdTimeUpdate()">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>








<!-- script area -->
<script type="text/javascript">
    /* form validation */
    $(document).ready(function() {
        $("#employeeMultipleWordRecordSearchBtn").validate({
            /* form tag off  */
            submitHandler: function(form) {
                return false;
            },
            /* form tag off  */
            rules: {
                emp_id: {
                    required: true,
                },

                month: {
                    required: true,
                },
                year: {
                    required: true,
                },

            },

            messages: {
                emp_id: {
                    required: "You Must Be Input This Field!",
                },
                month: {
                    required: "Please Select Month Name",
                },
                year: {
                    required: "Please Select Year",
                },

            },


        });
    });

    function employeeMultipleWordRecordSearchBtn() {
        var empId = $('#emp_id').val();
        var month = $('#month').val();
        var year = $('#year').val();


        if (empId != '' && empId != null) {

            $('#empMultiProjectWorkRecords').html('');

            $.ajax({
                type: "POST",
                url: "{{ route('employee.month.work.record.search') }}",
                data: {
                    emp_id: empId,
                    month: month,
                    year: year
                },
                dataType: "json",

                success: function(response) {
                    // alert(response.empMulProjWorkRecord[0].total_day);
                    var rows = "";
                    var counter = 0;
                    if (response.error != null) {
                        alert('Emolyee Work Record Not Found');
                        return;
                    }

                    $.each(response.empMulProjWorkRecord, function(key, value) {
                        counter++;
                        // rows +=
                        //         '<tr>'+
                        //             '<td>'+counter+'</td>'+
                        //             '<td>'+value.emp_id+'</td>'+
                        //             '<td>'+value.emp_id+'</td>'+
                        //             '<td>'+value.emp_id+'</td>'+
                        //             '<td>'+value.emp_id+'</td>'+
                        //             '<td>'+value.total_hour+'</td>'+
                        //             '<td>'+value.total_overtime+'</td>'+
                        //             '<td>'+value.total_day+'</td>'+
                        //             '<td>'+value.month+'</td>'+
                        //         '</tr>';

                        rows += `
                                <tr>
                                    <td>${counter}</td>
                                    <td>${value.employee_id}</td>
                                    <td>${value.employee_name}</td>
                                    <td>${value.akama_no}</td>
                                    <td>${value.proj_name}</td>
                                    <td>${value.spons_name}</td>
                                    <td>${value.total_hour}</td>
                                    <td>${value.total_overtime }</td>
                                    <td>${value.total_day}</td>
                                    <td>${value.month}</td>
                                    <td>
                                    <a href="" id="editRecord" data-toggle="modal" data-target="#exampleModalCenter" data-id="${value.empwh_auto_id}">Edit</a>
                                    <a href="#" onClick="deleteMultiProjectBtnClick(${value.empwh_auto_id})" vallue="${value.empwh_auto_id}" title="Delete"><i class="fa fa-trash fa-lg delete_icon"></i></a>
                                   

                                    
                                    </td>
                                </tr>
                               `


                    });
                    $('#empMultiProjectWorkRecords').html(rows);

                }

            });
        } else {
            alert('Please input date');
        }
    }

    function deleteMultiProjectBtnClick(id) {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })

            .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        type: 'GET',
                        url: "{{  url('admin/delete/employee/multiple/in/out') }}/" + id,
                        dataType: 'json',
                        success: function(response) {
                            employeeMultipleWordRecordSearchBtn();
                        }
                    });
                }
            });
    }

    $(document).on("click", "#editRecord", function(){
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            url: "{{ route('edit-multiproject-work-record') }}",
                data: {id: id},
            datatype:"json",
            success: function(response){

                if(response.workRecord != null){

                    $('#emp_auto_id').val(response.workRecord.employee.employee_id);
                    $('#month').val(response.workRecord.month);
                    $('#year').val(response.workRecord.year);
                    // $('#project_id').val(response.workRecord.project_id);
                    $('#total_day').val(response.workRecord.total_day);
                    $('#total_hour').val(response.workRecord.total_hour);
                    $('#total_overtime').val(response.workRecord.total_overtime);
                    $('.start_date').val(response.workRecord.start_date);
                    $('.endDate').val(response.workRecord.end_date);
                    $('#paid_amount').val(response.workRecord.paid_amount);
                    $('#empwh_auto_id').val(response.workRecord.empwh_auto_id);

                    if(response.project !=''){

                        $('select[id="project_id"]').empty();
                        $('select[id="project_id"]').append('<option>Please Select Project</option>');
                        $.each(response.project, function(key, value) {
                        $('select[id="project_id"]').append('<option value="' + value.proj_id + '">' + value.proj_name + '</option>');
                        });

                        $('#project_id').val(response.workRecord.project_id); 
                    }
                }else{
                    $('#errorData').text(response.error);
                }
                    // alert(response);
            }
        })

    });


</script>

@endsection


@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- <script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('click', '#submit', function(event) {
            event.preventDefault()
            var id = $("#color_id").val();
            var name = $("#name").val();

            $.ajax({
                url: 'color/' + id,
                type: "POST",
                data: {
                    id: id,
                    name: name,
                },
                dataType: 'json',
                success: function(data) {

                    $('#companydata').trigger("reset");
                    $('#practice_modal').modal('hide');
                    window.location.reload(true);
                }
            });
        });


        $('body').on('click', '#editCompany', function(event) {

            // event.preventDefault();
            var id = $(this).data('id');
            alert('id');

            // $.get('multiproject-work-record/' + id + '/edit', function (data) {
            //     // $('#userCrudModal').html("Edit category");
            //     // $('#submit').val("Edit category");
            //      $('#practice_modal').modal('show');
            $('#totalHourTime').val(100);
            //     // $('#name').val(data.data.name);
            //  })
        });

    });
</script> -->
@endpush