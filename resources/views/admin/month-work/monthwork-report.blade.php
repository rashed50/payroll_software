<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Monthly Wrok Report</title>
    <link rel="stylesheet" href="{{ asset('contents/admin') }}/assets/css/salary-bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('contents/admin') }}/assets/css/salary-style.css">
    <link rel="stylesheet" href="{{ asset('contents/admin') }}/assets/css/print.css" media="print">
</head>
<body>
  <section class="salary">
      <div class="container">
          <!-- salary header -->
          <div class="row align-center">
              <div class="col-md-6">
                  <div class="salary__header">

                  </div>
              </div>
              <div class="col-md-6"></div>
          </div>
          <!-- salary bottom header -->
          <div class="salary__header-bottom">
              <div class="row">
                <div class="col-md-12">
                  <div class="company_information" style="text-align:center">
                    <h4>{{$company->comp_name_en}}  <small>{{$company->comp_name_arb}} </small> </h4>
                    <address class="address">
                         {{$company->comp_address}}
                    </address>
                  </div>
                </div>

                {{-- second row --}}
                <div class="col-md-12">
                  <div class="row">

                    <div class="col-md-8">
                      <div class="project_info" style="margin-left:0">
                          <p class="project_name">  <strong>Salary Month :</strong> {{ $projectWiseReport[0]->month_id }} / {{ $projectWiseReport[0]->year_id }} </p>
                          <p class="project_name">  <strong>Project Name : </strong>  {{ $projectName }}  </p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="salary__download" style="text-align:right; display:flex-; justify-content: right; align-items:center; margin-bottom: 10px">
                        <p> <strong>Print Date</strong> {{ Carbon\Carbon::now()->format('d/m/Y') }} </p>
                        <a onclick="window.print()" class="print-button">PDF Or Pirnt</a>
                        {{-- {{ route('salary-report-pdf',$month) }} --}}
                      </div>





                </div>




              </div>
          </div>
          <!-- salary table -->


          <div class="salary__table-wrap">
              <div class="row">
                  <div class="col-md-12">
                      <table class="table table-responsive salary__table">
                          <thead>
                            <tr>
                              <!-- first-head-row -->
                              <tr class="first-head-row">
                                  <th> <span>S.N</span> </th>
                                  <th> <span>Emp. ID</span> </th>
                                  <th> <span>Employee Name</span> </th>
                                  <th> <span>Iqama No</span> </th>
                                  <th> <span>OT Hours</span> </th>
                                  <th> <span>Total Hours</span> </th>
                                  <th> <span>Total Day</span> </th>

                                  <th> <span> Emp. Type </span> </th>
                                  <th> <span> Trade</span> </th>
                                  <th> <span>Project Name</span> </th>
                                  <th> <span></span> </th>
                                  {{-- <th> <span></span> </th> --}}
                                  <th></th>
                                  <th><span>Signature</span></th>
                                  <th></th>
                                  <th></th>
                              </tr>
                            </tr>
                          </thead>
                          <tbody>

                            @foreach($projectWiseReport as $aEmployee)
                            <tr class="salary-row-parent">
                              <!-- first tr -->
                              <tr class="first-row">
                                  <td> <span>{{ $loop->iteration }}</span> </td>
                                  <td> <span>{{ $aEmployee->employee_id }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->employee_name }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->akama_no }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->overtime }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->total_hours }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->total_work_day }}</span> </td>
                                  <td colspan="1"> <span>{{ Str::limit($aEmployee->name,7) }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->catg_name }}</span> </td>
                                  <td colspan="1"> <span>{{ $aEmployee->proj_name }}</span> </td>
                                  <td></td>
                                  <td></td>
                                  {{-- <td></td> --}}
                                  <td colspan="4" class="signature_field"> <span class="signature">
                                    <span style="padding: 3px 5px">

                                    </span>
                                    <br>
                                  </span> </td>
                              </tr>
                            </tr>
                            {{-- @php  $counter++;   @endphp

                            @if(  $counter % 10 == 0)

                            @endif --}}
                              {{-- <p style="color: red">Data Not Found!</p> --}}
                            @endforeach

                          </tbody>
                      </table>
                      {{-- Officer Signature --}}
                      <div class="row">
                        <div class="officer-signature">
                          <p>Operation Manager</p>
                          <p>Accountant</p>
                          <p>General Manager</p>
                        </div>
                      </div>
                      {{-- Officer Signature --}}
                  </div>
              </div>
          </div>
      </div>
  </section>
</body>
</html>
