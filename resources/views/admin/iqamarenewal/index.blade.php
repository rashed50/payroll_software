@extends('layouts.admin-master')
@section('title') Add Iqama Renewal Information @endsection
@section('content')

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title"> Iqama Renewal Information</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Iqama Renewal Information</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-7">
        @if(Session::has('success'))
          <div class="alert alert-success alertsuccess" role="alert" style="margin-left: -20px">
             <strong>Successfully!</strong> Added New Iqama Cost for Employee.
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
             <strong>Opps!</strong> please try again.
          </div>
        @endif

        @if(Session::has('success_update'))
          <div class="alert alert-success alertsuccess" role="alert" style="margin-left: -20px">
             <strong>Successfully!</strong> Update Iqama Cost for Employee.
          </div>
        @endif

        @if(Session::has('already_exits'))
          <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
             <strong>Opps!</strong> This Document Already Exits!.
          </div>
        @endif

        @if(Session::has('IDNotFound'))
          <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
             <strong>Opps!</strong> This Employee Not Found!.
          </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" id="registration" method="post" action="{{ route('insert-iqamarenewal-fee') }}">
          @csrf
          <div class="card">
              <div class="card-header">
                  <div class="row">
                      <div class="col-md-8">
                          <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Employee Iqama Renewal Cost </h3>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
              <div class="card-body card_form">
                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">Employee ID:<span class="req_star">*</span></label>
                    <div class="col-sm-7">

                      <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">
                      <div id="showEmpId"></div>

                      @if ($errors->has('emp_id'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('emp_id') }}</strong>
                          </span>
                      @endif
                    </div>
                </div>

                @foreach ($title as $key=>$aTitle)
                <div class="form-group row custom_form_group{{ $errors->has('cost'.$loop->iteration) ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">{{ $aTitle->fee_title }}:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                      <input type="text" placeholder="Input Amount" class="form-control" id="cost{{ $loop->iteration }}" name="cost{{ $loop->iteration }}" value="{{old('cost'.$loop->iteration)}}" required>
                      @if ($errors->has('cost'.$loop->iteration))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('cost'.$loop->iteration) }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                @endforeach

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">Jawazat Fee Penalty:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                         <input type="text" placeholder="" class="form-control" id="jawazat_penalty" name="jawazat_penalty" value="0" required>
                    </div>
                </div>

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label"> Renewal Duration:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                        <select class="form-control" name="duration" id="duration">
                            <option value="3"> 3 Months</option>
                            <option value="6"> 6 Months</option>
                            <option value="12">12 Months</option>
                            <option value="24">24 Months</option>
                            <option value="36">36 Months</option>
                            <option value="48">48 Months</option>
                        </select>
                     </div>
                     
                </div>
                <div class="form-group row custom_form_group">
                    <label class="col-sm-3 control-label">Renewal Date:</label>
                    <div class="col-sm-7">
                      <input type="date" name="renewal_date" class="form-control" max="{{ Carbon\Carbon::now()->format('Y-m-d') }}" value="<?= date("Y-m-d") ?>">
                    </div>
                </div>

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">Remarks:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                         <input type="text" placeholder="Remarks Type Here" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>

                

              </div>
              <div class="card-footer card_footer_button text-center">
                  <button type="submit" id="onSubmit" class="btn btn-primary waves-effect">SAVE</button>
              </div>
          </div>
        </form>
    </div>
</div>
{{-- Iqama Cost List --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="card-title card_top_title"><i class="fab fa-gg-circle mr-2"></i>Iqama Renewal Cost List</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                                <thead>
                                    <tr>
                                        <th>Emp. ID</th>
                                        <th>Employee</th>
                                        @foreach ($title as $aTitle)
                                        <th>{{ $aTitle->fee_title }}</th>
                                        @endforeach
                                        <th>Jaw. Penalty</th>
                                        <th>Duration</th>
                                        <th>Date</th>
                                        <th>Remarks</th>
                                        <th>Total</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach($iqamaCost as $aIqamaCost)
                                    <tr>
                                        <td>{{ $aIqamaCost->employee->employee_id }}</td>
                                        <td>
                                          <p> <strong>Name : </strong> {{ $aIqamaCost->employee->employee_name }} </p>
                                          <p> <strong>Iqama : </strong> {{ $aIqamaCost->employee->akama_no }} </p>
                                        </td>
                                        <td>{{ $aIqamaCost->Cost1 }}</td>
                                        <td>{{ $aIqamaCost->Cost2 }}</td>
                                        <td>{{ $aIqamaCost->Cost3 }}</td>
                                        <td>{{ $aIqamaCost->Cost4 }}</td>
                                        <td>{{ $aIqamaCost->Cost5 }}</td>
                                        <td>{{ $aIqamaCost->jawazat_penalty }}</td>
                                        <td>{{ $aIqamaCost->duration }} Months</td>
                                        <td>{{ $aIqamaCost->renewal_date }}</td>
                                        <td>{{ $aIqamaCost->Cost1+$aIqamaCost->Cost2+$aIqamaCost->Cost3+$aIqamaCost->Cost4 + $aIqamaCost->Cost5 + $aIqamaCost->jawazat_penalty}}</td>
                                        <td>{{ $aIqamaCost->remarks }}</td>
                                        <td>
                                            <a href="{{ route('edit-iqamarenewal-fee',$aIqamaCost->IqamaRenewId ) }}" title="edit"><i class="fa fa-pencil-square fa-lg edit_icon"></i></a>
                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
