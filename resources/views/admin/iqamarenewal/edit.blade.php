@extends('layouts.admin-master')
@section('title') Add Iqama Renewal Information @endsection
@section('content')

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title"> Edit Iqama Renewal Information</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Edit Iqama Renewal Information</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-7">
        

        @if(Session::has('IDNotFound'))
          <div class="alert alert-warning alerterror" role="alert" style="margin-left: -20px">
             <strong>Opps!</strong> This Employee Not Found!.
          </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" id="registration" method="post" action="{{ route('update-iqamarenewal-fee') }}">
          @csrf
          <div class="card">
              <div class="card-header">
                  <div class="row">
                      <div class="col-md-8">
                          <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Employee Iqama Renewal Cost </h3>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
              <div class="card-body card_form">
                <input type="hidden" name="id" value="{{ $data->IqamaRenewId }}">

                @foreach ($title as $key=>$aTitle)
                <div class="form-group row custom_form_group{{ $errors->has('cost'.$loop->iteration) ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">{{ $aTitle->fee_title }}:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                      <input type="text" placeholder="Input Amount" class="form-control" id="cost{{ $loop->iteration }}" name="cost{{ $loop->iteration }}" 
                      value="@if($loop->iteration == 1) {{$data->Cost1}} @elseif($loop->iteration == 2) {{$data->Cost2}}
                      @elseif($loop->iteration == 3) {{$data->Cost3}} @elseif($loop->iteration == 4) {{$data->Cost4}} @elseif($loop->iteration == 5) {{$data->Cost5}} @endif" required>
                      @if ($errors->has('cost'.$loop->iteration))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('cost'.$loop->iteration) }}</strong>
                          </span>
                      @endif
                    </div>
                </div>
                @endforeach

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">Jawazat Fee Penalty:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                         <input type="text" placeholder="" class="form-control" id="jawazat_penalty" name="jawazat_penalty"  required value="{{$data->jawazat_penalty}}">
                    </div>
                </div>

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label"> Renewal Duration:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                         
                        <select class="form-control" name="duration" id="duration">
                            <option value="{{ $data->duration }}" {{ $data->duration == $data->duration ? 'selected' : '' }} >{{ $data->duration }} Months</option>
                            <option value="3"> 3 Months</option>
                            <option value="6"> 6 Months</option>
                            <option value="12">12 Months</option>
                            <option value="24">24 Months</option>
                            <option value="36">36 Months</option>
                            <option value="48">48 Months</option>
                        </select>
                     </div>
                     
                </div>
                <div class="form-group row custom_form_group">
                    <label class="col-sm-3 control-label">Renewal Date:</label>
                    <div class="col-sm-7">
                         <input type="date" name="renewal_date" class="form-control" max="{{ Carbon\Carbon::now()->format('Y-m-d') }}" value="{{ $data->renewal_date }}">
                    </div>
                </div>

                <div class="form-group row custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                    <label class="col-sm-3 control-label">Remarks:<span class="req_star">*</span></label>
                    <div class="col-sm-7">
                         <input type="text" placeholder="Remarks Type Here" class="form-control" id="remarks" name="remarks" value="{{ $data->remarks}}">
                    </div>
                </div>

              </div>
              <div class="card-footer card_footer_button text-center">
                  <button type="submit" id="onSubmit" class="btn btn-primary waves-effect">UPDATE</button>
              </div>
          </div>
        </form>
    </div>
</div>
@endsection
