@extends('layouts.admin-master')
@section('title') Employee Information Search @endsection
@section('content')

@section('internal-css')
<style media="screen">
    a.checkButton {
        background: teal;
        color: #fff !important;
        font-size: 13px;
        padding: 5px 10px;
        cursor: pointer;
    }
</style>
@endsection

<div class="row bread_part">
    <div class="col-sm-12 bread_col">
        <h4 class="pull-left page-title bread_title">Cash Payment By Employee</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li class="active">Cash Payment By Employee</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        @if(Session::has('success'))
        <div class="alert alert-success alertsuccess" role="alert" style="margin-left: -20px">
            <strong> {{ Session::get('success') }}</strong>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="alert alert-warning alerterror" role="alert">
            <strong> {{ Session::get('error') }} </strong>
        </div>
        @endif
    </div>
    <div class="col-md-2"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body card_form">
                <div class="row">
                    <div class="col-md-9">
                        {{-- checkbox for iqama no wise search employee --}}
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-3">
                                <div class="form-check" style="margin-bottom:15px;">
                                    <a id="iqamaWiseSearch" class="d-block checkButton" onclick="iqamaWiseSearch()">Iqama Wise Search?</a>
                                    <a id="idWiseSearch" class=" d-none checkButton" onclick="idWiseSearch()">ID Wise Search?</a>
                                </div>
                            </div>
                        </div>
                        {{-- Search Employee Id --}}
                        <div id="searchEmployeeId" class=" d-block">
                            <div class="form-group row custom_form_group ">
                                <label class="col-sm-5 control-label">Employee ID:</label>
                                <div class="col-sm-4">

                                    <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id_search" onkeyup="empSearch()" onfocus="showResult()" onblur="hideResult()">

                                    <div id="showEmpId"></div>
                                    <span id="error_show" class="d-none" style="color: red"></span>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" onclick="searchEmployeeAdvAdjust()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                        {{-- Search Employee IQama No --}}
                        <div id="searchIqamaNo" class=" d-none">
                            <div class="form-group row custom_form_group ">
                                <label class="col-sm-5 control-label">IQama No:</label>
                                <div class="col-sm-4">
                                    <input type="text" id="iqamaNoSearch" class="form-control typeahead" placeholder="Input IQama No" name="iqamaNo">
                                    <span id="error_show" class="d-none" style="color: red"></span>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" onclick="searchEmployeeAdvAdjust()" style="margin-top: 2px" class="btn btn-primary waves-effect">SEARCH</button>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>
                    </div>
                    {{-- show image --}}
                    <div class="col-md-3">
                        <div class="employee_photo_show" id="employee_photo_show">
                            <img src="{{ asset('contents/admin') }}/assets/images/avatar-1.jpg" alt="" class="image-resize">
                        </div>
                    </div>
                </div>
                <!-- show employee information -->
                <div class="col-md-12">
                    <div id="showEmployeeDetails" class="d-none">
                        <div class="row">
                            <!-- employee Deatils -->
                            <div class="col-md-6">
                                <div class="header_row">
                                    <span class="emp_info">Employee Information</span>
                                </div>
                                <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">
                                    <tr>
                                        <td> <span class="emp">Project Name:</span> <span id="show_employee_project_name" class="emp2"></span> </td>
                                    </tr>
                                    <tr>
                                        <td> <span class="emp">Employee Id:</span> <span id="show_employee_id" class="emp2"></span> </td>
                                    </tr>
                                    <tr>
                                        <td> <span class="emp">Employee Name:</span> <span id="show_employee_name" class="emp2"></span> </td>
                                    </tr>

                                    <tr>
                                        <td> <span class="emp">Iqama No:</span> <span id="show_employee_akama_no" class="emp2"></span> </td>
                                    </tr>

                                    <tr>
                                        <td> <span class="emp">Passport No:</span> <span id="show_employee_passport_no" class="emp2"></span> </td>
                                    </tr>


                                    <tr>
                                        <td> <span class="emp">Employee Type:</span> <span id="show_employee_type" class="emp2"></span> </td>
                                    </tr>
                                    <tr>
                                        <td> <span class="emp">Employee Designation:</span> <span id="show_employee_category" class="emp2"></span> </td>
                                    </tr>
                                    <tr>
                                        <td> <span class="emp">Department:</span> <span id="show_employee_department" class="emp2"></span> </td>
                                    </tr>

                                    <tr>
                                        <td> <span class="emp">Parmanent Address:</span> <span id="show_employee_address_Ds"></span>, <span id="show_employee_address_D"></span> , <span id="show_employee_address_C" class="emp2"></span> </td>
                                    </tr>

                                    <tr>
                                        <td> <span class="emp">Mobile Number:</span> <span id="show_employee_mobile_no" class="emp2"></span> </td>
                                    </tr>

                                    <tr>
                                        <td> <span class="emp">Joining Date:</span> <span id="show_employee_joining_date" class="emp2"></span> </td>
                                    </tr>

                                </table>
                            </div>
                            <!-- Salary Deatils -->
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="header_row">
                                            <span class="emp_info">Employee Salary Details</span>
                                        </div>
                                        <table class="table table-bordered table-striped table-hover custom_view_table show_employee_details_table" id="showEmployeeDetailsTable">
                                            <tr>
                                                <td> <span class="emp">Basic Amount:</span> <span id="show_employee_basic" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">House Rate:</span> <span id="show_employee_house_rent" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Hourly Rate:</span> <span id="show_employee_hourly_rent" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Mobile Allowance:</span> <span id="show_employee_mobile_allow" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Medical Allowance:</span> <span id="show_employee_medical_allow" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Local Travels Allowance:</span> <span id="show_employee_local_travel_allow" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Conveyance Allowance:</span> <span id="show_employee_conveyance_allow" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Increment No:</span> <span id="show_employee_increment_no" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Increment Amount:</span> <span id="show_employee_increment_amount" class="emp2"></span> </td>
                                            </tr>
                                            <tr>
                                                <td> <span class="emp">Others:</span> <span id="show_employee_others" class="emp2"></span> </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cash payment Form -->
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form class="form-horizontal" id="cash-payment-byemp" action="{{ route('employee-advance-payment-cash-receive') }}" method="post">
                            @csrf
                            <div class="card">

                                <div class="card-body card_form" style="padding-top: 0;">

                                    <div class="form-group custom_form_group{{ $errors->has('emp_id') ? ' has-error' : '' }}">
                                        <label class="control-label d-block" style="text-align: left;">Employee ID:</label>
                                        <div>
                                            <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" id="emp_id">
                                            @if ($errors->has('emp_id'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('emp_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div id="showEmpId"></div>
                                    </div>

                                    <div class="form-group custom_form_group{{ $errors->has('pay_amount') ? ' has-error' : '' }}">
                                        <label class="control-label d-block" style="text-align: left;">Payment Amount:<span class="req_star">*</span></label>
                                        <div>
                                            <input type="text" class="form-control" placeholder="Input Amount" name="pay_amount" value="0" required>
                                            @if ($errors->has('pay_amount'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('pay_amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>




                                    <div class="form-group custom_form_group">
                                        <label class="control-label">Payment Date</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" name="payment_date" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                        </div>
                                    </div>


                                    <div class="form-group custom_form_group">
                                        <label class="control-label d-block" style="text-align: left;">Remarks:</label>
                                        <div>
                                            <input type="text" class="form-control" placeholder="Type Here... " name="payment_remarks">
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer card_footer_button text-center">
                                    <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                                </div>


                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>

                <!-- Cash Payemnt List  -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Cash Payment Employee List</h3>
                                    </div>
                                    <div class="col-md-4">

                                        <a href="{{ route('employee.cash.receive.allrecords.with.cash-payment') }}" style="background-color: #35424A;border: none;color: white;  padding: 10px 22px;  text-align: center;  text-decoration: none;  display: inline-block;font-size: 16px;" id="allrecords"> All Records </a>

                                    </div>

                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>EmpID</th>
                                                        <th>Name</th>
                                                        <th>Iqama No</th>
                                                        <th>Project</th>
                                                        <th>Date</th>
                                                        <th>Amount</th>
                                                        <th>Month</th>
                                                        <th>Remarks</th>
                                                        <th>Manage</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($cashPaymentlist as $item)
                                                    <tr>
                                                        <td>{{ $item->employee_id }}</td>
                                                        <td>{{ $item->employee_name }}</td>
                                                        <td>{{ $item->akama_no }}</td>
                                                        <td>{{ $item->proj_name }}</td>
                                                        <td>{{ $item->date == NULL ? '--' : $item->date }}</td>
                                                        <td>{{ $item->adv_amount }}</td>
                                                        <td>{{ $item->month }}</td>
                                                        <td>{{ $item->adv_remarks }}</td>
                                                        <td>
                                                            <a href="{{ route('delete-employee-advance-payment-cash-receive',[$item->id]) }}" title="delete" id="delete"><i class="fa fa-trash fa-lg delete_icon"></i></a>
                                                            <!-- <a href="#" onClick="employee/advance/payment/receive-delete('{{ $item->id }}')" title="edit"><i id="" class="fa fa-trash fa-lg delete_icon"></i></a> -->

                                                        </td>

                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer card_footer_button text-center">

            </div>
        </div>
    </div>
</div>
{{-- iqama no wise Search --}}
<script type="text/javascript">
    // iqama Wise
    function iqamaWiseSearch() {
        $('#iqamaWiseSearch').removeClass('d-block').addClass('d-none');
        $('#idWiseSearch').removeClass('d-none').addClass('d-block');
        // input field
        $('#searchEmployeeId').removeClass('d-block').addClass('d-none');
        $('#searchIqamaNo').removeClass('d-none').addClass('d-block');

    }
    // id Wise
    function idWiseSearch() {
        $('#idWiseSearch').removeClass('d-block').addClass('d-none');
        $('#iqamaWiseSearch').addClass('d-block').removeClass('d-none');
        // input field
        $('#searchIqamaNo').removeClass('d-block').addClass('d-none');
        $('#searchEmployeeId').removeClass('d-none').addClass('d-block');
    }
</script>


<!-- ========= Cash Payment By EMployee =========== -->
<script type="text/javascript">
    function searchEmployeeAdvAdjust() {
        var emp_id = $("#emp_id_search").val();
        var iqamaNo = $("input[id='iqamaNoSearch']").val();
        $.ajax({
            type: 'POST',
            url: "{{ route('search.employee-adjustment') }}",
            data: {
                emp_id: emp_id,
                iqamaNo: iqamaNo
            },
            dataType: 'json',
            success: function(response) {

                if (response.status == "error") {
                    $("input[id='emp_id_search']").val('');
                    $("span[id='error_show']").text('This Id Dosn,t Match!');
                    $("span[id='error_show']").addClass('d-block').removeClass('d-none');
                    $("#showEmployeeDetails").addClass("d-none").removeClass("d-block");
                } else {
                    $("input[id='emp_id_search']").val('');

                    $("span[id='error_show']").removeClass('d-block').addClass('d-none');
                    $("#showEmployeeDetails").removeClass("d-none").addClass("d-block");
                }

                // $("#employee_photo_show").()
                /* show employee information in employee table */
                $("span[id='show_employee_id']").text(response.findEmployee.employee_id);
                // aler(response.findEmployee.employee_id);
                $("input[id='emp_id']").val(response.findEmployee.employee_id);

                $("span[id='show_employee_name']").text(response.findEmployee.employee_name);
                $("span[id='show_employee_akama_no']").text(response.findEmployee.akama_no);


                $("span[id='show_employee_passport_no']").text(response.findEmployee.passfort_no);

                /* conditionaly show project name */
                if (response.findEmployee.project_id == null) {
                    $("span[id='show_employee_project_name']").text("No Assigned Project!");
                } else {
                    $("span[id='show_employee_project_name']").text(response.findEmployee.project.proj_name);
                }

                /* Direct And Indirect Status */
                if (response.findEmployee.emp_type_id == 2) {
                    $("#work_hours_field").addClass('d-none').removeClass('d-block');
                } else {
                    $("#work_hours_field").addClass('d-block').removeClass('d-none');
                }
                /* Direct And Indirect Status */


                if (response.findEmployee.department_id == null) {
                    $("span[id='show_employee_department']").text("No Assigned Department");
                } else {
                    $("span[id='show_employee_department']").text(response.findEmployee.department.dep_name);
                }
                /* conditionaly show project name */
                /* show Relationaly data */
                if (response.findEmployee.emp_type_id == 1) {
                    $("span[id='show_employee_type']").text("Direct Man Power");
                } else {
                    $("span[id='show_employee_type']").text("Indirect Man Power");
                }
                $("span[id='show_employee_category']").text(response.findEmployee.category.catg_name);
                $("span[id='show_employee_address_C']").text(response.findEmployee.country.country_name);
                $("span[id='show_employee_address_D']").text(response.findEmployee.division.division_name);
                $("span[id='show_employee_address_Ds']").text(response.findEmployee.district.district_name);

                $("span[id='show_employee_mobile_no']").text(response.findEmployee.mobile_no);
                $("span[id='show_employee_joining_date']").text(response.findEmployee.joining_date);


                // show employee Salary 
                $("span[id='show_employee_basic']").text(response.salary.basic_amount);
                $("span[id='show_employee_house_rent']").text(response.salary.house_rent);
                $("span[id='show_employee_hourly_rent']").text(response.salary.hourly_rent);
                $("span[id='show_employee_mobile_allow']").text(response.salary.mobile_allowance);
                $("span[id='show_employee_medical_allow']").text(response.salary.medical_allowance);
                $("span[id='show_employee_local_travel_allow']").text(response.salary.local_travel_allowance);
                $("span[id='show_employee_conveyance_allow']").text(response.salary.conveyance_allowance);
                $("span[id='show_employee_increment_no']").text(response.salary.increment_no);
                $("span[id='show_employee_increment_amount']").text(response.salary.increment_amount);
                $("span[id='show_employee_others']").text(response.salary.others1 + response.salary.food_allowance);

                $("span[id='show_employee_address_details']").text(response.findEmployee.details);
                $("span[id='show_employee_present_address']").text(response.findEmployee.present_address);
                /* show salary details in input form */
                $('input[id="emp_auto_id"]').val(response.findEmployee.emp_auto_id);
                $('input[id="input_basic_amount"]').val(response.salary.basic_amount);
                $('input[id="input_hourly_rate"]').val(response.salary.hourly_rent);
                $('input[id="input_house_rate"]').val(response.salary.house_rent);
                $('input[id="input_mobile_allowance"]').val(response.salary.mobile_allowance);
                $('input[id="input_medical_allowance"]').val(response.salary.medical_allowance);
                $('input[id="input_local_travel_allowance"]').val(response.salary.local_travel_allowance);
                $('input[id="input_conveyance_allowance"]').val(response.salary.conveyance_allowance);
                $('input[id="input_others1"]').val(response.salary.others1);
                /*hidden field*/
                $('input[id="input_emp_id_in_desig"]').val(response.findEmployee.emp_auto_id);
                $('input[id="hidden_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
                $('input[id="hidden_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
                $('input[id="hidden_input_emp_email_in_user"]').val(response.findEmployee.email);
                // user module
                $('input[id="show_input_emp_name_in_user"]').val(response.findEmployee.employee_name);
                $('input[id="show_input_emp_category_in_user"]').val(response.findEmployee.category.catg_name);
                $('input[id="show_input_emp_mobile_in_user"]').val(response.findEmployee.mobile_no);
                // user module

                $('input[id="input_emp_desig_id_in_desig"]').val(response.findEmployee.designation_id);



                //Sallary info

                // $('input[id="salarySummary"]').val(response.salarySummary);
                // $('input[id="totalSalary"]').val(response.totalSalary);
                // $('input[id="totalHours"]').val(response.totalHours);
                // $('input[id="totalContribution"]').val(response.totalContribution);
                // $('input[id="totalIqama"]').val(response.totalIqama);
                // $('input[id="totalAdvance"]').val(response.totalAdvance + response.totalSaudiTax);
                // $('input[id="totalAmount"]').val(response.totalAmount);
                // $('input[id="totalSaudiTax"]').val(response.totalSaudiTax);
                // $('input[id="remainingAmount"]').val(response.totalAmount - response.totalAdvance);
                // $('input[id="anualFee"]').val(response.anualFee);




                // employee job experience list 

                // var job_experience = "";
                // $.each(response.find_job_experience, function(key, value) {
                //   var start_date = value.starting_date;
                //   var end_date = value.end_date;
                //   var st_date = new Date(start_date);
                //   var en_date = new Date(end_date);
                //   var total = (en_date - st_date);
                //   var days = total / 1000 / 60 / 60 / 24;


                //   job_experience += `
                //               <tr>
                //                 <td>${value.company_name}</td>
                //                 <td>${value.ejex_title}</td>
                //                 <td>${days}</td>
                //                 <td>${value.designation}</td>
                //                 <td>${value.responsibity}</td>
                //               </tr>

                //               `
                // });

                // $("#show_employee_job_experience_list").html(job_experience);
                // // employee contact person list 
                // var contact_person = "";
                // $.each(response.find_emp_contact_person, function(key, value) {

                //   contact_person += `
                //               <tr>
                //                 <td>${value.ecp_name}</td>
                //                 <td>${value.ecp_mobile1}</td>
                //                 <td>${value.ecp_relationship}</td>
                //               </tr>

                //               `
                // });
                // $("#show_employee_contact_person_list").html(contact_person);
                // // employee designation list 
                // var designation = "";
                // $.each(response.designation, function(key, value) {

                //   designation += `
                //               <option value="${value.catg_id}">${value.catg_name}</option>

                //               `
                // });
                // $("select[id='designation_id']").html(designation);



            }

        });
    }
</script>
<!-- ========= Cash Payment by Employee =========== -->

@endsection