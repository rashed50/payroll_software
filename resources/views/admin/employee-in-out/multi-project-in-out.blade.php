@extends('layouts.admin-master')
@section('title') Employee Entry & Out @endsection
@section('content')

<style>
  body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}


</style>
<div class="row bread_part">
  <div class="col-sm-12 bread_col">
    <h4 class="pull-left page-title bread_title">Employee Multi Project Work Entry</h4>
    <ol class="breadcrumb pull-right">
      <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      <li class="active">Employee Attendence</li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-md-2"></div>
  <div class="col-md-8">
    @if(Session::has('success'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> Added information.
    </div>
    @endif
    @if(Session::has('delete_multi_proj'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> {{ Session::get('delete_multi_proj')}}
    </div>
    @endif
    @if(Session::has('success_soft'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> delete information.
    </div>
    @endif

    @if(Session::has('success_update'))
    <div class="alert alert-success alertsuccess" role="alert">
      <strong>Successfully!</strong> update information.
    </div>
    @endif

    @if(Session::has('data_not_found'))
    <div class="alert alert-warning alerterror" role="alert">
      <strong>Opps!</strong> invalid Employee Id.
    </div>
    @endif
  </div>
  <div class="col-md-2"></div>
</div>


<div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-10">
    <form class="form-horizontal project-details-form" method="POST" action="">
      @csrf
      <div class="card">
             
        <div class="card-body card_form">

          <div class="form-group row custom_form_group">
            
            <label class="control-label col-md-3">Employee ID:</label>
            <div class="col-md-7">
              <input type="text" class="form-control typeahead" placeholder="Input Employee ID" name="emp_id" value="{{ old('emp_id') }}">
              <span class="error d-none" id="error_massage"></span>
            </div>
          </div>
          <div class="form-group row custom_form_group">
            <label class="control-label col-md-3">Project Name:</label>
            <div class="col-md-7">
              <select class="form-control" name="proj_name">
                <option value="">Select Project</option>
                @foreach($project as $proj)
                <option value="{{ $proj->proj_id }}">{{ $proj->proj_name }}</option>
                @endforeach
              </select>
              <span class="error d-none" id="error_massage"></span>
            </div>
          </div>

          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Total Work Houre:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control " name="totalHourTime" value="" required>
            </div>
          </div>
          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Total Overtime:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control " name="totalOverTime" value="0" required>
            </div>
          </div>
          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Work From:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control datepicker" id="datepicker" name="startDate" value="{{ Carbon\Carbon::now()->format('m/d/Y') }}" required>
            </div>
          </div>
          <div class="form-group row custom_form_group">
            <label class="col-sm-3 control-label">Work End:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control datepicker" id="workDatepicker" name="endDate" value="{{ Carbon\Carbon::now()->format('m/d/Y') }}" required>
            </div>
          </div>

        </div>
        <div class="card-footer card_footer_button text-center">
          <button type="button" class="btn btn-primary waves-effect" onclick="employeeMultipleEntryTime()">SAVE</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-1"></div>
</div>


<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-8">
            <h3 class="card-title card_top_title"><i class="fab fa-gg-circle"></i> Month Work History List</h3>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="table-responsive">
              <table id="alltableinfo" class="table table-bordered custom_table mb-0">
                <thead>
                  <tr>
                    <th>Emp. Id</th>
                    <th>Name</th>
                    <th>Project</th>
                    <th>Emp. Type</th>
                    <th>Hours</th>
                    <th>Overtime</th>
                    <th>Days</th>
                    <th>Month</th>
                    <th>Manage</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($empMulProj as $mulProject)
                  <tr>
                    <td>{{ $mulProject->employee->employee_id}}</td>
                    <td>{{ $mulProject->employee->employee_name}}</td>
                    <td>{{ $mulProject->projectName->proj_name}}</td>
                    <td>{{ $mulProject->employee->employeeType->name }}</td>
                    <td>{{ $mulProject->total_hour == NULL ? '--' : $mulProject->total_hour }}</td>
                    <td>{{ $mulProject->total_overtime == NULL ? '--' : $mulProject->total_overtime }}</td>
                    <td>{{ $mulProject->total_day }}</td>
                    <td>{{ $mulProject->month }}</td>
                    <td>
                      <a href="{{ route('edit.employee.multiple.project.in-out',$mulProject->empwh_auto_id) }}" title="edit"><i class="fa fa-pencil-square fa-lg edit_icon"></i></a>
                      <a href="#" onClick="deleteMultiProjectBtnClick('{{ $mulProject->empwh_auto_id}}')" vallue="{{$mulProject->empwh_auto_id}}" title="Delete"><i class="fa fa-trash fa-lg delete_icon"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<!-- script area -->
<script type="text/javascript">
  /* form validation */
  $(document).ready(function() {

    $("#employeeMultipleEntryTime").validate({
      /* form tag off  */
      submitHandler: function(form) {
        return false;
      },
      /* form tag off  */
      rules: {
        emp_id: {
          required: true,
        },
        proj_name: {
          required: true,
        },
        startDate: {
          required: true,
        },
        endDate: {
          required: true,
        },
        totalHourTime: {
          required: true,
          number: true,
        },
        totalOverTime: {
          number: true,
        },

      },

      messages: {
        emp_id: {
          required: "You Must Be Input This Field!",
        },
        proj_name: {
          required: "You Must Be Input This Field!",
        },
        startDate: {
          required: "You Must Be Select This Field!",
        },
        endDate: {
          required: "You Must Be Select This Field!",
        },
        totalHourTime: {
          required: "Please Input This Field!",
          number: "You Must Be Input Number!",
        },
        totalOverTime: {
          number: "You Must Be Input Number!",
        },
      },


    });
  });




  function deleteMultiProjectBtnClick(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this imaginary file!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })

    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
          type: 'GET',
            url: "{{  url('admin/delete/employee/multiple/in/out') }}/" + id,
            dataType: 'json',
            success: function(response)  {
              window.location.reload();
            } 
        });

        
      }
    });
     
  }
</script>



<!-- employee out time -->



@endsection