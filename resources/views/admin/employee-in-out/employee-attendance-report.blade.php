<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Employee Attendence for :{{ $monthName }},{{$year}}
    {{  $projectName->proj_name,  $sponserName->spons_name ?? 'All Sponse' }}
    </title>
    <!-- style -->
    <style>
        * {
            margin: 0;
            padding: 0;
            outline: 0;
        }


        @media print {
            .container {
                max-width: 98%;
                margin: 0 auto 20px;

            }



            @page {
                size: A4 landscape;
                margin: 15mm 0mm 10mm 0mm;
                /* top, right,bottom, left */

            }
            td.td__friday{
            background-color:#AED6F1;
            -webkit-print-color-adjust: exact; 
           }
           th.td__friday{
            background-color:#AED6F1;
            -webkit-print-color-adjust: exact; 
           }
        }


        .main__wrap {
            width: 90%;
            margin: 20px auto;
        }

        .header__part {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .title__part {
            text-align: center;
        }

        /* table part */
        .table__part {
            display: flex;
        }

        table {
            width: 100%;
            padding: 10px;
        }

        table,
        tr {
            border: 1px solid #333;
            border-collapse: collapse;
        }

        /* table tr {} */

        table th {
            font-size: 11px;
            border: 1px solid #333;
        }

        table td {
            text-align: left;
            font-size: 10px;
            border: 1px solid #333;

        }

        th,
        td {
            padding: 5px 2px;
            /* Top,Right,Bottom,left */
        }

        .td__left {

            text-align: left
        }

        .td__center {
            text-align: center
        }
  

        .td__friday{
            background-color:#AED6F1;
        }
        .td__employee_id{
            font-size:12px;
            color:red;
            text-align: center;

        }
        .td__emplyoee_info {
            font-size:12px;
            padding-bottom:5px;
            color: blue;
            font-weight: 300;
            text-align: left
        }

        a:link {
  color: green;
  background-color: transparent;
  text-decoration: none;
} 


       
        
         
        
    </style>
    <!-- style -->
</head>

<body>
    <div class="main__wrap">
        <!-- header part-->
        <section class="header__part">
            <!-- date -->
            <div class="date__part">
                <p> Salary Month : <strong class="td__red__color" > {{ $monthName }}, {{$year}} </strong> </p>
                <p> <strong>project & Sponser Name :</strong> {{$projectName->proj_name, $sponserName->spons_name ?? 'All Sponse'}}</p>
            </div>
            <!-- title -->
            <div class="title__part">
                <h4>{{$company->comp_name_en}} <small>{{$company->comp_name_arb}} </small> </h4>
                <address class="address">
                    {{$company->comp_address}}
                </address>
            </div>
            <!-- print button -->
            <div class="print__part">
                <p> <strong>Print Date</strong> {{ Carbon\Carbon::now()->format('d/m/Y') }} </p>
                <button type="" onclick="window.print()" class="print__button">Print</button>
            </div>
        </section>
        <!-- table part -->
        <section class="table__part">
            <table>
             

                <!-- table heading -->
                <thead>
                          <tr>
                            <th class="td__employee_id">ID</th>
                            <th class="td__emplyoee_info">Name, <br>Iqama & Trade </th>
                            <!-- Date  -->
                            @for($i=1; $i<=$numberOfDaysInThisMonth; $i++)
                            @if($holidayArray[$i] == 0)
                              <th>{{ $i }}</th>
                            @else 
                            <th class="td__friday">{{ $i }}</th>
                            @endif

                            @endfor
                            <!-- end date -->
                            <th>O.T</th>
                            <th>Total</th>
                          </tr>
                        </thead>

                <tbody>
 
 
                    @foreach($directEmp as $emp) <!-- per employee -->
                          
                        <tr style="border-bottom:0;">

                        <td class = "td__employee_id"> {{$emp->employee_id}}</td>
                        <td class="td__emplyoee_info"> <span>{{ $emp->employee_name }} <br> {{ $emp->akama_no }}, {{ $emp->category->catg_name }} </td>
                        
                            @php
                            $allAttenDays = $emp->atten;
                            $perDayHours = $emp->perDayHours;
                            
                            $totalWorkingHour = $emp->totalWorkingHour;
                            $totalWorkingDays = $emp->totalWorkingDays;
                            @endphp


                          @for($counter =1;$counter<=$numberOfDaysInThisMonth+1;$counter++)
                            
                            @if($holidayArray[$counter] == 0)
                                <td >
                                   <span>@if($counter <=$numberOfDaysInThisMonth)
                                   {{$totalWorkingDays[$counter]}}
                                   @else
                                   {{"="}}
                                   @endif
                                  </span>
                                 <span>{{ $allAttenDays[$counter]}}</span>
                                 </td>
                            @else 
                            <td class="td__friday"></td>
                             @endif
                                 
                          
                            @endfor
                              <td>{{$totalWorkingHour}}</td>
                          </tr>
                          @endforeach
                          <br>
                          <tr>
                             <td>=</td>
                            <td>T.H</td>
                            @for($day = 1; $day<=$numberOfDaysInThisMonth+1; $day++)
                                <td>{{ $totalHoursList[$day] }}</td>
                            @endfor
                          </tr>
                        </tbody>

                    <p style="page-break-after: always;"></p>
                </tbody>
            </table>
        </section>
        <!-- ---------- -->

        </section>
            {{-- Officer Signature --}}
                <div class="row" style="padding-top: 50px;">
                    <div class="officer-signature" style="display: flex; justify-content:space-between">
                        <p>Accountant</p>
                        <p>Verified</p>
                        <p>General Manager</p>
                    </div>
                </div>
            {{-- Officer Signature --}}
        <section>
    </div>
</body>

</html>