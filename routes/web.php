<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Fontend\FontendController;
use Illuminate\Support\Facades\Artisan;

use App\Http\Controllers\Admin\EmpCategoryController;
use App\Http\Controllers\Admin\EmployeeInfoController;
//use App\Http\Controllers\Admin\DesignationController;
use App\Http\Controllers\Admin\DeductionController;
use App\Http\Controllers\Admin\SalaryDetailsController;
use App\Http\Controllers\Admin\DistrictController;
use App\Http\Controllers\Admin\DivisionController;
use App\Http\Controllers\Admin\CountryController;

use App\Http\Controllers\Admin\ProjectInfoController;
use App\Http\Controllers\Admin\BannerInfoController;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\DailyCostController;
use App\Http\Controllers\Admin\CostTypeController;
use App\Http\Controllers\Admin\DailyWorkHistoryController;
use App\Http\Controllers\Admin\SubCompanyInfoController;
use App\Http\Controllers\Admin\MonthlyWorkHistoryController;
use App\Http\Controllers\Admin\AdvancePayController;
//use App\Http\Controllers\Admin\AdvancePayHistoryController;
use App\Http\Controllers\Admin\EmpContactPersonController;
use App\Http\Controllers\Admin\EmpJobExperienceController;
use App\Http\Controllers\Admin\SallaryGenerateController;
use App\Http\Controllers\Admin\IncomeSourceController;
use App\Http\Controllers\Admin\OfficeBuildingController;
use App\Http\Controllers\Admin\SponsorController;
use App\Http\Controllers\Admin\ExcelExportController;



use App\Http\Controllers\Admin\RequirementTools\ItemCategoryController;
use App\Http\Controllers\Admin\RequirementTools\ItemSubCategoryController;
use App\Http\Controllers\Admin\RequirementTools\OrderComponentController;
use App\Http\Controllers\Admin\Promosion\PromosionController;
use App\Http\Controllers\Admin\EmpLeave\EmpLeaveController;
use App\Http\Controllers\Admin\Report\ExpenditureReportController;
use App\Http\Controllers\Admin\Report\IqamaAdvacnePayController;
use App\Http\Controllers\Admin\Bill_Voucher\BillVoucherController;
use App\Http\Controllers\Admin\Report\IncomeReportController;
use App\Http\Controllers\Admin\Report\SalaryReportController;
use App\Http\Controllers\Admin\Report\ItemPurchaseController;
use App\Http\Controllers\Admin\ProjectImage\ProjectImgUploadController;
use App\Http\Controllers\Admin\Expire\IqamaExpireController;
use App\Http\Controllers\Admin\Expire\PassportExpireController;
use App\Http\Controllers\Admin\InOut\EmployeeInOutController;
use App\Http\Controllers\Admin\Vehicle\VehicleController;
use App\Http\Controllers\Admin\LogBook\LogBookController;
use App\Http\Controllers\Admin\CPF\ContributionController;
use App\Http\Controllers\Admin\AnualFee\AnualFeeDetailsController;
//use App\Http\Controllers\Admin\QrCodeController;
use App\Http\Controllers\Admin\Authentication\RoleController;
use App\Http\Controllers\Admin\Authentication\UserController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* cache clear route */

Route::get('/clear-cache', function () {
    $run = Artisan::call('config:clear');
    $run = Artisan::call('cache:clear');
    $run = Artisan::call('view:clear');
    $run = Artisan::call('config:cache');
    //return \Artisan::call('db:seed');

    return 'FINISHED';
});


Route::get('/', [FontendController::class, 'index']);
Route::get('/project/{proj_id}/details', [FontendController::class, 'projectDetails'])->name('project-details');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/my-home', [App\Http\Controllers\Admin\InOut\EmployeeInOutController::class, 'checkmyhome']);


Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'auth']], function () {
    /* ============================== Qr Code ============================== */

    Route::get('create-ebill-voucher', [BillVoucherController::class, 'BillVoucherFormLoad'])->name('bill-voucher.create');

    Route::get('sub-company/e-voucher/create', [BillVoucherController::class, 'eVoucher'])->name('e.voucher.add');
    Route::get('get/e-voucher/add-to-cart', [BillVoucherController::class, 'getCartInfo'])->name('get.evoucher-add.to.cart.data');
    Route::post('e-voucher/remove-to-cart', [BillVoucherController::class, 'removeCartInfo'])->name('evoucher-remove.to.cart');
    Route::post('e-voucher/process/add-to-cart', [BillVoucherController::class, 'addToCart'])->name('evoucher-item-add.tocart');
    Route::post('sub-company/e-voucher/process', [BillVoucherController::class, 'eVoucherProccess'])->name('e.voucher.process');
    Route::get('submited/bill/voucher', [BillVoucherController::class, 'submitedBillVoucherUi'])->name('submited-bill-voucher-ui');
    Route::get('submited/bill/voucher-regenerate/{id}', [BillVoucherController::class, 'showQRCodeBillVourcher'])->name('submited-bill-voucher-regenerate');



    //Route::get('generate-qrcode', [QrCodeController::class, 'index'])->name('qr-code');
    Route::get('dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    /* ========== ajax user route ========== */
    /* ======================== User Role Permision ======================== */
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    /* ======================== User Role Permision ======================== */

    /* ========== Company Profile ========== */
    Route::get('company/profile', [CompanyProfileController::class, 'profile'])->name('company-profiles');
    /* ========== Banner Information ========== */
    Route::get('banner/information', [BannerInfoController::class, 'index'])->name('banner-info');
    Route::get('banner/information/add', [BannerInfoController::class, 'add'])->name('add-banner-info');
    Route::get('banner/information/edit/{ban_id}', [BannerInfoController::class, 'edit'])->name('edit-banner-info');
    Route::get('banner/information/delete/{ban_id}', [BannerInfoController::class, 'delete'])->name('delete-banner-info');
    Route::post('banner/information/insert', [BannerInfoController::class, 'insert'])->name('insert-banner-info');
    Route::post('banner/information/update', [BannerInfoController::class, 'update'])->name('update-banner-info');
    Route::post('company/profile/update', [CompanyProfileController::class, 'updateProfile'])->name('update-profile');
    /* ========== Iqama Anual Fee ========== */
    Route::get('update/iqama-anual/fee', [AnualFeeDetailsController::class, 'index'])->name('anual-fee.details');
    Route::get('edit/iqama-anual/{IqamaRenewId}/fee', [AnualFeeDetailsController::class, 'edit'])->name('edit-iqamarenewal-fee');
    Route::post('insert/iqama-anual/fee/for-employee', [AnualFeeDetailsController::class, 'insert'])->name('insert-iqamarenewal-fee');
    Route::post('update/iqama-anual/fee/for-employee', [AnualFeeDetailsController::class, 'update'])->name('update-iqamarenewal-fee');


    //    Route::get('set/all-employee/iqama-cost', [AnualFeeDetailsController::class, 'setAllEmployeeIqamaCost'])->name('set.all-employee.iqama-cost');
    //    Route::post('set/all-employee/iqama-cost/process', [AnualFeeDetailsController::class, 'setAllEmployeeIqamaCostProcess'])->name('set.all-employee.iqama-cost.process');

    /* ========== Iqama Anual Fee ========== */
    /* ========== Project Information ========== */
    Route::get('sponser/add', [SponsorController::class, 'index'])->name('add-sponser');
    Route::get('sponser/{spons_id}/edit', [SponsorController::class, 'edit'])->name('edit.sponser');
    Route::get('sponser/{spons_id}/delete', [SponsorController::class, 'delete'])->name('delete.sponser');
    Route::post('sponser/insert', [SponsorController::class, 'insert'])->name('insert-new.sponser');
    Route::post('sponser/update', [SponsorController::class, 'update'])->name('update.sponser');


    /* ========== Project Information ========== */
    Route::get('project/information', [ProjectInfoController::class, 'index'])->name('project-info');
    Route::get('project/information/add', [ProjectInfoController::class, 'add'])->name('add-project-info');
    Route::get('project/information/edit/{proj_id}', [ProjectInfoController::class, 'edit'])->name('project-info-edit');
    Route::get('project/information/view/{proj_id}', [ProjectInfoController::class, 'view'])->name('project-info-view');
    Route::get('project/information/delete/{proj_id}', [ProjectInfoController::class, 'delete'])->name('project-info-delete');

    /* ========== Project Wise Total Hours ========== */
    Route::get('project/basic/total-working-hours', [ProjectInfoController::class, 'projectWiseTotalWorkHours'])->name('project-wise-total-hours');

    Route::post('project/basic/total-working-hours/process', [ProjectInfoController::class, 'projectWiseTotalWorkHoursGenerat'])->name('project-wise-total-hours.generat');
    /* ========== Project Wise Total Salary ========== */
    Route::get('project/basic/total-salary', [ProjectInfoController::class, 'projectWiseTotalSalary'])->name('project-wise-total-salary');

    Route::post('project/basic/total-salary/process', [ProjectInfoController::class, 'projectWiseTotalSalaryGenerate'])->name('project-wise-total-salary.generat');


    Route::get('project/incharge/add', [ProjectInfoController::class, 'addProjectInchage'])->name('add-project.incharge');

    Route::post('project/incharge/insert', [ProjectInfoController::class, 'InsertProjectInchage'])->name('insert-project.incharge');
    /* ========== Ajax Request ========== */
    Route::post('find-employee/for/project/incharge', [ProjectInfoController::class, 'findEmployee'])->name('findEmployeeForIncharge');

    Route::post('check/valid/employee', [ProjectInfoController::class, 'validEmployee'])->name('check.valid-emp-id');
    /* ========== Ajax Request ========== */
    Route::post('project/information/insert', [ProjectInfoController::class, 'insert'])->name('insert-project-info');
    Route::post('project/information/update', [ProjectInfoController::class, 'update'])->name('update-project-info');
    /* ========== Project Image Upload ========== */
    Route::get('project/image/upload', [ProjectImgUploadController::class, 'index'])->name('project-image-upload');

    Route::get('project/image/remove/{id}', [ProjectImgUploadController::class, 'removeImage']);

    Route::post('project/image/insert/upload', [ProjectImgUploadController::class, 'upload'])->name('upload-project-muliple-image');
    /* ========= Ajax Request ========== */
    Route::post('search/project-image', [ProjectImgUploadController::class, 'searchProjectImage'])->name('search-project-image');
    /* ========= Ajax Request ========== */

    /* ========== Sub Company Information ========== */
    Route::get('sub-company/create', [SubCompanyInfoController::class, 'create'])->name('sub-comp-info');
    Route::get('sub-company/edit/{sb_comp_id}', [SubCompanyInfoController::class, 'edit'])->name('edit-info');
    Route::get('sub-company/view/{sb_comp_id}', [SubCompanyInfoController::class, 'view'])->name('view-info');
    Route::get('sub-company/delete/{sb_comp_id}', [SubCompanyInfoController::class, 'delete'])->name('delete-info');
    Route::post('sub-company/insert', [SubCompanyInfoController::class, 'insert'])->name('insert-sub-company');
    Route::post('sub-company/update', [SubCompanyInfoController::class, 'update'])->name('update-sub-company');
    /* ========== Employee Category ========== */
    Route::get('employee/designation', [EmpCategoryController::class, 'index'])->name('emp-category');
    Route::get('employee/designation/edit/{catg_id}', [EmpCategoryController::class, 'edit'])->name('edit-employee-category');
    Route::get('employee/designation/delete/{catg_id}', [EmpCategoryController::class, 'delete'])->name('delete-employee-category');
    Route::post('employee/designation/insert', [EmpCategoryController::class, 'insert'])->name('insert-category');
    Route::post('employee/designation/update', [EmpCategoryController::class, 'update'])->name('update-category');
    /* ========== Employee Job Experience ========== */
    Route::get('employee/job/experience/info', [EmpJobExperienceController::class, 'index'])->name('emp-job-experience');
    Route::get('employee/job/experience/{ejex_id}/info/edit', [EmpJobExperienceController::class, 'edit'])->name('edit.emp-job-experience');
    Route::post('employee/job/experience/info/insert', [EmpJobExperienceController::class, 'insert'])->name('insert-job-experience.info');
    Route::post('employee/job/experience/info/update', [EmpJobExperienceController::class, 'update'])->name('update-job-experience.info');
    /* ========== Employee contact info ========== */
    Route::get('employee/contact/person/info', [EmpContactPersonController::class, 'index'])->name('emp-contact-person');
    Route::get('employee/contact/person/{ecp_id}/info/edit', [EmpContactPersonController::class, 'edit'])->name('edit-employee.contact-person');
    Route::post('employee/contact/person/info/insert', [EmpContactPersonController::class, 'insert'])->name('insert-contact-person.info');
    Route::post('employee/contact/person/info/update', [EmpContactPersonController::class, 'update'])->name('update-contact-person.info');

    /* ========== Employee involve ========== */
    Route::get('release/employee/list', [EmployeeInfoController::class, 'releaseList'])->name('releaseListemployee-list');
    Route::get('pre-release/employee/list', [EmployeeInfoController::class, 'preReleaseList'])->name('pre.releaseListemployee-list');
    Route::get('employee/list', [EmployeeInfoController::class, 'index'])->name('employee-list');
    Route::get('search/employee/details', [EmployeeInfoController::class, 'searchEmp'])->name('search-employee');
    Route::get('search/employee/status', [EmployeeInfoController::class, 'searchEmpStatus'])->name('search-employee.status');
    Route::get('search/employee/for/update/info', [EmployeeInfoController::class, 'searchEmpForUpdate'])->name('search-employee.update.info');


    Route::get('search/employee/summary/details', [EmployeeInfoController::class, 'searchEmpSummary'])->name('search-employee.summary');

    Route::get('employee/add', [EmployeeInfoController::class, 'add'])->name('add-employee');
    Route::get('employee/job-approve', [EmployeeInfoController::class, 'jobApprove'])->name('employee-job-approver');
    Route::get('employee/job-approve/success/{emp_auto_id}', [EmployeeInfoController::class, 'approvalOfNewEmployeeInsertion'])->name('employee-job-approve.success');
    Route::get('employee/add-salary/detalis/{insert}', [EmployeeInfoController::class, 'addSalaryDetails'])->name('add-salary-info');
    Route::get('pre-release/employee/status/{id}', [EmployeeInfoController::class, 'preReleaseUpdateStatus'])->name('pre.releaseListemployee-status');

    /* ajax request */
    // rashed 
    Route::post('search-employee/for/update', [EmployeeInfoController::class, 'findEmployeeForUpdate'])->name('search.employee-for-update');
    Route::get('division-get/ajax/{country_id}', [EmployeeInfoController::class, 'getDivision']);
    Route::get('/employee/category/ajax/{emp_type_id}', [EmployeeInfoController::class, 'getEmpCategory']);
    /* ajax request */

    Route::get('employee/view/{emp_auto_id}', [EmployeeInfoController::class, 'view'])->name('employee.view');
    Route::get('employee/edit/{emp_auto_id}', [EmployeeInfoController::class, 'edit']);
    Route::post('employee/insert', [EmployeeInfoController::class, 'insert'])->name('employee-insert');
    Route::post('employee/all/information/update', [EmployeeInfoController::class, 'updateEmployeeAllInformation'])->name('employee-all-information-update');
    Route::post('employee/information/update', [EmployeeInfoController::class, 'updateEmployeeInformationData'])->name('employee-information-update');
    Route::post('employee/image/update', [EmployeeInfoController::class, 'updateEmployeeUploadedFileImage'])->name('employee-image.update');
    Route::post('search/employee/status/update', [EmployeeInfoController::class, 'updateAnEmployeeJobStatus'])->name('search.employee.status.update');
    Route::post('search/employee/project/update', [EmployeeInfoController::class, 'projectStatus'])->name('search.employee.project.update');
    /* ============================= Employee Summary ============================= */


    //  UI for viewing  Report
    Route::get('employee/salary/summary', [EmployeeInfoController::class, 'EmployeeSummary'])->name('employee-salary.summary');
    Route::post('empId-wise/employee/salary/summary', [EmployeeInfoController::class, 'createAnEmployeeSalarySummaryReport'])->name('employee-salary.summary-process');

    /* ============================= Project Wise Employee List ============================= */
    Route::get('project/wise/employee/list', [EmployeeInfoController::class, 'projectWiseEmployeeList'])->name('project-wise.employee');
    Route::post('project/wise/employee/list/process', [EmployeeInfoController::class, 'projectWiseEmployeeListProcess'])->name('project-wise.employee.process');
    /* ============================= sponer Wise Employee List ============================= */
    Route::post('sponser/report/process', [SponsorController::class, 'reportProcess'])->name('report-sponser-process');


    /* ============================= Trade Wise Employee List ============================= */
    Route::get('trade/wise/employee/list', [EmployeeInfoController::class, 'tradeWiseEmployeeList'])->name('trade-wise.employee');
    Route::post('trade/wise/employee/list/process', [EmployeeInfoController::class, 'tradeWiseEmployeeListProcess'])->name('trade-wise.employee.process');

    /* ============================= sponser Wise Employee List ============================= */
    Route::get('sponser/wise/employee/list', [EmployeeInfoController::class, 'sponserWiseEmployeeList'])->name('sponser-wise.employee');
    Route::post('sponser/wise/employee/list/process', [EmployeeInfoController::class, 'sponserWiseEmployeeListProcess'])->name('sponser-wise.employee.process');

    Route::post('sponser/report/process', [SponsorController::class, 'reportProcess'])->name('report-sponser-process');

    /* ============================= Employe Type (basic Salalry) Employee List ============================= */
    //  Route::get('employee-type/wise/employee/list', [EmployeeInfoController::class, 'getEmployeeTypeWiseReportUIRequest'])->name('employee-type-wise.employee');
    //  Route::post('employee-type/wise/employee/list/process', [EmployeeInfoController::class, 'sponserWiseEmployeeListProcess'])->name('employee-type-wise.employee-list.process');

    /* =============== Check Employee Id For Ignore Duplicate =============== */
    Route::post('check/employee/unique-id', [EmployeeInfoController::class, 'checkEmployeeId'])->name('checked-employee.id');


    /* ========== Employee in-out ========== */
    Route::get('employee/entry/time', [EmployeeInOutController::class, 'index'])->name('employee-in.time');
    Route::get('employee/multiple/in/out/time', [EmployeeInOutController::class, 'multiProjectInOut'])->name('employee.multiple.in-out.time');
    Route::get('edit/employee/multiple/in/out/{recordId}', [EmployeeInOutController::class, 'editAnEmployeeMultiProjectWorkRecord'])->name('edit.employee.multiple.project.in-out');
    Route::get('delete/employee/multiple/in/out/{empwh_auto_id}', [EmployeeInOutController::class, 'deleteAnEmployeeMultiProjectWorkRecord'])->name('delete.employee.multiple.project.in-out');
    Route::get('employee/out/time', [EmployeeInOutController::class, 'entryList'])->name('employee-out.time');
    Route::get('employee/project/wise/in/out/time', [EmployeeInOutController::class, 'projectWiseList'])->name('project-wise.in-out.time');
    Route::get('employee/in-out/time/delete/{id}', [EmployeeInOutController::class, 'delete'])->name('emp.attendence.delete');
    Route::get('employee/monthly-work/record/searchui', [EmployeeInOutController::class, 'employeeMultipleProjectWorkRecordSearchUI'])->name('employee.month.work.record.searchui');
    Route::post('employee/monthly-work/record/search', [EmployeeInOutController::class, 'searchEmployeeMultipleProjectWorkRecord'])->name('employee.month.work.record.search');
    Route::get('employee/in-out/time/report/generat', [EmployeeInOutController::class, 'report'])->name('employee-entry-out-report');
    Route::post('employee/in-out/time/report/process', [EmployeeInOutController::class, 'reportProcess'])->name('employee-entry-out-report-process');
    Route::post('multiproject-work-record/edit', [EmployeeInOutController::class, 'getAnEmployeeMultiProjectWorkRecord'])->name('edit-multiproject-work-record');

    /* ============== Ajax Request ============== */
    Route::post('/employee/working/entry/time', [EmployeeInOutController::class, 'timeInsert'])->name('employee-entry-time-insert');
    Route::post('/employee/multiple/working/entry/time', [EmployeeInOutController::class, 'multipleInsert'])->name('employee-multiple-time-insert');
    Route::post('/employee/multiple/working/update/time', [EmployeeInOutController::class, 'EmployeeMultiprojectMonthlyWorkRecordUpdate'])->name('employee-multiple-time-update');

    Route::post('employee/entry/time/list/process', [EmployeeInOutController::class, 'processEntryList'])->name('show-employee-out.time');
    Route::post('employee/entry/out/time/list/process', [EmployeeInOutController::class, 'outTimeProcessEntryList'])->name('show-employee-entry.out.time');
    Route::post('employee/project/wise/in/out/time/list/process', [EmployeeInOutController::class, 'projectWiseInOutList'])->name('project.wise-employee-in-out.time');
    Route::post('employee/out/time/update', [EmployeeInOutController::class, 'outTimeInsert'])->name('employee-entry-time-update');
    /* ============== Ajax Request ============== */


    /* ========== Employee in-out ========== */
    Route::get('employee/entry/time', [EmployeeInOutController::class, 'index'])->name('employee-in.time');
    Route::get('employee/multiple/in/out/time', [EmployeeInOutController::class, 'multiProjectInOut'])->name('employee.multiple.in-out.time');
    Route::get('edit/employee/multiple/in/out/{recordId}', [EmployeeInOutController::class, 'editAnEmployeeMultiProjectWorkRecord'])->name('edit.employee.multiple.project.in-out');
    Route::get('delete/employee/multiple/in/out/{empwh_auto_id}', [EmployeeInOutController::class, 'deleteAnEmployeeMultiProjectWorkRecord'])->name('delete.employee.multiple.project.in-out');
    Route::get('employee/out/time', [EmployeeInOutController::class, 'entryList'])->name('employee-out.time');
    Route::get('employee/project/wise/in/out/time', [EmployeeInOutController::class, 'projectWiseList'])->name('project-wise.in-out.time');
    Route::get('employee/in-out/time/delete/{id}', [EmployeeInOutController::class, 'delete'])->name('emp.attendence.delete');
    Route::get('employee/monthly-work/record/searchui', [EmployeeInOutController::class, 'employeeMultipleProjectWorkRecordSearchUI'])->name('employee.month.work.record.searchui');
    Route::post('employee/monthly-work/record/search', [EmployeeInOutController::class, 'searchEmployeeMultipleProjectWorkRecord'])->name('employee.month.work.record.search');
    Route::get('employee/in-out/time/report/generat', [EmployeeInOutController::class, 'report'])->name('employee-entry-out-report');
    Route::post('employee/in-out/time/report/process', [EmployeeInOutController::class, 'reportProcess'])->name('employee-entry-out-report-process');
    Route::post('multiproject-work-record/edit', [EmployeeInOutController::class, 'getAnEmployeeMultiProjectWorkRecord'])->name('edit-multiproject-work-record');


    /* ==================== Employee Iqama Expire ==================== */
    Route::get('employee/iqama-expired', [IqamaExpireController::class, 'index'])->name('iqama-expired');
    Route::get('employee/passport-expired', [PassportExpireController::class, 'index'])->name('passport-expired');
    /* ============== Ajax Request ============== */
    Route::post('/employee/iqama-expired/date', [IqamaExpireController::class, 'expiredDate'])->name('iqama-expired-find');
    /* ============== Ajax Request ============== */




    /* ==================== Employee Leave ==================== */
    Route::get('employee/leave/work/apply', [EmpLeaveController::class, 'index'])->name('employee-leave-work');
    Route::get('employee/leave/work/apply/list', [EmpLeaveController::class, 'applyList'])->name('employee-leave-approver');
    /* ============== Ajax Request ============== */
    Route::get('/employee/leave/work/apply-form/{leave_id}', [EmpLeaveController::class, 'showForm']);
    /* ============== Ajax Request ============== */
    Route::post('employee/leave/work/apply/application', [EmpLeaveController::class, 'insert'])->name('employee-leave-application');
    Route::post('employee/leave/work/apply/approve', [EmpLeaveController::class, 'approve'])->name('employee-leave.approve');
    /* ==================== Report Expenditure ==================== */
    Route::get('report/expenditure', [ExpenditureReportController::class, 'index'])->name('report-expenditure');
    Route::post('report/expenditure/process', [ExpenditureReportController::class, 'process'])->name('report-expenditure-process');
    /* ==================== Report Founding Source ==================== */
    Route::get('report/founding-source', [IncomeReportController::class, 'index'])->name('report-income');
    Route::post('report/income/process', [IncomeReportController::class, 'process'])->name('report-income-process');


    Route::get('report/founding-source/download/pdf', [IncomeReportController::class, 'downloadPdf'])->name('download-pdf-income_report');
    /* ==================== Report Purchase ==================== */
    Route::get('report/tools-&-metarials/purchase', [ItemPurchaseController::class, 'index'])->name('tools-metarial-report');
    Route::post('report/tools-&-metarials/purchase/process', [ItemPurchaseController::class, 'process'])->name('tools-metarials-process');
    /* ==================== Report Item Stock Amount ==================== */
    Route::get('report/item/stock-amount', [ItemPurchaseController::class, 'itemStock'])->name('items-report');
    Route::post('report/item/stock-amount/process', [ItemPurchaseController::class, 'itemStockProcess'])->name('item.stock-process');

    /* ==================== Report Salary ==================== */
    Route::get('report/salary', [SalaryReportController::class, 'index'])->name('salary-report');
    Route::get('report/salary/pdf/{month}', [SalaryReportController::class, 'salaryPDF'])->name('salary-report-pdf');
    Route::post('report/salary/process', [SalaryReportController::class, 'process'])->name('report-salary-process');

    Route::get('report/single-employee/salary', [SalaryReportController::class, 'singleEmployeeSalary'])->name('single-employee-salary.report');

    Route::post('report/single-employee/salary/history/process', [SalaryReportController::class, 'SalaryHistoryProcess'])->name('singleEmpl-salary-history-process');

    //    ======##### Iqama Report process #####==========
    Route::get('employee/iqama/renewal/report', [IqamaAdvacnePayController::class, 'iqamaRenewal'])->name('employee-iqama-renewal');
    Route::get('project/wise/iqama/renewal/report', [IqamaAdvacnePayController::class, 'projectWiseIqamaRenewal'])->name('project.wise-employee-iqama-renewal');


    Route::post('report/single-employee/iqama/renewal/process', [IqamaAdvacnePayController::class, 'iqamaRenewalHistoryProcess'])->name('singleEmpl-iqwama-renewal-process');
    Route::post('report/project/wise-employee/iqama/renewal/process', [IqamaAdvacnePayController::class, 'projectAndSponsorWiseIqamaReport'])->name('project-wise-iqwama-renewal-process');




    /* ==================== Employee Promosion ==================== */
    Route::get('employee/promosion', [PromosionController::class, 'index'])->name('employee-promosion');
    /* ==== ajax request ==== */
    Route::post('search-employee/for/promosion', [PromosionController::class, 'findEmployee']);

    Route::post('search-employee/for/details', [PromosionController::class, 'findEmployeeDetails'])->name('search.employee-details');
    Route::post('search-employee/for/adjustment', [PromosionController::class, 'findEmployeeadjustment'])->name('search.employee-adjustment');
    Route::post('search-employee/for/status', [PromosionController::class, 'findEmployeeStatus'])->name('search.employee-status');
    /* ========= Employee Salary Currection ========= */
    Route::post('search-employee/for/salary/currection', [PromosionController::class, 'findEmployeeForSalaryCurrection'])->name('search.employee-salary-for.currection');



    /* ==== ajax request ==== */
    Route::post('employee/promosion/submit', [PromosionController::class, 'insertPromosion'])->name('employee-promosion.submit');

    /* ==================== Income Source ==================== */
    Route::get('company/income/source', [IncomeSourceController::class, 'index'])->name('income-source');
    Route::get('company/income/list', [IncomeSourceController::class, 'list'])->name('income-list');
    Route::get('company/income/{inc_id}/edit', [IncomeSourceController::class, 'edit'])->name('edit.income');
    Route::get('company/income/{inc_id}/approve', [IncomeSourceController::class, 'approve'])->name('income-approve');
    Route::get('company/income/{inc_id}/remove', [IncomeSourceController::class, 'delete'])->name('remove.income');
    Route::post('company/income/source/insert', [IncomeSourceController::class, 'insert'])->name('insert.income-source');
    Route::post('company/income/source/update', [IncomeSourceController::class, 'update'])->name('update.income-source');
    Route::post('company/income/source/update', [IncomeSourceController::class, 'update'])->name('update.income-source');
    Route::get('income-expense/report', [IncomeSourceController::class, 'incomeExpenseReportUI'])->name('income-expense-report-ui');
    Route::post('income-expense/report/form/submit', [IncomeSourceController::class, 'incomeExpenseReportProcess'])->name('income-expense-report-process-form-submit');



    /* ========== metarial & Tools Category ========== */
    Route::get('metarial-tools/category', [ItemCategoryController::class, 'index'])->name('metarial-tools-category');
    Route::get('metarial-tools/{icatg_id}/category/edit', [ItemCategoryController::class, 'edit'])->name('metarial-tools-category-edit');
    Route::post('metarial-tools/category/insert', [ItemCategoryController::class, 'insert'])->name('insert.item-type-category');
    Route::post('metarial-tools/category/update', [ItemCategoryController::class, 'update'])->name('update.item-type-category');
    /* ========== metarial & Tools Sub Category ========== */
    Route::get('metarial-tools/sub-category', [ItemSubCategoryController::class, 'index'])->name('metarial-tools-sub-category');
    Route::get('metarial-tools/{iscatg_id}/sub-category/edit', [ItemSubCategoryController::class, 'edit'])->name('metarial-tools-sub-category.edit');
    /* ==== ajax request ==== */
    Route::get('item/category/ajax/{itype_id}', [ItemSubCategoryController::class, 'findCategory']);
    Route::get('item/sub-category/ajax/{icatg_id}', [ItemSubCategoryController::class, 'findSubCategory']);
    /* ==== ajax request ==== */
    Route::post('metarial-tools/sub-category/insert', [ItemSubCategoryController::class, 'insert'])->name('insert.item-type-sub-category');
    Route::post('metarial-tools/sub-category/update', [ItemSubCategoryController::class, 'update'])->name('update.item-type-sub-category');
    /* ========== Order metarial & Tools ========== */
    Route::get('order/metarial-tools/', [OrderComponentController::class, 'index'])->name('order-metarial-tools');

    /* ==== ajax request in Cart ==== */
    Route::post('metarial-tools/cart/store', [OrderComponentController::class, 'addToCart']);
    Route::get('metarial-tools/list/view', [OrderComponentController::class, 'metarialListView']);
    Route::post('metarial-tools/order/complete', [OrderComponentController::class, 'orderComplete'])->name('metarial.tools-order-confirm');

    Route::get('/metarial-tools/single-list/remove/{rowId}', [OrderComponentController::class, 'metarialSingleListRemove']);

    /* ========== Cost Type ========== */
    Route::get('cost/type', [CostTypeController::class, 'create'])->name('cost-type');
    Route::get('cost/type/{cost_type_id}/edit', [CostTypeController::class, 'edit'])->name('edit-cost-type');
    Route::post('cost/type/insert', [CostTypeController::class, 'insert'])->name('insert-cost-type');
    Route::post('cost/type/update', [CostTypeController::class, 'update'])->name('update-cost-type');


    /* ========== Add Daily Cost History ========== */
    Route::get('daily/cost/create', [DailyCostController::class, 'create'])->name('daily-cost');
    Route::get('daily/cost/edit/{cost_id}', [DailyCostController::class, 'edit'])->name('edit-cost');
    Route::get('daily/cost/delete/{cost_id}', [DailyCostController::class, 'delete'])->name('delete-cost');
    Route::post('daily/cost/insert', [DailyCostController::class, 'insert'])->name('insert-daily-cost');
    Route::post('daily/cost/update', [DailyCostController::class, 'update'])->name('update-daily-cost');
    Route::get('daily/cost/source/list', [DailyCostController::class, 'costList'])->name('cost-list');
    Route::get('daily/{cost_id}/cost/approve', [DailyCostController::class, 'approve'])->name('approve-cost');




    /* ==================== Advance Payment ==================== */
    Route::get('employee/advance/payment', [AdvancePayController::class, 'index'])->name('addvance.payment');
    Route::get('employee/advance/payment/edit/{adv_pay_id}', [AdvancePayController::class, 'edit'])->name('edit-advance.pay');
    Route::get('employee/advance/payment/delete/{adv_pay_id}', [AdvancePayController::class, 'delete'])->name('delete-advance.pay');

    Route::post('employee/advance/payment/insert', [AdvancePayController::class, 'insert'])->name('insert-advance.pay');

    Route::post('employee/advance/payment/update', [AdvancePayController::class, 'update'])->name('update-advance.pay');
    /* ==================== Advance Adjust ==================== */
    Route::get('employee/advance/adjust/payment', [AdvancePayController::class, 'employeeMonthlyPaymentSetting'])->name('advance-pay.adjust');
    Route::post('employee/advance/adjust/payment/update', [AdvancePayController::class, 'updateAdvanceInstallAmount'])->name('update.advance-installAmount');

    Route::get('employee/search/for/cash/depoosit', [AdvancePayController::class, 'emplpyeeCashDepositFormRequest'])->name('employee.search.with.cash-payment');
    Route::get('employee/search/for/cash/depoosit-with-allrecords', [AdvancePayController::class, 'emplpyeeCashDepositFormRequestWithAllRecords'])->name('employee.cash.receive.allrecords.with.cash-payment');
    Route::post('employee/advance/payment/cash-receive', [AdvancePayController::class, 'advancePaymentReceivedFromEmployee'])->name('employee-advance-payment-cash-receive');
    Route::get('employee/advance/payment/receive-delete/{id}', [AdvancePayController::class, 'deleteCashDepositAdvancePayment'])->name('delete-employee-advance-payment-cash-receive');



    /* ========== Ajax Request ========== */
    Route::post('find-employee/for/advance/adjust/edit', [AdvancePayController::class, 'findEmployee'])->name('findEmployeeForLoan');

    /* ==================== Salary Generat ==================== */
    Route::get('employee/monthly/salary', [SallaryGenerateController::class, 'create'])->name('salary-processing-for-month');


    Route::post('employee/monthly/salary/store/salary-history', [SallaryGenerateController::class, 'salaryStore'])->name('salary-record.store');

    Route::get('employee/monthly/salary/generat', [SallaryGenerateController::class, 'index'])->name('salary-generat');

    Route::post('single-mployee/monthly/salary/report/', [SallaryGenerateController::class, 'singleEmployeeMonthWiseSalaryReport'])->name('single-employee-salary-generat');

    Route::post('sponser-wise/monthly/salary/report/', [SallaryGenerateController::class, 'sponserWiseMonthlySalaryReport'])->name('sponser-month.wise-salary-report');

    Route::post('month/year-wise/salary/summary/', [SallaryGenerateController::class, 'monthAndYearWiseSalarySummary'])->name('year-month.wise-salary-summary');

    Route::post('salary/report/project-wise-toalsalary-totalworking-hours', [SallaryGenerateController::class, 'projectwiseTotalWorkingHoursAndTotalSalary'])->name('project-wise-Total.Working.Hours-And-Total.Salary');

    Route::post('month/year-wise/salary/multipleid/', [SallaryGenerateController::class, 'multipleEmployeeIdBaseSalaryProcess'])->name('multiple-empidbase-salary-process');

    Route::post('project/employee/monthly/salary/report/', [SallaryGenerateController::class, 'projectEmployeeMonthWiseSalaryReport'])->name('project-month-empType.wise-salary');
    Route::post('project/employee-status/monthly/salary/report/', [SallaryGenerateController::class, 'projectEmployeeStatusMonthWiseSalaryReport'])->name('project-month-empStatus.wise-salary');

    Route::post('all/employee/monthly/salary/generat/without-project/employee-type', [SallaryGenerateController::class, 'monthWiseSalary'])->name('all-emp.salary-without-project-emp-type');
    // Salary Pending
    Route::get('employee/salary/pending', [SallaryGenerateController::class, 'SalaryPending'])->name('salary-pending');
    Route::post('employee/salary/pending/list', [SallaryGenerateController::class, 'SalaryPendingList'])->name('salary-pending.list');

    // Salary Paid
    Route::get('employee/salary/paid', [SallaryGenerateController::class, 'Salarypaid'])->name('salary-paid');
    Route::post('employee/salary/paid/list', [SallaryGenerateController::class, 'SalarypaidList'])->name('salary-paid.list');

    // salary sheet
    Route::get('employee/salary/sheet', [SallaryGenerateController::class, 'SalarySheet'])->name('salary-sheet');
    Route::get('employee/salary/sheet/delete/{id}', [SallaryGenerateController::class, 'deleteSalarySheet'])->name('salary-sheet.delete');
    Route::post('employee/salary/sheet', [SallaryGenerateController::class, 'SalarySheetStore'])->name('salary-sheet.store');
    // Salary Payment
    Route::get('employee/salary/payment', [SallaryGenerateController::class, 'SalaryPaymentComplete'])->name('salary-pay');

    Route::post('employee/salary/payment', [SallaryGenerateController::class, 'SalaryPayment'])->name('payment.salary');
    Route::post('employee/salary/payment/undo', [SallaryGenerateController::class, 'SalaryPaymentToUnPay'])->name('payment.salary.undo');
    /* +++++++++++++++ Salary Correction +++++++++++++++ */
    Route::get('employee/salary/currection', [SallaryGenerateController::class, 'SalaryCorrectionForm'])->name('salary-currection');
    Route::post('employee/salary/currection/process', [SallaryGenerateController::class, 'SalaryCurrectionProcess'])->name('salary.currection-process');
    /* +++++++++++++++ Salary Correction +++++++++++++++ */

    Route::post('employee/monthly/salary/add/history', [SallaryGenerateController::class, 'addSalaryHistory'])->name('add-salary-history');
    /* add employee salary in salary history */
    Route::post('single-employee/monthly/salary/add/history', [SallaryGenerateController::class, 'addSignleEmployeeSalaryHistory'])->name('add-salary-history.single');
    Route::get('pdf', [SallaryGenerateController::class, 'Pdf']);

    Route::get('employee/monthly/salary/generat/for/pdf', [SallaryGenerateController::class, 'downloadPdf'])->name('download.pdf-salary');


    // ==================== Month Work History ====================
    Route::get('add/month/work/history', [DailyWorkHistoryController::class, 'index'])->name('add-daily-work');
    Route::get('delete/work/history/{id}', [DailyWorkHistoryController::class, 'deleteDailyWorkHistory'])->name('delete-daily-work-history');
    Route::post('add/month/work/history/store', [DailyWorkHistoryController::class, 'store'])->name('store.monthly-work-history');
    Route::get('month/work/history/for-report', [DailyWorkHistoryController::class, 'getEmployeMonthlyWorkHistory'])->name('monthwork-reportprocess');
    Route::post('month/work/history/for-report/process', [DailyWorkHistoryController::class, 'getEmployeMonthlyWorkHistoryProcess'])->name('project-wise-employe.month-salary');
    Route::post('employee/month/work/status/summary-report', [DailyWorkHistoryController::class, 'processAllEmployeMonthlyWorkStatus'])->name('all-employee-work-status-summary');
    Route::post('daily-work-history/employee-notin-work-record', [DailyWorkHistoryController::class, 'processEmployeNotPresentInMonthlyWorkHistory'])->name('work-history-employee-notin-work-record');
    Route::get('edit/month/{day_work_id}/work/history', [DailyWorkHistoryController::class, 'edit'])->name('edit.month-work');
    Route::get('delete/month/{day_work_id}/work/history', [DailyWorkHistoryController::class, 'delete'])->name('delete.daily-work');

    /* ======== ajax request ======== */
    Route::post('find/employee-id', [DailyWorkHistoryController::class, 'autocomplete']);
    Route::post('find/indirectemployee-id', [DailyWorkHistoryController::class, 'conditionAutocomplete']);
    Route::post('find/employee/type-id', [DailyWorkHistoryController::class, 'findEmployeeTypeId']);
    Route::post('find/direct/employee-id', [DailyWorkHistoryController::class, 'findDirectEmployee']);
    /* ======== ajax request ======== */
    Route::post('insert/month/work/history', [DailyWorkHistoryController::class, 'insert'])->name('insert-month-work');
    Route::post('update/month/work/history', [DailyWorkHistoryController::class, 'update'])->name('update-month-work');




    /* salary details controller */
    Route::get('employee/salary-details', [SalaryDetailsController::class, 'index'])->name('salary-details');
    // employee status update
    Route::get('employee/directman/status/update/{emp_auto_id}', [SalaryDetailsController::class, 'directManStatusUpdate'])->name('directman-status-update');



    Route::get('employee/salary/single/edit/{sdetails_id}', [SalaryDetailsController::class, 'edit'])->name('salary-single-edit');
    Route::get('employee/salary/single/details/{sdetails_id}', [SalaryDetailsController::class, 'view'])->name('salary-single-details');
    Route::get('employee/salary/single/delete/{sdetails_id}', [SalaryDetailsController::class, 'delete'])->name('salary-single-delete');
    Route::post('employee/salary-details/insert', [SalaryDetailsController::class, 'insert'])->name('salary-detalis-insert');

    Route::post('employee/salary-details/update', [SalaryDetailsController::class, 'update'])->name('salary-detalis-update');

    /* ========== Employee CPF Contribution ========== */
    Route::get('employee/CPF/contribution', [SalaryDetailsController::class, 'setContributionAmount'])->name('cpf-Contribution.set');

    Route::post('employee/CPF/contribution/amount', [SalaryDetailsController::class, 'updateContributionAmount'])->name('update-contribution.amount');


    /* ========== Ajax Call ========== */
    Route::post('employee-information/for/CPF/contribution', [SalaryDetailsController::class, 'empInfoForContribution'])->name('emp-information-for.set-contribution');

    /* ========== Employee Contribution ========== */
    Route::get('employee/contribution-report', [ContributionController::class, 'EmployeeContribution'])->name('CPF-contribution-report');
    Route::post('employee/contribution-report/genarate', [ContributionController::class, 'EmployeeContributionReport'])->name('CPF-contribution-report-generat');
    /* ========== Employee Contribution ========== */



    /* ========== Deduction ========== */
    Route::get('employee/deduction', [DeductionController::class, 'index'])->name('employee-deduction');
    Route::get('employee/deduction/add', [DeductionController::class, 'add'])->name('add-employee-deduction');

    /* ========== Address Division ========== */
    Route::get('country/add', [CountryController::class, 'add'])->name('add-country');
    Route::post('country/insert', [CountryController::class, 'insert'])->name('insert-country');


    /* ========== Address Division ========== */
    Route::get('division/add', [DivisionController::class, 'add'])->name('add-division');
    Route::get('division/edit/{division_id}', [DivisionController::class, 'edit'])->name('edit-division');
    Route::post('division/insert', [DivisionController::class, 'insert'])->name('insert-division');
    Route::post('division/update', [DivisionController::class, 'update'])->name('update-division');
    /* ========== Ajax Request ========== */
    Route::post('check/division-name', [DivisionController::class, 'validDivision'])->name('check-division.name');
    /* ========== Ajax Request ========== */

    /* ========== Address District ========== */
    Route::get('district/add', [DistrictController::class, 'add'])->name('add-district');
    Route::get('district/edit/{district_id}', [DistrictController::class, 'edit'])->name('edit-district');
    Route::post('district/insert', [DistrictController::class, 'insert'])->name('insert-district');
    Route::post('district/update', [DistrictController::class, 'update'])->name('update-district');
    /* ajax request */
    Route::get('division/ajax/{country_id}', [DistrictController::class, 'getDivision']);
    Route::get('/district/ajax/{division_id}', [DistrictController::class, 'getDistrict']);
    /* ajax request */

    /* ==================== Vechicle Controller ==================== */
    Route::get('vechicle/add', [VehicleController::class, 'index'])->name('add-new.vehicle');
    Route::get('vechicle/edit/{veh_id}', [VehicleController::class, 'edit'])->name('edit-vechicle');
    Route::get('vechicle/delete/{veh_id}', [VehicleController::class, 'delete'])->name('delete-vechicle');
    Route::post('vechicle/insert', [VehicleController::class, 'insert'])->name('insert-new.vechicle');
    Route::post('vechicle/update', [VehicleController::class, 'update'])->name('update-vechicle');

    /* ==================== Vechicle Fine Controller ==================== */
    Route::get('vechicle/fine', [VehicleController::class, 'vehicleFineFormWithRecords'])->name('add.vehicle.fine');
    Route::post('vehicle/search', [VehicleController::class, 'searchVehicleInformation'])->name('search_vehicle_information');
    Route::post('vehicle/fine/submit', [VehicleController::class, 'insertVehicleFineFormSubmit'])->name('vehicle_fine_insert_form');
    Route::get('vehicle/fine/edit/{id}', [VehicleController::class, 'editVehicleFineFormSubmit'])->name('vehicle_fine_edit_form');
    Route::post('vehicle/fine/update', [VehicleController::class, 'updateVehicleFineFormSubmit'])->name('vehicle_fine_update_form');
    Route::get('vehicle/fine/delete/{id}', [VehicleController::class, 'deleteVehicleFineForm'])->name('vehicle_fine_delete_form');

    /* ==================== LogBook Controller ==================== */
    Route::get('new/log-book/add', [LogBookController::class, 'index'])->name('add-new.LogBook');
    Route::get('new/log-book/edit/{lgb_id}', [LogBookController::class, 'edit'])->name('edit-new.LogBook');
    Route::get('new/log-book/delete/{lgb_id}', [LogBookController::class, 'delete'])->name('delete-new.LogBook');
    Route::post('new/log-book/insert', [LogBookController::class, 'insert'])->name('insert-new.LogBook');
    Route::post('new/log-book/update', [LogBookController::class, 'update'])->name('update-new.LogBook');
    Route::get('log-book/report', [LogBookController::class, 'report'])->name('log-book.report');
    Route::post('log-book/report/generat', [LogBookController::class, 'reportGenerate'])->name('log-book.report-generat');

    /* ==================== Office Building Controller ==================== */
    Route::get('rent/new-building/add', [OfficeBuildingController::class, 'index'])->name('rent.new-building');
    Route::get('rent/{ofb_id}/new-building/delete', [OfficeBuildingController::class, 'delete'])->name('rent.new-building.delete');
    Route::get('rent/{ofb_id}/new-building/edit', [OfficeBuildingController::class, 'edit'])->name('rent.new-building.edit');
    Route::post('rent/new-building/insert', [OfficeBuildingController::class, 'insert'])->name('rent.new-building.insert');
    Route::post('rent/new-building/update', [OfficeBuildingController::class, 'update'])->name('rent.new-building.update');

    /* ==================== Office Building Controller ==================== */


    /* ==================== EXPORT AS EXCELL OR CSV FORMAT ==================== */

    Route::get('export/eployees-excell', [ExcelExportController::class, 'EmployeeslistExcellExport'])->name('export.eployees-list-excell');
});
