<?php

namespace App\Http\Controllers\DataServices;


use App\Models\User;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class AuthenticationDataService
{

    public function getAllUsers()
    {
        return $all = User::get();
    }

    public function findUserById($id)
    {
        return $find = User::where('id', $id)->first();
    }

    public function getUsersWithOutLoginUser($user_id)
    {
        return  $all = User::where('id', '!=', $user_id)->where('id', '!=', 1)->get();
    }

    public function findUserByEmpIdAndPhoneNumber($emp_id, $phone_number)
    {
        $fineEmp = User::where('emp_id', $emp_id)->where('phone_number', $phone_number)->first();
    }

    public function createNewUser($employee_name, $emp_id, $email, $phone_number, $password, $role_id)
    {
        $pass = Hash::make($password);
        return  $insert = User::insert([
            'name' => $employee_name,
            'emp_id' => $emp_id,
            'email' => $email,
            'phone_number' => $phone_number,
            'password' => $pass,
            'role_id' => $role_id,
            'created_at' => Carbon::now(),
        ]);
    }

    public function updateUserPassword($id, $newpass)
    {
        return  User::where('id', $id)->update([
            'password' => Hash::make($newpass)
        ]);
    }

    public function deactivateUser($id)
    {
        return  User::where('status', 1)->where('id', $id)->delete();
    }
    public function activateUserAuthentication($id)
    {
        return   User::where('id', $id)->update([
            'status' => 1,
            'updated_at' => Carbon::now(),
        ]);
    }


    public function getUserRole()
    {
        return $all = Role::get();
    }

    public function getARole($role_auto_id)
    {
        return  Role::where('role_auto_id', $role_auto_id)->orderBy('role_auto_id', 'ASC')->get();
    }
    public function updateUserRole($id, $role_id)
    {
        return $update = User::where('id', $id)->update([
            'role_id' => $role_id,
            'updated_at' => Carbon::now(),
        ]);
    }
}
