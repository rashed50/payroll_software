<?php

namespace App\Http\Controllers\DataServices;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\EmployeeInfo;
use App\Models\SalaryDetails;
use Carbon\Carbon;

class EmployeeDataService //extends Controller
{


    public function generateEmployeeId()
    {
        $all = EmployeeInfo::count();
        return $empId = "10" . $all;
    }


    public function insertNewEmployee($request)
    {

        return  $emp_auto_id = EmployeeInfo::insertGetId([
            'employee_id' => $request->emp_id,
            'employee_name' => $request->emp_name,
            'akama_no' => $request->akama_no,
            'akama_expire_date' => $request->akama_expire,
            'passfort_no' => $request->passfort_no,
            'passfort_expire_date' => $request->passfort_expire_date,
            'sponsor_id' => $request->sponsor_id,
            'mobile_no' => $request->mobile_no,

            'country_id' => $request->country_id,
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'post_code' => $request->post_code,
            'details' => $request->details,
            'present_address' => $request->present_address,
            'emp_type_id' => $request->emp_type_id,
            'designation_id' => $request->designation_id,
            'hourly_employee' => $request->hourly_employee,

            'project_id' => $request->project_id,
            'department_id' => $request->department_id,
            'date_of_birth' => $request->date_of_birth,
            'phone_no' => $request->phone_no,
            'email' => $request->email,
            'maritus_status' => $request->maritus_status,
            'gender' => $request->gender,
            'religion' => $request->religion,
            'joining_date' => $request->joining_date,
            'confirmation_date' => $request->confirmation_date,
            'appointment_date' => $request->appointment_date,
            'entry_date' => Carbon::now(),
            'entered_id' => Auth::user()->id,
            'created_at' => Carbon::now(),
        ]);
    }

    /*
     ========================================================================== 
     ============================= Count Method ============================ 
     ========================================================================== 
    */

    public function countTotalEmployees($jobstatus)
    {
        if ($jobstatus < 0 || $jobstatus == null) {
            return  EmployeeInfo::count();
        } else
            return  EmployeeInfo::where('job_status', $jobstatus)->count();
    }

    /*
     ========================================================================== 
     ============================= Employee List ============================ 
     ========================================================================== 
    */


    public function getAnEmployeeInfoByEmpAutoId($emp_auto_id)
    {
        return   EmployeeInfo::where('emp_auto_id', $emp_auto_id)->first();
    }
    public function getAnEmployeeInfoByEmpId($employee_id)
    {
        return EmployeeInfo::where('employee_id', $employee_id)->first();
    }

    public function getEmployeeByEmpIdWithLikeQuery($empId, $job_status)
    {
        return EmployeeInfo::where("employee_id", "LIKE", "%{$empId}%")->where('job_status', $job_status)->get();
    }
    public function getEmployeeByEmpIdAndEmpTypeWithLikeQuery($empId, $job_status, $emp_type_id)
    {
        //  EmployeeInfo::where("employee_id", "LIKE", "%{$request->empId}%")->where('job_status', 1)->where('emp_type_id', 2)->get();
        return EmployeeInfo::where("employee_id", "LIKE", "%{$empId}%")->where('job_status', $job_status)->where('emp_type_id', $emp_type_id)->get();
    }

    public function getAnEmployeeInfoByEmpIqamaNo($iqamaNo)
    {
        return  EmployeeInfo::where('akama_no', $iqamaNo)->first();
    }

    public function getAnEmployeeInfoWithSalaryDetailsByEmpAutoId($emp_auto_id)
    {
        return   EmployeeInfo::with('salarydetails')->where('job_status', 1)->where('emp_auto_id', $emp_auto_id)->first();
    }

    public function getAnEmployeeInfoWithSalaryDetailsByEmpId($employee_id)
    {
        return EmployeeInfo::with('salarydetails')->where('job_status', 1)->where('employee_id', $employee_id)->first();
    }



    public function getAllEmployeesInformation($pageLimit, $job_status)
    {
        if ($pageLimit <= 0 || $pageLimit == null) {
            return $all =  EmployeeInfo::where('job_status', $job_status)->orderBy('emp_auto_id', 'DESC')->get();
        } else {
            return $all =  EmployeeInfo::where('job_status', $job_status)->orderBy('emp_auto_id', 'DESC')->paginate($pageLimit);
        }
    }

    public function getEmployeeInformationForDropdownList()
    {
        return EmployeeInfo::select(
            'emp_auto_id',
            'employee_id',
            'employee_name'
        )->orderBy('employee_id')
            ->get();
    }

    public function getEmployeeListByEmpCategoryId($catg_id)
    {
        return EmployeeInfo::where('designation_id', $catg_id)->get();
    }

    public function getEmployeeListByCategoryIdEmpTypeId($catg_id, $empTypeId)
    {
        if ($catg_id == -1 && $empTypeId >= 1) {
            return  EmployeeInfo::where('emp_type_id', $empTypeId)->orderBy('employee_id', 'ASC')->get();
        } else if ($catg_id >= 1 && $empTypeId == -1) {
            return EmployeeInfo::where('designation_id', $catg_id)->orderBy('employee_id', 'ASC')->get();
        } else if ($catg_id >= 1 && $empTypeId >= 1) {
            return EmployeeInfo::where('designation_id', $catg_id)->where('emp_type_id', $empTypeId)->orderBy('employee_id', 'ASC')->get();
        }
    }

    public function getEmployeeListByCategoryIdEmpTypeAndHourlyEmp($catg_id, $empTypeId, $isHourly)
    {
        if ($catg_id == -1 && $empTypeId == -1) {
            return  EmployeeInfo::where('hourly_employee', $isHourly)->orderBy('employee_id', 'ASC')->get();
        } else if ($catg_id == -1 && $empTypeId >= 1) {
            return  EmployeeInfo::where('emp_type_id', $empTypeId)
                ->where('hourly_employee', $isHourly)->orderBy('employee_id', 'ASC')->get();
        } else if ($catg_id >= 1 && $empTypeId >= 1) {
            return EmployeeInfo::where('designation_id', $catg_id)->where('emp_type_id', $empTypeId)
                ->where('hourly_employee', $isHourly)->orderBy('employee_id', 'ASC')->get();
        }
    }


    public function getEmployeeListWithProjectAndSponsor($projId, $sponseId)
    {

        if ($projId != 0 && $sponseId != 0) {

            return $projAndSponsWiseEmp = EmployeeInfo::with('project', 'sponsor')->where('sponsor_id', $sponseId)->where('project_id', $projId)->get();
        } elseif ($projId != 0) {
            return  $projAndSponsWiseEmp = EmployeeInfo::with('project', 'sponsor')->where('project_id', $projId)->get();
        } elseif ($sponseId != 0) {
            return $projAndSponsWiseEmp = EmployeeInfo::with('project', 'sponsor')->where('sponsor_id', $sponseId)->get();
        } else {
            return  $projAndSponsWiseEmp = EmployeeInfo::all();
        }
    }

    public function getAllEmployeesWithReferenceTable($employee_id)
    {
        if ($employee_id == null || $employee_id <= 0) {
            return EmployeeInfo::leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->orderBy('employee_id')
                ->get();
        } else {
            return EmployeeInfo::leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->orderBy('employee_id')
                ->get()->where('employee_id', $employee_id);
        }
    }



    /*
     ========================================================================== 
     ============================= Employee Update ============================ 
     ========================================================================== 
    */

    public function updateEmployeeAllInformation($request)
    {
        return  EmployeeInfo::where('emp_auto_id', $request->id)->update([
            'employee_name' => $request->emp_name,
            'akama_no' => $request->akama_no,
            'akama_expire_date' => $request->akama_expire,
            'passfort_no' => $request->passfort_no,
            'passfort_expire_date' => $request->passfort_expire_date,
            'sponsor_id' => $request->sponsor_id,
            'mobile_no' => $request->mobile_no,
            'country_id' => $request->country_id,
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'post_code' => $request->post_code,
            'details' => $request->details,
            'present_address' => $request->present_address,
            // 'emp_type_id' => $request->emp_type_id,
            'project_id' => $request->projectStatus,
            'job_status' => $request->EmpStatus_id,
            'designation_id' => $request->designation_id,
            'department_id' => $request->department_id,
            'date_of_birth' => $request->date_of_birth,
            'phone_no' => $request->phone_no,
            'email' => $request->email,
            'maritus_status' => $request->maritus_status,
            'gender' => $request->gender,
            'religion' => $request->religion,
            'joining_date' => $request->joining_date,
            'confirmation_date' => $request->confirmation_date,
            'appointment_date' => $request->appointment_date,
            'entry_date' => Carbon::now(),
            'entered_id' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function updateEmployeeUploadedFileDbPath($emp_auto_id, $filePath, $dbColoumName)
    {
        if ($filePath == '' || $filePath == null)
            return false;
        return  EmployeeInfo::where('emp_auto_id', $emp_auto_id)->update([
            $dbColoumName => $filePath,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function updateEmployeeJobStatus($emp_auto_id, $job_status)
    {
        return EmployeeInfo::where('emp_auto_id', $emp_auto_id)->update([
            'job_status' => $job_status,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function updateEmployeeAssignedProject($emp_auto_id, $project_id)
    {
        return EmployeeInfo::where('emp_auto_id', $emp_auto_id)->update([
            'project_id' => $project_id,
        ]);
    }

    /*
     ========================================================================== 
     ============================= Employee Report ============================ 
     ========================================================================== 
    */
    public function exportEmployeeInformation()
    {
        return EmployeeInfo::select(
            'employee_id',
            'employee_name',
            'passfort_no',
            'passfort_expire_date',
            'akama_no',
            'akama_expire_date',
            'spons_name',
            'proj_name',
            'mobile_no',
            'email',
            'date_of_birth',
            'country_name',
            'hourly_employee',
            'catg_name',
            'name',
            'title',
            'basic_amount',
            'hourly_rent',
            'basic_hours',
            'food_allowance',
            'mobile_allowance'

        )
            ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
            ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
            ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
            ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
            ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
            ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
            ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
            ->orderBy('employee_id')
            ->get();
    }



    /*
     ========================================================================== 
     ============================= Employee Salary Information ================ 
     ========================================================================== 
    */


    public function getAnEmployeeSalaryDetailsByEmpAutoId($emp_auto_id)
    {
        return $salary = SalaryDetails::where('emp_id', $emp_auto_id)->first();
    }
    public function updateEmployeeSalaryAllInformation($request)
    {
        return  $update =  SalaryDetails::where('emp_id', $request->id)->update([

            'basic_amount' => $request->basic_amount,
            'basic_hours' => $request->basic_hours,
            'hourly_rent' => $request->hourly_rent,
            'house_rent' => $request->house_rent,
            'mobile_allowance' => $request->mobile_allowance,
            'medical_allowance' => $request->medical_allowance,
            'local_travel_allowance' => $request->local_travel_allowance,
            'conveyance_allowance' => $request->conveyance_allowance,
            'food_allowance' => $request->food_allowance,
            'cpf_contribution' => $request->contribution_amoun,
            'increment_amount' => $request->increment_amount,
            'saudi_tax' => $request->saudi_tax,
            'others1' => $request->employee_others,
            'created_at' => Carbon::now(),
        ]);
    }
}
