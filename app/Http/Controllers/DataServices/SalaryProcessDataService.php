<?php

namespace App\Http\Controllers\DataServices;

use App\Models\EmployeeInfo;
use App\Models\SalaryHistory;
use App\Models\SalarySheetUpload;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class SalaryProcessDataService
{
    public static function getAnEmployeeSalaryRecord($emp_auto_id, $month, $year)
    {
        return SalaryHistory::where('emp_auto_id', $emp_auto_id)->where('slh_month', $month)->where('slh_year', $year)->first();
    }
    public static function getAnEmployeeInfoWithSalaryHistory($emp_auto_id, $month, $year)
    {
        return SalaryHistory::with('employee')->where('emp_auto_id', $emp_auto_id)->where('slh_month', $month)->where('slh_year', $year)->first();
    }
    public static function getAnEmployeeSalaryHistorySummary($employeeId, $year)
    {
        return $employeeSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->orderBy('slh_month', 'ASC')->get();
    }

    public static function getAnEmployeeSalaryTotalAmount($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_total_salary');
    }


    public static function getAnEmployeeTotalUnPaidSalary($employeeId, $year)
    {
        if ($year == null || $year == '') {
            return SalaryHistory::where('emp_auto_id', $employeeId)
                ->where('Status', 0)
                ->sum('slh_total_salary');
        } else {
            return SalaryHistory::where('emp_auto_id', $employeeId)
                ->where('slh_year', $year)
                ->where('Status', 0)
                ->sum('slh_total_salary');
        }
    }

    public static function totalAdvance($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_saudi_tax');
    }

    public static function TotalIqamaRenewal($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_iqama_advance');
    }

    public static function TotalContribution($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_cpf_contribution');
    }

    public static function TotalHours($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_total_hours');
    }

    public static function slh_saudi_tax($employeeId, $year)
    {
        return $employeeTotalSalarySummary = SalaryHistory::where('emp_auto_id', $employeeId)->where('slh_year', $year)->sum('slh_saudi_tax');
    }



    // Month, Yearly Paid and Unpaid Salary Total Amount

    public static function getSalaryTotalAmount($project_id, $month, $salaryYear, $salaryStatus)
    {
        if ($project_id <= 0) {
            return $allSalaryAmount = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_total_salary');
        } else {
            return $allSalaryAmount = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)->where('project_id', $project_id)->sum('slh_total_salary');
        }
    }
    // public static function getSalaryTotalAmountByProjectId($month, $salaryYear, $salaryStatus, $project_id)
    // {
    //     return $allSalaryAmount = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
    //         ->where('salary_histories.Status', $salaryStatus)->where('project_id', $project_id)->sum('slh_total_salary');
    // }

    // public static function getSalaryTotalAmountByDateToDate($month, $fromYear, $toYear, $salaryStatus)
    // {
    //     return  SalaryHistory::whereIn('slh_month', array($month))->whereBetween('slh_year', array($fromYear, $toYear))
    //         ->where('salary_histories.Status', $salaryStatus)->sum('slh_total_salary');
    // }

    public static function getSalaryIqamaAdvanceTotalAmount($month, $salaryYear, $salaryStatus)
    {
        return  $iqamaAmount = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_iqama_advance');
    }

    public static function getSalaryMonthTotalHours($project_id, $month, $salaryYear, $salaryStatus,)
    {
        if ($project_id <= 0) {

            return     $totalHours = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)->sum('slh_total_hours');
        } else {
            return     $totalHours = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)->where('project_id', $project_id)->sum('slh_total_hours');
        }
    }

    // public static function getSalaryMonthTotalHoursByDateToDate($month, $salaryYear, $salaryStatus)
    // {
    //     return     $totalHours = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_total_hours');
    // }

    // public static function getSalaryMonthTotalHoursByProjectId($month, $salaryYear, $salaryStatus, $project_id)
    // {
    //     return     $totalHours = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
    //         ->where('salary_histories.Status', $salaryStatus)->where('project_id', $project_id)->sum('slh_total_hours');
    // }


    public static function getSalarySaudiTaxTotalAmount($month, $salaryYear, $salaryStatus)
    {
        return  $totalSaudiTax = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_saudi_tax');
    }

    public static function getSalaryMonthTotalOvertimeHours($project_id, $month, $salaryYear, $salaryStatus)
    {
        if ($project_id <= 0) {
            return  SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_total_overtime');
        } else {
            return   SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)->where('project_id', $project_id)->sum('slh_total_overtime');
        }
    }
    public static function getSalaryMonthTotalOvertimeAmount($month, $salaryYear, $salaryStatus)
    {
        return SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_overtime_amount');
    }
    public static function getSalaryMonthFoodAllowanceTotalAmount($month, $salaryYear, $salaryStatus)
    {
        return   SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('food_allowance');
    }
    public static function getSalaryMonthEmployeeCPFTotalAmount($month, $salaryYear, $salaryStatus)
    {
        return     $totalContribution = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_company_contribution');
    }

    public static function getSalaryMonthOtherAdvanceTotalAmount($month, $salaryYear, $salaryStatus)
    {
        return  $totalOtherAdvance = SalaryHistory::where('slh_month', $month)->where('slh_year', $salaryYear)->where('salary_histories.Status', $salaryStatus)->sum('slh_other_advance');
    }



    public function getAllSalarySheet()
    {
        return $allSheet =  SalarySheetUpload::orderBy('ss_auto_id', 'DESC')->get();
    }


    public static function updateAnEmployeeMonthlySalaryRecord($salaryHistoryAutoId, $anEmployee, $month, $salaryYear,)
    {
        if ($salaryHistoryAutoId > 0) {
            return  $insert = SalaryHistory::where('slh_auto_id', $salaryHistoryAutoId)->update([
                'emp_auto_id' => $anEmployee->emp_auto_id,
                'basic_amount' => $anEmployee->basic_amount,
                'basic_hours' => $anEmployee->basic_hours,
                'house_rent' => $anEmployee->house_rent,
                'hourly_rent' => $anEmployee->hourly_rent,
                'mobile_allowance' => $anEmployee->mobile_allowance,
                'medical_allowance' => $anEmployee->medical_allowance,
                'local_travel_allowance' => $anEmployee->local_travel_allowance,
                'conveyance_allowance' => $anEmployee->conveyance_allowance,
                'food_allowance' =>  $anEmployee->food_allowance,
                'others' => $anEmployee->others1,
                'slh_total_overtime' => $anEmployee->overtime,
                'slh_overtime_amount' =>  $anEmployee->slh_overtime_amount,
                /* New field in Salary History */
                'slh_total_salary' => $anEmployee->gross_salary,
                'slh_total_hours' => $anEmployee->total_hours,
                'slh_total_working_days' => $anEmployee->total_work_day,
                'slh_month' => $month,
                'slh_year' => $salaryYear,
                'slh_cpf_contribution' => $anEmployee->cpf_contribution,
                'slh_saudi_tax' => $anEmployee->saudi_tax,
                'slh_company_contribution' => 0,
                'slh_iqama_advance' => $anEmployee->iqama_adv_inst_amount,
                'slh_other_advance' => $anEmployee->other_adv_inst_amount,
                'project_id' => $anEmployee->project_id,
                'multProject' => $anEmployee->work_multi_project,
                'slh_salary_date' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ]);
        } else {
            return $insert = SalaryHistory::insertGetId([
                'emp_auto_id' => $anEmployee->emp_auto_id,
                'basic_amount' => $anEmployee->basic_amount,
                'basic_hours' => $anEmployee->basic_hours,
                'house_rent' => $anEmployee->house_rent,
                'hourly_rent' => $anEmployee->hourly_rent,
                'mobile_allowance' => $anEmployee->mobile_allowance,
                'medical_allowance' => $anEmployee->medical_allowance,
                'local_travel_allowance' => $anEmployee->local_travel_allowance,
                'conveyance_allowance' => $anEmployee->conveyance_allowance,
                'food_allowance' => $anEmployee->food_allowance,
                'others' => $anEmployee->others1,
                'slh_total_overtime' => $anEmployee->overtime,
                'slh_overtime_amount' => $anEmployee->slh_overtime_amount,
                'slh_total_salary' => $anEmployee->gross_salary,
                'slh_total_hours' => $anEmployee->total_hours,
                'slh_total_working_days' => $anEmployee->total_work_day,
                'slh_month' => $month,
                'slh_year' => $salaryYear,
                'slh_cpf_contribution' => $anEmployee->cpf_contribution,
                'slh_saudi_tax' => $anEmployee->saudi_tax,
                'slh_company_contribution' => 0,
                'slh_iqama_advance' => $anEmployee->iqama_adv_inst_amount,
                'slh_other_advance' => $anEmployee->other_adv_inst_amount,
                'slh_salary_date' => Carbon::now(),
                'Status' => 0,
                'project_id' => $anEmployee->project_id,
                'multProject' => $anEmployee->work_multi_project,
                'created_at' => Carbon::now(),
            ]);
        }
    }


    // Employee Details Information With Monthly Working Record
    public static function getEmployeeDetailsWithMonthlyWorkRecord($month, $year, $empId)
    {
        if ($empId == 0 || $empId == null || $empId == '') {

            return  $allEmpSalary =  EmployeeInfo::where("employee_infos.job_status", 1)
                ->where('monthly_work_histories.month_id', $month)
                ->where('monthly_work_histories.year_id', $year)
                ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
                ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
                ->get()->all();
        } else {
            return  $allEmpSalary =  EmployeeInfo::where("employee_infos.job_status", 1)
                ->where("employee_infos.employee_id", $empId)
                ->where('monthly_work_histories.month_id', $month)
                ->where('monthly_work_histories.year_id', $year)
                ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
                ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
                ->first();
        }
    }

    public static function calculateFoodAllowance($totalWorkingDay, $monthlyFoodAllowance)
    {

        if ($monthlyFoodAllowance > 0) {
            /* -------------------------- */
            if ($totalWorkingDay >= 24) {
                return $monthlyFoodAllowance;
            } elseif ($totalWorkingDay >= 18) {
                return (($monthlyFoodAllowance / 30) * ($totalWorkingDay + 3));
            } elseif ($totalWorkingDay >= 12) {
                return (($monthlyFoodAllowance / 30) * ($totalWorkingDay + 2));
            } elseif ($totalWorkingDay >= 6) {
                return (($monthlyFoodAllowance / 30) * ($totalWorkingDay + 1));
            } else {
                return (($monthlyFoodAllowance / 30) * $totalWorkingDay);
            }
        } else
            return 0;
    }
    public static function calculateOvertimeHoursAndAmount($anEmployee)
    {
        $over_amount = 0;
        $total_amount = 0;
        if ($anEmployee->hourly_employee == true) {
            $over_amount = ($anEmployee->overtime * $anEmployee->hourly_rent);
            $total_amount = ($anEmployee->total_hours * $anEmployee->hourly_rent) + $over_amount;
        } elseif ($anEmployee->basic_hours > 0 && $anEmployee->basic_amount > 0) {
            $over_amount = ($anEmployee->overtime * ($anEmployee->hourly_rent * 1.5));
            $total_amount = (($anEmployee->basic_amount / 30) * $anEmployee->total_work_day)  + $over_amount;
        } elseif ($anEmployee->basic_amount > 0) {

            $anEmployee->allOthers = ($anEmployee->house_rent + $anEmployee->conveyance_allowance + $anEmployee->others1);
            $over_amount = ($anEmployee->overtime * $anEmployee->hourly_rent);
            $total_amount = (($anEmployee->basic_amount / 30)) * ($anEmployee->total_work_day) + $over_amount;
        }
        $anEmployee->slh_overtime_amount =  $over_amount;
        $anEmployee->tem_total_amount =  $total_amount;
        return $anEmployee;
    }

    public static function calculateEmployeeSalaryTotalAmount($projectId, $month, $salaryYear)
    {
        if ($projectId == null || $projectId <= 0) {
            return  $total_records = SalaryHistory::select(
                'Status',
                DB::raw("COUNT(slh_auto_id) as total_emp"),
                DB::raw("SUM(slh_total_salary) as total_salary"),
                DB::raw("SUM(slh_saudi_tax) as total_saudi_tax"),
                DB::raw("SUM(slh_iqama_advance) as total_iqama_adv"),
                DB::raw("SUM(slh_other_advance) as total_other_adv"),
                DB::raw("SUM(slh_cpf_contribution) as total_contribution")
            )->groupBy("Status")->where('slh_month', $month)->where('slh_year', $salaryYear)->get();
        } else {
            return $total_records = SalaryHistory::select(
                'Status',
                DB::raw("COUNT(slh_auto_id) as total_emp"),
                DB::raw("SUM(slh_total_salary) as total_salary"),
                DB::raw("SUM(slh_saudi_tax) as total_saudi_tax"),
                DB::raw("SUM(slh_iqama_advance) as total_iqama_adv"),
                DB::raw("SUM(slh_other_advance) as total_other_adv"),
                DB::raw("SUM(slh_cpf_contribution) as total_contribution")
            )->groupBy("Status")->where('slh_month', $month)->where('slh_year', $salaryYear)
                ->where('salary_histories.project_id', $projectId)->get();
        }
    }




    public static function updateEmployeeSalaryStatusAsPaid($slh_auto_id,)
    {
        return SalaryHistory::where('Status', 0)->where('slh_auto_id', $slh_auto_id)->update([
            'Status' => 1,
            'updated_at' => Carbon::now(),
        ]);
    }

    public static function updateEmployeeSalaryStatusAsUnPaid($slh_auto_id,)
    {
        return SalaryHistory::where('Status', 1)->where('slh_auto_id', $slh_auto_id)->update([
            'Status' => 0,
            'updated_at' => Carbon::now(),
        ]);
    }


    public static function processEmployeeSalaryList($SponsId, $proj_id, $fromMonth, $toMonth, $status)
    {
        if ($SponsId != 0 && $proj_id != 0) {
            return $pendingSalary = SalaryHistory::with('employee.category', 'month', 'employee.sponsor',  'employee.type', 'employee.project')
                ->where('salary_histories.Status', $status)
                ->where('sponsor_id', $SponsId)
                ->where('salary_histories.project_id', $proj_id)
                ->leftjoin('employee_infos', 'salary_histories.emp_auto_id', '=', 'employee_infos.emp_auto_id')
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->whereBetween('slh_month', [$fromMonth, $toMonth])
                ->orderBy('employee_id', 'ASC')
                ->get();
        } elseif ($SponsId != 0) {
            return $pendingSalary = SalaryHistory::with('employee.category', 'month', 'employee.sponsor',  'employee.type')
                ->where('salary_histories.Status', $status)
                ->where('sponsor_id', $SponsId)
                ->leftjoin('employee_infos', 'salary_histories.emp_auto_id', '=', 'employee_infos.emp_auto_id')
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->whereBetween('slh_month', [$fromMonth, $toMonth])
                ->orderBy('employee_id', 'ASC')
                ->get();
        } elseif ($proj_id != 0) {
            return $pendingSalary = SalaryHistory::with('employee.category', 'month', 'employee.sponsor', 'employee.type', 'employee.project')
                ->where('salary_histories.Status', $status)
                ->where('salary_histories.project_id', $proj_id)
                ->leftjoin('employee_infos', 'salary_histories.emp_auto_id', '=', 'employee_infos.emp_auto_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->whereBetween('slh_month', [$fromMonth, $toMonth])
                ->orderBy('employee_id', 'ASC')
                ->get();
        } else {

            return $pendingSalary = SalaryHistory::with('employee.category', 'month', 'employee.sponsor',  'employee.type')
                ->where('salary_histories.Status', $status)
                ->leftjoin('employee_infos', 'salary_histories.emp_auto_id', '=', 'employee_infos.emp_auto_id')
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->whereBetween('slh_month', [$fromMonth, $toMonth])
                ->orderBy('employee_id', 'ASC')
                ->get();
        }
    }


    public static function getEmployeeSalaryHistory($projectId, $sponserId, $month, $salaryYear, $salaryStatus)
    {

        if ($projectId  <= 0 && $sponserId <= 0) {
            return $salaryReport = EmployeeInfo::where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        } else if ($projectId  <= 0 && $sponserId > 0) {
            return $salaryReport = EmployeeInfo::where("employee_infos.sponsor_id", $sponserId)
                ->where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        } else if ($projectId  > 0 && $sponserId <= 0) {
            return $salaryReport = EmployeeInfo::where('salary_histories.project_id', $projectId)
                ->where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        } else if ($projectId  > 0 && $sponserId > 0) {
            return $salaryReport = EmployeeInfo::where("employee_infos.sponsor_id", $sponserId)
                ->where('salary_histories.project_id', $projectId)
                ->where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        }
    }

    public static function getEmployeeSalaryHistoryWithProjectAndEmployeeType($projectId, $empType, $isHourlyEmp, $month, $salaryYear, $salaryStatus)
    {

        if ($projectId  > 0 && $empType > 0) {
            return $salaryReport = EmployeeInfo::where("salary_histories.project_id", $projectId)
                ->where('employee_infos.emp_type_id', $empType)
                ->where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->where('employee_infos.hourly_employee', $isHourlyEmp)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        } else if ($empType > 0) {
            return $salaryReport = EmployeeInfo::where('employee_infos.emp_type_id', $empType)
                ->where('salary_histories.slh_month', $month)
                ->where('salary_histories.slh_year', $salaryYear)
                ->where('salary_histories.Status', $salaryStatus)
                ->where('employee_infos.hourly_employee', $isHourlyEmp)
                ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
                ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
                ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
                ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
                ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
                ->orderBy('employee_id')
                ->get();
        }
    }

    public static function getEmployeeSalaryHistoryWithProjectAndEmployeeJobStatus($projectId, $empStatusId, $month, $salaryYear, $salaryStatus)
    {

        return   $salaryReport = EmployeeInfo::where("salary_histories.project_id", $projectId)
            ->where('employee_infos.job_status', $empStatusId)
            ->where('salary_histories.slh_month', $month)
            ->where('salary_histories.slh_year', $salaryYear)
            ->where('salary_histories.Status', $salaryStatus)
            ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
            ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
            ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
            ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
            ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
            ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
            ->orderBy('employee_id')
            ->get();
    }

    public static function getMultipleEmployeeIdBaseSalaryHistory($allEmplId, $month, $salaryYear, $salaryStatus)
    {
        return  $salaryReport = EmployeeInfo::where('salary_histories.slh_month', $month)
            ->where('salary_histories.slh_year', $salaryYear)
            // ->where('salary_histories.Status', $salaryStatus)
            ->whereIn('employee_infos.employee_id', $allEmplId)
            ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
            ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
            ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
            ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
            ->leftjoin('salary_histories', 'employee_infos.emp_auto_id', '=', 'salary_histories.emp_auto_id')
            ->orderBy('employee_id')
            ->get();
    }
}
