<?php

namespace App\Http\Controllers\DataServices;

use App\Models\BannerInfo;
use App\Models\CompanyProfile;
use App\Models\Currency;
use App\Models\Month;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CompanyDataService
{
    /*
     ========================================================================== 
     =============================Available Currency ========================== 
     ========================================================================== 
    */

    public function getAvailableCurrency()
    {
        return $all = Currency::get();
    }

    /*
     ========================================================================== 
     ============================= Company Profile ============================ 
     ========================================================================== 
    */
    public function getCompanyProfileInformation()
    {
        return $all = CompanyProfile::all();
    }

    public function findCompanryProfile()
    {
        $profile = CompanyProfile::where('comp_id', 1)->firstOrFail();
        if ($profile == null) {
            $profile = new CompanyProfile();
        }
        return $profile;
    }


    public function insertCompanyInformation($req)
    {
        return $update = CompanyProfile::where('comp_id', 1)->update([
            'comp_name_en' => $req->comp_name_en,
            'comp_name_arb' => $req->comp_name_arb,
            'curc_id' => $req->curc_id,
            'comp_address' => $req->comp_address,
            'comp_email1' => $req->comp_email1,
            'comp_email2' => $req->comp_email2,
            'comp_phone1' => $req->comp_phone1,
            'comp_phone2' => $req->comp_phone2,
            'comp_mobile1' => $req->comp_mobile1,
            'comp_mobile2' => $req->comp_mobile2,
            'comp_support_number' => $req->comp_support_number,
            'comp_hotline_number' => $req->comp_hotline_number,
            'comp_description' => $req->comp_description,
            'comp_mission' => $req->comp_mission,
            'comp_vission' => $req->comp_vission,
            'comp_contact_address' => $req->comp_contact_address,
            'updated_at' => Carbon::now(),
        ]);
    }





    /*
     ========================================================================== 
     ============================= Website Banner ============================= 
     ========================================================================== 
    */

    public function getBannersInformation()
    {
        return $all = BannerInfo::where('status', 1)->get();
    }

    public function findBannerInformation($id)
    {
        return $find = BannerInfo::where('status', 1)->where('ban_id', $id)->first();
    }


    public function insertNewBanner($req,)
    {
        $insert = BannerInfo::insert([
            'ban_title' => $req->ban_title,
            'ban_subtitle' => $req->ban_subtitle,
            'ban_caption' => $req->ban_caption,
            'ban_description' => $req->ban_description,
            'company_id' => $req->company_id,
            'entered_id' => Auth::user()->id,
            'ban_image' => $banner_image_name,
            'created_at' => Carbon::now(),
        ]);
    }


    public function deleteBanner($ban_id)
    {
        return $delete = BannerInfo::where('ban_id', $ban_id)->update([
            'status' => 0,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function updateBannerInformation($ban_id, $ban_title, $ban_subtitle, $ban_caption, $ban_description, $company_id)
    {
        return $update = BannerInfo::where('status', 1)->where('ban_id', $ban_id)->update([
            'ban_title' => $ban_title,
            'ban_subtitle' => $ban_subtitle,
            'ban_caption' => $ban_caption,
            'ban_description' => $ban_description,
            'company_id' => $company_id,
            'entered_id' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);
    }
    public function updateBannerInformationWithBannerImage($ban_id, $ban_title, $ban_subtitle, $ban_caption, $ban_description, $company_id, $banner_image_name)
    {
        return $update = BannerInfo::where('status', 1)->where('ban_id', $ban_id)->update([
            'ban_title' => $ban_title,
            'ban_subtitle' => $ban_subtitle,
            'ban_caption' => $ban_caption,
            'ban_description' => $ban_description,
            'company_id' => $company_id,
            'ban_image' => $banner_image_name,
            'entered_id' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);
    }

    /*
     ========================================================================== 
     ============================= Month ====================================== 
     ========================================================================== 
    */

    public function getAllMonth()
    {
        return $all = Month::get();
    }
    public function getMonthById($id)
    {
        if ($id > 12)
            return null;
        return $all = Month::where('month_id', $id)->first();
    }


    /*
     ========================================================================== 
     ============================= Country ==================================== 
     ========================================================================== 
    */
}
