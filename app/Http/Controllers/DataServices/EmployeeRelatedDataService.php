<?php

namespace App\Http\Controllers\DataServices;

use App\Models\Country;
use App\Models\Division;

use App\Models\Department;
use App\Models\District;
use App\Models\EmployeeCategory;
use App\Models\EmployeeProjectHistory;
use App\Models\EmployeeStatusRecord;
use App\Models\EmployeeType;
use App\Models\EmpProjectHistory;
use App\Models\JobStatus;
use App\Models\ProjectImgUpload;
use App\Models\ProjectInfo;
use App\Models\Sponsor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EmployeeRelatedDataService
{

    /*
     ========================================================================== 
     ============================= Country ==================================== 
     ========================================================================== 
    */
    public function insertNewCountry($country_name)
    {
        return Country::insert([
            'country_name' => $country_name,
        ]);
    }
    public function getAllCountry()
    {
        return $allCountries = Country::get();
    }

    /*
     ========================================================================== 
     ============================= Department  ================================
     ========================================================================== 
    */
    public function getAllDepartment()
    {
        return $all = Department::get();
    }


    /*
     ========================================================================== 
     ============================= Division  ================================== 
     ========================================================================== 
    */
    public function insertNewDivision($country_id, $division_name)
    {
        if ($this->checkDivisionIsExist($division_name, $country_id) == false) {
            return  $insert = Division::insert([
                'country_id' => $country_id,
                'division_name' => $division_name,
            ]);
        }
        return false;
    }
    public function getDivisions($division_id)
    {
        if ($division_id == null) {
            return  Division::get();
        } else {
            return Division::where('division_id', $division_id)->first();
        }
    }
    public function getDivisionByCountryId($country_id)
    {
        return Division::where('country_id', $country_id)->orderBy('division_name', 'ASC')->get();
    }

    public function checkDivisionIsExist($division_name, $country_id)
    {

        $count = Division::where('division_name', $division_name)->where('country_id', $country_id)->count();
        return $count > 0 ? true : false;
    }


    public function updateDivision($divId, $country_id, $division_name)
    {
        return $update = Division::where('division_id', $divId)->update([
            'country_id' => $country_id,
            'division_name' => $division_name,
            'updated_at' => Carbon::now(),
        ]);
    }




    /*
     ========================================================================== 
     ============================= District =================================== 
     ========================================================================== 
    */
    public function getDistricts($id)
    {
        if ($id == null) {
            return District::orderBy('district_id', 'DESC')->get();
        } else {
            return District::where('district_id', $id)->orderBy('district_id', 'DESC')->get();
        }
    }

    public function getDistrictByDivisionId($division_id)
    {
        return District::where('division_id', $division_id)->orderBy('district_name', 'ASC')->get();
    }
    public function checkDistrictIsExist($district_name, $division_id, $country_id)
    {

        $count = District::where('country_id', $country_id)->where('division_id', $division_id)->where('district_name', $district_name)->count();
        return $count > 0 ? true : false;
    }

    public function insertNewDistrict($district_name, $division_id, $country_id)
    {
        return District::insert([
            'country_id' => $country_id,
            'division_id' => $division_id,
            'district_name' => $district_name,
        ]);
    }
    public function updateDistrict($district_id, $district_name, $division_id, $country_id)
    {
        return District::where('district_id', $district_id)->update([
            'country_id' => $country_id,
            'division_id' => $division_id,
            'district_name' => $district_name,
        ]);
    }


    /*
     ========================================================================== 
     ============================= Employee Type ============================== 
     ========================================================================== 
    */

    public function getEmployeeJobStatusTitle($empStatusId)
    {
        return JobStatus::where('id', $empStatusId)->first()->title;
    }

    /*
     ========================================================================== 
     ============================= Employee Type ============================== 
     ========================================================================== 
    */

    public function getAnEmployeeType($id)
    {
        return  EmployeeType::where('id', $id)->get();
    }
    public function getAnEmployeeTypeName($id)
    {
        return  EmployeeType::where('id', $id)->pluck('name');
    }

    public function getAllEmployeeType()
    {
        return $all = EmployeeType::get();
    }


    /*
     ========================================================================== 
     ============================= Employee Category=========================== 
     ========================================================================== 
    */

    public function getEmployeeAllCategory()
    {
        return $all = EmployeeCategory::where('catg_status', 1)->orderBy('catg_id', 'DESC')->get();
    }

    public function getEmployeeCategory($id)
    {
        return $edit = EmployeeCategory::where('catg_status', 1)->where('catg_id', $id)->first();
    }
    public function insertNewCategoryName($catg_name, $emp_type_id)
    {
        return EmployeeCategory::insert([
            'catg_name' => $catg_name,
            'emp_type_id' => $emp_type_id,
            'created_at' => Carbon::now(),
        ]);
    }
    public function updateCategoryName($id, $catg_name, $emp_type_id)
    {
        return EmployeeCategory::where('catg_id', $id)->update([
            'catg_name' => $catg_name,
            'emp_type_id' => $emp_type_id,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function deleteCategory($id)
    {
        return EmployeeCategory::where('catg_id', $id)->update([
            'catg_status' => 0,
            'updated_at' => Carbon::now(),
        ]);
    }

    /*
     ========================================================================== 
     ============================= Employee Sponser ============================ 
     ========================================================================== 
    */
    public function getAllSponser()
    {
        return $all = Sponsor::where('status', 1)->orderBy('spons_id', 'DESC')->get();
    }

    public function findASponser($id)
    {
        return $edit = Sponsor::where('status', 1)->where('spons_id', $id)->first();
    }

    public function insertNewSponerName($spons_name,)
    {
        return Sponsor::insert([
            'spons_name' => $spons_name,
            'create_by_id' => Auth::user()->id, // $creator,
            'created_at' => Carbon::now(),
        ]);
    }
    public function updateSponerName($spons_id, $spons_name)
    {
        return Sponsor::where('spons_id', $spons_id)->update([
            'spons_name' => $spons_name,
            'create_by_id' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);
    }
    public function deleteSponerName($spons_id)
    {
        return Sponsor::where('spons_id', $spons_id)->update([
            'status' => 0,
            'updated_at' => Carbon::now(),
        ]);
    }

    /*
     ========================================================================== 
     ============================= Company Project ============================ 
     ========================================================================== 
    */


    public function getAllProjectInformation()
    {
        return $all = ProjectInfo::where('status', 1)->orderBy('proj_id', 'DESC')->get();
    }
    /* GET FIND Project */
    public function findAProjectInformation($proj_id)
    {
        return $find = ProjectInfo::where('status', 1)->where('proj_id', $proj_id)->first();
    }
    /* GET Project MULTIPLE IMAGE */
    public function getProjectMultipleImage($project_id)
    {
        return ProjectImgUpload::where('project_id', $project_id)->get();
    }

    public function deleteProject($proj_id)
    {
        return ProjectInfo::where('proj_id', $proj_id)->update([
            'status' => 0,
            'updated_at' => Carbon::now(),
        ]);
    }
    public function insertNewProjectInformation($proj_name, $starting_date, $address, $proj_code, $proj_budget, $proj_deadling, $proj_description, $proj_main_thumb_Path)
    {
        return ProjectInfo::insert([
            'proj_name' => $proj_name,
            'starting_date' => $starting_date,
            'address' => $address,
            'proj_code' => $proj_code,
            'proj_budget' => $proj_budget,
            'proj_deadling' => $proj_deadling,
            'proj_description' => $proj_description,
            'proj_main_thumb' => $proj_main_thumb_Path,
            'created_at' => Carbon::now(),
        ]);
    }
    public function updateProjectInformation($proj_id, $proj_name, $starting_date, $address, $proj_code, $proj_budget, $proj_deadling, $proj_description)
    {
        return ProjectInfo::where('proj_id', $proj_id)->update([
            'proj_name' => $proj_name,
            'starting_date' => $starting_date,
            'address' => $address,
            'proj_code' => $proj_code,
            'proj_budget' => $proj_budget,
            'proj_deadling' => $proj_deadling,
            'proj_description' => $proj_description,
            'updated_at' => Carbon::now(),
        ]);
    }

    public function assignProjectIncharge($proj_id, $emp_id)
    {
        return ProjectInfo::where('proj_id', $proj_id)->update([
            'proj_Incharge_id' => $emp_id,
            'updated_at' => Carbon::now()
        ]);
    }


    /*
     ========================================================================== 
     ================ Employee Assign to New Project ==========================
     ========================================================================== 
    */
    public function assignEmployeeToNewProject($emp_auto_id, $project_id, $created_by)
    {

        return  EmpProjectHistory::insert([
            'emp_id' => $emp_auto_id,
            'project_id' => $project_id,
            'asigned_date' => Carbon::now(),
            'create_by_id' => $created_by,
            'created_at' => Carbon::now(),
        ]);
    }
    public function assignEmployeeToNewProject1($emp_auto_id, $project_id, $asigned_date, $relesed_date, $created_by)
    {
        return $insert = EmployeeProjectHistory::insertGetId([
            'emp_id' => $emp_auto_id,
            'project_id' => $project_id,
            'asigned_date' => $asigned_date,
            'relesed_date' => $relesed_date,
            'create_by_id' => $created_by,
            'created_at' => Carbon::now(),
        ]);
    }


    public function insertEmployeeJobStatusUpdateRecord($emp_Auto_id,$date,$job_status,){
       return $insert = EmployeeStatusRecord::insertGetId([
            'emp_id' => $emp_Auto_id,
            'date' => $date,
            'job_status' => $job_status,
            'create_by' => Auth::user()->id,
            'created_at' => Carbon::now(),
          ]);
    }
}
