<?php

namespace App\Http\Controllers\admin\Bill_Voucher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\CompanyProfileController; 
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use App\Models\InvoiceRecordDetails;
use App\Models\InvoiceRecords;
use Salla\ZATCA\GenerateQrCode;
use Salla\ZATCA\Tags\InvoiceDate;
use Salla\ZATCA\Tags\InvoiceTaxAmount;
use Salla\ZATCA\Tags\InvoiceTotalAmount;
use Salla\ZATCA\Tags\Seller;
use Salla\ZATCA\Tags\TaxNumber;
use Carbon\Carbon;
use Auth;
use Cart;
 

class BillVoucherController extends Controller
{

  // middleware
  function __construct()
  {
    $this->middleware('permission:evoucher-list', ['only' => ['eVoucher', 'eVoucherProccess']]);
  }
  // middleware

  public function addToCart(Request $request)
  {

    $id =  uniqid();
    Cart::add([
      'id' => $id,
      'name' => $request->cartDescription,
      'qty' =>  $request->cartQuantity,
      'price' => $request->cartRate,
      'weight' => 1,
      'options' => [
        'qty_unit' => $request->qty_unit,
        'cartVat' => 0, //  $request->cartVat,
        'cartTotal' =>  $request->cartTotal
      ]
    ]);
    $carts = Cart::content();

    return response()->json(['success' => 'Successfully Added Cart On Item']);
  }


  public function getCartInfo()
  {
    $carts = Cart::content();
    $cartQty = Cart::count();
    $cartTotal = Cart::total();
    return response()->json(array(
      'cartContect' => $carts,
      'cartQty' => $cartQty,
      'cartTotal' => $cartTotal,
    ));
  }


  public function removeCartInfo(Request $request)
  {
   
    Cart::remove($request->rowId);
    return response()->json(['success' => 'Successfully Remove The Item']);
  }

  public function saveNewInvoiceDetailsRecord($carts, $invoiceRecordId)
  {

    foreach ($carts  as $item) {

      $invRecord = new InvoiceRecordDetails();
      $invRecord->invoice_record_id = $invoiceRecordId;
      $invRecord->items_details = $item->name;
      $invRecord->percent_of_retention = 0; // $item->percent_of_retention;
      $invRecord->percent_of_vat = 0; // $item->options->cartVat;
      $invRecord->quantity = $item->qty;
      $invRecord->rate = $item->price;
      $invRecord->total = $item->options->cartTotal;
      $invRecord->save();
    }
  }

  public function getInvoiceRecordDetilaByInvoiceRecordId($invoice_record_id)
  {
    return InvoiceRecordDetails::where('invoice_record_id', $invoice_record_id)->get();
  }
  public function getInvoiceRecordById($invoiceRecordId)
  {
    return InvoiceRecords::where('invoice_record_auto_id', $invoiceRecordId)->first();
  }


  // E-Voucher FORM Submit for Create Bill With QRCode 
  public function eVoucherProccess(Request $request)
  {

    $companyOBJ = new CompanyProfileController();
    $company = $companyOBJ->findCompanry();
    $entered_id = Auth::user()->id;
    $todayDate = $request->voucher_date;

    $carts = Cart::content();
    $cartQty = Cart::count();
    $cartTotal = Cart::total();


    $invoiceNo = $request->invoice_no;
    $mainContEn = $request->main_contractor_en;
    $mainContArb = $request->main_contractor_rb;
    $mainContVatNo = $request->main_con_vat_no;
    $subContEn = $request->sub_contractor_en;
    $subContArb = $request->sub_contractor_rb;
    $subContVatNo = $request->sub_con_vat_no;
    $percent_of_vat = $request->vat; // percent_of_vat
    $total_vat = $request->vat_total;
    $percent_of_retention = $request->retention; // percent_of_vat
    $total_retention = $request->retention_total;
    $total_with_vat = $request->total_with_vat;
    $remarks = $request->remarks ?? ' ';
    $projectId = $request->project_id;
    $submittedDate = $request->submitted_date;
    $invoiceStatus = $request->invoice_status;
    $submittedEmpId = $request->employee_id;


    $invoiceRecordId = InvoiceRecords::insertGetId([
      'income_sources_id' => 2,
      'invoice_no' => $invoiceNo,
      'main_contractor_en' => $mainContEn,
      'main_contractor_arb' => $mainContArb,
      'main_con_vat_no' => $mainContVatNo,
      'sub_contractor_en' => $subContEn,
      'sub_contractor_arb' => $subContArb,
      'sub_con_vat_no' => $subContVatNo,
      'percent_of_vat' => $percent_of_vat,
      'total_vat' => $total_vat,
      'percent_of_retention' => $percent_of_retention,
      'total_retention' => $total_retention,
      'total_amount' => $total_with_vat,
      'remarks'  => $remarks,
      'project_id' => $projectId,
      'submitted_date' => $submittedDate,
      'entered_by_id' => $submittedEmpId,
      'invoice_status_id' => $invoiceStatus,
      'status' => true,
      'created_at' => Carbon::now(),
    ]);

    $this->saveNewInvoiceDetailsRecord($carts, $invoiceRecordId);

    $invoiceRecord = $this->getInvoiceRecordById($invoiceRecordId);
    $invoiceRecordDetails = $this->getInvoiceRecordDetilaByInvoiceRecordId($invoiceRecordId);
    $qrCoded = $this->createBillVoucherQRCode($subContEn, $subContVatNo, $submittedDate, $total_with_vat, $total_vat);
    Cart::destroy(); // reset cart list
    return view('admin.bill_voucher.invoice_with_qr', compact('company', 'qrCoded', 'invoiceRecord', 'invoiceRecordDetails'));
  }
  public function createBillVoucherQRCode($subContrEn, $subVat, $date, $totalWithVat, $totalVat)
  {
    return  GenerateQrCode::fromArray([
      new Seller($subContrEn), // seller name
      new TaxNumber($subVat), // seller tax number
      new InvoiceDate($date), // invoice date as Zulu ISO8601 @see https://en.wikipedia.org/wiki/ISO_8601
      new InvoiceTotalAmount($totalWithVat), // invoice total amount
      new InvoiceTaxAmount($totalVat) // invoice tax amount
    ])->render();
  }

  public function showQRCodeBillVourcher($invoiceRecordId)
  {
    $company = (new CompanyProfileController())->findCompanry();
    $invoiceRecord = $this->getInvoiceRecordById($invoiceRecordId);
    $invoiceRecordDetails = $this->getInvoiceRecordDetilaByInvoiceRecordId($invoiceRecordId);
    $invoiceRecord = $this->getInvoiceRecordById($invoiceRecordId);
    $qrCoded = $this->createBillVoucherQRCode($invoiceRecord->sub_contractor_en, $invoiceRecord->sub_con_vat_no, $invoiceRecord->submitted_date, $invoiceRecord->total_amount, $invoiceRecord->total_vat);

    return view('admin.bill_voucher.invoice_with_qr', compact('company', 'qrCoded', 'invoiceRecord', 'invoiceRecordDetails'));
  }




  // ======================= blade ============================

  public function BillVoucherFormLoad()
  {

    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
     
    $employee =  (new EmployeeDataService())->getEmployeeInformationForDropdownList();
    return view('admin.bill_voucher.index', compact('project', 'employee'));
  }

  public function eVoucher()
  {

    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    
    $employee =  (new EmployeeDataService())->getEmployeeInformationForDropdownList();

    return view('admin.bill_voucher.index', compact('project', 'employee'));
  }



  // ======================= Submited Bill Voucher ============================

  public function submitedBillVoucherUi()
  {
    $billVoucher = InvoiceRecords::where('invoice_records.status', 1)
      ->leftjoin('employee_infos', 'invoice_records.entered_by_id', '=', 'employee_infos.emp_auto_id')
      ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
      ->get();
    return view('admin.bill_voucher.submited_bill_voucher_ui', compact('billVoucher'));
  }
}
