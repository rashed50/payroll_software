<?php

namespace App\Http\Controllers\Admin\Vehicle;

use App\Http\Controllers\Admin\AdvancePayController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\EmployeeInfoController;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\EmployeeInfo;
use App\Models\SalaryDetails;
use App\Models\VehicleFineRecord;
use Carbon\Carbon;
use Session;
use Auth;

class VehicleController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  |  DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  public function getAll()
  {
    return $all = Vehicle::where('status', 1)->orderBy('veh_id', 'DESC')->get();
  }
  public function findVehicle($id)
  {
    return $edit = Vehicle::where('status', 1)->where('veh_id', $id)->first();
  }

  public function findVehicleFineRecord($id)
  {
    return VehicleFineRecord::where('vfr_auto_id', $id)->first();
  }


  public function delete($id)
  {
    $delete = Vehicle::where('status', 1)->where('veh_id', $id)->update([
      'status' => 0,
      'updated_at' => Carbon::now(),
    ]);
    /* redirect back */
    if ($delete) {
      Session::flash('delete', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  public function insert(Request $request)
  {
    $emp_id = $request->emp_id;

    $findEmp = EmployeeInfo::where('employee_id', $emp_id)->first();




    $crator = Auth::user()->id;



    if ($findEmp) {
      // insert data in database
      $insert = Vehicle::insert([
        'veh_name' => $request->veh_name,
        'veh_plate_number' => $request->veh_plate_number,
        'veh_model_number' => $request->veh_model_number,
        'veh_brand_name' => $request->veh_brand_name,
        'veh_insurrance_date' => $request->veh_insurrance_date,
        'veh_price' => $request->veh_price,
        'veh_purchase_date' => $request->veh_purchase_date,
        'veh_present_metar' => $request->veh_present_metar,
        'veh_color' => $request->veh_color,
        'driver_id' => $findEmp->emp_auto_id,
        'remarks' => $request->remarks,
        'create_by_id' => $crator,
        'created_at' => Carbon::now(),
      ]);
      /* redirect back */
      if ($insert) {
        Session::flash('success', 'value');
        return redirect()->back();
      } else {
        Session::flash('error', 'value');
        return redirect()->back();
      }
    } else {
      // insert data in database
      $insert = Vehicle::insert([
        'veh_name' => $request->veh_name,
        'veh_plate_number' => $request->veh_plate_number,
        'veh_model_number' => $request->veh_model_number,
        'veh_brand_name' => $request->veh_brand_name,
        'veh_insurrance_date' => $request->veh_insurrance_date,
        'veh_price' => $request->veh_price,
        'veh_purchase_date' => $request->veh_purchase_date,
        'veh_present_metar' => $request->veh_present_metar,
        'veh_color' => $request->veh_color,
        'driver_id' => 0,
        'remarks' => $request->remarks,
        'create_by_id' => $crator,
        'created_at' => Carbon::now(),
      ]);
      /* redirect back */
      if ($insert) {
        Session::flash('success', 'value');
        return redirect()->back();
      } else {
        Session::flash('error', 'value');
        return redirect()->back();
      }
    }
  }

  public function update(Request $request)
  {
    $emp_id = $request->emp_id;
    $id = $request->id;
    $crator = Auth::user()->id;

    $findEmp = EmployeeInfo::where('employee_id', $emp_id)->first();

    if ($emp_id == "") {
      // insert data in database
      $update = Vehicle::where('veh_id', $id)->update([
        'veh_name' => $request->veh_name,
        'veh_plate_number' => $request->veh_plate_number,
        'veh_model_number' => $request->veh_model_number,
        'veh_brand_name' => $request->veh_brand_name,
        'veh_insurrance_date' => $request->veh_insurrance_date,
        'veh_price' => $request->veh_price,
        'veh_purchase_date' => $request->veh_purchase_date,
        'veh_present_metar' => $request->veh_present_metar,
        'veh_color' => $request->veh_color,
        'driver_id' => 0,
        'remarks' => $request->remarks,
        'create_by_id' => $crator,
        'updated_at' => Carbon::now(),
      ]);
      /* redirect back */
      if ($update) {
        Session::flash('success_update', 'value');
        return redirect()->route('add-new.vehicle');
      } else {
        Session::flash('error', 'value');
        return redirect()->back();
      }
    } else {
      if ($findEmp) {
        // insert data in database
        $update = Vehicle::where('veh_id', $id)->update([
          'veh_name' => $request->veh_name,
          'veh_plate_number' => $request->veh_plate_number,
          'veh_model_number' => $request->veh_model_number,
          'veh_brand_name' => $request->veh_brand_name,
          'veh_insurrance_date' => $request->veh_insurrance_date,
          'veh_price' => $request->veh_price,
          'veh_purchase_date' => $request->veh_purchase_date,
          'veh_present_metar' => $request->veh_present_metar,
          'veh_color' => $request->veh_color,
          'driver_id' => $findEmp->emp_auto_id,
          'remarks' => $request->remarks,
          'create_by_id' => $crator,
          'updated_at' => Carbon::now(),
        ]);
        /* redirect back */
        if ($update) {
          Session::flash('success_update', 'value');
          return redirect()->route('add-new.vehicle');
        } else {
          Session::flash('error', 'value');
          return redirect()->back();
        }
      } else {
        Session::flash('data_not_match', 'value');
        return redirect()->back();
      }
    }
  }


  public function vehicleFineFormWithRecords()
  {
    $vehicleFineRecord = VehicleFineRecord::where('vehicle_fine_records.status', 1)
      ->join('vehicles', 'vehicle_fine_records.veh_id', '=', 'vehicles.veh_id')
      ->join('employee_infos', 'vehicle_fine_records.employee_id', '=', 'employee_infos.emp_auto_id')
      ->orderBy('vfr_auto_id', 'DESC')->get();
    return view('admin.vechicle.vechicle_fine', compact('vehicleFineRecord'));
  }


  public function searchVehicleInformation(Request $request)
  {

    $vehicle = Vehicle::where('veh_plate_number', $request->value)
      ->join('employee_infos', 'vehicles.driver_id', '=', 'employee_infos.emp_auto_id')
      ->firstOrFail();

    if ($vehicle) {
      return  response()->json(['data' => $vehicle, 'success' => 'true', 'status_code' => 200]);
    } else {
      return response()->json(['success' => 'false', 'status_code' => '401', 'error' => 'error', 'message' => $validator->errors()]);
    }
  }

  /*
  |--------------------------------------------------------------------------
  |  Vehicle Fine OPERATION
  |--------------------------------------------------------------------------
  */

  public function editVehicleFineFormSubmit($id)
  {
    $fineRecord = VehicleFineRecord::where('status', 1)->where('vfr_auto_id', $id)->firstOrFail();
    return view('admin.vechicle.vechicle_fine_edit', compact('fineRecord'));
  }
  public function insertVehicleFineFormSubmit(Request $request)
  {
    $creator = Auth::user()->id;
    $insert = VehicleFineRecord::insertGetId([
      'employee_id' => $request['emp_auto_id'],
      'veh_id' => $request['veh_id'],
      'date' => $request['date'],
      'amount' => $request['amount'],
      'remarks' => $request['remarks'],
      'entered_id' => $creator,
      'created_at' => Carbon::now()->toDateTimeString(),
    ]);
    $amount = $request['amount'];
    // insert fine amount as advance pay to that employee
    $advPurposeType = 5;  // Vehicle Penalty Fee
    $installMonth = 1;
    $insert =  (new AdvancePayController())->insertEmployeeAdvance($request['emp_auto_id'], $advPurposeType, $amount, $installMonth, $request['remarks'], $request['date'], $creator);

    $others = (new AdvancePayController())->updateEmployeeOthersAdvanceDeductionAmount($request['emp_auto_id'], $amount);


    if ($insert) {
      Session::flash('success', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }
  public function updateVehicleFineFormSubmit(Request $request)
  {
    $id = $request['id'];
    $creator = Auth::user()->id;
    $update = VehicleFineRecord::where('status', 1)->where('vfr_auto_id', $id)->update([
      'employee_id' => $request['emp_auto_id'],
      'veh_id' => $request['veh_id'],
      'date' => $request['date'],
      'amount' => $request['amount'],
      'remarks' => $request['remarks'],
      'entered_id' => $creator,
      'updated_at' => Carbon::now()->toDateTimeString(),
    ]);

    if ($update) {
      Session::flash('success', 'value');
      return redirect()->route('add.vehicle.fine');
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }
  public function deleteVehicleFineForm($id)
  {

    $record = $this->findVehicleFineRecord($id);
    $advPurposeTypeId = 5;
    $advanceRecord = (new AdvancePayController())->findLastAdvanceRecordByEmpAutoIdAndAdvancePurposeId($record->employee_id, $advPurposeTypeId);
    //  dd($advanceRecord);
    if ($advanceRecord->adv_amount == $record->amount) {

      $delete = VehicleFineRecord::where('status', 1)->where('vfr_auto_id', $id)->update([
        'status' => 0
      ]);
      $insert =  (new AdvancePayController())->deleteAdvancePaymentRecord($advanceRecord->id);
      $others = (new AdvancePayController())->updateEmployeeOthersAdvanceDeductionAmount($record->employee_id, -1 * $record->amount);
      if ($delete) {
        Session::flash('delete_success', 'value');
        return redirect()->back();
      } else {
        Session::flash('delete_error', 'value');
        return redirect()->back();
      }
    } else {
      Session::flash('delete_error', 'value');
      return redirect()->back();
    }
  }















  /*
  |--------------------------------------------------------------------------
  |  BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index()
  {
    $all = $this->getAll();
    return view('admin.vechicle.all', compact('all'));
  }

  public function edit($id)
  {
    $edit = $this->findVehicle($id);
    return view('admin.vechicle.edit', compact('edit'));
  }


















  /*
  |--------------------------------------------------------------------------
  |  API OPERATION
  |--------------------------------------------------------------------------
  */

  /* _____________________________ === _____________________________ */
}
