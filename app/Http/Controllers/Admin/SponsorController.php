<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use Illuminate\Http\Request;
use App\Models\EmployeeInfo;
use Illuminate\Support\Facades\Session;
//use Image;

class SponsorController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    |  DATABASE OPERATION
    |--------------------------------------------------------------------------
    */
  // Permision
  function __construct()
  {
    $this->middleware('permission:sponser-list', ['only' => ['index']]);
    $this->middleware('permission:sponser-create', ['only' => ['index', 'insert']]);
    $this->middleware('permission:sponser-edit', ['only' => ['edit', 'update']]);
    $this->middleware('permission:sponser-delete', ['only' => ['delete']]);
  }
  // Permision
  public function getAll()
  {
    return $all =  (new EmployeeRelatedDataService())->getAllSponser();
  }

  public function findSponser($id)
  {
    return $edit =  (new EmployeeRelatedDataService())->findASponser($id);
  }

  /* Dhaka Insert */
  public function insert(Request $request)
  {

    /* form validation */
    $this->validate($request, [
      'spons_name' => 'required',
    ], [
      'spons_name.required' => 'You Must Be Input This Field!',
    ]);
    /* insert data in database */
    $insert = (new EmployeeRelatedDataService())->insertNewSponerName($request->spons_name);

    /* redirect back */
    if ($insert) {
      Session::flash('success', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  public function update(Request $request)
  {

    $id = $request->id;
    /* form validation */
    $this->validate($request, [
      'spons_name' => 'required',
    ], [
      'spons_name.required' => 'You Must Be Input This Field!',
    ]);
    /* insert data in database */
    $update = (new EmployeeRelatedDataService())->updateSponerName($id, $request->spons_name);

    /* redirect back */
    if ($update) {
      Session::flash('success_update', 'value');
      return redirect()->route('add-sponser');
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  public function delete($id)
  {
    $updateStatus = (new EmployeeRelatedDataService())->deleteSponerName($id);

    /* redirect back */
    if ($updateStatus) {
      Session::flash('delete', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }
  /* ==================== Report Process ==================== */
  public function reportProcess(Request $request)
  {
    $sponserId = $request->spons_id;
    /* company profile call */
    $companyOBJ = new CompanyProfileController();
    $company = $companyOBJ->findCompanry();
    /* sponer wise employee find */
    $sponserWiseEmployee = (new EmployeeDataService())->getEmployeeListWithProjectAndSponsor(0,$sponserId);
    // EmployeeInfo::where('sponsor_id', $sponserId)->get();
    // dd($sponserWiseEmployee);
    return view('admin.sponser.report_process', compact('company', 'sponserWiseEmployee'));
  }


  /*
    |--------------------------------------------------------------------------
    |  BLADE OPERATION
    |--------------------------------------------------------------------------
    */
  public function index()
  {
    $all = $this->getAll();
    return view('admin.sponser.all', compact('all'));
  }

  public function edit($id)
  {
    $edit = $this->findSponser($id);
    return view('admin.sponser.edit', compact('edit'));
  }

  // public function report(){
  //   $sponser = $this->getAll();
  //   return view('admin.sponser.report',compact('sponser'));
  // }








  /*
    |--------------------------------------------------------------------------
    |  API OPERATION
    |--------------------------------------------------------------------------
    */



  /* _____________________________ === _____________________________ */
}
