<?php

namespace App\Http\Controllers\Admin\Helper;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File; 

// use App\Models\EmployeeInfo;
// use Illuminate\Http\Request;
use DateTime;
use Carbon\Carbon;

class UploadDownloadController extends Controller
{
 
  
  public function uploadEmployeeProfilePhoto($file,$oldFilePath)
  { 
    if($file == null) 
    return null;
    $appoint_name = 'Emp-' . time() . '.' . $file->getClientOriginalExtension();
    $destinationPath = "uploads/employee/";
    $uploadPath =  $destinationPath . $appoint_name;

    $file->move($destinationPath, $appoint_name);
    if($oldFilePath != null && $oldFilePath != "" && File::exists($oldFilePath))
    {  unlink($oldFilePath);}         // File::delete($oldFilePath);

   return $uploadPath;
      
  } 
  
  public function uploadEmployeePassportFile($file,$oldFilePath)
  { 
    if($file == null) 
    return null;
    $appoint_name = 'Passport-' . time() . '.' . $file->getClientOriginalExtension();
    $destinationPath = "uploads/employee/";
    $uploadPath =  $destinationPath . $appoint_name;
    $file->move($destinationPath, $appoint_name);
    if($oldFilePath != null && $oldFilePath != "" && File::exists($oldFilePath))
    {  unlink($oldFilePath);}         // File::delete($oldFilePath);

   return $uploadPath;
      
  } 

  public function uploadEmployeeIqamaFile($file,$oldFilePath)
  { 
    if($file == null) 
    return null;
    $appoint_name = 'Iqama-' . time() . '.' . $file->getClientOriginalExtension();
    $destinationPath = "uploads/employee/";
    $uploadPath =  $destinationPath . $appoint_name;
    $file->move($destinationPath, $appoint_name);
    if($oldFilePath != null && $oldFilePath != "" && File::exists($oldFilePath))
    {  unlink($oldFilePath);}         // File::delete($oldFilePath);

   return $uploadPath;
      
  }  
  
  public function uploadEmployeeAppointmentLetterFile($file,$oldFilePath)
  { 
    if($file == null) 
    return null;
    $appoint_name = 'Appointment-' . time() . '.' . $file->getClientOriginalExtension();
    $destinationPath = "uploads/employee/";
    $uploadPath =  $destinationPath . $appoint_name;
    $file->move($destinationPath, $appoint_name);
    if($oldFilePath != null && $oldFilePath != "" && File::exists($oldFilePath))
    {  unlink($oldFilePath);}         // File::delete($oldFilePath);

   return $uploadPath;   
  }

  public function uploadEmployeeMedicalReportFile($file,$oldFilePath)
  { 
    if($file == null) 
    return null;
    $appoint_name = 'Medical-' . time() . '.' . $file->getClientOriginalExtension();
    $destinationPath = "uploads/employee/";
    $uploadPath =  $destinationPath . $appoint_name;
    $file->move($destinationPath, $appoint_name);
    if($oldFilePath != null && $oldFilePath != "" && File::exists($oldFilePath))
        {  unlink($oldFilePath);}
         // File::delete($oldFilePath);
   return $uploadPath;
      
  }

 
}
