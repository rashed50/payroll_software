<?php

namespace App\Http\Controllers\Admin\Helper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobStatus;
use DateTime;
use Carbon\Carbon;

class HelperController extends Controller
{
  // get month name for integer value
  public function getMonthName($monthId)
  {
    $dateObj   = DateTime::createFromFormat('!m', $monthId);
    return $monthName = $dateObj->format('F');
  }

  public function getYear()
  {
    return $pdate = date('Y');
  }

  public function getEmployeeStatus()
  {

    return $status = JobStatus::all();
  }

  public function getNextFirdayDate($day, $month, $year)
  {

    $date = $year . '-' . $month . '-' . $day;
    $date    =  new DateTime($date);
    $day = $date->format('l');  //pass
    return $day;
  }
  public function getNumberOfDaysInMonthAndYear($month, $year)
  {
    $noOfDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    if ($noOfDaysInMonth == null) {
      return 0;
    } else {
      return $noOfDaysInMonth;
    }
  }


  public function getMonthFromDateValue($dateValue)
  {
    if ($dateValue == null) {
      return 0;
    }
    $time = strtotime($dateValue);
    return  $month = (int) date("m", $time);
  }

  public function getYearFromDateValue($dateValue)
  {
    if ($dateValue == null) {
      return 0;
    }
    $time = strtotime($dateValue);
    return $year = date("Y", $time);
  }


  function getMonthsInRangeOfDate($startDate, $endDate)
  {
    $months = array();

    while (strtotime($startDate) <= strtotime($endDate)) {
      $months[] = array(
        'year' => date('Y', strtotime($startDate)),
        'month' => date('m', strtotime($startDate)),
      );

      // Set date to 1 so that new month is returned as the month changes.
      $startDate = date('01 M Y', strtotime($startDate . '+ 1 month'));
    }

    return $months;
  }
}
