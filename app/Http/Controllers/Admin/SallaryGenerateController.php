<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AnualFee\AnualFeeDetailsController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Permission\AccessPermissionController;

use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\CPF\ContributionController;
use App\Http\Controllers\Admin\EmployeeMultiProjectWorkHistoryController;
use App\Http\Controllers\Admin\Helper\HelperController;
use App\Http\Controllers\DataServices\CompanyDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use App\Http\Controllers\DataServices\SalaryProcessDataService;
use Illuminate\Http\Request;

use App\Models\SalaryHistory;
use App\Models\SalarySheetUpload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class SallaryGenerateController extends Controller
{



  /* ===================== All Employee Store Salary Record ===================== */
  public function salaryStore(Request $request)
  {

    $month = $request->month;
    $year = $request->year;
    $loginUserId = Auth::user()->id;

    $accessPermConObj = new AccessPermissionController();
    $hasAccessPermission = $accessPermConObj->checkAccessPermissionWithDate($month, $year);

    if ($hasAccessPermission != null) {
      // Get All Employee Monthly Salary Record  
      $allEmployeeMontlySalaryDetails = SalaryProcessDataService::getEmployeeDetailsWithMonthlyWorkRecord($month, $year, null);
      if ($allEmployeeMontlySalaryDetails == NULL) {
        Session::flash('not_record', 'value');
        return redirect()->back();
      }

      //$counter =0;
      /* ============== Salary Processing ============== */
      foreach ($allEmployeeMontlySalaryDetails as $anEmployee) {

        $this->processAnEmployeeSalaryCalculation($anEmployee, $month, $year, $loginUserId);
      }

      Session::flash('success', 'value');
      return redirect()->back();
    } else {
      Session::flash('error_date', 'Permission Date over!');
      return redirect()->back();
    }
  }
  /* ===================== All Employee Store Salary Record ===================== */


  public function multipleEmployeeIdBaseSalaryProcess(Request $request)
  {

    $month = $request->month;
    $salaryYear = $request->year;
    $salaryStatus = 1; // $request->salary_status; // 0 = Unpaid 1= Paid
    $allEmplId = explode(",", $request->multiple_emp_Id);
    $allEmplId = array_unique($allEmplId); // remove multiple same empl ID
    $HelperOBJ = new HelperController();
    $monthName = $HelperOBJ->getMonthName($month);
    $companyOBJ = new CompanyProfileController();
    $company = $companyOBJ->findCompanry();
    $salaryReport = SalaryProcessDataService::getMultipleEmployeeIdBaseSalaryHistory($allEmplId, $month, $salaryYear, $salaryStatus);
    return view('admin.salary-generate.all-employee.all-employees-salary', compact('salaryReport', 'month', 'monthName', 'salaryYear', 'company'));
  }


  private function processAnEmployeeSalaryCalculation($anEmployee, $month, $salaryYear, $loginUserId)
  {

    // ================ Calculation ================

    $duplicateSalary  = SalaryProcessDataService::getAnEmployeeSalaryRecord($anEmployee->emp_auto_id, $month, $salaryYear);
    // Salary Already Paid so  Processing is not Possible 
    if ($duplicateSalary != null) {
      if ($duplicateSalary->Status == 1) {
        return $duplicateSalary;
      }
    }
    $totalOthers = ($anEmployee->house_rent + $anEmployee->conveyance_allowance + $anEmployee->mobile_allowance
      + $anEmployee->medical_allowance + $anEmployee->others1 + $anEmployee->local_travel_allowance);

    $tem_anEmployee  = SalaryProcessDataService::calculateOvertimeHoursAndAmount($anEmployee);
    $anEmployee->slh_overtime_amount = $tem_anEmployee->slh_overtime_amount;
    $anEmployee->food_allowance = SalaryProcessDataService::calculateFoodAllowance($anEmployee->total_work_day, $anEmployee->food_allowance);
    $netSalary = ($tem_anEmployee->tem_total_amount + $totalOthers + $anEmployee->food_allowance);

    $grossSalary = ($netSalary - ($anEmployee->cpf_contribution + $anEmployee->saudi_tax));
    $anEmployee->gross_salary = $grossSalary;


    // find Employee Iqama Advance Payment 
    $advancePayConObj = new AdvancePayController();

    $totalIqamaAdvance =  (new AnualFeeDetailsController())->getAnEmployeeIqamaTotalCost($anEmployee->emp_auto_id);
    $advancePaidRecordConObj = new AdvancePayRecordController();
    $thisMonthIqamaPaidRecord = $advancePaidRecordConObj->getThisMonthAdvancePaidRecord($anEmployee->emp_auto_id, $salaryYear, $month, 1);
    $iqamaAdvanceTotalPaidAmount = $advancePaidRecordConObj->getAnEmployeeAdvancePaidTotalAmount($anEmployee->emp_auto_id, $salaryYear, 1);

    $differenceAmount =  $totalIqamaAdvance -  $iqamaAdvanceTotalPaidAmount;

    if ($thisMonthIqamaPaidRecord != null && $differenceAmount >= 0) {
      $iqamaAdvanceTotalPaidAmount -= $thisMonthIqamaPaidRecord->adv_amount;
      $differenceAmount =  $totalIqamaAdvance -  ($iqamaAdvanceTotalPaidAmount + $anEmployee->iqama_adv_inst_amount);

      if ($differenceAmount < 0) {
        $anEmployee->iqama_adv_inst_amount = $anEmployee->iqama_adv_inst_amount + $differenceAmount;
      }
      $advancePaidRecordConObj->updateAdvancePaidRecord($thisMonthIqamaPaidRecord->id, $anEmployee->emp_auto_id, $anEmployee->iqama_adv_inst_amount, $salaryYear, $month, 1, $loginUserId);
    } else if ($thisMonthIqamaPaidRecord == null && $differenceAmount >= 0) {
      $differenceAmount =  $totalIqamaAdvance -  ($iqamaAdvanceTotalPaidAmount + $anEmployee->iqama_adv_inst_amount);
      if ($differenceAmount < 0) {
        $anEmployee->iqama_adv_inst_amount = $anEmployee->iqama_adv_inst_amount + $differenceAmount;
      }

      if ($anEmployee->iqama_adv_inst_amount > 0) {
        $advancePaidRecordConObj->insertAdvancePaidRecord($anEmployee->emp_auto_id, $anEmployee->iqama_adv_inst_amount, $salaryYear, $month, 1, $loginUserId);
      }
    }


    // find Employee Other Advance Payment    
    $thisMonthOtherAdvPaidRecord = $advancePaidRecordConObj->getThisMonthAdvancePaidRecord($anEmployee->emp_auto_id, $salaryYear, $month, 2);
    $otherWithdrawTotalAmount = $advancePayConObj->getAnEmployeeTotalAdvanceAmount($anEmployee->emp_auto_id, $salaryYear, 0);  // 0= All Other Advance  
    $otherAdvTotalPaidAmount = $advancePaidRecordConObj->getAnEmployeeAdvancePaidTotalAmount($anEmployee->emp_auto_id, $salaryYear, 2);
    $diffAmount =  $otherWithdrawTotalAmount -  $otherAdvTotalPaidAmount;

    if ($thisMonthOtherAdvPaidRecord == null) {
      if ($diffAmount > 0 && $anEmployee->other_adv_inst_amount > 0) {
        if ($diffAmount <  $anEmployee->other_adv_inst_amount) {
          $anEmployee->other_adv_inst_amount = $diffAmount;
        }
        $advancePaidRecordConObj->insertAdvancePaidRecord($anEmployee->emp_auto_id, $anEmployee->other_adv_inst_amount, $salaryYear, $month, 2, $loginUserId);
      } else {
        $anEmployee->other_adv_inst_amount = 0;
      }
    } else {

      $otherAdvTotalPaidAmount -= $thisMonthOtherAdvPaidRecord->adv_amount;
      $diffAmount =  $otherWithdrawTotalAmount -  $otherAdvTotalPaidAmount;
      if ($diffAmount <= $anEmployee->other_adv_inst_amount) {
        $anEmployee->other_adv_inst_amount = $diffAmount;
      }
      if ($anEmployee->other_adv_inst_amount != $thisMonthOtherAdvPaidRecord->adv_amount) {
        $advancePaidRecordConObj->updateAdvancePaidRecord($thisMonthOtherAdvPaidRecord->id, $anEmployee->emp_auto_id, $anEmployee->other_adv_inst_amount, $salaryYear, $month, 2, $loginUserId);
      }
    }

    $anEmployee->gross_salary =  $anEmployee->gross_salary - ($anEmployee->iqama_adv_inst_amount + $anEmployee->other_adv_inst_amount);
    $anEmployee->work_multi_project = (new  EmployeeMultiProjectWorkHistoryController())->getIsAnEmployeeMultiprojectWorkHistory($anEmployee->emp_auto_id, $month, $salaryYear);

    // ================ End Calculation ================= 

    if ($duplicateSalary) {
      if ($duplicateSalary->Status == 0) {  // // if salary is not paid yet
        SalaryProcessDataService::updateAnEmployeeMonthlySalaryRecord($duplicateSalary->slh_auto_id, $anEmployee, $month, $salaryYear);
      }
    } else {
      SalaryProcessDataService::updateAnEmployeeMonthlySalaryRecord(-1, $anEmployee, $month, $salaryYear);
    }

    // ================ Employee Contribution ================= 
    $contributionContObj = new ContributionController();
    $emplContributionRecord = $contributionContObj->getEmployeeContribution($anEmployee->emp_auto_id, $month, $salaryYear);
    if ($emplContributionRecord) {
      $contributionContObj->updateEmployeeContribution($emplContributionRecord->EmpContHistId, $anEmployee->cpf_contribution, $loginUserId);
    } else {
      $contributionContObj->saveEmployeeContribution($anEmployee->emp_auto_id, $anEmployee->cpf_contribution, $month, $salaryYear, $loginUserId);
    }
    return $anEmployee;
  }

  /* ===================== Single Employee Salary Processing & Report ===================== */
  public function singleEmployeeMonthWiseSalaryReport(Request $request)
  {

    $salaryYear = $request->year;
    $month = $request->month;
    $anEmployee =  SalaryProcessDataService::getEmployeeDetailsWithMonthlyWorkRecord($month, $salaryYear, $request->emp_id);

    $loginUserId = Auth::user()->id;

    if ($anEmployee == NULL) {
      Session::flash('employeNotFound', 'value');
      return redirect()->back();
    } else if ($anEmployee->total_work_day == NULL) {
      Session::flash('not_record', 'value');
      return redirect()->back();
    }
    $insert =  $this->processAnEmployeeSalaryCalculation($anEmployee, $month, $salaryYear, $loginUserId);

    if ($insert) {
      $year = $request->salaryYear;
      $HelperOBJ = new HelperController();
      $monthName = $HelperOBJ->getMonthName($month);
      $companyOBJ = new CompanyProfileController();
      $company = $companyOBJ->findCompanry();
      $salary = SalaryProcessDataService::getAnEmployeeInfoWithSalaryHistory($anEmployee->emp_auto_id, $month, $salaryYear);
      $allSalaryAmount = SalaryHistory::where('slh_month', $month)->where('emp_auto_id', $anEmployee->emp_auto_id)->where('slh_year', $salaryYear)->sum('slh_total_salary');
      $iqamaAmount = SalaryHistory::where('slh_month', $month)->where('emp_auto_id', $anEmployee->emp_auto_id)->where('slh_year', $salaryYear)->sum('slh_iqama_advance');
      $totalHours = SalaryHistory::where('slh_month', $month)->where('emp_auto_id', $anEmployee->emp_auto_id)->where('slh_year', $salaryYear)->sum('slh_total_hours');
      if ($salary) {
        return view('admin.salary-generate.single_employee.salary', compact('salary', 'monthName', 'salaryYear', 'company', 'allSalaryAmount', 'iqamaAmount', 'totalHours'));
      }
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  } // METHOD END

  /* ====================   Employee Type (Basic Salary, Hourly,Direct) salary report ==================== */
  public function projectEmployeeMonthWiseSalaryReport(Request $request)
  {
    $empRelDSObj = new EmployeeRelatedDataService();

    $projectId = $request->proj_id;
    $month = $request->month;
    $salaryYear = $request->year;
    $salaryStatus = $request->salary_status; // 0 = Unpaid 1= Paid
    $empType = $request->emp_type_id;
    $monthName = (new HelperController())->getMonthName($month);
    $company = (new CompanyProfileController())->findCompanry();
    $project = $empRelDSObj->findAProjectInformation($projectId);

    $employeeType = 'All';
    if ($empType == 0 || $empType == null) {
      $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistory($projectId, -1, $month, $salaryYear, $salaryStatus);
    } else {

      $hourly_employee = null;
      if ($empType > 1) {   // Indirect Employee

        $employeeType = $empRelDSObj->getAnEmployeeTypeName($empType);
        $hourly_employee = NULL;
      } else if ($empType == 1) { // Hourly Salary Employee
        $hourly_employee = true;
        $employeeType = $empRelDSObj->getAnEmployeeTypeName($empType) . "-Hourly";
      } else if ($empType == -1) {  // Basic Salary Employee
        $hourly_employee = null;
        $empType = 1;
        $employeeType = $empRelDSObj->getAnEmployeeTypeName($empType) . "-Basic Salary";
      }
      $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistoryWithProjectAndEmployeeType($projectId, $empType, $hourly_employee, $month, $salaryYear, $salaryStatus);
    }
    return view('admin.salary-generate.projectwise.projectwise-salary-report', compact('salaryReport', 'monthName', 'salaryYear', 'company', 'project', 'empType', 'employeeType'));
  }



  /* ====================  Employee Project & status Wise Salary Report ==================== */
  public function projectEmployeeStatusMonthWiseSalaryReport(Request $request)
  {

    $projectId = $request->proj_id;
    $month = $request->month;
    $salaryStatus = $request->salary_status; // 0 = Unpaid 1= Paid
    $salaryYear = $request->year;
    $empStatusId = $request->emp_status_id;

    $monthName = (new HelperController())->getMonthName($month);
    $company = (new CompanyProfileController())->findCompanry();
    $project = (new EmployeeRelatedDataService())->findAProjectInformation($projectId);
    // (new ProjectInfoController())->getFindId($projectId);


    $employeeStatus = 'All';
    if ($empStatusId == 0 || $empStatusId == null) {

      $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistory($projectId, -1, $month, $salaryYear, $salaryStatus);
    } else {
      $employeeStatus =  (new EmployeeRelatedDataService())->getEmployeeJobStatusTitle($empStatusId);
      // job_status
      $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistoryWithProjectAndEmployeeJobStatus($projectId, $empStatusId, $month, $salaryYear, $salaryStatus);
    }
    return view('admin.salary-generate.projectwise.statuswise-salary-report', compact('salaryReport', 'monthName', 'salaryYear', 'company', 'project',  'employeeStatus'));
  }


  /* ==================== Sponser Wise Employee Salary Report ==================== */
  public function sponserWiseMonthlySalaryReport(Request $request)
  {

    $projectId = $request->proj_id;
    $sponserId = $request->SponsId;
    $month = $request->month;
    $salaryYear = $request->year;
    $salaryStatus = $request->salary_status; // 0 = Unpaid 1= Paid

    $monthName = (new HelperController())->getMonthName($month);
    $company = (new CompanyProfileController())->findCompanry();
    $sponser = (new EmployeeRelatedDataService())->findASponser($sponserId);
    // (new SponsorController())->findSponser($sponserId);

    $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistory($projectId, $sponserId, $month, $salaryYear, $salaryStatus);

    return view('admin.salary-generate.spnserwise.sponserwise-salary-report', compact('sponserId', 'salaryReport', 'monthName', 'salaryYear', 'company'));
  }


  /* =================== Month wise All Employee Salary with PAID/Unpaid =================== */
  public function monthWiseSalary(Request $request)
  {


    $month = $request->month;
    $salaryYear = $request->year;
    $salaryStatus = $request->salary_status; // 0 = Unpaid 1= Paid
    $monthName = (new HelperController())->getMonthName($month);
    $company = (new CompanyProfileController())->findCompanry();


    $salaryReport = SalaryProcessDataService::getEmployeeSalaryHistory(-1, -1, $month, $salaryYear, $salaryStatus);
    $allSalaryAmount = SalaryProcessDataService::getSalaryTotalAmount(-1, $month, $salaryYear, $salaryStatus);
    $iqamaAmount =  SalaryProcessDataService::getSalaryIqamaAdvanceTotalAmount($month, $salaryYear, $salaryStatus);
    $totalHours =  SalaryProcessDataService::getSalaryMonthTotalHours(-1, $month, $salaryYear, $salaryStatus);
    $totalSaudiTax = SalaryProcessDataService::getSalarySaudiTaxTotalAmount($month, $salaryYear, $salaryStatus);
    $totalOverTimeHours = SalaryProcessDataService::getSalaryMonthTotalOvertimeHours(-1, $month, $salaryYear, $salaryStatus);
    $totalOverTimeAmount = SalaryProcessDataService::getSalaryMonthTotalOvertimeAmount($month, $salaryYear, $salaryStatus);
    $totalFoodAllowance = SalaryProcessDataService::getSalaryMonthFoodAllowanceTotalAmount($month, $salaryYear, $salaryStatus);
    $totalContribution = SalaryProcessDataService::getSalaryMonthEmployeeCPFTotalAmount($month, $salaryYear, $salaryStatus);
    $totalOtherAdvance = SalaryProcessDataService::getSalaryMonthOtherAdvanceTotalAmount($month, $salaryYear, $salaryStatus);

    // find Salary Record This Month
    if ($salaryReport) {
      // Condition Wise Check
      foreach ($salaryReport as $salary) {
        if ($salary->hourly_employee == true) {
          $others = $salary->others;
        } else {
          $others = ($salary->house_rent + $salary->conveyance_allowance + $salary->medical_allowance + $salary->others);
        }
        // Condition Wise Check
        $salary->otherAmount = $others;
      }

      return view('admin.salary-generate.all-employee.all-employees-salary', compact('salaryReport', 'month', 'monthName', 'salaryYear', 'company', 'allSalaryAmount', 'iqamaAmount', 'totalHours', 'totalSaudiTax', 'totalOverTimeHours', 'totalOverTimeAmount', 'totalFoodAllowance', 'totalContribution', 'totalOtherAdvance'));
    } else {
      Session::flash('salaryRecordNotFound', 'value');
      return redirect()->back();
    }
  }




  /* =================== Project Wise All Employee Salary Paid and Unpaid Summary  =================== */
  public function monthAndYearWiseSalarySummary(Request $request)
  {
    $month = $request->month;
    $salaryYear = $request->year;
    $projectId = $request->proj_id;

    // 0 = Unpaid 1= Paid

    $monthName = (new HelperController())->getMonthName($month);

    $company = (new CompanyProfileController())->findCompanry();

    $projectName = "All Project";
    if ($projectId > 0) {
      $projectName = (new EmployeeRelatedDataService())->findAProjectInformation($projectId)->proj_name;
    }
    $total_records = SalaryProcessDataService::calculateEmployeeSalaryTotalAmount($projectId, $month, $salaryYear);

    if (count($total_records) >= 1) {
      return view('admin.salary-generate.all-employee.salary-summary', compact('total_records', 'monthName', 'salaryYear', 'company', 'projectName'));
    } else {
      Session::flash('salaryRecordNotFound', 'value');
      return redirect()->back();
    }
  }


  public function projectwiseTotalWorkingHoursAndTotalSalary(Request $request)
  {
    // dd($request->all());

    $company = (new CompanyProfileController())->findCompanry();
    $proj_id = $request->proj_id;
    $projectName = "All Project";
    if ($proj_id > 0) {
      $projectName = (new EmployeeRelatedDataService())->findAProjectInformation($proj_id)->proj_name;
    }
    $monthwithYears =  (new HelperController())->getMonthsInRangeOfDate($request->from_date, $request->to_date);
    $salaryReport = array();
    $counter = 0;
    foreach ($monthwithYears as $my) {
      $totalSalary = (new SalaryProcessDataService())->getSalaryTotalAmount($proj_id, $my['month'], $my['year'], 1);
      $totalHours = (new SalaryProcessDataService())->getSalaryMonthTotalHours($proj_id, $my['month'], $my['year'], 1);
      $overTime =  (new SalaryProcessDataService())->getSalaryMonthTotalOvertimeHours($proj_id, $my['month'], $my['year'], 1);
      $summaryRecords = (new SalaryProcessDataService())->calculateEmployeeSalaryTotalAmount($proj_id, $my['month'], $my['year']);
      // dd($summaryRecords);
      if (count($summaryRecords) > 0) {
        $unpaidRecord = $summaryRecords[0];
        $totalUnPaidSalaryAllIncluded = $unpaidRecord['total_salary'] + $unpaidRecord['total_saudi_tax'] + $unpaidRecord['total_iqama_adv'] + $unpaidRecord['total_other_adv'] + $unpaidRecord['total_contribution'];
        $grossTotalUnPaidSalary = $unpaidRecord['total_salary'];

        $paidRecord = $summaryRecords[1];
        $totalPaidSalaryAllIncluded = $paidRecord['total_salary'] + $paidRecord['total_saudi_tax'] + $paidRecord['total_iqama_adv'] + $paidRecord['total_other_adv'] + $paidRecord['total_contribution'];
        $grossTotalPaidSalary = $paidRecord['total_salary'];
      }
      $info = array(
        'year' => $my['year'],
        'month_name' => (new HelperController())->getMonthName($my['month']),
        'month' => $my['month'],
        'total_hours' => $totalHours,
        'total_over_time' => $overTime,
        'unpaid_gross_salary' => $grossTotalUnPaidSalary,
        'paid_gross_salary' => $grossTotalPaidSalary,
        'total_salary' => $totalUnPaidSalaryAllIncluded + $totalPaidSalaryAllIncluded,
        'gross_total_salary' => $grossTotalPaidSalary+$grossTotalUnPaidSalary, 

      );
      $salaryReport[$counter++] = $info;
    }
    // dd($report);

    return view('admin.salary-generate.projectwise.project-wise-salary-amount-report', compact('salaryReport', 'company', 'projectName'));
  }



  public function SalarySheetStore(Request $request)
  {
    // dd($request->all());

    $data = $request->all();
    $data['file_path'] = '';
    $data['file_name'] = '';
    $insert = SalarySheetUpload::create($data);

    if ($request->hasFile('file_name')) {

      $image = $request->file('file_name');
      $imageName = 'salary_sheet' . time() . '.' . $image->getClientOriginalExtension();
      Image::make($image)->save('uploads/sheet/' . $imageName);

      SalarySheetUpload::where('ss_auto_id', $insert->ss_auto_id)->update([
        'file_name' => $imageName,
        'file_path' => 'uploads/sheet/'
      ]);
    }

    if ($insert) {
      Session::flash('success', 'Salary Sheet Save');
      return Redirect()->back();
    } else {
      Session::flash('error', 'value');
      return Redirect()->back();
    }
  }




  /*
  |--------------------------------------------------------------------------
  |  BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index()
  {
    return null;
  }

  public function create()
  {
    $currentMonth = Carbon::now()->format('m');

    $projects = (new EmployeeRelatedDataService())->getAllProjectInformation();

    $month = (new CompanyDataService())->getAllMonth();
    $emp_types = (new EmployeeRelatedDataService())->getAllEmployeeType();
    $sponser = (new EmployeeRelatedDataService())->getAllSponser();
    $allEmployeeStatus = (new HelperController())->getEmployeeStatus();

    return view('admin.salary-generate.salary-proccessing', compact('allEmployeeStatus', 'month', 'currentMonth', 'projects', 'emp_types', 'sponser'));
  }


  // Employee Pending Salary UI Loading
  public function SalaryPending()
  {


    $projectlist = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // (new ProjectInfoController())->getAllInfo();
    $sponserList = (new EmployeeRelatedDataService())->getAllSponser();
    // (new SponsorController())->getAll();

    return view('admin.salary-generate.salary_pending.pending-salary', compact('projectlist', 'sponserList'));
  }
  // Employee Paid Salary UI Loading
  public function Salarypaid()
  {
    $projectlist = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // (new ProjectInfoController())->getAllInfo();
    $sponserList = (new EmployeeRelatedDataService())->getAllSponser();
    // (new SponsorController())->getAll();

    return view('admin.salary-generate.salary-paid.paid-salary', compact('projectlist', 'sponserList'));
  }


  // Employee Salary sheet UI Loading
  public function SalarySheet()
  {
    $allSheet =  $this->getAllSalarySheet();
    $month = (new CompanyDataService())->getAllMonth();
    $projectlist = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // (new ProjectInfoController())->getAllInfo();
    $sponserList = (new EmployeeRelatedDataService())->getAllSponser();
    // (new SponsorController())->getAll();

    return view('admin.salary-generate.salary-sheet', compact('projectlist', 'sponserList', 'month', 'allSheet'));
  }


  // date wise salary pending
  public function SalaryPendingList(Request $request)
  {

    $fromMonth = (int)date('n', strtotime($request->fromDate));
    $toMonth = (int)date('n', strtotime($request->toDate));

    (int)$sponser_id = $request->SponsId;
    (int)$proj_id = $request->proj_id;


    $pendingSalary = (SalaryProcessDataService::processEmployeeSalaryList($sponser_id, $proj_id, $fromMonth, $toMonth, 0));
    return response()->json(['pendingSalary' => $pendingSalary, 'fromMonth' => $fromMonth, 'toMonth' => $toMonth]);
  }


  // date wise salary pending
  public function SalarypaidList(Request $request)
  {

    $fromMonth = (int)date('n', strtotime($request->fromDate));
    $toMonth = (int)date('n', strtotime($request->toDate));

    (int)$sponser_id = $request->SponsId;
    (int)$proj_id = $request->proj_id;

    $pendingSalary = (SalaryProcessDataService::processEmployeeSalaryList($sponser_id, $proj_id, $fromMonth, $toMonth, 1));

    return response()->json(['pendingSalary' => $pendingSalary, 'fromMonth' => $fromMonth, 'toMonth' => $toMonth]);
  }




  public function SalaryPaymentComplete()
  {
    return view('admin.salary-generate.payment-salary');
  }


  // salary payment confirmation
  public function SalaryPayment(Request $request)
  {
    $slh_auto_id = $request->id;
    $payment = SalaryProcessDataService::updateEmployeeSalaryStatusAsPaid($slh_auto_id);

    if ($payment == true) {
      return response()->json(["success" => "Salary Paid, Success"]);
    } else {
      return response()->json(['error' => "Data Not Found!"]);
    }
  }

  public function SalaryPaymentToUnPay(Request $request)
  {
    $slh_auto_id = $request->id;
    $payment = SalaryProcessDataService::updateEmployeeSalaryStatusAsUnPaid($slh_auto_id);
    if ($payment == true) {
      return response()->json(["success" => "Salary Status Updated"]);
    } else {
      return response()->json(['error' => "Data Not Found!"]);
    }
  }

  /* +++++++++++ Salary Currection +++++++++++ */
  public function SalaryCorrectionForm()
  {
    $currentMonth = Carbon::now()->format('m');
    $previousMonth = Carbon::now()->subMonth(1)->format('m');

    $currentMonthName = Carbon::now()->format('F');
    $previousMonthName = Carbon::now()->subMonth(1)->format('F');

    $currentYear = Carbon::now()->format('Y');
    $previousYear = Carbon::now()->subYear(1)->format('Y');
    return view('admin.salary-generate.employee-salary-correction', compact('currentYear', 'previousYear', 'currentMonth', 'previousMonth', 'currentMonthName', 'previousMonthName'));
  }


  public function SalaryCurrectionProcess(Request $request)
  {

    $month = $request->slh_month;
    $slary_year = $request->slh_year;

    $update = SalaryProcessDataService::updateAnEmployeeMonthlySalaryRecord($request->salaryHistoryId, $request, $month, $slary_year);
    Session::flash('update_success', 'value');
    return redirect()->back();
  }



  /*
  |--------------------------------------------------------------------------
  |  API OPERATION
  |--------------------------------------------------------------------------
  */
}
