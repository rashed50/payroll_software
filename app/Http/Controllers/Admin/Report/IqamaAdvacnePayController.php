<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\AdvancePayController;
use App\Http\Controllers\Admin\AdvancePayRecordController;
use App\Http\Controllers\Admin\AnualFee\AnualFeeDetailsController; 
use App\Http\Controllers\DataServices\CompanyDataService;
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use Illuminate\Http\Request;
use App\Models\EmployeeInfo; 
use Illuminate\Support\Facades\Session;

class IqamaAdvacnePayController extends Controller
{

  public function iqamaRenewal()
  { 
    $allMonth = (new CompanyDataService())->getAllMonth();
    return view('admin.employee-info.iqama-renewal', compact('allMonth'));
  }



  public function projectWiseIqamaRenewal()
  {
    //  $projectOBJ = new ProjectInfoController();
    $projects = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // $projectOBJ->getAllInfo();

    // $sponsirObj = new SponsorController();
    $sponsor = (new EmployeeRelatedDataService())->getAllSponser();
    // $sponsirObj->getAll();

    return view('admin.employee-info.due-iqama-renewal', compact('projects', 'sponsor'));
  }

  public function iqamaRenewalHistoryProcess(Request $request)
  {
    $findEmployee = EmployeeInfo::where('employee_id', $request->emp_id)->first();


    $companyObj = new CompanyProfileController();
    $company = $companyObj->findCompanry();

    if ($findEmployee) {
      $IqamaAdvance = EmployeeInfo::where('employee_infos.emp_auto_id', $findEmployee->emp_auto_id)
        ->where('advance_pays.adv_pay_purpose', 1)
        ->where('advance_pay_histories.aph_year', $request->year_id)
        ->leftjoin('advance_pays', 'employee_infos.emp_auto_id', '=', 'advance_pays.emp_id')

        ->leftjoin('advance_purposes', 'advance_pays.adv_pay_purpose', '=', 'advance_purposes.id')

        ->leftjoin('advance_pay_histories', 'advance_pays.adv_pay_id', '=', 'advance_pay_histories.adv_pay_id')
        ->get();
      return view('admin.report.iqama.iqama-renewal', compact('findEmployee', 'IqamaAdvance', 'company'));
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }



  public function projectAndSponsorWiseIqamaReport(Request $request)
  {

    $companyOBJ = new CompanyProfileController();
    $company = $companyOBJ->findCompanry();

    $projId = $request->proj_id;
    $sponsId = $request->spons_id;
    $year = $request->year;


    //$empConObj = new EmployeeInfoController();
    //  $projAndSponsWiseEmp = $empConObj->getEmployeeListWithProjectAndSponsor($projId, $sponsId);
    $projAndSponsWiseEmp = (new EmployeeDataService())->getEmployeeListWithProjectAndSponsor($projId, $sponsId);
    $projectName = 'All';
    $sponsorName = 'All';

    if ($projId != 0) {
      // $projConObj = new ProjectInfoController();
      $projectName = (new EmployeeRelatedDataService())->getAllProjectInformation($projId)->proj_name;
      // $projConObj->getFindId($projId)->proj_name;
    }
    if ($sponsId != 0) {
      // $sponsConObj = new SponsorController();
      $sponsorName = (new EmployeeRelatedDataService())->findASponser($sponsId)->spons_name;
      // $sponsConObj->findSponser($sponsId)->spons_name;
    }

    $totalIqmaAdvAmount = 0;
    $totalPaidAmount = 0;
    $totalDueAmount = 0;
    foreach ($projAndSponsWiseEmp as $anEmployee) {
      $advPayConObj = new AdvancePayController();
      $anEmployeeIqamaAdvanceAmont = (new AnualFeeDetailsController())->getAnEmployeeIqamaTotalCost($anEmployee->emp_auto_id);

      $advPaidRecordObj = new AdvancePayRecordController();
      $anEmployeePaidAoumt = $advPaidRecordObj->getAnEmployeeAdvancePaidTotalAmount($anEmployee->emp_auto_id, $year, 1);

      $anEmployee->totalPaid = $anEmployeePaidAoumt;
      $anEmployee->advanc = $anEmployeeIqamaAdvanceAmont;

      $totalIqmaAdvAmount += $anEmployeePaidAoumt;
      $totalPaidAmount += $anEmployeeIqamaAdvanceAmont;
      $totalDueAmount += ($anEmployeeIqamaAdvanceAmont - $anEmployeePaidAoumt);
    }


    return view('admin.report.iqama.proj-spons-iqama', compact('projAndSponsWiseEmp', 'sponsorName', 'projectName',  'company', 'totalIqmaAdvAmount', 'totalPaidAmount', 'totalDueAmount'));
  }


  // public function projectWiseIqamaRenewalHistoryProcess(Request $request)
  // {


  //   $companyOBJ = new CompanyProfileController();
  //   $company = $companyOBJ->findCompanry();

  //   $projectWiseEmployeeIqamaAdvance = EmployeeInfo::where('employee_infos.project_id', $request->proj_id)
  //     ->where('advance_pays.adv_pay_purpose', 1)
  //     ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
  //     ->leftjoin('advance_pays', 'employee_infos.emp_auto_id', '=', 'advance_pays.emp_id')
  //     ->get();


  //   $totalAdvanceIqamaRenewal = EmployeeInfo::where('employee_infos.project_id', $request->proj_id)
  //     ->where('advance_pays.adv_pay_purpose', 1)
  //     ->leftjoin('advance_pays', 'employee_infos.emp_auto_id', '=', 'advance_pays.emp_id')
  //     ->sum('adv_pay_amount');


  //   $totalPaidIqamaRenewal = EmployeeInfo::where('employee_infos.project_id', $request->proj_id)
  //     ->where('advance_pays.adv_pay_purpose', 1)
  //     ->leftjoin('advance_pays', 'employee_infos.emp_auto_id', '=', 'advance_pays.emp_id')
  //     ->sum('total_paid');


  //   //  dd($projectWiseEmployeeIqamaAdvance);

  //   return view('admin.report.iqama.project-iqama', compact('projectWiseEmployeeIqamaAdvance', 'company', 'totalAdvanceIqamaRenewal', 'totalPaidIqamaRenewal'));
  //   Session::flash('error', 'value');
  //   return redirect()->back();
  // }


}
