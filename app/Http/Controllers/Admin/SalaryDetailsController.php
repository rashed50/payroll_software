<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SalaryDetails;
use App\Models\EmployeeInfo;
use App\Http\Controllers\Admin\EmployeeInfoController;
use App\Http\Controllers\DataServices\EmployeeDataService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class SalaryDetailsController extends Controller{
    /*
    =============================
    =====DATABSE OPEREATION======
    =============================
    */

    public function addEmployeeSalaryDetails($emp_auto_id,$basic_amount,$basic_hours,$hourly_rate,$house_rent,$mobile_allowance,
            $medical_allowance,$local_travel_allowance,$conveyance_allowance,$food_allowance,$others1){
     return SalaryDetails::insert([
              'emp_id' => $emp_auto_id,
              'basic_amount' =>$basic_amount,
              'basic_hours' => $basic_hours,
              'hourly_rent' => $hourly_rate,
              'house_rent' => $house_rent,
              'mobile_allowance' => $mobile_allowance,
              'medical_allowance' => $medical_allowance,
              'local_travel_allowance' =>$local_travel_allowance,
              'conveyance_allowance' => $conveyance_allowance,
              'food_allowance' =>$food_allowance,
              'others1' => $others1,
              'created_at' => Carbon::now(),
            ]);
    }
    public function getSalaryList(){
      return $all = SalaryDetails::orderBy('sdetails_id','DESC')->paginate(300);
    }

    public function getSingleSalary($id){
      return $find_id =  SalaryDetails::where('sdetails_id',$id)
      ->leftjoin('employee_infos','salary_details.emp_id','=','employee_infos.emp_auto_id')
      ->select('employee_infos.emp_type_id','salary_details.*')->first();

    }

    public function delete($id){
      $delete = SalaryDetails::where('sdetails_id',$id)->delete();
      if($delete){
        Session::flash('success_soft','value');
        return Redirect()->back();
      }else{
        Session::flash('error','value');
        return Redirect()->back();
      }
    }

    /* Direct Man power status update */
    public function directManStatusUpdate($empId){
     // dd($empId);
      $findEmployee = EmployeeInfo::where('job_status',1)->where('emp_auto_id',$empId)->first();
      if($findEmployee == null){
        Session::flash('error','value');
        return Redirect()->back();
      }
     // dd($findEmployee);
      if($findEmployee->emp_type_id == 1 && $findEmployee->hourly_employee == 1){
        EmployeeInfo::where('emp_auto_id',$empId)->update([
          'hourly_employee' => NULL,
        ]);
      }else{
        EmployeeInfo::where('emp_auto_id',$empId)->update([
          'hourly_employee' => 1,
        ]);
      }
      return Redirect()->back();
    }
    /* Direct Man power status update */

    /* salary data insert */
    public function insert(Request $req){

        $id = $req->empId;
        $type = $req->employeetype;
        $hourlyRate = $req->hourly_rate;
        $houreRate = '';

        if($type == 1){
          if($hourlyRate == 0){
            $houreRate = ($req->basic_amount / $req->basic_hours);
          }else{
            $houreRate = $req->hourly_rate;
          }

        }elseif($type == 1 && $req->hourlyEmployee == 1){
          $houreRate = $req->hourly_rate;
        }else{
          if($hourlyRate == 0){
            $houreRate = ($req->basic_amount / 30);
          }else {
            $houreRate = $req->hourly_rate;
          }
        }




        $salaryDetails = SalaryDetails::where('emp_id',$id)->pluck('emp_id');

        $update = SalaryDetails::where('emp_id',$salaryDetails)->update([
          'emp_id' => $id,
          'basic_amount' => $req->basic_amount,
          'basic_hours' => $req->basic_hours,
          'hourly_rent' => $houreRate,
          'house_rent' => $req->house_rent,
          'mobile_allowance' => $req->mobile_allowance,
          'medical_allowance' => $req->medical_allowance,
          'local_travel_allowance' => $req->local_travel_allowance,
          'conveyance_allowance' => $req->conveyance_allowance,

          'food_allowance' => $req->food_allowance,
          'others1' => $req->others1,
          'created_at' => Carbon::now(),
        ]);
        if($update){
          Session::flash('success','value');
          return Redirect()->route('add-employee');
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }
    }

    public function updateIqamaAndOtherAdvanceNextMonthPayAmount($emp_auto_id,$iqamaAdvNextPayAmount,$otherAdvNextPayAmount){
      
      $update = SalaryDetails::where('emp_id',$emp_auto_id)->update([
        'iqama_adv_inst_amount' => $iqamaAdvNextPayAmount,
        'other_adv_inst_amount' => $otherAdvNextPayAmount,
      ]);
      
  }

    /* ================= Salary Details Update ================= */
    public function update(Request $req){
        /* data insert in database */
        $id = $req->id;

        // dd($request->all);
        // dd($req->hourly_rate);

        $update = SalaryDetails::where('sdetails_id',$id)->update([

          'basic_amount' => $req->basic_amount,
          'basic_hours' => $req->basic_hours,
          'hourly_rent' => $req->hourly_rate  ,
          'house_rent' => $req->house_rent,
          'mobile_allowance' => $req->mobile_allowance,
          'medical_allowance' => $req->medical_allowance,
          'local_travel_allowance' => $req->local_travel_allowance,
          'conveyance_allowance' => $req->conveyance_allowance,

          'food_allowance' => $req->food_allowance,
          'others1' => $req->others1,
          'created_at' => Carbon::now(),
        ]);
        if($update){
          Session::flash('success_update','value');
          return Redirect()->route('salary-details');
        }else{
          Session::flash('error','value');
          return Redirect()->back();
        }

    }

    /* Employee Information For CPF Contribution */
    public function empInfoForContribution(Request $request){
      $empId = $request->employeeID;
      $findEmpl = EmployeeInfo::where('job_status',1)->where('employee_id',$empId)->first();
      if($findEmpl){
        $data = SalaryDetails::with('employee')->where('emp_id',$findEmpl->emp_auto_id)->first();
        return response()->json([ 'data' => $data ]);
      }else{
        return response()->json([ 'invalidEmpId' => 'Employee Id Dosen,t Match!' ]);
      }
    }

    public function updateContributionAmount(Request $request){
      $empId = $request->empId;

       $update = SalaryDetails::where('emp_id',$empId)->update([
        'cpf_contribution' => $request->amount,
        'saudi_tax' => $request->tax,
        'updated_at' => Carbon::now(),
      ]);
      if($update){
        Session::flash('success','value');
        return Redirect()->back();
      }

    }
    public function updateEmployeeAdvaceInstallAmount($empId,$amount,$purposeId){
     
      if($purposeId == 1){
      $update = SalaryDetails::where('emp_id',$empId)->update([
        'iqama_adv_inst_amount' => $amount,
        'updated_at' => Carbon::now(),
      ]);
     }else
       $update = SalaryDetails::where('emp_id',$empId)->update([
        'other_adv_inst_amount' => $amount,
        'updated_at' => Carbon::now(),
      ]);
      return $update;
    }


    /*
    =============================
    =======BLADE OPEREATION======
    =============================
    */

    public function index(){
      $all = $this->getSalaryList();
      //$activEmpObj = new EmployeeInfoController();
      $allActive = (new EmployeeDataService())->countTotalEmployees(1);
      return view('admin.salary-details.index',compact('all', 'allActive'));
    }

    public function edit($id){
        $data = $this->getSingleSalary($id);
        return view('admin.salary-details.edit',compact('data'));
    }

    public function view($id){
        $view = $this->getSingleSalary($id);
        return view('admin.salary-details.view',compact('view'));
    }

    public function setContributionAmount(){
      return view('admin.salary-details.contribution');
    }


    /* ======================================================================= */
}
