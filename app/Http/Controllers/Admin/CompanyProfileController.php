<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller; 
use App\Http\Controllers\DataServices\CompanyDataService;
use Illuminate\Http\Request;  
use Session;

class CompanyProfileController extends Controller
{

  // Middleware
  function __construct()
  {
    $this->middleware('permission:company-profile', ['only' => ['getAll', 'findCompanry', 'profile', 'updateProfile']]);
  }
  // Middleware

  public function getAll()
  {
    return $all =  (new CompanyDataService())->getCompanyProfileInformation();
    //CompanyProfile::all();
  }

  public function findCompanry()
  {
    return $profile = (new CompanyDataService())->findCompanryProfile();
  }

  public function profile()
  {

    $comDSObj = new CompanyDataService();
    $cur = $comDSObj->getAvailableCurrency();
    $profile =  $comDSObj->findCompanryProfile();
    return view('admin.company-profile.profile', compact('profile', 'cur'));
  }

  public function updateProfile(Request $req)
  {
    // form validation
    $this->validate($req, [
      'comp_name_en' => 'required',
      'comp_name_arb' => 'required',
      'curc_id' => 'required',
      'comp_email1' => 'required',
      'comp_email2' => 'required',
      'comp_phone1' => 'required',
      'comp_phone2' => 'required',
      'comp_mobile1' => 'required',
      'comp_mobile2' => 'required',
      'comp_address' => 'required',
      'comp_mission' => 'required',
      'comp_vission' => 'required',
      'comp_contact_address' => 'required',
      'comp_support_number' => 'required',
      'comp_hotline_number' => 'required',
      'comp_description' => 'required',
    ], []);

    $update = (new CompanyDataService())->insertCompanyInformation($req);

    if ($update) {
      Session::flash('success', 'value');
      return Redirect()->back();
    } else {
      Session::flash('error', 'value');
      return Redirect()->back();
    }
  }
}
