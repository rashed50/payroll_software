<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Exports\EmployeeExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class ExcelExportController extends Controller
{

    public function EmployeeslistExcellExport(Request $request)
    {

        $connection_statusId = $request['connection_statusId'];
        $packageId = $request['packageId'];
        return Excel::download(new EmployeeExport(1, 1), 'employee_list.xlsx');
    }
}
