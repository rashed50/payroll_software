<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AnualFee\AnualFeeDetailsController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdvancePayRecordController;
use App\Http\Controllers\Admin\EmployeeInfoController;
use App\Http\Controllers\Admin\Helper\HelperController;
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\SalaryProcessDataService;
use Illuminate\Http\Request;
use App\Models\AdvanceInfo;
use App\Models\AdvancePayRecord;
//use App\Models\AdvancePayRecord;
use App\Models\AdvancePurpose;
//use App\Models\EmployeeInfo;
use App\Models\SalaryDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdvancePayController extends Controller
{
  /*
      =================================
      ========== DATABASE OPERATION ===
      =================================
    */
  public function getAllPurpose()
  {
    return $all = AdvancePurpose::get();
  }

  public function getAll()
  {
    return $all = AdvanceInfo::where('status', 1)->orderBy('id', 'DESC')->get();
  }

  public function findId($id)
  {
    return $all = AdvanceInfo::where('status', 1)->where('id', $id)->first();
  }
  public function findByEmpId($emp_id)
  {
    return $all = AdvanceInfo::where('status', 1)->where('emp_id', $emp_id)->first();
  }

  public function findLastAdvanceRecordByEmpAutoIdAndAdvancePurposeId($emp_auto_id, $purposeTypeId)
  {
    return $all = AdvanceInfo::where('status', 1)->where('emp_id', $emp_auto_id)->where('adv_purpose_id', $purposeTypeId)->latest()->first();
  }

  public function deleteAdvancePaymentRecord($id)
  {
    return $all = AdvanceInfo::where('status', 1)->where('id', $id)->delete();
  }

  public function findEmployeeOthersAmount($id)
  {

    return  SalaryDetails::where('emp_id', $id)->first();
  }

  public function getAnEmployeeTotalAdvanceAmount($employeeAutoId, $year, $advanceTypeId)
  {


    if ($advanceTypeId == 0) {
      // other Advance
      $totalAdvanceAmount = AdvanceInfo::where('emp_id', $employeeAutoId)
        ->where('adv_purpose_id', '!=', 1)->sum('adv_amount');
    } else {
      // Iqama Advance
      $totalAdvanceAmount = AdvanceInfo::where('emp_id', $employeeAutoId)
        //->where('year',$year)
        ->where('adv_purpose_id', 1)->sum('adv_amount');
    }

    if ($totalAdvanceAmount == null) {
      return 0;
    } else {
      return $totalAdvanceAmount;
    }
  }

  public function updateEmployeeOthersAdvanceDeductionAmount($empAutoId, $amount)
  {

    $others = $this->findEmployeeOthersAmount($empAutoId);
    $other_adv_inst_amount = $others->other_adv_inst_amount;

    $allEmpSalary =  SalaryDetails::where('emp_id', $empAutoId)->update([
      'other_adv_inst_amount' => $amount + $other_adv_inst_amount
    ]);
  }

  public function insertEmployeeAdvance($empAutoId, $advPurposeId, $advAmount, $installMonth, $remarks, $date, $insertById)
  {
    $insert = new AdvanceInfo();
    $insert->emp_id = $empAutoId;
    $insert->adv_purpose_id = $advPurposeId;
    $insert->adv_amount = $advAmount;
    $insert->installes_month = $installMonth;
    $insert->adv_remarks = $remarks;
    $insert->year = date('Y', strtotime(Carbon::now()));
    $insert->date = $date;
    $insert->create_by = $insertById;
    $insert->created_at = Carbon::now();
    return  $insert->save();
  }

  // Save EMployee Advance 
  public function insert(Request $request)
  {

    // dd('okkkk');
    $this->validate($request, [
      'emp_id' => 'required',
      'adv_purpose_id' => 'required',
      'adv_amount' => 'required',
      'installes_month' => 'required',
    ], []);
    if ($request->installes_month != 0 && $request->adv_amount != 0) {
      $installes_amount = ($request->adv_amount / $request->installes_month);
    } else {
      Session::flash('error_0', 'value');
      return redirect()->back();
    }


    $creator = Auth::user()->id;
    // $findEmployeeObj = new EmployeeInfoController();
    // $employee_id = $findEmployeeObj->getEmployeeByEmpId($request->emp_id);
    $employee_id = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($request->emp_id);

    $emp_id = $employee_id->emp_auto_id;

    $others = $this->findEmployeeOthersAmount($emp_id);
    $other_adv_inst_amount = $others->other_adv_inst_amount;

    if ($request->adv_purpose_id == 1) {
      Session::flash('error', 'value');
      return redirect()->back();
    } else {

      // $insert = new AdvanceInfo();
      // $insert->emp_id = $emp_id;
      // $insert->adv_purpose_id = $request->adv_purpose_id;
      // $insert->adv_amount = $request->adv_amount;
      // $insert->installes_month = $request->installes_month;
      // $insert->adv_remarks = $request->adv_remarks;
      // $insert->year = date('Y', strtotime(Carbon::now()));
      // $insert->date = $request->adv_date;
      // $insert->create_by = $creator;
      // $insert->created_at = Carbon::now();
      // $insert->save();

      $insert =  $this->insertEmployeeAdvance($emp_id, $request->adv_purpose_id, $request->adv_amount, $request->installes_month, $request->adv_remarks, $request->adv_date, $creator);

      $this->updateEmployeeOthersAdvanceDeductionAmount($emp_id, $installes_amount);
      // $allEmpSalary =  SalaryDetails::where('emp_id', $emp_id)->update([
      //   'other_adv_inst_amount' => $installes_amount + $other_adv_inst_amount
      // ]);
    }
    if ($insert > 0) {
      Session::flash('success', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  public function update(Request $request)
  {

    // form validation
    $this->validate($request, [
      'emp_id' => 'required',
      'adv_purpose_id' => 'required',
      'adv_amount' => 'required',
      'installes_month' => 'required',
    ], []);

    if ($request->installes_month != 0 && $request->adv_amount != 0) {
      $installes_amount = ($request->adv_amount / $request->installes_month);
    } else {
      Session::flash('error_0', 'value');
      return redirect()->back();
    }

    $creator = Auth::user()->id;
    // $findEmployeeObj = new EmployeeInfoController();
    // $anEmployee = $findEmployeeObj->getEmployeeByEmpId($request->emp_id);
    $anEmployee = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($request->emp_id);

    $emp_id = $anEmployee->emp_auto_id;

    $others = $this->findEmployeeOthersAmount($emp_id);
    $other_adv_inst_amount = $others->other_adv_inst_amount;
    $iqama_adv_inst_amount = $others->iqama_adv_inst_amount;

    $id = $request->id;

    $update = AdvanceInfo::where('status', 1)->where('id', $id)->update([
      'emp_id' => $emp_id,
      'adv_purpose_id' => $request->adv_purpose_id,
      'adv_amount' => $request->adv_amount,
      'installes_month' => $request->installes_month,
      'adv_remarks' => $request->adv_remarks,
      'date' => Carbon::now(),
      'year' => date('Y', strtotime(Carbon::now())),
      'create_by' => $creator,
      'updated_at' => Carbon::now(),
    ]);
    if ($request->adv_purpose_id == 1) {
      SalaryDetails::where('emp_id', $emp_id)->update([
        'iqama_adv_inst_amount' => $installes_amount + $iqama_adv_inst_amount

      ]);
    } else {
      SalaryDetails::where('emp_id', $emp_id)->update([
        'other_adv_inst_amount' => $installes_amount + $other_adv_inst_amount
      ]);
    }


    if ($update) {
      Session::flash('success_update', 'value');
      return redirect()->route('addvance.payment');
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  // Employee Advance Setting Json Reponse 
  public function findEmployee(Request $request)
  {
    $emp_id = $request->emp_id;
    $year = date('Y', strtotime(Carbon::now()));


    // $findEmployeeObj = new EmployeeInfoController();
    // $findEmployeeById = $findEmployeeObj->getEmployeeByEmpId($emp_id);
    $findEmployeeById = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($emp_id);

    $empAutoId = $findEmployeeById->emp_auto_id;

    $advPayRecordObj = new AdvancePayRecordController();
    if ($findEmployeeById) {

      $salaryDetatils = $this->findEmployeeOthersAmount($empAutoId);

      $totalPaidIqama = $advPayRecordObj->getAnEmployeeAdvancePaidTotalAmount($empAutoId, $year, 1);
      $totalPaidOthers = $advPayRecordObj->getAnEmployeeAdvancePaidTotalAmount($empAutoId, $year, 2);
      $totalIqama = (new AnualFeeDetailsController())->getAnEmployeeIqamaTotalCost($empAutoId);

      $totalOthers = $this->getAnEmployeeTotalAdvanceAmount($empAutoId, $year, 0); // 0= All Other Advance 
      return response()->json([
        'totalIqama' => $totalIqama,
        'totalPaidIqama' => $totalPaidIqama,
        'totalOthers' => $totalOthers,
        'totalPaidOthers' => $totalPaidOthers,
        'empAutoId' => $empAutoId,
        'salaryDetatils' => $salaryDetatils,
      ]);
    } else
      return response()->json(['status' => 'error']);
  }


  /* update install advance amount */
  public function updateAdvanceInstallAmount(Request $request)
  {
    // dd($request->all());

    $nextPayIqama = $request->nextPayIqama;
    $nextPayOthers = $request->nextPayOthers;

    if ($nextPayOthers == "") {
      $nextPayOthers = 0;
    } elseif ($nextPayOthers == "" && $nextPayIqama == "") {
      $nextPayIqama = 0;
      $nextPayOthers = 0;
    } elseif ($nextPayIqama == "") {
      $nextPayIqama = 0;
    } else {
      $nextPayIqama = $request->nextPayIqama;
      $nextPayOthers = $request->nextPayOthers;
    }
    // dd($nextPayIqama."=".$nextPayOthers);
    $update = SalaryDetails::where('emp_id', $request->id)->update([
      'other_adv_inst_amount' => $nextPayOthers,
      'iqama_adv_inst_amount' => $nextPayIqama,
      'updated_at' => Carbon::now(),
    ]);


    /* redirect back */
    if ($update) {
      Session::flash('success', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }


  /*
      =================================
      ====== BLADE OPERATION ==========
      =================================
    */
  public function index()
  {
    // dd('dkdkdkd');
    $all = $this->getAll();
    $purpose = $this->getAllPurpose();
    return view('admin.emp-loan.all', compact('all', 'purpose'));
  }

  public function edit($id)
  {
    $edit = $this->findId($id);
    $purpose = $this->getAllPurpose();
    return view('admin.emp-loan.edit', compact('edit', 'purpose'));
  }


  public function delete($id)
  {

    $advaceRecord = $this->findId($id);
    $timestamp = strtotime($advaceRecord->date);
    $month = (int) date('m', $timestamp);
    $salaryRecord = SalaryProcessDataService::getAnEmployeeInfoWithSalaryHistory($advaceRecord->emp_id, $month, $advaceRecord->year);
    //(new SallaryGenerateController())->getAnEmployeeSalaryHistory($advaceRecord->emp_id, $month, $advaceRecord->year);
    if ($salaryRecord != null) {
      if ($salaryRecord->Status == 0) {
        (new AdvancePayRecordController())->deleteEmployeeAdvancePaidRecord($advaceRecord->emp_id, $salaryRecord->slh_month, $advaceRecord->year);
        (new SalaryDetailsController())->updateEmployeeAdvaceInstallAmount($advaceRecord->emp_id, 0, $advaceRecord->adv_purpose_id);
        $this->deleteAdvancePaymentRecord($id);
        Session::flash('success_delete', 'value');
        return redirect()->back();
      } else {
        Session::flash('error', 'value');
        return redirect()->back();
      }
    } else {
      $this->deleteAdvancePaymentRecord($id);
      (new SalaryDetailsController())->updateEmployeeAdvaceInstallAmount($advaceRecord->emp_id, 0, $advaceRecord->adv_purpose_id);
      Session::flash('success_delete', 'value');
      return redirect()->back();
    }
  }

  public function employeeMonthlyPaymentSetting()
  {

    return view('admin.emp-loan.advance-adjust');
  }


  // UI FOrm for Cash Received From Employee
  public function emplpyeeCashDepositFormRequest()
  {

    $cashPaymentlist = (new AdvancePayRecordController())->getAdvancePaidRecordWithPagination(100);
    //dd($cashPaymentlist);
    return view('admin.cash-deposit.employee-cash-deposit-form', compact('cashPaymentlist'));
  }

  public function emplpyeeCashDepositFormRequestWithAllRecords()
  {

    $cashPaymentlist = (new AdvancePayRecordController())->getAdvancePaidAllRecordNoPagination(100);
    //dd($cashPaymentlist);
    return view('admin.cash-deposit.employee-cash-deposit-form', compact('cashPaymentlist'));
  }

  // 

  public function deleteCashDepositAdvancePayment($id)
  {

    $result = (new AdvancePayRecordController())->deleteEmployeeCashPaymentRecord($id);
    if ($result) {
      Session::flash('success', 'Record is Deleted');
      return redirect()->route('employee.search.with.cash-payment');
    } else {
      Session::flash('error', 'Record Did not Delete. Try Again');
      return redirect()->back();
    }
  }

  public function advancePaymentReceivedFromEmployee(Request $request)
  {

    $userId = Auth::user()->id;
    // $empInfo = (new EmployeeInfoController())->getEmployeeByEmpId($request->emp_id);
    $empInfo = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($request->emp_id);

    $month = (new HelperController())->getMonthFromDateValue($request->payment_date);
    $year = (new HelperController())->getYearFromDateValue($request->payment_date);
    if ($empInfo == null) {
      Session::flash('error', 'value');
      return redirect()->back();
    }
    // 100 = cash payment
    $insertion = (new AdvancePayRecordController())->insertAdvancePaidRecord($empInfo->emp_auto_id, $request->pay_amount, $year, $month, 100, $userId);
    if ($insertion) {
      Session::flash('success', 'Successfully Completed Cash Payment ');
      return redirect()->back();
    } else {
      Session::flash('error', 'Error Occured, Please Input Correct Value');
      return redirect()->back();
    }
  }
}
