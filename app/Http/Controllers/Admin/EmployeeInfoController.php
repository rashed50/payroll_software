<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\ProjectInfoController;
use App\Http\Controllers\Admin\SponsorController;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\EmpCategoryController;
use App\Http\Controllers\Admin\AnualFee\AnualFeeDetailsController;
use App\Http\Controllers\Admin\Helper\HelperController;
use App\Http\Controllers\DataServices\EmployeeDataService;
//use App\Http\Controllers\Admin\DailyWorkHistoryController;
use App\Http\Controllers\Admin\Helper\UploadDownloadController;
use App\Http\Controllers\DataServices\SalaryProcessDataService;
use App\Models\EmpContactPerson;
use App\Models\EmpJobExperience;
use Illuminate\Http\Request;
use App\Models\IqamaRenewalDetails;
use App\Models\ProjectInfo;
use App\Models\EmployeeInfo;
//use App\Models\Division;
use App\Models\Religion;
use App\Models\JobStatus;
use App\Models\EmployeeCategory;

use App\Models\EmployeeType;
use App\Models\Sponsor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
// use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Enum\JobStatusEnum;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;

class EmployeeInfoController extends Controller
{




  public function getEmpCategory($emp_type_id)
  {
    $getEmpCatg = EmployeeCategory::where('emp_type_id', $emp_type_id)->where('catg_status', 1)->get();
    return json_encode($getEmpCatg);
  }

  public function checkEmployeeId(Request $request)
  {
    $findId = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($request->empId);
    if ($findId) {
      return response()->json(['data' => 1]);
    } else {
      return response()->json(['data' => 0]);
    }
  }




  /*
  |--------------------------------------------------------------------------
  |  FROM BLADE FILE REQUEST OPERATION
  |--------------------------------------------------------------------------
  */

  // ============== insert Employee Information in DATABASE ==============
  public function insert(Request $request)
  {
    $this->validate($request, [

      'emp_name' => 'required|string|max:30',
      'akama_no' => 'required|string|max:40|unique:employee_infos',
      'passfort_no' => 'required|string|max:40|unique:employee_infos',
      'mobile_no' => 'required|max:20|unique:employee_infos',
      'akama_expire' => 'required',
      'passfort_expire_date' => 'required',
      'sponsor_id' => 'required',
      'project_id' => 'required',

    ], [
      'emp_name.required' => 'please enter employee name!',
    ]);


    // $hourlyEmpValue = $request->hourly_employee;
    $creator = Auth::user()->id;

    $emp_auto_id = (new EmployeeDataService())->insertNewEmployee($request);

    if ($emp_auto_id > 0) {

      (new SalaryDetailsController())->addEmployeeSalaryDetails($emp_auto_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

      // Insert Employee Work as Project Work Record
      if ($request->project_id == '' || $request->project_id == null) {
        $request->project_id = 0;
      }
      (new EmployeeRelatedDataService())->assignEmployeeToNewProject($emp_auto_id, $request->project_id, $creator);

      $UploadDownloadConObj = new  UploadDownloadController();

      // profile photo
      if ($request->hasFile('profile_photo')) {
        $file = $request->file('profile_photo');
        $uplodedPath = $UploadDownloadConObj->uploadEmployeeProfilePhoto($file, null);
        // $this->updateEmployeeUploadedFileDbPath($emp_auto_id, $uplodedPath, 'profile_photo');
        $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($emp_auto_id, $uplodedPath, 'profile_photo');
      }

      // pasfort_photo upload
      if ($request->hasFile('pasfort_photo')) {
        $file = $request->file('pasfort_photo');
        $uplodedPath = $UploadDownloadConObj->uploadEmployeePassportFile($file, null);
        // $this->updateEmployeeUploadedFileDbPath($emp_auto_id, $uplodedPath, 'pasfort_photo');
        $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($emp_auto_id, $uplodedPath, 'pasfort_photo');
      }
    }
    if ($emp_auto_id) {
      return Redirect()->route('add-salary-info', [$emp_auto_id]);
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  // === Update Employee Basic & Salary Information ===
  public function updateEmployeeAllInformation(Request $request)
  {

    $id = $request->id;
    $creator = Auth::user()->id;
    $employee = (new EmployeeDataService())->getAnEmployeeInfoByEmpAutoId($id);
    $update = (new EmployeeDataService())->updateEmployeeAllInformation($request);
    $update = (new EmployeeDataService())->updateEmployeeSalaryAllInformation($request);

    if ($employee->project_id != $request->projectStatus) {

      $insert = (new EmployeeRelatedDataService())->assignEmployeeToNewProject1($request['id'], $request['projectStatus'], $request['asign_date'], $request['asign_date'], $creator);
    }

    if ($employee->job_status != $request->EmpStatus_id) {
      (new EmployeeRelatedDataService())->insertEmployeeJobStatusUpdateRecord($request['id'], Carbon::now(), $request['EmpStatus_id']);
    }


    if ($update) {
      Session::flash('success_update', 'value');
      return Redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }
  // === Update Employee Basic Information without file ===
  public function updateEmployeeInformationData(Request $request)
  {

    $update = (new EmployeeDataService())->updateEmployeeAllInformation($request);

    if ($update) {
      Session::flash('success_update', 'value');
      return Redirect()->route('employee-list');
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  // === Upload Employee File/Image ===
  public function updateEmployeeUploadedFileImage(Request $request)
  {
    $id = $request->id;
    $anEmployee = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpAutoId($id);
    $UploadDownloadConObj = new  UploadDownloadController();
    $update = false;
    // profile photo
    if ($request->hasFile('profile_photo')) {
      $file = $request->file('profile_photo');
      $uplodedPath = $UploadDownloadConObj->uploadEmployeeProfilePhoto($file, $anEmployee->profile_photo);
      // $update = $this->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'profile_photo');
      $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'profile_photo');
    }
    // pasfort_photo upload
    if ($request->hasFile('pasfort_photo')) {
      $file = $request->file('pasfort_photo');
      $uplodedPath = $UploadDownloadConObj->uploadEmployeePassportFile($file, $anEmployee->pasfort_photo);
      // $update = $this->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'pasfort_photo');
      $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'pasfort_photo');
    }


    // Iqama File
    if ($request->hasFile('akama_photo')) {
      $file = $request->file('akama_photo');
      $uplodedPath = $UploadDownloadConObj->uploadEmployeeIqamaFile($file, $anEmployee->akama_photo);
      // $update = $this->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'akama_photo');
      $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'akama_photo');
    }


    // Medical Report
    if ($request->hasFile('medical_report')) {
      $file = $request->file('medical_report');
      $uplodedPath = $UploadDownloadConObj->uploadEmployeeMedicalReportFile($file, $anEmployee->medical_report);
      //  $update = $this->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'medical_report');
      $update = (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'medical_report');
    }

    // appoint letter update
    if ($request->hasFile('appoint_latter')) {
      $file = $request->file('appoint_latter');
      $uplodedPath = $UploadDownloadConObj->uploadEmployeeAppointmentLetterFile($file, $anEmployee->employee_appoint_latter);
      //  $update = $this->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'employee_appoint_latter');
      $update =  (new EmployeeDataService())->updateEmployeeUploadedFileDbPath($id, $uplodedPath, 'employee_appoint_latter');
    }

    if ($update) {
      Session::flash('success_update_image', 'value');
      return Redirect()->route('employee-list');
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  // === Updated Employee Job Status ===
  public function approvalOfNewEmployeeInsertion($id)
  {
    //  1 = Active Employee
    $appoval = (new EmployeeDataService())->updateEmployeeJobStatus($id, JobStatusEnum::Active);
    if ($appoval) {
      Session::flash('approve', 'value');
      return redirect()->back();
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  // display employee salary summary report
  public function createAnEmployeeSalarySummaryReport(Request $request)
  {

    $employee_id = $request->emp_id;
    $iqamaNo = $request->iqamaNo;
    $year = $request->year;

    if ($request->chbox_sal_details != null) {
      return $this->profcessAndDisplayAnEmployeeSalarySummaryDetailsReport($request);
    } else {
      if ($employee_id != "") {
        $employee = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($employee_id);
      } else {
        $employee = (new EmployeeDataService())->getAnEmployeeInfoByEmpIqamaNo($iqamaNo);
      }


      if ($employee) {
        $company = (new CompanyProfileController())->findCompanry();
        $totalContribution = SalaryProcessDataService::TotalContribution($employee->emp_auto_id, $year);
        $totalIqamaAmountPaid = SalaryProcessDataService::TotalIqamaRenewal($employee->emp_auto_id, $year);
        $totalSaudiTax = SalaryProcessDataService::totalAdvance($employee->emp_auto_id, $year);
        $cashReceiveTotalPaidAmount =  (new AdvancePayRecordController())->getAnEmployeeAdvancePaidTotalAmount($employee->emp_auto_id, $year, 100);
        // 100 = Cash Received By Employee
        $totalUnPaidSalary = SalaryProcessDataService::getAnEmployeeTotalUnPaidSalary($employee->emp_auto_id, null);
        $iqamaRenewalTotalExpence = (int) (new AnualFeeDetailsController())->getAnEmployeeIqamaTotalCost($employee->emp_auto_id);

        // dd($iqamaRenewalTotalExpence);
        return view(
          'admin.report.employee_summary.employee_salary_summary',
          compact('totalContribution', 'totalIqamaAmountPaid', 'totalSaudiTax', 'totalUnPaidSalary', 'iqamaRenewalTotalExpence', 'cashReceiveTotalPaidAmount', 'employee', 'company', 'year')
        );
      } else {
        Session::flash('not_found', 'value');
        return redirect()->back();
      }
    }
  }

  public function profcessAndDisplayAnEmployeeSalarySummaryDetailsReport(Request $request)
  {

    $employee_id = $request->emp_id;
    $iqamaNo = $request->iqamaNo;
    $year = $request->year;

    if ($employee_id != "") {
      $employee = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($employee_id);
    } else {
      $employee = (new EmployeeDataService())->getAnEmployeeInfoByEmpIqamaNo($iqamaNo);
    }

    if ($employee) {


      $salary = SalaryProcessDataService::getAnEmployeeSalaryHistorySummary($employee->emp_auto_id, $year);
      $totalUnPaidSalary = SalaryProcessDataService::getAnEmployeeTotalUnPaidSalary($employee->emp_auto_id, null);
      $company = (new CompanyProfileController())->findCompanry();
      $anualFeeControllerOBJ = new AnualFeeDetailsController();
      $anualFee = $anualFeeControllerOBJ->getAll();

      $iqamaDetails = $anualFeeControllerOBJ->getAnEmployeeIqamaAnnualCost($employee->emp_auto_id, $year);
      if ($iqamaDetails ==  null) {
        $iqamaDetails = new IqamaRenewalDetails();
      }
      $cashReceiveTotalPaidAmount =  (new AdvancePayRecordController())->getAnEmployeeAdvancePaidTotalAmount($employee->emp_auto_id, $year, 100);
      // 100 = Cash Received By Employee
      $iqamaRenewalTotalExpence = $anualFeeControllerOBJ->getAnEmployeeIqamaRenewAnnualTotalCost($employee->emp_auto_id, $year);
      return view('admin.report.employee_summary.emp_summary_details', compact('totalUnPaidSalary', 'salary',  'cashReceiveTotalPaidAmount', 'company', 'anualFee', 'iqamaRenewalTotalExpence', 'employee', 'iqamaDetails', 'year'));
    } else {
      Session::flash('not_found', 'value');
      return redirect()->back();
    }
  }



  // ========###### employee list Report start #####=========

  public function projectWiseEmployeeListProcess(Request $request)
  {

    $company = (new CompanyProfileController())->findCompanry();
    $empRelatedDSObj = new EmployeeRelatedDataService();
    if ($request->proj_id == null) {
      $project = "All Project";
    } else {
      $project = $empRelatedDSObj->findAProjectInformation($request->proJ_id)->proj_name;
      // (new ProjectInfoController())->getFindId($request->proj_id)->proj_name;
      $project = "Project Name:" . $project;
    }
    // dd($request->all());
    $empType = $request->emp_type_id;
    if ($empType != 0) {
      $project = 'Employee Type:';
      $project = $project . ' ' . 'All';
      $hourly_employee = null;
      if ($empType > 1) {   // Indirect Employee
        $project = $project . ' ' . $empRelatedDSObj->getAnEmployeeTypeName($empType);
        // EmployeeType::where('id', $empType)->first()->name;
        $hourly_employee = NULL;

        $employee = EmployeeInfo::with('project', 'status')
          ->where('employee_infos.emp_type_id', $empType)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($empType == 1) { // Hourly Salary Employee
        $hourly_employee = true;
        $project = $project . ' ' . $empRelatedDSObj->getAnEmployeeTypeName($empType) . "-Hourly";

        $employee = EmployeeInfo::with('project', 'status')
          ->where('employee_infos.emp_type_id', $empType)
          ->where('employee_infos.hourly_employee', $hourly_employee)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($empType == -1) {  // Basic Salary Employee
        $hourly_employee = null;
        $empType = 1;
        $project = $project . ' ' . $empRelatedDSObj->getAnEmployeeTypeName($empType) . "-Basic Salary";

        $employee = EmployeeInfo::with('project', 'status')
          ->where('employee_infos.emp_type_id', $empType)
          ->where('employee_infos.hourly_employee', $hourly_employee)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      }
    } else {

      if ($request->proj_id ==  null &&  $request->spons_id ==  null && $request->catg_id == null && $request->job_status == null) {
        // 0000
        $employee = EmployeeInfo::with('project', 'status')
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id ==  null && $request->catg_id == null && $request->job_status != null) {
        //dd('0001');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('job_status', $request->job_status)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id ==  null && $request->catg_id != null && $request->job_status == null) {
        // dd('0010');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id ==  null && $request->catg_id != null && $request->job_status != null) {
        // dd('0011');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('job_status', $request->job_status)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id !=  null && $request->catg_id == null && $request->job_status == null) {
        // dd('0100');

        $employee = EmployeeInfo::with('project', 'status')
          ->where('sponsor_id', $request->spons_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id !=  null && $request->catg_id == null && $request->job_status != null) {
        // dd('0101');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('sponsor_id', $request->spons_id)
          ->where('job_status', $request->job_status)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id !=  null && $request->catg_id != null && $request->job_status == null) {
        // dd('0110');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('sponsor_id', $request->spons_id)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id ==  null && $request->spons_id !=  null && $request->catg_id != null && $request->job_status != null) {
        //dd('0111');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('job_status', $request->job_status)
          ->where('sponsor_id', $request->spons_id)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null &&  $request->spons_id ==  null && $request->catg_id == null && $request->job_status == null) {
        // dd('1000');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id ==  null && $request->catg_id == null && $request->job_status != null) {
        // dd('1001');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('job_status', $request->job_status)
          ->orderBy('employee_id', 'ASC')
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id ==  null && $request->catg_id != null && $request->job_status == null) {
        // dd('1010');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id ==  null && $request->catg_id != null && $request->job_status != null) {
        // dd('1011');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('job_status', $request->job_status)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id !=  null && $request->catg_id == null && $request->job_status == null) {
        // dd('1100');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('sponsor_id', $request->spons_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id !=  null && $request->catg_id == null && $request->job_status != null) {
        // dd('1101');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('sponsor_id', $request->spons_id)
          ->where('job_status', $request->job_status)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id !=  null && $request->catg_id != null && $request->job_status == null) {
        //  dd('1110');

        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('sponsor_id', $request->spons_id)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      } else if ($request->proj_id !=  null && $request->spons_id !=  null && $request->catg_id != null && $request->job_status != null) {
        //dd('1111');
        $employee = EmployeeInfo::with('project', 'status')
          ->where('project_id', $request->proj_id)
          ->where('job_status', $request->job_status)
          ->where('sponsor_id', $request->spons_id)
          ->where('designation_id', $request->catg_id)
          ->leftjoin('salary_details', 'employee_infos.emp_auto_id', '=', 'salary_details.emp_id')
          ->orderBy('employee_id', 'ASC')
          ->get();
      }
    }
    //   dd($employee[0]);
    if ($employee->count() > 0) {
      return view('admin.employee-info.project_wise.report', compact('employee', 'company', 'project'));
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }

  public function tradeWiseEmployeeListProcess(Request $request)
  {

    $catg_id = $request->catg_id;
    $emp_type_id = $request->emp_type_id;

    if ($emp_type_id == -1 && $catg_id == 0) {
      Session::flash('error', 'value');
      return redirect()->back();
    }
    $empTypeName = "All";
    $company = (new CompanyProfileController())->findCompanry();
    $tradeName =  "All";
    $employee = null;

    if ($emp_type_id == -1 && $catg_id >= 1) { // no type but trade
      $empTypeName = "All";
      $employee = (new EmployeeDataService())->getEmployeeListByEmpCategoryId($catg_id);
      $tradeName = EmployeeCategory::where('catg_id', $request->catg_id)->pluck('catg_name');
    } else if ($emp_type_id >= 0 && $catg_id == 0) {  // seleced type but no trade

      if ($emp_type_id == 0) {
        $empTypeName = "Direct Employee(Basic Salary)";
        $employee = (new EmployeeDataService())->getEmployeeListByCategoryIdEmpTypeAndHourlyEmp(-1, 1, null);
        // $employee = EmployeeInfo::where('emp_type_id', 1)->where('hourly_employee', null)->orderBy('employee_id', 'ASC')->get();
      } else if ($emp_type_id == 1) {

        $empTypeName = "Direct Employee(Hourly)";
        $employee = (new EmployeeDataService())->getEmployeeListByCategoryIdEmpTypeAndHourlyEmp(-1, 1, 1);
        // $employee = EmployeeInfo::where('emp_type_id', 1)->where('hourly_employee', 1)->orderBy('employee_id', 'ASC')->get();
      } else {
        $empTypeName = (new EmployeeRelatedDataService())->getAnEmployeeTypeName($request->emp_type_id);
        $employee = EmployeeInfo::where('emp_type_id', $emp_type_id)->orderBy('employee_id', 'ASC')->get();
      }
    } else {  // selected both menu

      $tradeName = EmployeeCategory::where('catg_id', $request->catg_id)->pluck('catg_name');

      if ($emp_type_id == 0) {
        $empTypeName = "Direct Employee(Basic Salary)";
        // $employee = EmployeeInfo::where('designation_id', $catg_id)->where('emp_type_id', 1)->where('hourly_employee', null)->orderBy('employee_id', 'ASC')->get();
        $employee = (new EmployeeDataService())->getEmployeeListByCategoryIdEmpTypeAndHourlyEmp($catg_id, 1, null);
      } else if ($emp_type_id == 1) {

        $empTypeName = "Direct Employee(Hourly)";
        $employee = (new EmployeeDataService())->getEmployeeListByCategoryIdEmpTypeAndHourlyEmp($catg_id, 1, 1);
        // $employee = EmployeeInfo::where('designation_id', $catg_id)->where('emp_type_id', 1)->where('hourly_employee', 1)->orderBy('employee_id', 'ASC')->get();
      } else {
        $empTypeName = (new EmployeeRelatedDataService())->getAnEmployeeTypeName($request->emp_type_id);
        $employee = (new EmployeeDataService())->getEmployeeListByCategoryIdEmpTypeAndHourlyEmp($catg_id, $emp_type_id, null);
        // $employee = EmployeeInfo::where('designation_id', $catg_id)->where('emp_type_id', $emp_type_id)->orderBy('employee_id', 'ASC')->get();
      }
    }

    if ($employee != NULL) {
      return view('admin.employee-info.trade_wise.report', compact('employee', 'company', 'tradeName', 'empTypeName'));
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }


  public function sponserWiseEmployeeListProcess(Request $request)
  {
    $company = (new CompanyProfileController())->findCompanry();
    $sponsor = Sponsor::where('spons_id', $request->spons_id)->pluck('spons_name');
    $employee = (new EmployeeDataService())->getEmployeeListWithProjectAndSponsor(0, $request->spons_id);
    // EmployeeInfo::where('sponsor_id', $request->spons_id)->orderBy('employee_id', 'ASC')->get();

    if ($employee != NULL) {
      return view('admin.employee-info.sponser_wise.report', compact('employee', 'company', 'sponsor'));
    } else {
      Session::flash('error', 'value');
      return redirect()->back();
    }
  }


  // Find An Employee update
  public function findEmployeeForUpdate(Request $request)
  {
    $employee_id = $request->emp_id;
    $iqamaNo = $request->iqamaNo;
    // dd($employee_id, $iqamaNo);
    if ($employee_id != "") {
      $employee = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($employee_id);
    } else {
      $employee = (new EmployeeDataService())->getAnEmployeeInfoByEmpIqamaNo($iqamaNo);
    }

    if ($employee) {

      /* find employee in request employee id wise */
      $findEmployee =  EmployeeInfo::with('country', 'division', 'district', 'employeeType', 'project', 'category', 'department', 'sponsor', 'status')
        ->where('emp_auto_id', $employee->emp_auto_id)->first();

      $salary = (new EmployeeDataService())->getAnEmployeeSalaryDetailsByEmpAutoId($employee->emp_auto_id);


      $allCountry =  (new EmployeeRelatedDataService())->getAllCountry();

      $districtObj = new DistrictController();
      $allDistrict = $districtObj->getAllDistrict();


      $allDivision = (new EmployeeRelatedDataService())->getDivisions(null);

      $allEmpType = (new EmployeeRelatedDataService())->getAllEmployeeType();

      $allDepartment = (new EmployeeRelatedDataService())->getAllDepartment();

      $allSponsor = (new EmployeeRelatedDataService())->getAllSponser();

      $getAllProject = (new EmployeeRelatedDataService())->getAllProjectInformation();

      $employeeStatusOBJ = new HelperController();
      $allEmployeeStatus = $employeeStatusOBJ->getEmployeeStatus();

      /* ====== Employee Designation Query ====== */
      $designationOBJ = new EmpCategoryController();
      $designation = $designationOBJ->getAllCategory();
      /* ====== Employee Job Experience Query ====== */
      $find_job_experience = EmpJobExperience::where('emp_id', $employee->emp_auto_id)->get();
      /* ====== Employee Contact Person Query ====== */
      $find_emp_contact_person = EmpContactPerson::where('emp_id', $employee->emp_auto_id)->get();
      /* ====== return json ====== */
      return json_encode([
        'allCountry' => $allCountry,
        'allDistrict' => $allDistrict,
        'allSponsor' => $allSponsor,
        'allDepartment' => $allDepartment,
        'allEmpType' => $allEmpType,
        'allDivision' => $allDivision,
        'allEmployeeStatus' => $allEmployeeStatus,
        'getAllProject' => $getAllProject,
        'find_job_experience' => $find_job_experience,
        'find_emp_contact_person' => $find_emp_contact_person,
        'findEmployee' =>  $findEmployee,
        'salary' => $salary,
        'designation' => $designation,
      ]);
    } else {
      return json_encode([
        'success' => false,
        'status-code' => 404,
        'status' => "error",
      ]);
    }
  }


  // ========###### employee list end #####=========

  public function updateAnEmployeeJobStatus(Request $request)
  {
    $id = $request->emp_auto_id;
    $update = (new EmployeeDataService())->updateEmployeeJobStatus($id, $request['empStatus']);
    (new EmployeeRelatedDataService())->insertEmployeeJobStatusUpdateRecord($request->emp_auto_id, Carbon::now(), $request['empStatus']);

    if ($update) {
      Session::flash('success', 'Successfully Updated');
      return redirect()->back();
    } else {
      Session::flash('erroe', 'Please try again');
    }
  }


  public function projectStatus(Request $request)
  {

    $emp_auto_id = $request->emp_auto_id;
    $project_id = $request->projectStatus;

    $update = (new EmployeeDataService())->updateEmployeeAssignedProject($emp_auto_id, $project_id);

    $creator = Auth::user()->id;

    $insert = (new EmployeeRelatedDataService())->assignEmployeeToNewProject1($request['emp_auto_id'], $request['projectStatus'], $request['date'], $request['date'], $creator);

    if ($update && $insert) {
      Session::flash('successProject', 'Successfully insert project info');
      return redirect()->back();
    } else {
      Session::flash('erroe', 'Please try again');
    }
  }


  public function preRelease(Request $request)
  {
    $all = (new EmployeeDataService())->getAllEmployeesInformation(-1, JobStatusEnum::Release);
    // 5 = job status release
    return view('admin.salary-generate.pending-salary', compact('pendingSalary'));
  }

  public function preReleaseUpdateStatus($id)
  {

    $update = (new EmployeeDataService())->updateEmployeeJobStatus($id, JobStatusEnum::Vacation);
    $all = (new EmployeeDataService())->getAllEmployeesInformation(-1, JobStatusEnum::PreRelease);
    // 4 = prerelease job status

    if ($update) {
      Session::flash('success', 'successfuly update employee status');
      return redirect()->back();
    } else {
      Session::flash('error', 'Please try again');
      return redirect()->back();
    }
  }

  /*
  |--------------------------------------------------------------------------
  |  BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index()
  {

    $all =  (new EmployeeDataService())->getAllEmployeesInformation(2000, JobStatusEnum::Active);
    // 2000 = page limit, 1 = active employee
    $allActive = (new EmployeeDataService())->countTotalEmployees(1); // 1 = active employee

    return view('admin.employee-info.index', compact('all', 'allActive'));
  }

  public function jobApprove()
  {
    $all = (new EmployeeDataService())->getAllEmployeesInformation(-1, JobStatusEnum::ApprovalPending);
    // -1 = no limit, 0 = approval pending emlpoyee 
    return view('admin.employee-info.job-approve', compact('all'));
  }

  public function preReleaseList()
  {
    // $all = $this->getAllPrereles();
    $all = (new EmployeeDataService())->getAllEmployeesInformation(-1, JobStatusEnum::PreRelease);

    return view('admin.employee-info.pre-release', compact('all'));
  }

  public function releaseList()
  {
    //$all = $this->getAllRelease();
    $all = (new EmployeeDataService())->getAllEmployeesInformation(-1, JobStatusEnum::Vacation);

    return view('admin.employee-info.release', compact('all'));
  }

  public function searchEmp()
  {
    return view('admin.employee-info.search-emp');
  }

  public function searchEmpForUpdate()
  {

    return view('admin.employee-info.emp-update');
  }


  public function searchEmpStatus()
  {

    return view('admin.employee-info.search-status');
  }


  public function getReligion()
  {
    return $all = Religion::get();
  }



  public function add()
  {

    $EmpCategoryConObj = new EmpCategoryController();
    $designationList = $EmpCategoryConObj->getAllCategory();


    $countryList =  (new EmployeeRelatedDataService())->getAllCountry();


    $empTypes = (new EmployeeRelatedDataService())->getAllEmployeeType();


    $allDepart = (new EmployeeRelatedDataService())->getAllDepartment();

    $empIdGeneret = (new EmployeeDataService())->generateEmployeeId();
    $relig = $this->getReligion();
    $proj = (new EmployeeRelatedDataService())->getAllProjectInformation();

    $sponsor = (new EmployeeRelatedDataService())->getAllSponser();

    return view('admin.employee-info.add', compact('designationList', 'sponsor', 'proj', 'relig', 'countryList', 'empTypes', 'allDepart', 'empIdGeneret'));
  }

  public function addSalaryDetails($emp_auto_id)
  {
    $employee = (new EmployeeDataService())->getAnEmployeeInfoByEmpAutoId($emp_auto_id);
    return view('admin.employee-info.add-salary-info', compact('emp_auto_id', 'employee'));
  }

  public function edit($emp_auto_id)
  {
    Cache::flush();
    cache()->flush();
    $EmpCategoryConObj = new EmpCategoryController();
    $designationList = $EmpCategoryConObj->getAllCategory();

    $countryList =  (new EmployeeRelatedDataService())->getAllCountry();
    /* employee type */

    $empTypes = (new EmployeeRelatedDataService())->getAllEmployeeType();

    /* call Department */

    $allDepart = (new EmployeeRelatedDataService())->getAllDepartment();
    /* main query */
    $edit = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpAutoId($emp_auto_id);
    /* Religion */
    $relig = $this->getReligion();

    $proj = (new EmployeeRelatedDataService())->getAllProjectInformation();

    $sponsor = (new EmployeeRelatedDataService())->getAllSponser();


    return view('admin.employee-info.edit', compact('designationList', 'sponsor', 'edit', 'proj', 'relig', 'countryList', 'empTypes', 'allDepart'));
  }

  public function view($emp_auto_id)
  {
    $view = (new EmployeeDataService())->getAnEmployeeInfoByEmpAutoId($emp_auto_id);

    return view('admin.employee-info.view', compact('view'));
  }

  /* ==================== Employee Salary Summary ==================== */
  //  UI for viewing Emp. Salary Summary and Details Report
  public function EmployeeSummary()
  {
    return view('admin.employee-info.salary-summary');
  }

  /* ==================== Project Wise Employee List Report UI form ==================== */
  public function projectWiseEmployeeList()
  {

    $projects = (new EmployeeRelatedDataService())->getAllProjectInformation();

    $sponser = (new EmployeeRelatedDataService())->getAllSponser();

    $categoryOBJ = new EmpCategoryController();
    $category = $categoryOBJ->getAllCategory();

    $jobStatus = JobStatus::all();

    $emp_types = (new EmployeeRelatedDataService())->getAllEmployeeType();


    return view('admin.employee-info.project_wise.all', compact('projects', 'sponser', 'category', 'jobStatus', 'emp_types'));
  }
  /* ==================== Trade Wise Employee List ==================== */
  public function tradeWiseEmployeeList()
  {
    $category = (new EmpCategoryController())->getAllCategory();
    $empTypeList = (new EmployeeRelatedDataService())->getAllEmployeeType();
    return view('admin.employee-info.trade_wise.all', compact('category', 'empTypeList'));
  }

  // UI For Creating EMployee Report 
  public function sponserWiseEmployeeList()
  {

    $sponser = (new EmployeeRelatedDataService())->getAllSponser();

    return view('admin.employee-info.sponser_wise.all', compact('sponser'));
  }




  /*
  |--------------------------------------------------------------------------
  |  API OPERATION
  |--------------------------------------------------------------------------
  */







  /* ======== end class bracket ======== */
}
