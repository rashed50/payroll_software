<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\admin\EmployeeMultiProjectWorkHistoryController;
use App\Http\Controllers\Admin\Helper\HelperController;
use App\Http\Controllers\Admin\InOut\EmployeeInOutController;
use App\Http\Controllers\Admin\Permission\AccessPermissionController;
use App\Http\Controllers\Admin\Services\EmployeeService;
use App\Http\Controllers\Admin\Services\EmployeeWorkService;
use App\Http\Controllers\DataServices\CompanyDataService;
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\MonthlyWorkHistory;
use App\Models\EmployeeInfo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class DailyWorkHistoryController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  |  DATABASE OPERATION
  |--------------------------------------------------------------------------
  */
  public function getAll()
  {
    // $currentMonth =  Carbon::now()->format('m');
    return $all = MonthlyWorkHistory::where('status', 1)->orderBy('month_work_id', 'desc')->take(100)->get();
  }
  public function getMonthlyWorkRecords($month, $year)
  {

    $previousMonth = $month;
    if ($month == 1) {
      $previousMonth = 12;
    } else {
      $previousMonth = $month - 1;
    }

    return $all = MonthlyWorkHistory::where('month_id', $month)->orWhere('month_id', $previousMonth)->where('status', 1)->orderBy('month_work_id', 'desc')->take(20)->get();
  }

  public function getfindId($id)
  {
    return $find = MonthlyWorkHistory::with('employee')->where('status', 1)->where('month_work_id', $id)->firstOrFail();
  }

  public function deleteIdWiseMonthlyWorkHistory($month_work_id)
  {
    return $find = MonthlyWorkHistory::where('status', 1)->where('month_work_id', $month_work_id)->delete();
  }

  public function deleteMonthAndYearWiseMonthlyWorkHistory($month, $year)
  {
    return $find = MonthlyWorkHistory::where('status', 1)
      ->where('month_id', $month)
      ->where('year_id', $year)
      ->delete();
  }


  public function insrtEmpMonthlyWorkRecord($emp_id, $month, $year, $totalHourTime, $totalOverTime, $totalDays)
  {
    return $insert = MonthlyWorkHistory::insert([
      'emp_id' => $emp_id,
      'month_id' => $month,
      'year_id' => $year,
      'total_hours' => $totalHourTime,
      'overtime' =>  $totalOverTime,
      'total_work_day' =>  $totalDays,
      'entered_id' =>  Auth::user()->id,
      'created_at' => Carbon::now(),
    ]);
  }



  public function store(Request $request)
  {

    $accessPermConObj = new AccessPermissionController();
    $hasAccessPermission = $accessPermConObj->checkAccessPermissionWithDate($request->month, $request->year);

    if ($hasAccessPermission != null) {
      if ($request->total_work_day == null || $request->work_hours == null || $request->total_work_day == 0) {
        Session::flash('error_null_0', 'value');
        return Redirect()->back();
      }

      $month = $request->month;
      $year = $request->year;
      $emp_auto_id = $request->emp_id;
      $recode = MonthlyWorkHistory::where('emp_id', $emp_auto_id)
        ->where('year_id', $year)
        ->where('month_id', $month)->first();
      if ($recode) {
        Session::flash('duplicate_data_error', '');
        return Redirect()->back();
      } else {

        $totalHourTime =  $request->work_hours == null ? 0 : $request->work_hours;

        // $employeeInfoConObj = new EmployeeInfoController();
        //$empInfoObj =  $employeeInfoConObj->getEmployeeById($emp_auto_id);
        $empInfoObj = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpAutoId($emp_auto_id);


        $startDate = Carbon::now();
        $endDate = Carbon::now()->addDay($request->total_work_day);

        $insert = $this->insrtEmpMonthlyWorkRecord($emp_auto_id, $month, $year, $totalHourTime, $request->overtime, $request->total_work_day);

        $empInOutConObj = new EmployeeInOutController();
        $insert = $empInOutConObj->insrtEmpMultiProjectWorkRecord($startDate, $endDate, $emp_auto_id, $empInfoObj->project_id, $month, $year, $request->total_work_day, $totalHourTime, $request->overtime);

        Session::flash('success', 'value');
        return Redirect()->back();
      }
    } else {
      Session::flash('error_date', 'Permission Date over!');
      return redirect()->back();
    }
  }

  public function insertIndirect(Request $request)
  {

    /* form validation */
    $this->validate($request, [
      'indirect_emp_id' => 'required',
      'total_work_day' => 'required|integer',
    ], []);
    /* insert data in database */
    $year = Carbon::now()->format('Y');
    $month = $request->month;
    $entered_id = Auth::user()->id;
    /*find employee id */
    $findId = (new EmployeeDataService())->getAnEmployeeInfoByEmpId($request->indirect_emp_id);
    // EmployeeInfo::where('employee_id', $request->indirect_emp_id)->where('job_status', 1)->first();
    $employee_id = $findId->emp_auto_id;

    $recode = MonthlyWorkHistory::where('emp_id', $employee_id)->where('month_id', $month)->first();

    if ($recode) {
      Session::flash('duplicate', 'value');
      return Redirect()->back();
    } else {
      $insert = MonthlyWorkHistory::insert([
        'emp_id' => $employee_id,
        'month_id' => $month,
        'year_id' => $year,
        'total_hours' => 0,
        'overtime' => $request->overtime,
        'total_work_day' => $request->total_work_day,
        'entered_id' => $entered_id,
        'created_at' => Carbon::now(),
      ]);

      if ($insert) {
        Session::flash('success', 'value');
        return Redirect()->back();
      } else {
        Session::flash('error', 'value');
        return Redirect()->back();
      }
    }
  }


  public function update(Request $request)
  {

    $this->validate($request, [
      'emp_id' => 'required',
      'total_work_day' => 'required|integer',
    ], []);

    if ($request->total_work_day == null || $request->work_hours == null || $request->total_work_day == 0 || $request->work_hours == 0) {
      Session::flash('error_null_0', 'value');
      return Redirect()->back();
    }
    /* insert data in database */
    // $year = Carbon::now()->format('Y');
    // $month = Carbon::now()->format('m');
    $entered_id = Auth::user()->id;
    /*find employee id */
    $findId = (new EmployeeDataService())->getAnEmployeeInfoByEmpId($request->emp_id);
    // EmployeeInfo::where('employee_id', $request->emp_id)->where('job_status', 1)->first();
    $employee_id = $findId->emp_auto_id;

    $recode = MonthlyWorkHistory::where('emp_id', $employee_id)->where('month_id', $request->month)->first();

    $id = $request->id;

    $monhWorkConObj = new MonthlyWorkHistoryController();
    $update = $monhWorkConObj->monthliWorkHistRecrdUpdate($request->id, $request->month, $request->year, $request->work_hours, $request->overtime, $request->total_work_day);

    if ($update) {
      Session::flash('success_update', 'value');
      return Redirect()->route('add-daily-work');
    } else {
      Session::flash('error', 'value');
      return Redirect()->back();
    }
  }

  /*
  |--------------------------------------------------------------------------
  |  AJAX OPERATION
  |--------------------------------------------------------------------------
  */

  public function autocomplete(Request $request)
  {
    $data = (new EmployeeDataService())->getEmployeeByEmpIdWithLikeQuery($request->empId, 1);
    // EmployeeInfo::where("employee_id", "LIKE", "%{$request->empId}%")->where('job_status', 1)->get();
    return view('admin.month-work.search', compact('data'));
  }

  public function conditionAutocomplete(Request $request)
  {
    $data = (new EmployeeDataService())->getEmployeeByEmpIdAndEmpTypeWithLikeQuery($request->empId, 1, 2);
    // EmployeeInfo::where("employee_id", "LIKE", "%{$request->empId}%")->where('job_status', 1)->where('emp_type_id', 2)->get();
    return view('admin.month-work.search', compact('data'));
  }

  public function findDirectEmployee(Request $request)
  {
    $data = (new EmployeeDataService())->getEmployeeByEmpIdAndEmpTypeWithLikeQuery($request->empId, 1, 1);
     return view('admin.month-work.search2', compact('data'));
  }

  public function findEmployeeTypeId(Request $request)
  {
   // dd('calling to findEmployeeTypeId');
    $findType = EmployeeInfo::where('emp_type_id', $request->emp_type_id)->where('job_status', 1)->first();
    return json_encode($findType);
  }


  public function deleteDailyWorkHistory($id)
  {


    $empMonthWorkHistory = $this->getfindId($id);
    $emp_id = $empMonthWorkHistory->emp_id;
    $month_id = $empMonthWorkHistory->month_id;
    $year_id = $empMonthWorkHistory->year_id;

    $month_work_id = $empMonthWorkHistory->month_work_id;

    $DeleteEmpMonthWorkHistory = $this->deleteIdWiseMonthlyWorkHistory($month_work_id);

    $empMultiProjConObj = new EmployeeMultiProjectWorkHistoryController();
    $deleteEmpMultiProject =  $empMultiProjConObj->deleteMonthAndYearWiseAnEmpMultiprojectWorkHistory($emp_id, $month_id, $year_id);

    return response()->json();
  }





  /*
  |--------------------------------------------------------------------------
  |  BLADE OPERATION
  |--------------------------------------------------------------------------
  */
  public function index()
  {


    /* employee type controller call */
    $emp_type_id = (new EmployeeRelatedDataService())->getAllEmployeeType();
    // $employee_type->getEmployeeTypeAll();
    $currentMonth =  Carbon::now()->format('m');
    $currentYear =  Carbon::now()->format('y');
    $all = $this->getMonthlyWorkRecords((int)$currentMonth, (int)$currentYear);

    $month = (new CompanyDataService())->getAllMonth();

    // dd((int) $currentMonth);
    return view('admin.month-work.create', compact('all', 'emp_type_id', 'month', 'currentMonth'));
  }


  public function edit($id)
  {

    $month = (new CompanyDataService())->getAllMonth();
    $currentMonth =  Carbon::now()->format('m');

    $edit = $this->getfindId($id);
    return view('admin.month-work.edit', compact('edit', 'currentMonth', 'month'));
  }


  // Blade File For Monthly Work Hisotry Processing Report
  public function getEmployeMonthlyWorkHistory()
  {
    $projects = (new EmployeeRelatedDataService())->getAllProjectInformation();

    $month = (new CompanyDataService())->getAllMonth();

    $sponser = (new EmployeeRelatedDataService())->getAllSponser();


    $emplyoyeeStatus = (new HelperController())->getEmployeeStatus();

    return view('admin.month-work.monthwork-reportprocess', compact('projects', 'month', 'sponser', 'emplyoyeeStatus'));
  }

  /* +++++++++++++++ Project Wise Month Work History +++++++++++++++ */
  public function getEmployeMonthlyWorkHistoryProcess(Request $request)
  {
    $companyOBJ = new CompanyProfileController();
    $company = $companyOBJ->findCompanry();

    $month = $request->month;
    $projectId = $request->proj_id;
    $sponserId = $request->SponsId;
    // $projectOBJ = new ProjectInfoController();
    $projectName = (new EmployeeRelatedDataService())->findAProjectInformation($projectId);

    if ($projectId == 0 && $sponserId == 0) {
      $projectName = "All";
      $projectWiseReport = EmployeeInfo::where('monthly_work_histories.month_id', $month)
        ->where('monthly_work_histories.year_id', Carbon::now()->format('Y'))
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->get();
    } else {
      $projectName = $projectName->proj_name;
      $projectWiseReport = EmployeeInfo::where("employee_infos.project_id", $projectId)
        ->where("employee_infos.sponsor_id", $sponserId)
        ->where('monthly_work_histories.month_id', $month)
        ->where('monthly_work_histories.year_id', Carbon::now()->format('Y'))
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->get();
    }

    if (count($projectWiseReport) > 0) {
      return view('admin.month-work.monthwork-report', compact('projectWiseReport', 'company', 'projectName'));
    } else {
      return 'Records Not found ';
    }
  }

  // EMPLOYEE THOSE ARE NOT IN WORK RECROD REPORT
  public function processEmployeNotPresentInMonthlyWorkHistory(Request $request)
  {

    $company = (new CompanyProfileController())->findCompanry();
    $month = $request->month;
    $monthName = (new HelperController())->getMonthName($month);
    $projectId = $request->proj_id;
    $sponserId = $request->SponsId;
    $emp_status_id = $request->emp_status_id;
    $year = Carbon::now()->format('Y');
    $projectName = (new EmployeeRelatedDataService())->findAProjectInformation($projectId);


    $list =  (new EmployeeMultiProjectWorkHistoryController())->getListOfEmployeeAutoIdExistInMultiProectWorkRecord($month, $year);
    if ($emp_status_id == 0 && $projectId == 0 && $sponserId == 0) {
      // $project = "All Project";

      $employee = EmployeeInfo::leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();

      // dd($employee[0]);
    } else if ($emp_status_id == 0 && $projectId == 0 && $sponserId > 0) {
      //$project = 'All Project' . $projectName->proj_name;

      $employee = EmployeeInfo::where("employee_infos.sponsor_id", $sponserId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id == 0 && $projectId > 0 && $sponserId == 0) {
      $employee = EmployeeInfo::where("employee_infos.project_id", $projectId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id == 0 && $projectId > 0 && $sponserId > 0) {
      $employee = EmployeeInfo::where("employee_infos.sponsor_id", $sponserId)
        ->where("employee_infos.project_id", $projectId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id > 0 && $projectId == 0 && $sponserId == 0) {

      $employee = EmployeeInfo::where('employee_infos.job_status', $emp_status_id)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id > 0 && $projectId == 0 && $sponserId > 0) {

      $employee = EmployeeInfo::where('employee_infos.job_status', $emp_status_id)
        ->where("employee_infos.sponsor_id", $sponserId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id > 0 && $projectId > 0 && $sponserId == 0) {

      $employee = EmployeeInfo::where('employee_infos.job_status', $emp_status_id)
        ->where("employee_infos.project_id", $projectId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    } else if ($emp_status_id > 0 && $projectId > 0 && $sponserId > 0) {
      $employee = EmployeeInfo::where('employee_infos.job_status', $emp_status_id)
        ->where("employee_infos.sponsor_id", $sponserId)
        ->where("employee_infos.project_id", $projectId)
        ->leftjoin('sponsors', 'employee_infos.sponsor_id', '=', 'sponsors.spons_id')
        ->leftjoin('countries', 'employee_infos.country_id', '=', 'countries.id')
        ->leftjoin('job_statuses', 'employee_infos.job_status', '=', 'job_statuses.id')
        ->leftjoin('employee_types', 'employee_infos.emp_type_id', '=', 'employee_types.id')
        ->leftjoin('employee_categories', 'employee_infos.designation_id', '=', 'employee_categories.catg_id')
        ->leftjoin('project_infos', 'employee_infos.project_id', '=', 'project_infos.proj_id')
        ->leftjoin('monthly_work_histories', 'employee_infos.emp_auto_id', '=', 'monthly_work_histories.emp_id')
        ->whereNotIn('employee_infos.emp_auto_id', $list)
        ->get();
    }
    if (count($employee) > 0) {
      return view('admin.month-work.report.report-employee-not-in-work-record', compact('employee', 'company', 'monthName'));
    } else {
      return 'Record Not Found ';
    }
  }


  public function processAllEmployeMonthlyWorkStatus(Request $request)
  {
    $year = (new HelperController())->getYear();
    //dd($year);
    $project = "All";
    $sponser = 'All';
    $month = (new HelperController())->getMonthName($request->month);
    $company = (new CompanyProfileController())->findCompanry();
    $totalActiveEmployee = (new EmployeeService())->getTotalAllActiveInactiveEmployees(1);
    $totalEmployee = (new EmployeeService())->getTotalAllActiveInactiveEmployees(0);
    $totalWokringEmp = (new EmployeeWorkService())->getTotalWorkingEmployees($request->month, $year);

    // dd($totalActiveEmployee, $totalEmployee, $totalWokringEmp);

    return view('admin.month-work.report.employee-work-status-summary', compact('company', 'project', 'month', 'year', 'totalActiveEmployee', 'totalEmployee', 'totalWokringEmp'));
  }




  /* +++++++++++++++ Project Wise Month Work History +++++++++++++++ */

  public function displayEmployeMonthlyWorkHistoryReport()
  {

    $emp_type_id = (new EmployeeRelatedDataService())->getAllEmployeeType();

    $all = $this->getAll();

    $month = (new CompanyDataService())->getAllMonth();

    $currentMonth =  Carbon::now()->format('m');
    return view('admin.month-work.report-employe-monthwork', compact('all', 'emp_type_id', 'month', 'currentMonth'));
  }
}
