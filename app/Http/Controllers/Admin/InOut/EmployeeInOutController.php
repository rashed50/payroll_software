<?php

namespace App\Http\Controllers\Admin\InOut;

use App\Http\Controllers\Controller;
 
use App\Http\Controllers\Admin\CompanyProfileController;
use App\Http\Controllers\Admin\DailyWorkHistoryController;
 
use App\Http\Controllers\Admin\Helper\HelperController;
use App\Http\Controllers\Admin\EmployeeMultiProjectWorkHistoryController;
use App\Http\Controllers\Admin\MonthlyWorkHistoryController;
use App\Http\Controllers\DataServices\CompanyDataService;
use App\Http\Controllers\DataServices\EmployeeDataService;
use App\Http\Controllers\DataServices\EmployeeRelatedDataService;
use Illuminate\Http\Request;
use App\Models\EmployeeInOut;
use App\Models\EmployeeInfo;
use App\Models\MonthlyWorkHistory;
use App\Models\SalaryDetails;
use App\Models\EmployeeMultiProjectWorkHistory;
use Carbon\Carbon;
 
use Illuminate\Support\Facades\Auth;

class EmployeeInOutController extends Controller
{


  public function myedit($id)
  {
    return response()->json([
      'data' => ['name' => 'rashed', 'id' => 1]
    ]);
  }

  public function getAnEmployeeMultiProjectWorkRecord(Request $request)
  {
    //  return $request->id;

    $EmpMultiProjWorkHisConObj = new EmployeeMultiProjectWorkHistoryController();
    $workRecord = $EmpMultiProjWorkHisConObj->findIdWiseAnEmpMultiprojectWorkHistory($request->id);
    
    $month =(new CompanyDataService())->getAllMonth(); 
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
     
    $error = "data not Found!";
    return response()->json(['workRecord' => $workRecord, 'month' => $month, 'project' => $project, 'error' => $error]);
  }

  public function insrtEmpMultiProjectWorkRecord($startDate, $endDate, $emp_id, $proj_name, $month, $year, $totalDays, $totalHourTime, $totalOverTime)
  {
    return $insert = EmployeeMultiProjectWorkHistory::insert([
      'emp_id' => $emp_id,
      'project_id' => $proj_name,
      'month' => $month,
      'year' => $year,
      'total_day' => $totalDays,
      'total_hour' => $totalHourTime,
      'total_overtime' => $totalOverTime,
      'start_date' => $startDate,
      'end_date' => $endDate,
      'created_at' => Carbon::now()->toDateTimeString()
    ]);
  }

  /*
    |--------------------------------------------------------------------------
    |  DATABASE OPERATION
    |--------------------------------------------------------------------------
    */

  /* Ajax method */
  /* =============== Employee Multiproject Monthly Work Insert =============== */

  public function multipleInsert(Request $request)
  {


    $emp_id = $request->emp_id;
    $proj_name = $request->proj_name;
    $startDate = Carbon::parse($request->startDate);
    $endDate =  Carbon::parse($request->endDate);
    $creator = Auth::user()->id;

    $fromDate = $request->startDate;
    $date = date('d', strtotime($fromDate));
    $month = date('m', strtotime($fromDate));
    $year = date('Y', strtotime($fromDate));

    $totalHourTime = $request->totalHourTime;
    $totalOverTime = $request->totalOverTime;
    $totalHours = $totalOverTime + $totalHourTime;

    $totalDays = $startDate->diffInDays($endDate) + 1;
    $workHour = $totalHours / $totalDays;
    // dd($request->all());
    $findEmployee = EmployeeInfo::where('employee_id', $emp_id)->where('job_status', 1)->first();
    if ($findEmployee) {

      if ($totalDays != 0) {

        /* update month work history */
        $findEmployeeInMonthWork = MonthlyWorkHistory::where('emp_id', $findEmployee->emp_auto_id)
          ->where('month_id', $month)
          ->where('year_id', $year)
          ->first();


        if ($findEmployeeInMonthWork) {
          /* old hours ,day overtime */
          $oldOvertime = $findEmployeeInMonthWork->overtime;
          $oldTotalHours = $findEmployeeInMonthWork->total_hours;
          $oldDays = $findEmployeeInMonthWork->total_work_day;

          //
          $update = MonthlyWorkHistory::where('emp_id', $findEmployee->emp_auto_id)->where('month_id', $month)
            ->where('year_id', $year)->update([
              'total_hours' => $oldTotalHours + $totalHourTime,
              'overtime' => $oldOvertime + $totalOverTime,
              'total_work_day' => $oldDays + $totalDays,
              'updated_at' => Carbon::now()
            ]);
        } else {
          $empInOutConObj = new DailyWorkHistoryController();
          $insert = $empInOutConObj->insrtEmpMonthlyWorkRecord($findEmployee->emp_auto_id, $month, $year, $totalHourTime, $totalOverTime, $totalDays);
          // $insert = MonthlyWorkHistory::insert([
          //   'emp_id' => $findEmployee->emp_auto_id,
          //   'month_id' => $month,
          //   'year_id' => $year,
          //   'total_hours' => $totalHourTime,
          //   'overtime' =>  $totalOverTime,
          //   'total_work_day' =>  $totalDays,
          //   'entered_id' =>  Auth::user()->id,
          //   'created_at' => Carbon::now(),
          // ]);

        }
        /* update multi project work history */
        $proj_exit = EmployeeMultiProjectWorkHistory::where('emp_id', $findEmployee->emp_auto_id)
          ->where('month', $month)
          ->where('year', $year)
          ->where('project_id', $proj_name)
          ->first();

        if ($proj_exit != null) {
          $update = EmployeeMultiProjectWorkHistory::where('emp_id', $findEmployee->emp_auto_id)
            ->where('month', $month)
            ->where('year', $year)->where('project_id', $proj_name)
            ->update([
              'total_day' => $proj_exit->total_day + $totalDays,
              'total_hour' => $proj_exit->total_hour + $totalHourTime,
              'total_overtime' => $totalOverTime,
              'start_date' => $startDate,
              'end_date' => $endDate,
              'updated_at' => Carbon::now()->toDateTimeString()
            ]);
        } else {

          // $insert = EmployeeMultiProjectWorkHistory::insert([
          //   'emp_id' => $findEmployee->emp_auto_id,
          //   'project_id' => $proj_name,
          //   'month' => $month,
          //   'year' => $year,
          //   'total_day' => $totalDays,
          //   'total_hour' => $totalHourTime,
          //   'total_overtime' => $totalOverTime,
          //   'created_at' => Carbon::now()->toDateTimeString()
          // ]);

          $insert = $this->insrtEmpMultiProjectWorkRecord($startDate, $endDate, $findEmployee->emp_auto_id, $proj_name, $month, $year, $totalDays, $totalHourTime, $totalOverTime);
        }
        return response()->json(['success' => 'Successfully Added Employee Entry Time']);
      } else {
        return response()->json(['error' => 'Days Not Found!']);
      }
    } else {
      return response()->json(['error' => 'Employee Not Found!']);
    }
  }


  /* =============== Employee Multiproject Monthly Work Update =============== */

  public function EmployeeMultiprojectMonthlyWorkRecordUpdate(Request $request)
  {

    $emp_auto_id = $request->emp_id;
    $empwh_auto_id = $request->empwh_auto_id;
    $proj_name = $request->proj_name;
    $startDate = Carbon::parse($request->startDate);
    $endDate =  Carbon::parse($request->endDate);
    $creator = Auth::user()->id;

    $fromDate = $request->startDate;
    // $date = date('d', strtotime($fromDate));
    // $month = $request->month; //  date('m', strtotime($fromDate));
    // $year = date('Y', strtotime($fromDate));

    $totalHourTime = $request->totalHourTime;
    $totalOverTime = $request->totalOverTime;
    //$totalHours = $totalOverTime + $totalHourTime; // hour and over time

    $totalDays = $request->total_day; // $startDate->diffInDays($endDate) + 1;
    // $workHour = $totalHours / $totalDays;
    // dd($request->all());
    $proj_exit = EmployeeMultiProjectWorkHistory::where('empwh_auto_id', $empwh_auto_id)->first();


    $findEmployee = EmployeeInfo::where('emp_auto_id', $proj_exit->emp_id)->first();
    if ($findEmployee) {
      if ($totalDays != 0) {




        // $findEmployeeInMonthWork = MonthlyWorkHistory::where('emp_id', $findEmployee->emp_auto_id)
        //   ->where('month_id', $proj_exit->month)
        //   ->where('year_id', $proj_exit->year)
        //   ->first();

        $update = EmployeeMultiProjectWorkHistory::where('empwh_auto_id', $empwh_auto_id)->update([
          'total_day' => $totalDays,
          'total_hour' => $totalHourTime,
          'total_overtime' => $totalOverTime,
          'month' => $proj_exit->month,
          'year' => $proj_exit->year,
          'project_id' => $proj_name,
          'start_date' => $startDate,
          'end_date' => $endDate,
          'updated_at' => Carbon::now()->toDateTimeString()
        ]);


        $total_hour = EmployeeMultiProjectWorkHistory::where('emp_id', $proj_exit->emp_id)
          ->where('month', $proj_exit->month)
          ->where('year', $proj_exit->year)
          ->sum('total_hour');

        $total_day = EmployeeMultiProjectWorkHistory::where('emp_id', $proj_exit->emp_id)
          ->where('month', $proj_exit->month)
          ->where('year', $proj_exit->year)
          ->sum('total_day');

        $total_overtime = EmployeeMultiProjectWorkHistory::where('emp_id', $proj_exit->emp_id)
          ->where('month', $proj_exit->month)
          ->where('year', $proj_exit->year)
          ->sum('total_overtime');

        //dd($proj_exit);
        $update = MonthlyWorkHistory::where('emp_id', $findEmployee->emp_auto_id)->where('month_id', $proj_exit->month)
          ->where('year_id', $proj_exit->year)->update([
            'total_hours' => $total_hour,
            'overtime' => $total_overtime,
            'total_work_day' => $total_day,
            'month_id' => $proj_exit->month,
            'year_id' => $proj_exit->year,
            'updated_at' => Carbon::now()
          ]);
        /* update multi project work history */




        return response()->json(['success' => 'Successfully Updated Employee Multi Project Work Recors']);
      } else {
        return response()->json(['error' => 'Days Not Found!']);
      }
    } else {
      return response()->json(['error' => 'Employee Not Found!']);
    }
  }


  //  Employee Daily Attendance

  public function timeInsert(Request $request)
  {

    //onclick="employeeMultipleEntryTime()" 

    // return "fff";
    /* insert data in database */
    $emp_id = $request->emp_id;
    $creator = Auth::user()->id;
    $emp_io_shift = $request->emp_io_shift;

    $catchDate = $request->date;
    $date = date('d', strtotime($catchDate));
    $month = date('m', strtotime($catchDate));
    $year = date('Y', strtotime($catchDate));

    // $helperOBJ = new HelperController();
    // $day = $helperOBJ->getNextFirdayDate($date,$month,$year);
    // if( $day == "Friday"){
    //   return  response()->json(['error' => 'Your are Trying to insert Firday Attendance']);
    // }

    $findEmployee = EmployeeInfo::where('employee_id', $emp_id)->where('job_status', 1)->first();

    if ($findEmployee) {

      $findTodayEntry = EmployeeInOut::where('emp_io_date', $date)->where('emp_io_month', $month)->where('emp_io_year', $year)->where('emp_id', $findEmployee->emp_auto_id)->first();


      if ($findTodayEntry) {
        return response()->json(['error' => 'This Data Already Exists']);
      } else {
        $insert = EmployeeInOut::insert([
          'emp_id' => $findEmployee->emp_auto_id,
          'proj_id' => $findEmployee->project_id,
          'emp_io_date' => $date,
          'emp_io_month' => $month,
          'emp_io_year' => $year,
          'emp_io_shift' => $emp_io_shift,

          'emp_io_entry_time' => $request->entry_time,
          'emp_io_out_time' => 0,
          'daily_work_hours' => 0,
          'create_by_id' => $creator,
          'emp_io_entry_date' => Carbon::now(),
          'created_at' => Carbon::now(),
        ]);

        return response()->json(['success' => 'Successfully Added Employee Entry Time']);
      }
    } else {
      return response()->json(['error' => 'Employee Not Found!']);
    }
  }



  /* =============== Out Time Insert =============== */
  public function outTimeInsert(Request $request)
  {
    // dd('ookk');
    // Catch Request Value
    $empInOutId = $request->id;
    $out_time = $request->out_time;
    $empId = $request->empId;
    $month = $request->month;
    $year = $request->year;
    $project = $request->project;
    $hours = 0.0;

    $attend = EmployeeInOut::where('emp_io_id', $empInOutId)->first();

    if ($attend->emp_io_shift == 1) {  // night sheeft
      $hours  = ($out_time - $attend->emp_io_entry_time) + 24.0;
    } else {
      $hours = ($out_time - $attend->emp_io_entry_time);
    }

    $outTimeInsert = EmployeeInOut::where('emp_io_id', $empInOutId)->update([
      'emp_io_out_time' => $out_time,
      'daily_work_hours' => $hours,
      'updated_at' => Carbon::now(),
    ]);

    // fine total work time
    $totalTime = EmployeeInOut::where('emp_io_id', $empInOutId)->first();
    /* update month work history */
    $findEmployeeInMonthWork = MonthlyWorkHistory::where('emp_id', $empId)->where('month_id', $month)->where('year_id', $year)->first();
    /* =============== Overtime Calculation =============== */
    $overtime = 0.0;

    if ($totalTime->employee->hourly_employee == true) {
      $overtime = 0.0;
    } else {
      if (($totalTime->daily_work_hours - 10) > 0) {
        $overtime = $totalTime->daily_work_hours - 10;
      } else {
        $overtime = 0.0;
      }
    }

    if ($findEmployeeInMonthWork) {
      /* old hours ,day overtime */
      $oldOvertime = $findEmployeeInMonthWork->overtime;
      $oldTotalHours = $findEmployeeInMonthWork->total_hours;
      $oldDays = $findEmployeeInMonthWork->total_work_day;

      //
      $update = MonthlyWorkHistory::where('emp_id', $empId)->where('month_id', $month)->where('year_id', $year)->update([
        'total_hours' => $oldTotalHours + $totalTime->daily_work_hours,
        'overtime' => $oldOvertime + $overtime,
        'total_work_day' => $oldDays + 1,
        'updated_at' => Carbon::now()
      ]);
    } else {
      $insert = MonthlyWorkHistory::insert([
        'emp_id' => $empId,
        'month_id' => $month,
        'year_id' => $year,
        'total_hours' => $totalTime->daily_work_hours,
        'overtime' =>  $overtime,
        'total_work_day' =>  1,
        'entered_id' =>  Auth::user()->id,
        'created_at' => Carbon::now(),
      ]);
    }


    // $multipleProjectWorkHistory = new EmployeeMultiProjectWorkHistoryController();
    // $multipleProject = $multipleProjectWorkHistory->storeUpdate($empId,$month,$year,$project);

    $proj_exit = EmployeeMultiProjectWorkHistory::where('emp_id', $empId)->where('month', $month)->where('year', $year)->where('project_id', $project)->first();

    if ($proj_exit != null) {

      $update = EmployeeMultiProjectWorkHistory::where('emp_id', $empId)->where('month', $month)->where('year', $year)->where('project_id', $project)->update([

        'total_day' => $proj_exit->total_day + 1,
        'total_hour' => $proj_exit->total_hour + $totalTime->daily_work_hours,
        'updated_at' => Carbon::now('Asia/Dhaka')->toDateTimeString()
      ]);
    } else {
      $insert = EmployeeMultiProjectWorkHistory::insert([
        'emp_id' => $empId,
        'project_id' => $project,
        'month' => $month,
        'year' => $year,
        'total_day' => 1,
        'total_hour' => $totalTime->daily_work_hours,
        'created_at' => Carbon::now('Asia/Dhaka')->toDateTimeString()
      ]);
    }

    /* update month work history */

    // return response()->json(['data' => $totalTime->daily_work_hours]);

    if ($outTimeInsert) {
      return response()->json(['success' => 'Successfully Added Employee Out Time']);
    } else {
      return response()->json(['error' => 'Opps! Please Try Again']);
    }
  }

  // public function getEmployeeEntryTime(){
  //   $getAll = EmployeeInOut::with('employee')->orderBy('emp_io_id','DESC')->get();
  //   return json_encode($getAll);\


  // }
  /* Ajax method */

  public function outTimeProcessEntryList(Request $request)
  {
    // dd($request->date);
    // $proj_id = $request->proj_name;

    $catchDate = $request->date;
    $date = date('d', strtotime($catchDate));
    $month = date('m', strtotime($catchDate));
    $year = date('Y', strtotime($catchDate));



    $getAll = EmployeeInOut::where('employee_in_outs.emp_io_date', $date)
      ->where('employee_in_outs.emp_io_month', $month)
      ->where('employee_in_outs.emp_io_year', $year)
      ->where('employee_in_outs.emp_io_out_time', 0)
      ->leftjoin('employee_infos', 'employee_in_outs.emp_id', '=', 'employee_infos.emp_auto_id')
      ->get();

    if ($getAll == true) {
      return response()->json(["entryList" => $getAll]);
    } else {
      return response()->json(['error' => "Data Not Found!"]);
    }
  }



  public function processEntryList(Request $request)
  {
    // dd('ookk');
    $proj_id = $request->proj_name;

    $catchDate = $request->date;
    $date = Carbon::createFromFormat('Y-m-d', $catchDate)->format('d');
    $month = Carbon::createFromFormat('Y-m-d', $catchDate)->format('m');
    $year = Carbon::createFromFormat('Y-m-d', $catchDate)->format('Y');



    $getAll = EmployeeInOut::where('employee_infos.project_id', $proj_id)
      ->where('employee_in_outs.emp_io_date', $date)
      ->where('employee_in_outs.emp_io_month', $month)
      ->where('employee_in_outs.emp_io_year', $year)
      ->where('employee_in_outs.emp_io_out_time', 0)
      ->leftjoin('employee_infos', 'employee_in_outs.emp_id', '=', 'employee_infos.emp_auto_id')
      ->get();

    if ($getAll == true) {
      return response()->json(["entryList" => $getAll]);
    } else {
      return response()->json(['error' => "Data Not Found!"]);
    }
  }

  public function projectWiseInOutList(Request $request)
  {
    // dd('ookk');

   // $projObj = new ProjectInfoController();
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    //  $projObj->getAllInfo();

    $proj_id = $request->proj_name;

    $catchDate = $request->date;
    $date = date('d', strtotime($catchDate));
    $month = date('m', strtotime($catchDate));
    $year = date('Y', strtotime($catchDate));

    $getAll = EmployeeInOut::where('employee_infos.project_id', $proj_id)
      ->where('employee_in_outs.emp_io_date', $date)
      ->where('employee_in_outs.emp_io_month', $month)
      ->where('employee_in_outs.emp_io_year', $year)
      ->leftjoin('employee_infos', 'employee_in_outs.emp_id', '=', 'employee_infos.emp_auto_id')
      ->get();

    if ($getAll) {
      return view('admin.employee-in-out.proj-wise-list', compact('project', 'getAll'));
    } else {
      return view('admin.employee-in-out.proj-wise-list', compact('project'));
    }
  }

  // Employee Attendance Report
  public function reportProcess(Request $request)
  {


    $month = $request->month_id;
    $project = $request->project_id;
    $year = $request->year_id;

    $helperOBJ = new HelperController();
    $monthName = $helperOBJ->getMonthName($month);
    $numberOfDaysInThisMonth = $helperOBJ->getNumberOfDaysInMonthAndYear($month, $year);
    $compObj = new CompanyProfileController();
    $company = $compObj->findCompanry();

    /* project object */
    // $projObj = new ProjectInfoController();
    $projectName = (new EmployeeRelatedDataService())->getAllProjectInformation();
    //$projObj->getFindId($project);

    if ($request->sponserId == 'all') {

      $sponserName = '';
      $directEmp = EmployeeInfo::where('project_id', $project)->where('job_status', 1)->get(); // active employee

    } else {

      $sponser = $request->sponserId;
      //$sponserOBJ = new SponsorController();
      $sponserName =(new EmployeeRelatedDataService())->findASponser($sponser);
      // $sponserOBJ->findSponser($sponser);
      $directEmp = EmployeeInfo::where('sponsor_id', $sponser)->where('project_id', $project)->where('job_status', 1)->get(); // active employee

    }

    $count = 0;
    // $directEmp all employee in variable at emp_info table er data
    foreach ($directEmp as $emp) {

      // witeOut in out condition 
      $Attendence = EmployeeInOut::where('emp_id', $emp->emp_auto_id)->where('emp_io_year', $year)->where('emp_io_month', $month)->orderBy('emp_io_date', 'ASC')->get();

      $findBasicHours = SalaryDetails::where('emp_id', $emp->emp_auto_id)->first();

      $basicHours = $findBasicHours->basic_hours;
      $hoursPerDay = 10; //$basicHours > 0 ? round(($basicHours / 26)): 0;
      $emp->perDayHours = $hoursPerDay; //push in

      $allAttend = array_fill(0, $numberOfDaysInThisMonth + 2, ' ');

      $totalWorkingDays = array_fill(0, $numberOfDaysInThisMonth + 2, ' ');

      $totalHours = 0;
      $totalWorkingHour = 0;
      foreach ($Attendence as $attend) {
        $hoursPerDay = 10;
        $attendday = (int) $attend->emp_io_date;
        $allAttend[$attendday] = 0;
        if ($attend->daily_work_hours >= 10) {
          $allAttend[$attendday] = ($attend->daily_work_hours - $hoursPerDay);
          $totalWorkingHour += $attend->daily_work_hours;
        } else {
          $hoursPerDay = $attend->daily_work_hours;
          $totalWorkingHour += $attend->daily_work_hours;
        }

        $totalHours +=   $allAttend[$attendday];

        $totalWorkingDays[$attendday] = $hoursPerDay;
      }
      $emp->totalWorkingHour = $totalWorkingHour;
      $allAttend[$numberOfDaysInThisMonth + 1] = $totalHours;
      $emp->atten = $allAttend;
      $emp->totalWorkingDays = $totalWorkingDays;
    }


    $totalHoursList = array_fill(1, $numberOfDaysInThisMonth + 1, 0);
    $holidayArray = array_fill(1, $numberOfDaysInThisMonth + 1, 0);

    $helperOBJ = new HelperController();

    // dd($noOfDaysInMonth);
    for ($i = 1; $i <= $numberOfDaysInThisMonth; $i++) {
      $day = $helperOBJ->getNextFirdayDate($i, $month, $year);
      if ($day == "Friday") {
        $holidayArray[$i] = 1;
      }
    }

    return view('admin.employee-in-out.employee-attendance-report', compact('numberOfDaysInThisMonth', 'holidayArray', 'sponserName', 'projectName', 'company', 'directEmp', 'monthName', 'year', 'totalHoursList'));

    // return view('admin.employee-in-out.report-generate', compact('numberOfDaysInThisMonth','holidayArray','sponserName', 'projectName', 'company', 'directEmp', 'monthName', 'year', 'totalHoursList'));
  }


  public function deleteAnEmployeeMultiProjectWorkRecord($empwh_auto_id)
  {
    //

    $empMultiProjWorkHisConObj = new EmployeeMultiProjectWorkHistoryController();
    $multyProjWorkRecord = $empMultiProjWorkHisConObj->findIdWiseAnEmpMultiprojectWorkHistory($empwh_auto_id);
    $noOfProWorkInThisMonth = $empMultiProjWorkHisConObj->countNoOfProjectWorkThisMonth($multyProjWorkRecord->emp_id, $multyProjWorkRecord->month, $multyProjWorkRecord->year);

    $monthlyWorkHistoryConObj = new MonthlyWorkHistoryController();
    $monthWorkRecord = $monthlyWorkHistoryConObj->findIdWiseMonthlyWorkHistory($multyProjWorkRecord->emp_id, $multyProjWorkRecord->month, $multyProjWorkRecord->year);


    $nowTotalHours = $monthWorkRecord->total_hours - $multyProjWorkRecord->total_hour;
    $nowOvertime = $monthWorkRecord->overtime - $multyProjWorkRecord->total_overtime;
    $nowTotal_work_day = $monthWorkRecord->total_work_day - $multyProjWorkRecord->total_day;

    if ($noOfProWorkInThisMonth == 1) {
      $empMultiProjWorkHisConObj->deleteAnEmpMultiprojectWorkHistory($empwh_auto_id);
      $DeleteEmpMonthWorkHistory = $monthlyWorkHistoryConObj->deleteMonthlyWorkRecord($multyProjWorkRecord->emp_id, $multyProjWorkRecord->month, $multyProjWorkRecord->year);
    } else {
      $empMultiProjWorkHisConObj->deleteAnEmpMultiprojectWorkHistory($empwh_auto_id);
      $monthlyWorkHistoryConObj->monthliWorkHistRecrdUpdate($monthWorkRecord->month_work_id, $monthWorkRecord->month_id, $monthWorkRecord->year_id, $nowTotalHours, $nowOvertime, $nowTotal_work_day);
    }
    $alertmsg = "delete multi project work history";
    return json_encode($alertmsg);
  }
  /*
    |--------------------------------------------------------------------------
    |  BLADE OPERATION
    |--------------------------------------------------------------------------
    */

  public function index()
  { 
    $month = (new CompanyDataService())->getAllMonth();
    return view('admin.employee-in-out.index', compact('month'));
  }


  // Load Employee multiple project Attandance form
  public function multiProjectInOut()
  {
    // return 'olll';
    $month = date('m');
    $year = date('Y');
    $EmpMultiProjWorkHisConObj = new EmployeeMultiProjectWorkHistoryController();
    $empMulProj = $EmpMultiProjWorkHisConObj->getLastTwoMonthEmployeeMultiprojectWorkRecord($month, $year);
 
    $month = (new CompanyDataService())->getAllMonth();

    //$projObj = new ProjectInfoController();
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // $projObj->getAllInfo();

    return view('admin.employee-in-out.multi-project-in-out', compact('month', 'project', 'empMulProj'));
  }


  public function editAnEmployeeMultiProjectWorkRecord($recordId)
  {
    $EmpMultiProjWorkHisConObj = new EmployeeMultiProjectWorkHistoryController();
    $multyProjInfoAnEmp = $EmpMultiProjWorkHisConObj->findIdWiseAnEmpMultiprojectWorkHistory($recordId);
    
    $month = (new CompanyDataService())->getAllMonth();

   // $projObj = new ProjectInfoController();
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // $projObj->getAllInfo();
    // dd($multyProjInfoAnEmp);
    return view('admin.employee-in-out.edit-multi-project-in-out', compact('month', 'project', 'multyProjInfoAnEmp'));
  }

  public function employeeMultipleProjectWorkRecordSearchUI()
  {

    
    $months = (new CompanyDataService())->getAllMonth();
    $currentMonth = Carbon::now()->format('m');
    return view('admin.month-work.search-work-record', compact('months', 'currentMonth'));
  }
  public function searchEmployeeMultipleProjectWorkRecord(Request $request)
  {

    // $empInfoConObj = new  EmployeeInfoController();
    //$empInfo = $empInfoConObj->getEmployeeByEmpId($request->emp_id);
    $empInfo = (new EmployeeDataService())->getAnEmployeeInfoWithSalaryDetailsByEmpId($request->emp_id);

    $EmpMultiProjWorkHisConObj = new EmployeeMultiProjectWorkHistoryController();
    $empMulProjWorkRecord = $EmpMultiProjWorkHisConObj->searchAnEmployeeMultiprojectWorkRecord($empInfo->emp_auto_id, $request->month, $request->year);
    if (count($empMulProjWorkRecord) == 0) {
      return response()->json(["error" => "Employee Work Record Not Found"]);
    }
    return response()->json(["empMulProjWorkRecord" => $empMulProjWorkRecord, "error" => null]);
  }

  public function entryList()
  {
    // dd('ookk');
    /* project object */
    //$projObj = new ProjectInfoController();
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // $projObj->getAllInfo();

    return view('admin.employee-in-out.all', compact('project'));
  }

  public function projectWiseList()
  {
    // dd('ookk');
    /* project object */
    // $projObj = new ProjectInfoController();
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
    // $projObj->getAllInfo();

    return view('admin.employee-in-out.proj-wise-list', compact('project'));
  }

  public function report()
  { 
    $project = (new EmployeeRelatedDataService())->getAllProjectInformation();
     
    $sponser =(new EmployeeRelatedDataService())->getAllSponser();
     
    $month = (new CompanyDataService())->getAllMonth();
    return view('admin.employee-in-out.report', compact('month', 'project', 'sponser'));
  }



  /*
    |--------------------------------------------------------------------------
    |  API OPERATION
    |--------------------------------------------------------------------------
    */
}
