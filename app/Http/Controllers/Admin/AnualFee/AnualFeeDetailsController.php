<?php

namespace App\Http\Controllers\Admin\AnualFee;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DataServices\EmployeeDataService;
use Illuminate\Http\Request;
use App\Models\AnualFeesDetails;
use App\Models\IqamaRenewalDetails;
use App\Models\EmployeeInfo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
    //AnualFeesDetailsController
class AnualFeeDetailsController extends Controller{
  public function getAll(){
    return $all = AnualFeesDetails::orderBy('anu_fee_id','ASC')->get();
  }
  public function getlimit(){
    return $all = AnualFeesDetails::orderBy('anu_fee_id','ASC')->limit(5)->get();
  }

  public function getIqamaCostAll(){
    return $all = IqamaRenewalDetails::get();
  }
  public function findIqamaCostAll($id){
    return $data = IqamaRenewalDetails::where('IqamaRenewId',$id)->first();
  }

  public function getAnEmployeeIqamaAnnualCost($emoloyeeAutoId,$year){
    return $all = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get();//->where('Year',$year)->first();
  }

  public function getAnEmployeeIqamaTotalCost($emoloyeeAutoId){
      $cost1 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost1');

      $cost2 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost2' );

      $cost3 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost3' );

      $cost4 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost4');
      $cost5 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost5');
      $cost6 = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('Cost6');
      $jawazat_penalty = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->get()
                  ->sum('jawazat_penalty');
      return $cost1 + $cost2 + $cost3 + $cost4 + $cost5 + $cost6 + $jawazat_penalty;
  }

  public function getAnEmployeeIqamaRenewAnnualTotalCost($emoloyeeAutoId,$year){
    $iqamaDetails = IqamaRenewalDetails::where('EmplId',$emoloyeeAutoId)->where('Year',$year)->first();
    if( $iqamaDetails != null){
     return $iqamaDetails->Cost1 + $iqamaDetails->Cost2 + $iqamaDetails->Cost3 + $iqamaDetails->Cost4 + $iqamaDetails->Cost5 + $iqamaDetails->Cost6 + $iqamaDetails->jawazat_penalty;
    }
    return 0.00;
  }

  public function totalAmount(){
    return $all = AnualFeesDetails::sum('amount');
  }

  // Employee Anual Iqama Renewal UI
  public function index(){
    $title = $this->getlimit();
    $iqamaCost = $this->getIqamaCostAll();
     return view('admin.iqamarenewal.index',compact('title','iqamaCost'));
  }

  public function edit($id){
    $title = $this->getlimit();
    $data = $this->findIqamaCostAll($id);
    return view('admin.iqamarenewal.edit',compact('title','data'));
  }

  public function insert(Request $request){
   
    $this->validate($request,[
      'emp_id' => 'required',
      'cost1' => 'required',
      'cost2' => 'required',
      'cost3' => 'required',
      'cost4' => 'required',
      'cost5' => 'required',
      'jawazat_penalty' => 'required',
    ],[

    ]); 
   
    $findEmployee = (new EmployeeDataService())->getAnEmployeeInfoByEmpId($request->emp_id);  
    
    if(!$findEmployee){
      Session::flash('IDNotFound','value');
      return redirect()->back();
    }else{
      $findIqamaDetails = null;// IqamaRenewalDetails::where('EmplId',$findEmployee->emp_auto_id)->where('Year',Carbon::now()->format('Y'))->first();
      if($findIqamaDetails){
        Session::flash('already_exits','value');
        return redirect()->back();
      }else{
        $insert = IqamaRenewalDetails::insert([
          'EmplId' => $findEmployee->emp_auto_id,
          'Cost1' => $request->cost1,
          'Cost2' => $request->cost2,
          'Cost3' => $request->cost3,
          'Cost4' => $request->cost4,
          'Cost5' => $request->cost5,
          'Year' => Carbon::now()->format('Y'),
          'jawazat_penalty' => $request->jawazat_penalty,
          'duration' => $request->duration,
          'renewal_date' => $request->renewal_date,
          'remarks' => $request->remarks ?? '',
          'created_at' => Carbon::now(),
        ]);
        Session::flash('success','value');
        return redirect()->back();
      }
     }
 
   }

  public function update(Request $request){
    $this->validate($request,[
      'cost1' => 'required',
      'cost2' => 'required',
      'cost3' => 'required',
      'cost4' => 'required',
      'cost5' => 'required',
    ],[

    ]);
    
    $update = IqamaRenewalDetails::where('IqamaRenewId',$request->id)->update([
      'Cost1' => $request->cost1,
      'Cost2' => $request->cost2,
      'Cost3' => $request->cost3,
      'Cost4' => $request->cost4,
      'Cost5' => $request->cost5,
      'Year' => Carbon::now()->format('Y'),
      'jawazat_penalty' => $request->jawazat_penalty,
      'duration' => $request->duration,
      'renewal_date' => $request->renewal_date,
      'remarks' => $request->remarks ?? '',
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    Session::flash('success_update','value');
    return redirect()->route('anual-fee.details');
 

   }

   // UI For Set All Emplpoyee Iqama Cost
  public function setAllEmployeeIqamaCost(){
  
    return view('admin.iqamarenewal.set_iqama_cost');
  }

  public function setAllEmployeeIqamaCostProcess(Request $request){
    Session::flash('success','value');
    return redirect()->back();

    $getAllEmployeeIqamaCost = IqamaRenewalDetails::where('iqama_renewal_details.Year',Carbon::now()->subYear(1)->format('Y'))->
    where('employee_infos.job_status',1)->
    leftjoin('employee_infos','iqama_renewal_details.EmplId','=','employee_infos.emp_auto_id')->get();

    foreach($getAllEmployeeIqamaCost as $employeeIqamaCost){
      $insert = IqamaRenewalDetails::insert([
        'EmplId' => $employeeIqamaCost->emp_auto_id,
        'Cost1' => $employeeIqamaCost->Cost1,
        'Cost2' => $employeeIqamaCost->Cost2,
        'Cost3' => $employeeIqamaCost->Cost3,
        'Cost4' => $employeeIqamaCost->Cost4,
        'Cost5' => $employeeIqamaCost->Cost5,
        'Year' => Carbon::now()->format('Y'),
        'created_at' => Carbon::now(),
      ]);
    }
   


  }

}
