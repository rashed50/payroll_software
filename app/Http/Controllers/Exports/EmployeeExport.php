<?php

namespace App\Http\Controllers\Exports;

use App\Http\Controllers\DataServices\EmployeeDataService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\EmployeeInfo;

class EmployeeExport implements FromCollection, WithHeadings
{

    protected $connection_statusId;
    protected $packageId;

    function __construct($connection_statusId, $packageId)
    {
        $this->connectionStatusId = $connection_statusId;
        $this->packageId = $packageId;
    }

    public function headings(): array
    {
        return [
            'employee_id', 'employee_name', 'passfort_no',
            'passfort_expire_date', 'akama_no', 'akama_expire_date',
            'sponser_name', 'project_name', 'mobile_no', 'email', 'date_of_birth',
            'country_name', 'hourly_employee', 'catg_name', 'Emp_Type', 'Emp_Status',
            'basic_salary',
            'hourly_rate',
            'basic_hours',
            'food_allowance',
            'mobile_allowance'


        ];
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $empllist = (new EmployeeDataService())->exportEmployeeInformation();
        // dd($empllist[0]);
        return collect($empllist);
    }
}
