<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next){
      /* user banner */
      //user banned and unbanned
        if (Auth::check() && Auth::user()->status) {
            $banned = Auth::user()->status == '1';
            Auth::logout();
            if ($banned == 1) {
               $message = "Your Account Has Been Banned.Please Contact With Admin";
            }
            return redirect()->route('login')->with('status', $message)->withErrors([
                'banned' => 'Your Account Has been Banned. Please Contact Administrator'
            ]);
        }



      /* check authentication */
      if (Auth::check()) {
        return $next($request);
      }else{
         return redirect()->route('login');
      }






    }
}
